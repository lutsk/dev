<?php
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$lang->load('com_exchanger', JPATH_SITE);

$registry = new JRegistry;
$registry->loadString($item->currency_from_params);
$item->currency_from_params = $registry->toArray();
$currency_from_simbols = $item->currency_from_params['simbols'];
$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
$registry->loadString($item->currency_to_params);
$item->currency_to_params = $registry->toArray();
$currency_to_simbols = $item->currency_to_params['simbols'];
$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';

$actualRate = $item->currency_to_course / $item->currency_from_course;
$amountTo = sprintf($currency_to_pattern, $actualRate * $item->min_buy);

$token = \JFactory::getSession()->getFormToken();
?>
<div class="container<?= $moduleclass_sfx; ?>">
	<form id="exchange_form" class="exchanger exchanger--main" action="index.php?option=com_exchanger&task=exchange.step2" method="post" data-get-rate-action="/index.php?option=com_exchanger&task=exchange.exchangeReverce">
		<div id="form_exchanger">
					
					<div class="exchanger__rates">
						<div class="exchanger__rates-inner">
							<div id="buy-rate" class="exchanger__rate"><span><?= JText::_('COM_EXCHANGER_BUY_RATE'); ?>:</span><span><?= sprintf($currency_from_pattern, $item->currency_from_course) ;?> <?= $item->code ;?> = <?= sprintf($currency_to_pattern, $item->currency_to_course) ;?> <?= $item->to_currency_code ;?></span></div>
							<div id="sell-rate" class="exchanger__rate"><span><?= JText::_('COM_EXCHANGER_SELL_RATE'); ?>:</span><span><?= sprintf($currency_to_pattern, $item->currency_from_reverse_course) ;?> <?= $item->to_currency_code ;?> = <?= sprintf($currency_from_pattern, $item->currency_to_reverse_course) ;?> <?= $item->code ;?></span></div>
						</div>
                    </div>
                    <div class="exchanger__currency">
                    	<div class="exchanger__currency-inner">
							<label><?= JText::_('COM_EXCHANGER_I_GIVE'); ?></label>
							<div class="exchanger__currency-input-group">
								<input id="amount_from_input" class="imask-number" type="text" name="exchange_from_amount_input" value="<?= sprintf($currency_from_pattern, $item->min_buy) ;?>" autocomplete="off" required>
								<input id="amount_from_hint_input" class="amount-from-text" type="text" value="<?= ($item->fee->total > 0) ? sprintf($currency_from_pattern, $item->fee->total) : sprintf($currency_from_pattern, $item->min_buy);?>" placeholder="<?= JText::_('COM_EXCHANGER_WITH_COMMISSION'); ?>">
								<input type="hidden" id="exchange-form-from-curr" name="currency_from_id" value="<?= $item->currency_from_id ;?>">
								<button id="change-currency-from-btn" class="exchanger-currency-dropdown-trigger" type="button"><img src="<?= $item->image ;?>" alt="<?= $item->name ;?>" /><span><?= $item->name ;?></span></button>
							</div>
						</div>
						<div class="exchanger-currency-dropdown" id="exchanger-currency-dropdown-2-1">
							<button class="close-button exchanger-currency-dropdown__close" type="button"></button>
							<div class="exchanger-currency-dropdown__min-max">
                            	<div><?= JText::_('COM_EXCHANGER_MINIMUM_AMOUNT'); ?>:<span>0</span></div>
                                <div><?= JText::_('COM_EXCHANGER_MAXIMUM_AMOUNT'); ?>:<span>0</span></div>
                            </div>
                            <ul class="payments-list">
                                <?php foreach ($fromLists as $i => $fromList) : 
		                            $exParams = json_decode($fromList->params);
		                            $pattern = (string) '%01.'.$exParams->simbols.'f';
		                            ?>
                                <li>
	                                <?php if (isset($fromList->parent)) : ?>
	                                <button class="exchanger-currency-dropdown__button exchanger-currency-dropdown__submenu-trigger<?php if ($i > 4) echo ' upward'; ?>" type="button" title="<?= $fromList->name ;?>"><img src="<?= $fromList->big_image ;?>" alt="<?= $fromList->name ;?>" /><span><?= $fromList->name ;?></span>
	                                </button>
	                                <div class="exchanger-currency-dropdown__submenu<?php if ($i > 4) echo ' upward'; ?>">
		                                <?php foreach ($fromList->childList as $child) :
		                                	$exParams = json_decode($child->params);
		                                	$pattern = (string) '%01.'.$exParams->simbols.'f';
		                                	?>
		                                	<button class="curr-switcher" direction="from" data-currency="true" data-min="<?= sprintf($pattern, $child->min_buy) ;?> <?= $child->code ;?>" data-max="<?= sprintf($pattern, $child->max_buy) ;?> <?= $child->code ;?>" data-text="<?= $child->name ;?>" data-id="<?= $child->id ;?>"<?php if ($i > 4) echo ' data-position="top"'; ?> type="button">
		                                	<?= $child->code ;?>
		                                	<span><span><?= JText::_('COM_EXCHANGER_MIN'); ?>:<span><?= sprintf($pattern, $child->min_buy) ;?> <?= $child->code ;?></span></span><span><?= JText::_('COM_EXCHANGER_MAX'); ?>:<span><?= sprintf($pattern, $child->max_buy) ;?> <?= $child->code ;?></span></span></span>
		                                	</button>
	                                	<?php endforeach; ?>
                                	</div>
	                                <?php else : ?>
		                                <button class="curr-switcher exchanger-currency-dropdown__button" direction="from" data-currency="true" data-min="<?= sprintf($pattern, $fromList->min_buy) ;?> <?= $fromList->code ;?>" data-max="<?= sprintf($pattern, $fromList->max_buy) ;?> <?= $fromList->code ;?>" data-text="<?= $fromList->name ;?>" data-id="<?= $fromList->id ;?>" type="button" title="<?= $fromList->name ;?>">
		                                	<img src="<?= $fromList->image ;?>" alt="<?= $fromList->name ;?>" />
		                                	<span><span><?= $fromList->name ;?></span><span><span><?= JText::_('COM_EXCHANGER_MIN'); ?>:<span><?= sprintf($pattern, $fromList->min_buy) ;?> <?= $fromList->code ;?></span></span><span><?= JText::_('COM_EXCHANGER_MAX'); ?>:<span><?= sprintf($pattern, $fromList->max_buy) ;?> <?= $fromList->code ;?></span></span></span></span>
		                                </button>
	                                <?php endif; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
						</div>
						<div class="form-checkbox-group exchanger__checkbox-group">
							<input id="from-hint-check" class="form-checkbox-group__input" type="checkbox">
							<label class="form-checkbox-group__label" for="from-hint-check"><?= JText::_('COM_EXCHANGER_TAKE_COMMISSION_PS'); ?> <span id="curr_fee"><?= $item->amount_fee ;?></span> % <span class="info-btn" data-popup-target="info-popup-exchanger-2" data-popup-left="-300" data-popup-top="20"></span></label>
                        </div>
                    </div>
                    
                    <div class="exchanger__swap">
                		<button id="button_swap" type="button"></button>
                	</div>
                	
                	<div class="exchanger__currency">
                		<div class="exchanger__currency-inner">
                			<label><?= JText::_('COM_EXCHANGER_I_RECEIVE'); ?></label>
                			<div class="exchanger__currency-input-group">
                				<input id="amount_to_input" class="imask-number" type="text" name="exchange_from_to_input" value="<?= $amountTo ;?>" autocomplete="off" placeholder="<?= JText::_('COM_EXCHANGER_WANT_TO_GET'); ?>">
                				<button id="change-currency-to-btn" class="exchanger-currency-dropdown-trigger" type="button"><img src="<?= $item->to_currency_image ;?>" alt="<?= $item->to_currency_name ;?>" /><span><?= $item->to_currency_name ;?></span></button>
                				<input type="hidden" id="exchange-form-to-curr" name="currency_to_id" value="<?= $item->currency_to_id ;?>">
                			</div>
                        </div>
                        <div class="exchanger-currency-dropdown exchanger-currency-dropdown--big" id="exchanger-currency-dropdown-2-2">
                        	<button class="close-button exchanger-currency-dropdown__close" type="button"></button>
                        	<div class="exchanger-currency-dropdown__min-max">
                        		<div><?= JText::_('COM_EXCHANGER_MINIMUM_AMOUNT'); ?>:<span>0</span></div>
                        		<div><?= JText::_('COM_EXCHANGER_MAXIMUM_AMOUNT'); ?>:<span>0</span></div>
                        	</div>
                        	<ul>
                                <?php foreach ($toLists as $k => $toList) :
		                            $exParams = json_decode($toList->params);
		                            $pattern = (string) '%01.'.$exParams->simbols.'f';
		                            ?>
                                <li>
	                                <?php if (isset($toList->parent)) : ?>
	                                <button class="exchanger-currency-dropdown__button exchanger-currency-dropdown__submenu-trigger" type="button" title="<?= $toList->name ;?>"><img src="<?= $toList->big_image ;?>" alt="<?= $item->to_currency_name ;?>" /><span><?= $toList->name ;?></span></button>
	                                <div class="exchanger-currency-dropdown__submenu<?php if ($k > 8) echo ' upward'; ?>">
		                                <?php foreach ($toList->childList as $child) :
		                                	$exParams = json_decode($child->params);
		                                	$pattern = (string) '%01.'.$exParams->simbols.'f';
		                                	?>
		                                	<button class="curr-switcher" direction="to" data-text="<?= $child->name ;?>" data-min="<?= sprintf($pattern, $child->min_sell) ;?> <?= $child->code ;?>" data-max="<?= sprintf($pattern, $child->max_sell) ;?> <?= $child->code ;?>" data-id="<?= $child->id ;?>"<?php if ($k > 8) echo ' data-position="top"'; ?> type="button">
		                                		<?= $child->code ;?>
		                                		<span><span><?= JText::_('COM_EXCHANGER_MIN'); ?>:<span><?= sprintf($pattern, $child->min_sell) ;?> <?= $child->code ;?></span></span><span><?= JText::_('COM_EXCHANGER_MAX'); ?>:<span><?= sprintf($pattern, $child->max_sell) ;?> <?= $child->code ;?></span></span></span>
		                                	</button>
	                                	<?php endforeach; ?>
                                	</div>
	                                <?php else : ?>
	                                <button class="curr-switcher exchanger-currency-dropdown__button" direction="to" data-text="<?= $toList->name ;?>" data-min="<?= sprintf($pattern, $toList->min_sell) ;?> <?= $toList->code ;?>" data-max="<?= sprintf($pattern, $toList->max_sell) ;?> <?= $toList->code ;?>" data-id="<?= $toList->id ;?>" type="button" title="<?= $toList->name ;?>">
		                                <img src="<?= $toList->image ;?>" alt="<?= $item->to_currency_name ;?>" />
		                                <span><span><?= $toList->name ;?></span><span><span><?= JText::_('COM_EXCHANGER_MIN'); ?>:<span><?= sprintf($pattern, $toList->min_sell) ;?> <?= $toList->code ;?></span></span><span><?= JText::_('COM_EXCHANGER_MAX'); ?>:<span><?= sprintf($pattern, $toList->max_sell) ;?> <?= $toList->code ;?></span></span></span></span>
	                                </button>
	                                <?php endif; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="exchanger__currency-info">
                        	<div class="exchanger__max-amount"><span><?= JText::_('COM_EXCHANGER_MAX_EXCHANGE_AMOUNT'); ?> <span class="max-limit-to-curr"><?= sprintf($pattern, $item->max_sell) ;?> <?= $item->to_currency_code ;?></span></span><a href="#" data-modal="#modal-need-more"><?= JText::_('COM_EXCHANGER_NEED_MORE'); ?></a></div>
                        	<?php if ($item->fee->comission > 0) : ?>
                        	<div class="exchanger__fee">
                        		<span><?= JText::_('COM_EXCHANGER_FEE'); ?> <span id="system-comission-info-system-title">&laquo;<?= ($item->currency_from_parent > 0) ? ExchangerSiteHelper::getCurrencyTitle($item->currency_from_parent) : $item->name ;?>&raquo;:</span> <span><span id="system-comission-info-value"><?= $item->fee->comission ;?></span> <span id="system-comission-info-curr-title"><?= $item->code ;?></span></span></span>
                        		<a href="/rates" target="_blank"><?= JText::_('COM_EXCHANGER_CHECK_RATE'); ?></a>
                        	</div>
                        	<?php endif; ?>
                        </div>
                	</div>
                	
                	<div id="main-form-error" class="form-error-block"></div>
                	
                	<div class="exchanger__button-container">
                		<a id="exchange_check" class="btn btn--filled btn--wide-md exchanger__button" href="#"><?= JText::_('COM_EXCHANGER_EXCHANGE'); ?></a>
                	</div>
                	<?php //if ($item->amount_fee > 0) : ?>
                	<div class="info-popup" id="info-popup-exchanger-2">
                		<?= $item->description ;?>
                	</div>
                	<?php //endif; ?>
                	
                	<input id="from_title" type="hidden" name="from_title" value="<?= $item->name;?>" />
                	<input id="to_title" type="hidden" name="to_title" value="<?= $item->to_currency_name ;?>" />
                	</div>
                	
                	<input id="token" type="hidden" name="token" value="<?= $token;?>" />
	</form>
</div>