<?php
defined('_JEXEC') or die;

JLoader::register('ExchangerSiteHelper', JPATH_BASE.'/components/com_exchanger/helpers/exchanger.php');
JLoader::register('modMainexchangeHelper', __DIR__ . '/helper.php');

$item = modMainexchangeHelper::getItem();
$fromLists	 = modMainexchangeHelper::getFromList();
$toLists	 = modMainexchangeHelper::getToList();

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require(JModuleHelper::getLayoutPath('mod_main_exchange', $params->get('layout', 'default')));