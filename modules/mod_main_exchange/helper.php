<?php
defined('_JEXEC') or die;

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_exchanger/models', 'ExchangerModel');

class modMainexchangeHelper
{
	static function getItem()
	{
		$model = JModelLegacy::getInstance('Exchange', 'ExchangerModel', array('ignore_request' => true));
		return $model->getItem();
	}
	
	static function getFromList()
	{
		$model = JModelLegacy::getInstance('Exchange', 'ExchangerModel', array('ignore_request' => true));
		return $model->getFromList();
	}
	
	static function getToList()
	{
		$model = JModelLegacy::getInstance('Exchange', 'ExchangerModel', array('ignore_request' => true));
		return $model->getToList();
	}

}