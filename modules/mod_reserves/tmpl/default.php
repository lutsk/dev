<?php
defined('_JEXEC') or die;
$i = 1;
$divid = intdiv(count($items), 2) + 1;
?>

<section class="reserves reserves--only-desktop">
	<div class="container<?php echo $moduleclass_sfx; ?>">
		<h2 class="h2 reserves__title"><?= JText::_('MOD_RESERVES'); ?></h2>
		<div class="reserves__inner">
			<ul class="currencies-list reserves__currencies-list">
				<?php foreach ($items as $item) : ?>
				<li class="currencies-list__item reserves__currencies-list-item">
					<div class="currencies-list__currency currencies-icon"><img src="<?= $item->image ;?>" alt="<?= $item->name ;?>" width="40" style="margin-right:20px" /><?= $item->name ;?></div>
					<div class="currencies-list__ammount"><?= $item->amount ;?> <?= $item->code ;?></div>
				</li>
				<?php
				$i++;
				if($divid == $i) :
				?>
				</ul><ul class="currencies-list reserves__currencies-list">
				<?php
				endif;
				endforeach;
				?>
			</ul>
		</div>
		<a class="btn btn--accent reserves__see-more" href="<?= JRoute::_('index.php?option=com_exchanger&view=reserves'); ?>"><?= JText::_('MOD_MORE_RESERVES'); ?></a>
	</div>
</section>