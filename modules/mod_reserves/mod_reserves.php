<?php
defined('_JEXEC') or die;

JLoader::register('modReservesHelper', __DIR__ . '/helper.php');

$items = modReservesHelper::getReserves($params->get('limit', 12));

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require(JModuleHelper::getLayoutPath('mod_reserves', $params->get('layout', 'default')));