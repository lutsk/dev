<?php
defined('_JEXEC') or die;

class modReservesHelper
{
	
	static function getReserves($limit)
	{
		$registry = new JRegistry;
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select(array(
				$db->qn('id'), $db->qn('code'), $db->qn('image'), $db->qn($name, 'name'), $db->qn('amount')
			))
			->from($db->qn('#__abc_currency'))
			->setLimit($limit);
		
		$db->setQuery($query);
		
		$items = $db->loadObjectList();
		
		foreach ($items as $item)
		{
			$currencyParams = ExchangerSiteHelper::getCurrencyParams($item->id);
			$registry->loadString($currencyParams);
			$currencyParams = $registry->toArray();
			$simbols = $currencyParams['simbols'];
			$pattern = (string) '%01.'.$simbols.'f';
			
			$item->amount = sprintf($pattern, $item->amount);
		}
		
		return $items;
	}

}