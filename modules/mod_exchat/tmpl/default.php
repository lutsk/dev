<?php
defined('_JEXEC') or die;
$user = JFactory::getUser();
$userStatus = $user->guest;
?>

<audio class="msg-audio">
	<source src="/modules/mod_exchat/audio/msg.mp3" />
</audio>
<?php if(EXSystem::isMob()) : ?>
<div class="modal__title"><?= JText::_('СONNECT_CHAT'); ?></div>
<?php else : ?>
<div class="just-exchanged__title just-exchanged__title--chat"><?= ($userStatus == 0) ? JText::_('CHAT') : JText::_('SIGN_CHAT') ?></div>
<?php endif; ?>

<div class="chat<?php echo $moduleclass_sfx; ?>">
	<div class="chat__header">
		<div id="usersNum" class="chat__users-number"><?= $users; ?></div>
	</div>
	<div class="chat__content">
		<?php foreach($data as $item) : ?>
		<div class="chat__message">
			<div class="chat__message-icon" data-chat-abbr="<?= $item->abbr; ?>" style="background-color: <?= $item->color; ?>;"></div>
			<div class="chat__message-inner">
				<div class="chat__message-header">
					<div class="chat__message-name"><?= $item->name; ?></div>
					<div class="chat__message-date"><?= $item->time; ?></div>
				</div>
				<div class="chat__message-text"><?= $item->text; ?></div>
			</div>
		</div>
		<?php endforeach; ?>
		
	</div>
	<div class="chat__footer">
		<?php if ($userStatus) : ?>
		<div class="chat__login-text">
		<?= JText::sprintf('REGISTER_SEND_MESSAGE', JText::_('COM_EXCHANGER_SIGN_IN'), JText::_('COM_EXCHANGER_REGISTER')); ?>
		</div>
		<?php else : ?>
		<div class="chat__send">
			<textarea id="msgText" class="chat__send-message" title="<?= JText::_('TYPE_MESSAGE'); ?>"></textarea>
			<button id="sendText" class="chat__send-btn"></button>
		</div>
		<?php endif; ?>
	</div>
	
</div>