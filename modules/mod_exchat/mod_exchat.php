<?php
defined('_JEXEC') or die;

require_once(dirname( __FILE__ ) . '/chat.php');

$config = JFactory::getConfig();
$chatParams = JComponentHelper::getParams('com_exchat');
$user = JFactory::getUser();
$doc = JFactory::getDocument();

$userCometHash = ExСhat::getJWT(['user_id' => $user->id, "exp" => $config->get('lifetime') * 60], $chatParams->get('dev_key'), $chatParams->get('dev_id'));

$data = ExСhat::getData($params);
/*$users = ExСhat::getUsersAuth();
$users = ExСhat::getUsersOnTrack();*/
$users = ExСhat::getUsersOnline($config);

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require( JModuleHelper::getLayoutPath('mod_exchat', $params->get('layout', 'default')));