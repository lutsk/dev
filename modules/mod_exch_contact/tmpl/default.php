<?php
defined('_JEXEC') or die;
$optfields = $params->get('theme', '');
$captcha = $params->get('captcha', '0');
?>
<form id="exch_contact" action="<?php echo JURI::current(); ?>" method="post">
	<?php if (!empty($optfields)) : 
		$fields_array = explode("\n", $optfields);
	?>
	<div class="form-select-group">		
		<label class="form-select-group__label"><?= JText::_('MOD_EXCH_CONTACT_THEME'); ?>*</label>
		<select class="form-select-group__select" name="theme" required>
			<option value=""><?= JText::_('MOD_EXCH_CONTACT_CHOOSE'); ?></option>
			<?php foreach ($fields_array as $value) : ?>
				<option value="<?= $value; ?>"><?= $value; ?></option>
			<?php endforeach ; ?>
		</select>
	</div>
	<?php endif ; ?>
	<div class="form-group">
		<label class="form-group__label"><?= JText::_('MOD_EXCH_CONTACT_NAME'); ?>*</label>
		<input class="requiredField form-group__input" type="text" name="name" required>
	</div>
	<div class="form-group">
		<label class="form-group__label"><?= JText::_('MOD_EXCH_CONTACT_EMAIL'); ?>*</label>
		<input class="requiredField email form-group__input" type="email" name="email" required>
	</div>
	<div class="form-group">
		<label class="form-group__label"><?= JText::_('MOD_EXCH_CONTACT_DESCRIPTION'); ?>*</label>
		<textarea class="requiredField form-group__textarea" name="message" required></textarea>
	</div>
	<?php if ($captcha) : ?>
	<div class="form-group">
		<label class="form-group__label"><?= JText::_('MOD_EXCH_CONTACT_CAPTCHA'); ?>*</label>
		<?php
		$captcha = JCaptcha::getInstance('recaptcha', array('namespace' => 'captcha_keystring_rent_request'));
		echo $captcha->display('recaptcha', 'recaptcha', 'required');
		?>
	</div>
	<?php endif ; ?>
	<div class="form-button-group contact-form__button-group">
		<button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('MOD_EXCH_CONTACT_SEND'); ?></button>
		<a class="btn form-button-group__btn" type="button" href="/"><?= JText::_('JCANCEL'); ?></a>
	</div>
</form>
 
 <script>
	jQuery(document).ready(function () {
		jQuery('form#exch_contact').submit(function () {
			var hasError = false;
			jQuery('.requiredField').each(function () {
				console.log(jQuery(this));
				console.log(jQuery(this).val());
				if (jQuery.trim(jQuery(this).val()) == '') {
                    hasError = true;
				} else if (jQuery(this).hasClass('email')) {
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if (!emailReg.test(jQuery.trim(jQuery(this).val()))) {
						hasError = true;
					}
				}
			});
			
			if (!hasError) {
				var formInput = jQuery(this).serializeArray();
				request = {
	                'option' : 'com_ajax',
	                'module' : 'exch_contact',
	                'data'   : formInput,
	                'format' : 'jsonp'
	            };
	            jQuery.ajax({
	                type   : 'POST',
	                data   : request,
	                beforeSend: function(){
	                    jQuery('.form-button-group').before('<p><span class="ajax_send_loading"></span></p>');
	                },
	                success: function (response) {
	                    jQuery('form#exch_contact').slideUp("fast", function () {
	                	jQuery("html, body").animate({scrollTop: 0}, 200);
							jQuery(this).before("<span class='success'><?= JText::_('MOD_EXCH_CONTACT_THANKS'); ?></span>");
						});
	                }
	            });
			}
			else {
				alert('Error!');
			}
			return false;
		});
	});
</script>