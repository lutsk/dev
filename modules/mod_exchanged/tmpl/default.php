<?php
defined( '_JEXEC' ) or die;
?>
<div class="just-exchanged__title"><?= JText::_('MODULE_EXCHANGED'); ?></div>
<div id="exchangedList" class="just-exchanged__lists<?= $moduleclass_sfx; ?>">
	<ul id="exTextList" class="just-exchanged__text-list">
		<?php foreach($data as $item) : ?>
		<li class="just-exchanged__text-list-item">Just exchanged</li>
		<?php endforeach; ?>
	</ul>
	<ul id="exTextList1" class="just-exchanged__value-list-1">
		<?php foreach($data as $item) : ?>
		<li class="just-exchanged__value-list-1-item"><?= $item->amount_from; ?> <?= $item->currency_from_code; ?></li>
		<?php endforeach; ?>
	</ul>
	<ul id="exTextList2" class="just-exchanged__value-list-2">
		<?php foreach($data as $item) : ?>
		<li class="just-exchanged__value-list-2-item"><?= $item->amount_to; ?> <?= $item->currency_to_code; ?></li>
		<?php endforeach; ?>
	</ul>
</div>