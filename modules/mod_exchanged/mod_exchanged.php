<?php
defined( '_JEXEC' ) or die;

require_once(dirname( __FILE__ ) . '/helper.php');

$data = ExExchange::getData($params);

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require( JModuleHelper::getLayoutPath('mod_exchanged', $params->get('layout', 'default')));