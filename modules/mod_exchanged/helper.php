<?php
defined( '_JEXEC' ) or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;

class ExExchange
{
	static function getData($params)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$messages = array();
		$queryList = self::loadMessages();
		foreach ($queryList as $key => $message)
		{
			$obj = json_decode(stripcslashes($message->message));
			
			$messages[$key] = $obj;
		}
		return $messages;
	}
	
	protected static function loadMessages()
	{
		$list = array();
		
		$params = JComponentHelper::getParams('com_exchat');
		
		$link = mysqli_connect("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		if ( !$link ) die ("Unable to connect to CometQL");
		  
		$query = "SELECT * FROM pipes_messages WHERE name = 'web_exchange_data';";
		
		$result = mysqli_query($link, $query);
		if(!$result)
		{
		    throw new Exception(sprintf('Не удалось выполнить запрос к БД, код ошибки %d, текст ошибки: %s', mysqli_errno($link), mysqli_error($link)));
		}
		
		$i = 0;
		while($row = mysqli_fetch_array($result))
		{
		    $list[$i] = new stdClass();
		    $list[$i]->message = $row['message'];
		    $i++;
		}
		mysqli_close($link);
		return array_reverse($list);
	}
	
	static function getUsersAuth()
	{
		$list = array();
		$params = JComponentHelper::getParams('com_exchat');
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('user_id')->from($db->qn('#__abc_user'));
		$db->setQuery($query);
		$users = $db->loadColumn();
		$users = implode(', ',$users);
		
		$link = mysqli_connect("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		if ( !$link ) die ("Unable to connect to CometQL");
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		
		$query = "SELECT * FROM users_auth WHERE id IN(".$users.")";
		
		$result = mysqli_query($link, $query);
		if(!$result)
		{
		    throw new Exception(sprintf('Не удалось выполнить запрос к БД, код ошибки %d, текст ошибки: %s', mysqli_errno($link), mysqli_error($link)));
		}
		
		$i = 0;
		while($row = mysqli_fetch_array($result))
		{
		    $list[$i] = new stdClass();
		    $list[$i]->id = $row['id'];
		    $list[$i]->hash = $row['hash'];
		    $i++;
		}
		mysqli_close($link);
		
		return $list;
	}
	
	static function getUsersOnTrack()
	{
		$list = array();
		$params = JComponentHelper::getParams('com_exchat');
		
		$link = mysqli_connect("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		if ( !$link ) die ("Unable to connect to CometQL");
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		
		$query = "SELECT * FROM users_in_pipes WHERE name = 'track_chat_pipe';";
		
		$result = mysqli_query($link, $query);
		if(!$result)
		{
		    throw new Exception(sprintf('Не удалось выполнить запрос к БД, код ошибки %d, текст ошибки: %s', mysqli_errno($link), mysqli_error($link)));
		}
		
		$i = 0;
		while($row = mysqli_fetch_array($result))
		{
		    $list[$i] = new stdClass();
		    $list[$i]->id = $row['user_id'];
		    $list[$i]->user_uuid = $row['user_uuid'];
		    $i++;
		}
		mysqli_close($link);
		
		return $list;
	}
	
	static function getUsersOnline($config)
	{
		$total_users = 0;
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		if ($config->get('shared_session', '0'))
		{
			$query->select('COUNT(session_id)')
				->from('#__session')
				->where('guest = 0');
		}
		else {
			$query->select('COUNT(session_id)')
				->from('#__session')
				->where('guest = 0 AND client_id = 0');
		}
		$db->setQuery($query);
		$total_users = (int) $db->loadResult();
		
		return $total_users;
	}
	
	static function getJWT($data, $pass, $dev_id = 0)
	{
		// Create token header as a JSON string
		$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
		 
		if(isset($data['user_id']))
		{
		    $data['user_id'] = (int)$data['user_id'];
		}
		 
		// Create token payload as a JSON string
		$payload = json_encode($data);
		 
		// Encode Header to Base64Url String
		$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
		 
		// Encode Payload to Base64Url String
		$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
		 
		// Create Signature Hash
		$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $pass.$dev_id, true);
		 
		// Encode Signature to Base64Url String
		$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
		 
		// Create JWT
		return trim($base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature);
	}
	
}