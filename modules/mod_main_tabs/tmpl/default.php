<?php
defined('_JEXEC') or die;
?>
<div class="tabs-section<?php echo $moduleclass_sfx; ?>">
          <div class="tabs-section__tab-btns"><a class="tabs-section__tab-btn active" href="#"><span><?= $params->get('title_1', ''); ?></span></a><a class="tabs-section__tab-btn" href="#"><span><?= $params->get('title_2', ''); ?></span></a><a class="tabs-section__tab-btn" href="#"><span><?= $params->get('title_3', ''); ?></span></a></div>
          <div class="tabs-section__tabs">
            <div class="tabs-section__tab">
              <div class="tabs-section__content">
                <?= $params->get('description_1', ''); ?>
               <?= EXSystem::loadModules('news_tab'); ?>
              </div>
            </div>
            <div class="tabs-section__tab">
              <div class="tabs-section__content">
                <?= $params->get('description_2', ''); ?>
              </div>
            </div>
            <div class="tabs-section__tab">
              <div class="tabs-section__content">
                <?= $params->get('description_3', ''); ?>
              </div>
            </div>
          </div>
</div>