<?php
function pagination_list_render($list)
{
	$class = '';
	$html = '<ul>';
	//$html .= '<li class="pagination-prev">' . $list['previous']['data'] . '</li>';
	foreach ($list['pages'] as $page) {
		$html .= '<li>' . $page['data'] . '</li>';
	}
	//$html .= '<li class="pagination-next">' . $list['next']['data'] . '</li>';
	$html .= '</ul>';
	return $html;
}

function pagination_item_active(JPaginationObject $item)
{
	$title = '';
	if (!is_numeric($item->text)) {
		$title = ' class="hasTooltip" title="' . $item->text . '"';
	}
	return '<a' . $title . ' href="' . $item->link . '" class="">' . $item->text . '</a>';
}

function pagination_item_inactive(JPaginationObject $item)
{
	return '<span>' . $item->text . '</span>';
}