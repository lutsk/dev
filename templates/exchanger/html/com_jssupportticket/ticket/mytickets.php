<?php
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE."/components/com_exchanger/helpers/exchanger.php");

$lang = JFactory::getLanguage();
$lang->load('com_exchanger', JPATH_SITE);

$user = JSSupportTicketCurrentUser::getInstance();
$uid = $user->getId();
$uName = $user->getName();
$uEmail = $user->getEmail();

$rows = $this->result;

$dash = '-';
$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat, $dash, 0);
$firstvalue = substr($dateformat, 0, $firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat, $dash, $firstdash);
$secondvalue = substr($dateformat, $firstdash, $seconddash - $firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash, strlen($dateformat) - $seconddash);
$js_dateformat = '%' . $firstvalue . $dash . '%' . $secondvalue . $dash . '%' . $thirdvalue;

$ticketdata = $this->getJSModel('ticket')->getFormData(0, array());
$departments = $ticketdata[2]['departments'];
$priorities = $ticketdata[2]['priorities'];
$fieldsordering = $ticketdata[4];
?>
    <script>
        function myValidate(f){
            if (document.formvalidator.isValid(f)){
                f.check.value = '<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
            }else{
                alert("<?php echo JText::_('Some values are not acceptable please retry'); ?>");
                return false;
            }
            jQuery('#submit_app_button').attr('disabled',true);
    		f.submit();
            return true;
        }        
    </script>
<div class="account">
	<div class="container account__container">
		<div class="account-content">
			<div class="account-tickets">
				<div class="account-tickets__btn-container">
					<button class="btn btn--filled btn--wide-md" data-modal="#modal-report-a-problem"><?= JText::_('COM_EXCHANGER_REPORT_PROBLEM'); ?></button>
				</div>
				<?php if (!(empty($rows)) && is_array($rows)) : ?>
				<div class="account-tickets__wrapper">
					<?php foreach ($rows as $row) :
						$link = JFilterOutput::ampReplace('index.php?option=com_jssupportticket&c=ticket&layout=ticketdetail&id=' . $row->id. '&Itemid=' . $this->Itemid);
						$totalReplies = ExchangerSiteHelper::getTicketReplies($row->id);
					?>
					<div class="account-ticket">
						<div><span><?= JHtml::_('date', $row->created, JText::_('DATE_FORMAT_LC5')); ?></span><span><a href="<?= $link; ?>">#<?= $row->id; ?></a></span></div>
						<div><span><a href="<?= $link; ?>"><?= $row->departmentname; ?></a></span><span><?= $this->escape($row->subject); ?></span></div>
						<div><span><a href="<?= $link; ?>"><?= $totalReplies; ?></a></span></div>
					</div>
					<?php endforeach; ?>
				</div>
				<div class="pagination">
					<?= $this->pagination->getPagesLinks(); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="modal modal--centered modal--mobile-fixed-width" id="modal-report-a-problem">
			<div class="modal__content">
			<button class="close-button modal__close"></button>
				<div class="modal__content-inner">
					<form action="index.php" method="post" name="adminForm" id="adminForm" class="form jsticket_form" enctype="multipart/form-data">
						<div class="form__title"><?= JText::_('COM_EXCHANGER_REPORT_PROBLEM'); ?></div>
						<div class="form-select-group">
							<label class="form-select-group__label">&nbsp;<font color="red">*</font><?= JText::_('COM_EXCHANGER_REPORT_THEME'); ?></label>
							<?= $departments; ?>
						</div>
						<div class="form-select-group">
							<label class="form-select-group__label">&nbsp;<font color="red">*</font><?= JText::_('COM_EXCHANGER_REPORT_PRIORITY'); ?></label>
							<?= $priorities; ?>
						</div>
						<div class="form-group">
							<label class="form-group__label"><?= JText::_('COM_EXCHANGER_TRANSACTION_NUMBER'); ?></label>
							<input class="form-group__input" type="text" name="transaction_number">
						</div>
						<?php
						foreach($fieldsordering as $field) {
							switch($field->field){
                        		case 'subject':
									if ($field->published == 1) {
										?>
										<div class="form-group">
											<label class="form-group__label"><?= JText::_('COM_EXCHANGER_REPORT_THEME'); ?>&nbsp;<font color="red">*</font></label>
											<input class="form-group__input required" type="text" name="subject" id="subject" size="40" maxlength="255" value="" required />
										</div>
										<?php
		                            }
		                            break;
	                            
		                        case 'fullname':
		                            if ($field->published == 1) {
		                                ?>
										<div class="form-group">
											<label class="form-group__label"><?= JText::_('COM_EXCHANGER_USER_NAME_LABEL'); ?>&nbsp;<font color="red">*</font></label>
											<input class="form-group__input required" type="text" name="name" id="name"size="40" maxlength="255" value="<?php if(!empty($uName)) echo $uName; ?>" required />
										</div>
		                                <?php
		                            }
		                            break;
	                            
	                            case 'email':
	                            	if ($field->published == 1) {
		                                $readonly = '';
		                                if(isset($field->readonly) && $field->readonly == 1){
		                                    $readonly = 'readonly';
		                                }
	                            	?>
									<div class="form-group">
										<label class="form-group__label">&nbsp;<font color="red">*</font>Email</label>
										<input class="form-group__input required validate-email" type="text" name="email" id="email" size="40" maxlength="255" value="<?php if(!empty($uEmail)) echo $uEmail; ?>" required />
									</div>
									<?php
	                            	}
	                            break;
							}
						}
						?>
						<div class="form-group">
							<label class="form-group__label"><?= JText::_('COM_EXCHANGER_REPORT_DESCRIPTION'); ?></label>
							<textarea class="form-group__textarea" name="message" required></textarea>
						</div>
						
						<div class="form-button-group">
							<button id="submit_app_button" class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JSUBMIT'); ?></button>
							<button class="btn form-button-group__btn" type="button" data-modal-close><?= JText::_('JCANCEL'); ?></button>
						</div>
						
                        <input type="hidden" name="created" value="<?= date('Y-m-d H:i:s'); ?>" />
                        <input type="hidden" name="view" value="ticket" />
                        <input type="hidden" name="c" value="ticket" />
                        <input type="hidden" name="layout" value="formticket" />
                        <input type="hidden" name="option" value="<?= $this->option; ?>" />
                        <input type="hidden" name="task" value="saveticket" />
                        <input type="hidden" name="check" value="" />
                        <input type="hidden" name="status" value="0" />
                        <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
                        <input type="hidden" name="id" value="" />
                        <?php echo JHtml::_('form.token'); ?>
					</form>
 				</div>
			</div>
		</div>
		
		<div class="account-sidebar">
            <?= EXSystem::loadModules('sidebar'); ?>
		</div>
	</div>
</div>