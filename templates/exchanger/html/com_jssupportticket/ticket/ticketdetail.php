<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.formvalidation');
$document = JFactory::getDocument();
$document->addScript('administrator/components/com_jssupportticket/include/js/file/file_validate.js');

require_once(JPATH_SITE."/components/com_exchanger/helpers/exchanger.php");

JText::script('Error file size too large');
JText::script('Error file extension mismatch');

$lang = JFactory::getLanguage();
$lang->load('com_exchanger', JPATH_SITE);
//echo '<pre>'; print_r($this->ticket); echo '</pre>'; die();
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_exchanger/models', 'ExchangerModel');
$model = JModelLegacy::getInstance('Order', 'ExchangerModel');
$order = $model->getItem($this->ticket->transaction_number);
?>
<script>
        function myValidate(f) {
            var chk_reopen = jQuery('input#isreopen').val();
            if (document.formvalidator.isValid(f)) {
                if (chk_reopen == "") {
                    var msg_obj = jQuery("#message-required");
                    if (typeof msg_obj !== 'undefined' && msg_obj !== null) {
                        var msg_required_val = jQuery("#message-required").val();
                        if (msg_required_val != '') {
                            var message = jQuery('textarea#message').val();
                            if (message == '') {
                                alert("<?php echo JText::_('COM_EXCHANGER_NOT_ACCEPTABLE'); ?>");
                                 jQuery('textarea#message').focus();
                                return false;
                            }
                        }
                    }
                } else if (chk_reopen == 1) {
                    var msg_ro_obj = jQuery("#reopen-message-required");
                    if (typeof msg_ro_obj !== 'undefined' && msg_ro_obj !== null) {
                        var msg_ro_required_val = jQuery("#reopen-message-required").value;
                        if (msg_ro_required_val != '') {                        
                            var message_ro = jQuery('textarea#messages').val();
                            if (message_ro == '') {
                                alert("<?php echo JText::_('COM_EXCHANGER_NOT_ACCEPTABLE'); ?>");
                                jQuery('textarea#messages').focus();
                                return false;
                            }
                        }
                    }
                }
                f.check.value = '<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
            } else {
                alert("<?php echo JText::_('COM_EXCHANGER_NOT_ACCEPTABLE'); ?>");
                return false;
            }
            return true;
        }
</script>
<div class="account"> 
    <div class="container account__container">
		<div class="account-content">
			<div id="tk_detail_wraper" class="account-ticket-details">
				<?php 
				if ($this->config['offline'] == '1') {
					messagesLayout::getSystemOffline($this->config['title'],$this->config['offline_text']);
				} else {
					?>
			        <?php if(!empty($this->ticket)){ ?>
					<div id="tk_detail_wraper">
						<div class="account-title account-ticket-details__title"><?= JText::_('COM_EXCHANGER_TICKET'); ?> # <?= $this->ticket->id; ?></div>
						<div class="account-ticket-details-info">
							<span><?= $this->ticket->departmentname; ?></span>
							<?php if ($order) { ?>
							<a class="account-ticket-details-info__details-link" href="#" data-modal="#modal-transaction-details"><?= JText::_('COM_EXCHANGER_TRANSACTION_DETAILS'); ?></a>
							<?php } ?>
						</div>
						
						<div class="account-ticket-details-comments">
			    			<!-- Ticket Detail Box -->
				    		<div class="account-ticket-details-comment">
				    			<div><span><?= $this->ticket->name; ?></span><span><?= JHtml::_('date', $this->ticket->created, 'd/m/Y H:i'); ?></span></div>
				    			<div><?= $this->ticket->message; ?></div>
				    			<?php
				    			if (isset($this->attachment[0]->filename)) { ?>
				    				<div class="ticket-attachments-wrp">
				    					<?php foreach ($this->attachment as $attachment) {
				    						echo '
				    						<div class="js_ticketattachment">
				    							<span class="ticket-download-file-title">'
				    								. $attachment->filename . "&nbsp(" . round($attachment->filesize, 2) . " KB)";
				    							echo '</span> 
				    							<a class="download-button" target="_blank" href="index.php?option=com_jssupportticket&c=ticket&task=getdownloadbyid&id='.$attachment->attachmentid.'&'. JSession::getFormToken() .'=1"><img class="ticket-download-img" src="components/com_jssupportticket/include/images/download-all.png"></a>  
				    						</div>';
				    					}?>
				    				</div>
				    			<?php     
				    			}
				    			?>
				    		</div>
				    		<?php for ($i = 0, $n = count($this->messages); $i < $n; $i++) {
                                $row = & $this->messages[$i]; ?>
				    			<div class="account-ticket-details-comment account-ticket-details-comment<?php if($row->staffid != 0) echo '--admin'; ?>">
					    			<div><span><?php if($row->name) echo $row->name; else echo $this->ticket->name; ?></span><span><?php echo JHtml::_('date',$row->created,"d/m/Y H:i");?></span></div>
					    			<div><?= $row->message; ?></div>
					    			<?php
					    			$count = $row->count;
					    			if ($count >= 1) {
					    				$outdex = $i + $count;
					    				echo ' <div class="ticket-attachments-wrp">';
					    					for ($j = $i; $j < $outdex; $j++) {
					    						if ($row->filename && $row->filename <> '') {
					    							$datadirectory = $this->config['data_directory'];
					    							$path = 'index.php?option=com_jssupportticket&c=ticket&task=getdownloadbyid&id='.$row->attachmentid.'&'.JSession::getFormToken().'=1';
					    							echo '  <div class="js_ticketattachment">
					    								<a class="download-button" target="_blank" href="' . $path . '"><span class="ticket-download-file-title">'
					    									. $row->filename . "&nbsp(" . round($row->filesize, 2) . " KB)";
					    								echo '</span></a>
					    							</div>'; 
					    						} 
					    					};
					    				echo ' </div>';
					    				$i = $outdex - 1; 
					    			}
					    			?>
				    			</div>
				    		<?php } ?>
						</div>
						
						<?php if ($this->ticket->lock == 0 && $this->ticket->status != 4 && $isautoclose != 1) { ?>
						<form action="index.php" method="post" name="adminForm" id="adminForm" class="account-ticket-details-form form-validate" enctype="multipart/form-data"  onSubmit="return myValidate(this);">
							<div class="feedback-message account-ticket-details-form__message">
								<textarea id='message-required' class="feedback-message__textarea" name="message" required></textarea>
								<button class="feedback-message__btn" type="submit" name="submit_app" title="<?= JText::_('JSUBMIT'); ?>"></button>
							</div>
							<div class="account-ticket-details-form__controls">
								<a class="back-btn hidden-mobile" href="<?= JFilterOutput::ampReplace('index.php?option=com_jssupportticket&view=ticket&layout=mytickets&Itemid=' . $this->Itemid); ?>"><?= JText::_('COM_EXCHANGER_BACK'); ?></a>
								<div class="attachment-link">
									<label><?= JText::_('COM_EXCHANGER_UPLOAD_ATTACHMENT'); ?></label>
									<input type="file" class="inputbox attachment-inputbox form-input-field-attachment" name="filename[]" onchange="uploadfile(this, '<?php echo $this->config["filesize"]; ?>', '<?php echo $this->config["fileextension"]; ?>');" size="20" maxlenght="30"/>
									<div id="attachment-option">
										<?php echo JText::_('COM_EXCHANGER_MAX_FILE_SIZE') . ' (' . $this->config['filesize']; ?>KB)<br><?php echo JText::_('COM_EXCHANGER_EXTENSION_TYPE') . ' (' . $this->config['fileextension'] . ')'; ?>
									</div>
								</div>
								<a class="back-btn hidden-desktop" href="<?= JFilterOutput::ampReplace('index.php?option=com_jssupportticket&view=ticket&layout=mytickets&Itemid=' . $this->Itemid); ?>"><?= JText::_('COM_EXCHANGER_BACK'); ?></a>
							</div>
							
							<input type="hidden" name="created" value="<?php echo $curdate = date('Y-m-d H:i:s'); ?>" />
							<input type="hidden" name="view" value="ticket" />
	                        <input type="hidden" name="c" value="ticket" />
	                        <input type="hidden" name="layout" value="ticketdetail" />
	                        <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
	                        <input type="hidden" name="task" value="actionticket" />
	                        <input type="hidden" name="check" value="" />
	                        <input type="hidden" name="email" value="<?php echo $this->email; ?>" />
	                        <input type="hidden" name="callaction" id="callaction" value="" />
	                        <input type="hidden" name="callfrom" id="callfrom" value="savemessage" />
	                        <input type="hidden" name="isreopen" id="isreopen" value="" />
	                        <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	                        <input type="hidden" name="lastreply" value="<?php echo $this->ticket->lastreply; ?>" />
	                        <input type="hidden" name="ticketid" value="<?php if (isset($this->ticket)) echo $this->ticket->id; ?>" />
	                        <input type="hidden" name="ticketrandomid" value="<?php if (isset($this->ticket)) echo $this->ticket->ticketid; ?>" />
                        	<?php echo JHtml::_('form.token'); ?>
						</form>
						<?php } ?>
			    	</div>
					<?php
					}else{
						messagesLayout::getRecordNotFound();
					}
				}
			    ?>
			</div>
			
			<?php if ($order) { ?>
			<div class="modal modal--centered modal--mobile-fixed-width" id="modal-transaction-details">
				<div class="modal__content">
					<button class="close-button modal__close"></button>
					<div class="modal__content-inner">
						<div class="modal__title modal__title--offset-bottom-md"><?= JText::_('COM_EXCHANGER_TRANSACTION_DETAILS'); ?> #<?= $this->ticket->id; ?></div>
						<div class="modal__currency-details">
							<div class="modal__currency-details-currency"><span><?= $order->amount_from; ?></span>
								<div>
									<img class="currencies-icon" src="<?= $order->currency_from_image ;?>" width="40" /><?= $order->currency_from_code; ?>
								</div>
							</div>
							<div class="modal__currency-details-exchange-icon"></div>
							<div class="modal__currency-details-currency"><span><?= $order->amount_to; ?></span>
								<div>
									<img class="currencies-icon" src="<?= $order->currency_to_image ;?>" width="40" /><?= $order->currency_to_code; ?>
								</div>
							</div>
							<div class="modal__currency-details-status"><?= JText::_('COM_EXCHANGER_STATUS'); ?><span class="<?= $order->statusClass; ?>"><?= $order->statusTitle; ?></span></div>
						</div>
						<div class="modal__button-container"><a class="btn btn--filled" href="<?= JRoute::_('index.php?option=com_exchanger&view=order&id=' . $order->id, false); ?>" target="_blank"><?= JText::_('COM_EXCHANGER_TRANSACTION_DETAILS'); ?></a></div>
					</div>
				</div>
			</div>
			<?php } ?>
			
		</div>
		
		<div class="account-sidebar">
            <?= EXSystem::loadModules('sidebar'); ?>
		</div>
    </div>
</div>
<script>
    
        jQuery("#attachment-add").click(function () {
            var obj = this;
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            var append_text = "<span class='attachment-file-box'><input name='filename[]' class=' attachment-inputbox form-input-field-attachment' type='file' onchange=uploadfile(this,'<?php echo $this->config['filesize']; ?>','<?php echo $this->config['fileextension']; ?>'); size='20' maxlenght='30' /><span  class='attachment-remove'></span></span>";
            if (current_files < total_allow) { 
                jQuery("#attachment-files").append(append_text);
            } else if ((current_files === total_allow) || (current_files > total_allow)) {
                alert("<?php echo JText::_('File upload limit exceed'); ?>");
                jQuery(obj).hide();
            }
        });
        jQuery(document).delegate(".attachment-remove", "click", function (e) {
            var current_files = jQuery('input[name="filename[]"]').length;
            if(current_files!=1)
                jQuery(this).parent().remove();
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            if (current_files < total_allow) {
                jQuery("#attachment-add").show();
            }
        });

    function actioncall(value) {
        if (value == 8) {
            /*
            jQuery('#isreopen').val(1);
            jQuery('div#tk_reopenticket').slideDown();
            tinyMCE.get('messages').focus();
            */
            jQuery('#callfrom').val('action');
            jQuery('#callaction').val(value);
            document.adminForm.submit();
        } else if (value == 3) {
            var yesclose = confirm('<?php echo JText::_('Are you sure to close ticket'); ?>');                
            if(yesclose == true){
                jQuery('#callfrom').val('action');
                jQuery('#callaction').val(value);
                document.adminForm.submit();
            }
        }
    }
    function closereopndiv() {
        jQuery('div#tk_reopenticket').slideUp();
    }
</script>
