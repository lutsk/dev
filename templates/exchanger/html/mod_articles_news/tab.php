<?php
defined('_JEXEC') or die;
?>
<div class="tabs-news<?= $params->get('moduleclass_sfx'); ?>">
	<?php for ($i = 0, $n = count($list); $i < $n; $i ++) : ?>
		<?php $item = $list[$i]; ?>
		<div class="tabs-news__item">
			<?php if ($params->get('img_intro_full') !== 'none' && !empty($item->imageSrc)) : ?>
			<div class="tabs-news__item-img-block"><img class="tabs-news__item-img" src="<?= $item->imageSrc; ?>" alt="<?= $item->imageAlt; ?>"></div>
			<?php endif; ?>
			<div class="tabs-news__item-content">
				<?php if ($params->get('item_title')) : ?>
					<?php if ($item->link !== '' && $params->get('link_titles')) : ?>
						<a href="<?= $item->link; ?>"><?= $item->title; ?></a>
					<?php else : ?>
						<div class="tabs-news__item-title"><?= $item->title; ?></div>
					<?php endif; ?>
				<?php endif; ?>
				<div class="tabs-news__item-date"><?= JHtml::_('date', $item->created, 'M d, Y'); ?></div>
				<?php if ($params->get('show_introtext', 1)) : ?>
				<div class="tabs-news__item-text">
					<?= $item->introtext; ?>
					<?= $item->afterDisplayContent; ?>
				</div>
				<?php endif; ?>
				<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) : ?>
				<a class="arrow-btn tabs-news__item-read-more-btn" href="<?= $item->link; ?>"><?= $item->linkText; ?></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endfor; ?>
</div>
<div class="tabs-section__btn-group"><a class="btn btn--accent tabs-section__btn" href="<?= $params->get('link', '/'); ?>"><?= $params->get('link_title', '');?></a></div>