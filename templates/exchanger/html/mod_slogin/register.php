<?php
defined('_JEXEC') or die();

$lang = JFactory::getLanguage();
$language_tag = $lang->getTag();
$lang->load('com_users', JPATH_SITE, $language_tag);
\JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
$model = JModelLegacy::getInstance('Registration', 'UsersModel');
$form = $model->getForm();
/*$form->reset(true);
$form->loadFile( dirname(__FILE__) . '/forms/registration.xml');*/
?>

<form id="member-registration" class="form" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate form-horizontal well" enctype="multipart/form-data">
	<div class="form__title"><?= JText::_('JREGISTER'); ?></div>
	<?php
	$form->removeField('spacer');
	?>
	<?php foreach ($form->getFieldsets() as $fieldset) : ?>
			<?php $fields = $form->getFieldset($fieldset->name); ?>
			<?php if (count($fields)) : ?>
				<fieldset class="<?php echo $fieldset->class; ?>">
					<?php echo $form->renderFieldset($fieldset->name); ?>
				</fieldset>
			<?php endif; ?>
	<?php endforeach; ?>
	
	<div class="form-button-group">
		<button class="btn btn--filled form-button-group__btn" type="submit"><?php echo JText::_('JREGISTER'); ?></button>
		<button class="btn form-button-group__btn" type="button" data-modal="#modal-login"><?= JText::_('JLOGIN') ?></button>
	</div>
	
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="registration.register" />
	<?php echo JHtml::_('form.token'); ?>
</form>