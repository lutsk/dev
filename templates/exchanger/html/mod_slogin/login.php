<?php
defined('_JEXEC') or die();
$app = JFactory::getApplication();
$lang = JFactory::getLanguage();
$language_tag = $lang->getTag();
$lang->load('com_users', JPATH_SITE, $language_tag);

$loginMenu	 = $app->getMenu()->getItems('link', 'index.php?option=com_users&view=login', true);
$loginMenuParams = $loginMenu->params;
\JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
$loginModel = JModelLegacy::getInstance('Login', 'UsersModel');
$loginForm = $loginModel->getForm();
$return = $loginForm->getValue('return', '', $loginMenuParams->get('login_redirect_url', $loginMenuParams->get('login_redirect_menuitem')));
?>
<noindex>
<?php if ($params->get('inittext')): ?>
    <div class="pretext">
        <p><?= $params->get('inittext'); ?></p>
    </div>
<?php endif; ?>
<?php if (count($plugins)): ?>
<div id="slogin-buttons" class="slogin-buttons slogin-compact">
    <?php
    foreach($plugins as $link):
        $linkParams = '';
        if(isset($link['params'])){
            foreach($link['params'] as $k => $v){
                $linkParams .= ' ' . $k . '="' . $v . '"';
            }
        }
        $title = (!empty($link['plugin_title'])) ? ' title="'.$link['plugin_title'].'"' : '';
        ?>
        <a rel="nofollow" <?= $linkParams.$title;?> href="<?= JRoute::_($link['link']);?>"><span class="<?= $link['class'];?>">&nbsp;</span></a>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<?php if ($params->get('pretext')): ?>
    <div class="pretext">
        <p><?= $params->get('pretext'); ?></p>
    </div>
<?php endif; ?>

<?php if ($params->get('show_login_form')): ?>
    <form id="login-form" class="form" action="<?= JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post">
		<div class="form__title"><?= JText::_('COM_EXCHANGER_LOGIN') ?></div>
              <div class="form-group">
                <label class="form-group__label"><?= JText::_('MOD_SLOGIN_VALUE_USERNAME') ?></label>
                <input class="form-group__input" type="text" name="username" required>
              </div>
              <div class="form-group">
                <label class="form-group__label"><?= JText::_('JGLOBAL_PASSWORD') ?></label>
                <button class="form-group__password-show" type="button"></button>
                <input class="form-group__input" type="password" name="password" required>
              </div>
              <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
              <div class="form-checkbox-group modal__form-checkbox-group">
                <input class="form-checkbox-group__input" id="login-remember-me" type="checkbox" name="login-remember-me" value="yes">
                <label class="form-checkbox-group__label modal__form-checkbox-group-label" for="login-remember-me"><?= JText::_('MOD_SLOGIN_REMEMBER_ME') ?></label>
              </div>
              <?php endif; ?>
              <div class="form-button-group">
                <button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JLOGIN') ?></button>
                <button class="btn form-button-group__btn" type="button" data-modal="#modal-register"><?= JText::_('JREGISTER'); ?></button>
              </div>
              <div class="form__info"><?= JText::_('COM_EXCHANGER_FORGOT_PASSWORD_DESC'); ?> <a href="#" data-modal="#modal-reset-password"><?= JText::_('COM_EXCHANGER_FORGOT_PASSWORD'); ?></a></div>
              
            <input type="hidden" name="return" value="<?= base64_encode($return); ?>" />
			<?= JHtml::_('form.token'); ?>
	</form>
<?php endif; ?>

<div id="slogin-buttons" class="slogin-buttons slogin-default">

    <?php if (count($plugins)): ?>
    <?php
        foreach($plugins as $link):
            $linkParams = '';
            if(isset($link['params'])){
                foreach($link['params'] as $k => $v){
                    $linkParams .= ' ' . $k . '="' . $v . '"';
                }
            }
			$title = (!empty($link['plugin_title'])) ? ' title="'.$link['plugin_title'].'"' : '';
            ?>
            <a  rel="nofollow" class="link<?php echo $link['class'];?>" <?php echo $linkParams.$title;?> href="<?php echo JRoute::_($link['link']);?>"><span class="<?php echo $link['class'];?> slogin-ico">&nbsp;</span><span class="text-socbtn"><?php echo $link['plugin_title'];?></span></a>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
</noindex>