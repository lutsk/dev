<?php
defined('_JEXEC') or die();

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

$lang = JFactory::getLanguage();
$language_tag = $lang->getTag();
$lang->load('com_users', JPATH_SITE, $language_tag);
$lang->load('com_exchanger', JPATH_SITE, $language_tag);
\JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
$model = JModelLegacy::getInstance('Reset', 'UsersModel');
$form = $model->getForm();
?>
<noindex>
<form class="form" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post">
	<div class="form__title"><?php echo JText::_('COM_USERS_RESET'); ?></div>
	<?php foreach ($form->getFieldsets() as $fieldset) : ?>
			<fieldset>
              <div class="form__subtitle"><?php echo JText::_('COM_EXCHANGER_RESET'); ?></div>
				<?php echo $form->renderFieldset($fieldset->name); ?>
			</fieldset>
		<?php endforeach; ?>
              <div class="form-button-group">
                <button class="btn btn--filled form-button-group__btn" type="submit"><?php echo JText::_('JSUBMIT'); ?></button>
              </div>
	<?php echo JHtml::_('form.token'); ?>
	</form>
</noindex>