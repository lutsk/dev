<?php
defined('JPATH_BASE') or die;

extract($displayData);

$class = empty($options['class']) ? '' : ' ' . $options['class'];
$rel   = empty($options['rel']) ? '' : ' ' . $options['rel'];
$checkbox = mb_stripos($input, 'type="checkbox"');

if ($checkbox !== false){ ?>
<div class="form-checkbox-group modal__form-checkbox-group<?php echo $class; ?>"<?php echo $rel; ?>>
	<?php echo $input; ?>
	<?php if (empty($options['hiddenLabel'])) : ?>
		<?php echo $label; ?>
	<?php endif; ?>
</div>
<?php } else { ?>
<div class="form-group<?php echo $class; ?>"<?php echo $rel; ?>>
	<?php if (empty($options['hiddenLabel'])) : ?>
		<?php echo $label; ?>
	<?php endif; ?>
	<?php echo $input; ?>
</div>
<?php }
?>

