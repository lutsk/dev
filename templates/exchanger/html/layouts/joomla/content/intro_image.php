<?php
defined('_JEXEC') or die;

$params = $displayData->params;
$caption = '';
?>
<?php $images = json_decode($displayData->images); ?>
<?php if (!empty($images->image_intro)) : ?>
	<?php $imgfloat = empty($images->float_intro) ? $params->get('float_intro') : $images->float_intro; ?>
	<div class="pull-<?= htmlspecialchars($imgfloat, ENT_COMPAT, 'UTF-8'); ?> news__item-img-block">
	<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
		<?php if ($images->image_intro_caption) {
			$caption = ' caption';
		} ?>
		<a href="<?= JRoute::_(ContentHelperRoute::getArticleRoute($displayData->slug, $displayData->catid, $displayData->language)); ?>"><img class="news__item-img<?= $caption; ?>"
		<?php if ($images->image_intro_caption) : ?>
			<?= ' title="' . htmlspecialchars($images->image_intro_caption) . '"'; ?>
		<?php endif; ?>
		src="<?= htmlspecialchars($images->image_intro, ENT_COMPAT, 'UTF-8'); ?>" alt="<?= htmlspecialchars($images->image_intro_alt, ENT_COMPAT, 'UTF-8'); ?>"/></a>
	<?php else : ?><img class="news__item-img<?= $caption; ?>"
		<?php if ($images->image_intro_caption) : ?>
			<?= ' title="' . htmlspecialchars($images->image_intro_caption, ENT_COMPAT, 'UTF-8') . '"'; ?>
		<?php endif; ?>
		src="<?= htmlspecialchars($images->image_intro, ENT_COMPAT, 'UTF-8'); ?>" alt="<?= htmlspecialchars($images->image_intro_alt, ENT_COMPAT, 'UTF-8'); ?>"/>
	<?php endif; ?>
	</div>
<?php endif; ?>
