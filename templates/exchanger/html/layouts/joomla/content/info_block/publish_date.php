<?php
defined('_JEXEC') or die;
?>
<div class="news__item-date">
	<?= JHtml::_('date', $displayData['item']->publish_up, 'd F, Y'); ?>
</div>