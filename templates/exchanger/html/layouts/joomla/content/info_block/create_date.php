<?php
defined('_JEXEC') or die;
?>
<div class="news__item-date">
	<?= JHtml::_('date', $displayData['item']->created, 'd F, Y'); ?>
</div>