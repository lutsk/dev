<?php
defined('_JEXEC') or die;
?>
<ul class="mobile-lang-list">
	<?php foreach ($list as $language) : ?>
		<?php if (!$language->active) : ?>
			<li class="mobile-lang-list__item">
				<a class="mobile-lang-list__link" href="<?php echo htmlspecialchars_decode(htmlspecialchars($language->link, ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES); ?>">
					<?php echo JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true); ?>
					<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef); ?>
				</a>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
</ul>
