<?php
defined('_JEXEC') or die;

//echo '<pre>'; print_r($list); echo '</pre>'; die();

?>
<?php if ($headerText) : ?>
	<div class="pretext"><p><?php echo $headerText; ?></p></div>
<?php endif; ?>

<?php if ($params->get('dropdown', 0) && !$params->get('dropdownimage', 1)) : ?>
<div class="mod-languages<?php echo $moduleclass_sfx; ?>">
	<form name="lang" method="post" action="<?php echo htmlspecialchars_decode(htmlspecialchars(JUri::current(), ENT_COMPAT, 'UTF-8'), ENT_NOQUOTES); ?>">
	<select class="inputbox advancedSelect" onchange="document.location.replace(this.value);" >
	<?php foreach ($list as $language) : ?>
		<option dir=<?php echo $language->rtl ? '"rtl"' : '"ltr"'; ?> value="<?php echo htmlspecialchars_decode(htmlspecialchars($language->link, ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES); ?>" <?php echo $language->active ? 'selected="selected"' : ''; ?>>
		<?php echo $language->title_native; ?></option>
	<?php endforeach; ?>
	</select>
	</form>
</div>
<?php elseif ($params->get('dropdown', 0) && $params->get('dropdownimage', 1)) : ?>
	<?php foreach ($list as $language) : ?>
		<?php if ($language->active) : ?>
			<a class="header-user-panel__lang dropdown-link dropdown__lang-list-item-link--<?= $language->image; ?>" href="#" title="<?php echo $language->title_native; ?>"></a>
		<?php endif; ?>
	<?php endforeach; ?>
	
	<div class="dropdown dropdown--lang">
		<ul class="dropdown__lang-list dropdown__lang-list--long" dir="<?php echo JFactory::getLanguage()->isRtl() ? 'rtl' : 'ltr'; ?>">
		<?php foreach ($list as $language) : ?>
			<?php if (!$language->active) : ?>
			<li class="dropdown__lang-list-item">
				<a class="dropdown__lang-list-item-link dropdown__lang-list-item-link--<?= $language->image; ?>" href="<?= htmlspecialchars_decode(htmlspecialchars($language->link, ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES); ?>" title="<?php echo $language->title_native; ?>"></a>
			</li>
			<?php endif; ?>
		<?php endforeach; ?>
		</ul>
	</div>
<?php else : ?>
<div class="mod-languages<?php echo $moduleclass_sfx; ?>">
	<ul class="<?php echo $params->get('inline', 1) ? 'lang-inline' : 'lang-block'; ?>" dir="<?php echo JFactory::getLanguage()->isRtl() ? 'rtl' : 'ltr'; ?>">
	<?php foreach ($list as $language) : ?>
		<?php if (!$language->active) : ?>
			<li>
			<a href="<?php echo htmlspecialchars_decode(htmlspecialchars($language->link, ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES); ?>">
			<?php if ($params->get('image', 1)) : ?>
				<?php if ($language->image) : ?>
					<?php echo JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true); ?>
				<?php else : ?>
					<span class="label"><?php echo strtoupper($language->sef); ?></span>
				<?php endif; ?>
			<?php else : ?>
				<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef); ?>
			<?php endif; ?>
			</a>
			</li>
		<?php elseif ($params->get('show_active', 1)) : ?>
			<?php $base = JUri::getInstance(); ?>
			<li class="lang-active">
			<a href="<?php echo htmlspecialchars_decode(htmlspecialchars($base, ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES); ?>">
			<?php if ($params->get('image', 1)) : ?>
				<?php if ($language->image) : ?>
					<?php echo JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true); ?>
				<?php else : ?>
					<span class="label"><?php echo strtoupper($language->sef); ?></span>
				<?php endif; ?>
			<?php else : ?>
				<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef); ?>
			<?php endif; ?>
			</a>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<?php if ($footerText) : ?>
	<div class="posttext"><p><?php echo $footerText; ?></p></div>
<?php endif; ?>
