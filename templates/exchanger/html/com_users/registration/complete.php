<?php
defined('_JEXEC') or die;

?>
<div class="account<?php echo $this->pageclass_sfx; ?>">
	<div class="container account__container">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	<?php endif; ?>
	</div>
</div>
