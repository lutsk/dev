<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="account<?php echo $this->pageclass_sfx; ?> text-section">
<div class="container account__container">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<fieldset>
				<?php if (isset($fieldset->label)) : ?>
					<p><?php echo JText::_($fieldset->label); ?></p>
				<?php endif; ?>
				<div class="form-group" style="max-width: 340px;"><?php echo $this->form->renderFieldset($fieldset->name); ?></div>
			</fieldset>
		<?php endforeach; ?>
		<div class="form-button-group">
            <button class="btn btn--filled form-button-group__btn validate" type="submit"><?php echo JText::_('JSUBMIT'); ?></button>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
</div>
