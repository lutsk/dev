<?php
defined('_JEXEC') or die;

$cookieLogin = $this->user->get('cookieLogin');

if (!empty($cookieLogin) || $this->user->get('guest'))
{
	echo $this->loadTemplate('login');
}
else
{
	echo $this->loadTemplate('logout');
}
