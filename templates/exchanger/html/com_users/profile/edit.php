<?php
defined('_JEXEC') or die;

use Joomla\CMS\Form\Form;
use Joomla\CMS\Factory;

$lang = JFactory::getLanguage();
$lang->load('com_exchanger', JPATH_SITE);

$advForm = Form::getInstance("edit", __DIR__ . "/forms/edit.xml", array("control" => "advdata"));
$advFormData = EXSystem::getAdvFormData($this->data->id);
$advForm->bind($advFormData);
?>
	<script type="text/javascript">
		Joomla.twoFactorMethodChange = function(e)
		{
			var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

			jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el)
			{
				if (el.id != selectedPane)
				{
					jQuery('#' + el.id).hide(0);
				}
				else
				{
					jQuery('#' + el.id).show(0);
				}
			});
		}
	</script>
<div class="account<?= $this->pageclass_sfx; ?>">
    <div class="container account__container">
		<div class="account-content">
			<div class="account-details">
				<?php if ($this->params->get('show_page_heading')) : ?>
				<div class="account-title"><?= $this->escape($this->params->get('page_heading')); ?></div>
				<?php endif; ?>
			<div class="account-details-section account-my-account">
				<div>
					<div><?= JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?>:</div>
					<div>
						<div><?= $this->escape($this->data->name); ?></div>
						<div><a class="account-my-account-icon-edit" href="#" data-modal="#modal-account-edit"><?= JText::_('COM_USERS_EDIT_PROFILE'); ?></a></div>
					</div>
				</div>
				<!--<div>
					<div><?php //echo JText::_('COM_EXCHANGER_USER_PHONE'); ?>:</div>
					<div>
						<div><?php //echo $this->escape($advFormData['phone']); ?></div>
						<div><a class="account-my-account-icon-change" href="#" data-modal="#modal-account-change-password"><?php //echo JText::_('COM_EXCHANGER_CHANGE'); ?></a></div>
					</div>
				</div>-->
				<div>
					<div><?= JText::_('COM_EXCHANGER_USER_EMAIL'); ?>:</div>
					<div>
						<div><?= $this->escape($this->data->email); ?></div>
					</div>
				</div>
			</div>
			
			<div class="account-title"><?= JText::_('COM_EXCHANGER_USER_IDENTIFICATION'); ?></div>
				<form class="account-details-section account-identification" action="<?= JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" enctype="multipart/form-data">
					<div>
						<div><?= JText::_('COM_EXCHANGER_USER_SCAN_PASSPORT'); ?>:</div>
						<div>
							<?php //if (empty($advFormData['photo_passport'])) : ?>
							<div class="account-attachment-info"><?= JText::_('COM_EXCHANGER_USER_NO_UPLOAD'); ?></div>
							<?php //endif; ?>
							<div>
								<div class="account-attachment">
									<?= $advForm->getLabel('photo_passport'); ?>
									<?= $advForm->getInput('photo_passport'); ?>
								</div><span><?= JText::_('COM_EXCHANGER_USER_ATTACH_IMAGE'); ?></span>
							</div>
		                 </div>
					</div>
					<div>
		                <div><?= JText::_('COM_EXCHANGER_USER_CARD_PHOTO'); ?>:</div>
		                <div>
							<?php //if (empty($advFormData['photo_card'])) : ?>
							<div class="account-attachment-info"><?= JText::_('COM_EXCHANGER_USER_NO_UPLOAD'); ?></div>
							<?php //endif; ?>
							<div>
		                    	<div class="account-attachment">
									<?= $advForm->getLabel('photo_card'); ?>
									<?= $advForm->getInput('photo_card'); ?>
								</div><span><?= JText::_('COM_EXCHANGER_USER_ATTACH_IMAGE'); ?></span>
							</div>
		                </div>
					</div>
					<?php if (!empty($advFormData['photo_passport'])) : ?>
					<div class="attachment-img"><img src="../images/users/<?= $advFormData['user_id']; ?>/<?= $advFormData['photo_passport']; ?>" alt="" width="200" /></div>
					<?php endif; ?>
					<?php if (!empty($advFormData['photo_card'])) : ?>
					<div class="attachment-img"><img src="../images/users/<?= $advFormData['user_id']; ?>/<?= $advFormData['photo_card']; ?>" alt="" width="200" /></div>
					<?php endif; ?>
					
					<div class="control-group">
						<div class="controls">
							<button type="submit" class="btn btn-primary validate">
								<?php echo JText::_('COM_EXCHANGER_UPLOAD'); ?>
							</button>
						</div>
					</div>
					<div style="visibility:hidden;opacity:0">
						<?= $this->form->getInput('name'); ?>
						<?= $this->form->getInput('email1'); ?>
						<?= $this->form->getInput('email2'); ?>
					</div>
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="profile.save" />
					<input type="hidden" name="option" value="com_users" />
					<?= JHtml::_('form.token'); ?>
				</form>
				<div class="account-title"><?= JText::_('COM_EXCHANGER_USER_SOCIAL_NETWORK'); ?></div>
					<div class="account-details-section">
						<div class="account-social"><a class="account-social-link account-social-link--facebook" href="#"><?= JText::_('COM_EXCHANGER_ENABLE'); ?></a><a class="account-social-link account-social-link--twitter" href="#"><?= JText::_('COM_EXCHANGER_ENABLE'); ?></a></div>
				</div>
			</div>
		</div>
		<div class="account-sidebar">
			<?= EXSystem::loadModules('sidebar'); ?>
		</div>
	</div>
</div>
	
<div class="modal modal--centered modal--mobile-fixed-width" id="modal-account-edit">
	<div class="modal__content">
		<button class="close-button modal__close"></button>
		<div class="modal__content-inner">
            <form action="<?= JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="form-validate form-horizontal well" enctype="multipart/form-data">
				<div class="form__title"><?= JText::_('COM_USERS_PROFILE_DEFAULT_LABEL'); ?></div>
				
				<div class="form-group">
					<?= $this->form->getLabel('email1'); ?>
					<?= $this->form->getInput('email1'); ?>
				</div>
				<div class="form-group">
					<?= $this->form->getLabel('email2'); ?>
					<?= $this->form->getInput('email2'); ?>
				</div>
				<div class="form-group">
					<?= $this->form->getLabel('password1'); ?>
					<?= $this->form->getInput('password1'); ?>
				</div>
				<div class="form-group">
					<?= $this->form->getLabel('password2'); ?>
					<?= $this->form->getInput('password2'); ?>
				</div>
				<div class="form-group">
					<?= $this->form->getLabel('name'); ?>
					<?= $this->form->getInput('name'); ?>
				</div>
				
				<div class="form-group">
					<?= $advForm->getLabel('surname'); ?>
					<?= $advForm->getInput('surname'); ?>
				</div>
				<div class="form-group">
					<?= $advForm->getLabel('middle_name'); ?>
					<?= $advForm->getInput('middle_name'); ?>
				</div>
				<div class="form-group">
					<?= $advForm->getLabel('passport_series'); ?>
					<?= $advForm->getInput('passport_series'); ?>
				</div>
				<div class="form-group">
					<?= $advForm->getLabel('passport_id'); ?>
					<?= $advForm->getInput('passport_id'); ?>
				</div>
				<div class="form-group">
					<?= $advForm->getLabel('phone'); ?>
					<?= $advForm->getInput('phone'); ?>
				</div>
				<div class="form-checkbox-group modal__form-checkbox-group">
					<?= $advForm->getInput('subscribe'); ?>
					<?= $advForm->getLabel('subscribe'); ?>
				</div>
				<div class="form-button-group">
					<button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JAPPLY'); ?></button>
					<button class="btn form-button-group__btn" type="button" data-modal-close><?= JText::_('JCANCEL'); ?></button>
				</div>
				
				<?= JHtml::_('form.token'); ?>
			</form>
		</div>
	</div>
</div>
