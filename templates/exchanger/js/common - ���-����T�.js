$(function() {

  /**
   * Custom select
   */

  // https://stackoverflow.com/questions/19999388/check-if-user-is-using-ie
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");
  if ( !(msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) ) {
    $('.form-select-group__select')
      .selectBoxIt()
      .addClass('required');
  }

  $('.select-sort-by').selectBoxIt();


  /**
   * This function should give you width of scrollbar
   * https://stackoverflow.com/a/13382873/8313588
   */

  function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
  }

  /**
   * Body lock
   */

  var disableBodyScroll = bodyScrollLock.disableBodyScroll,
      clearAllBodyScrollLocks = bodyScrollLock.clearAllBodyScrollLocks,
      bodyLockFixMenuTarget = '.header-bottom';

  function bodyLocked(target) {
    var viewportWidth = $(window).width();
    var disableTarget = document.querySelector(target);

    if ($(target).data('modal-second') === undefined) {
      disableBodyScroll(disableTarget, {
        reserveScrollBarGap: true
      });
      // set max-width to fixed menu, because of flickering when scrollbar is hiding
      if ($(bodyLockFixMenuTarget).hasClass('fixed')) {
        $(bodyLockFixMenuTarget).css('max-width', viewportWidth + 40); // + padding
      }
    }
  }
  function bodyUnLocked() {
    clearAllBodyScrollLocks();
  }
  // return to normal width after scrollbar back
  $(window).resize(function() {
    $(bodyLockFixMenuTarget).css('max-width', '');
  });
  $(window).scroll(function() {
    $(bodyLockFixMenuTarget).css('max-width', '');
  });

  /**
   * Overlay
   */

  function openOverlay(action, subclasses, modal) {
    $('.overlay').unbind('click').remove();
    if (subclasses === undefined) { subclasses = ''; }
    else { subclasses = ' ' + subclasses; }

    if (modal) {
      $(modal).append('<div class="overlay' + subclasses + '"></div>');
    } else {
      $('body').append('<div class="overlay' + subclasses + '"></div>');
    }

    $('.overlay').fadeIn(300);
    $('.overlay').click(function(e) {
      action();
    });
  }

  function closeOverlay() {
    $('.overlay').fadeOut(300);
    setTimeout(function() {
      $('.overlay').unbind('click').remove();
    }, 300);
  }

  /**
   * Dropdown
   */

  var dropdownLink = '.dropdown-link',
      dropdownLinkTarget = '.dropdown',
      headerBottom = '.header-bottom',
      // headerBottomBordered = 'header-bottom--bordered',
      dropdownCloseSelector = '.dropdown__currencies-list-link';

  $(dropdownLink).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    // var left = this.getBoundingClientRect().left;

    closeExchangerDropdown();

    $(dropdownLink).not(this).removeClass('open').next(dropdownLinkTarget).removeClass('open');
    $(this).toggleClass('open').next(dropdownLinkTarget).toggleClass('open'); //.css('left', left);

    // $(headerBottom).removeClass(headerBottomBordered);
    // if ($(this).parents(headerBottom).length) {
    //   headerBottomBorderAddition();
    // }

  });

  // function headerBottomBorderAddition() {
  //   if ( $(dropdownLinkTarget).hasClass('open') ) {
  //     $(headerBottom).addClass(headerBottomBordered);
  //   } else {
  //     $(headerBottom).removeClass(headerBottomBordered);
  //   }
  // }

  // $(window).resize(function() {
  //  if ( $(dropdownLinkTarget).is(':visible') ) {
  //    $(dropdownLink).each(function(index, el) {
  //      var left = el.getBoundingClientRect().left;
  //      $(el).next(dropdownLinkTarget).css('left', left);
  //    });
  //  }
  // });

  $(window).click(function(e) {
    if ( !$(e.target).closest(dropdownLinkTarget).length ) {
      dropdownClose();
    }
  });

  $(dropdownCloseSelector).click(function(e) {
    e.preventDefault();

    dropdownClose();
  });

  function dropdownClose() {
    $(dropdownLink).removeClass('open').next(dropdownLinkTarget).removeClass('open');
    // $(headerBottom).removeClass(headerBottomBordered);
  }


  /**
   * Exchanger currencies dropdown
   */

   var exchangerCurrency = '.exchanger__currency';
   var exchangerDropdown = '.exchanger-currency-dropdown';
   var exchangerDropdownCloseBtn = '.exchanger-currency-dropdown__close';


//$(document).ready(function() {
   var exchangerDropdownTrigger = '.exchanger-currency-dropdown-trigger';

  //$(exchangerDropdownTrigger).click(function(e) {
$("body").on("click", exchangerDropdownTrigger, function(e) {
    e.preventDefault();
    e.stopPropagation();

    closeAllPopups();
    dropdownClose();

    var dropdown = $(this).parent().parent().parent().find(exchangerDropdown);
    var isDesktop = window.matchMedia('(min-width: 1200px)').matches;
    var modal = dropdown.closest('.modal');

    if (isDesktop && $(exchangerDropdown).not(dropdown).hasClass('open')) {
      closeExchangerDropdown();
    }

    if (!dropdown.hasClass('open')) {

      dropdown.addClass('open');

      if (!isDesktop) {
        bodyLocked('#' + dropdown.attr('id'));
        openOverlay(function() {
          closeExchangerDropdown();
          if (modal.length) bodyLocked('#modal-exchanger');
        }, 'overlay--mobile-menu', modal.length ? modal : null);
      }

    } else {
      closeExchangerDropdown();
    }
  });

//});

   $(exchangerDropdownCloseBtn).click(function(e) {
     e.preventDefault();
     e.stopPropagation();

    if ($(this).closest(exchangerDropdown).hasClass('exchanger-currency-dropdown--overlay')) {
      closeAllExchangerSubmenus();
    } else {
      var modal = $(this).closest('.modal');
      closeExchangerDropdown();
      if (modal.length) bodyLocked('#modal-exchanger');
    }
   });

  $(window).click(function(e) {
    var exDropdown = $(exchangerDropdown);
    var isDesktop = window.matchMedia('(min-width: 1200px)').matches;
    var isThis = !!$(e.target).closest(exchangerDropdown).length;

    if (isDesktop && exDropdown.hasClass('open') && !isThis) {
      closeExchangerDropdown();
    }
  });

  function closeExchangerDropdown() {
    $(exchangerDropdown).removeClass('open');
    closeAllExchangerSubmenus('deferred');
    bodyUnLocked();
    closeOverlay();
  }


  /**
   * Exchanger currencies dropdown submenu
   */

  var exchangerCurrSubmenuTrigger = '.exchanger-currency-dropdown__submenu-trigger';
  var exchangerCurrSubmenu = '.exchanger-currency-dropdown__submenu';

  $(exchangerCurrSubmenuTrigger).click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var isDesktop = window.matchMedia('(min-width: 1200px)').matches;

    var currentSubmenu = $(this).parent().find(exchangerCurrSubmenu);
    var currentDropdown = $(this).closest(exchangerDropdown);


    if (isDesktop) {
      var submenuLeft = ($(this).position().left + $(this).outerHeight() / 2) - (currentSubmenu.outerWidth() / 2);
      if (
        submenuLeft < 0 ||
        submenuLeft + currentSubmenu.outerWidth() > currentDropdown.outerWidth()
      ) {
        submenuLeft = 0;
      }
      currentSubmenu.css('left', submenuLeft);
    } else {
      currentDropdown.scrollTop(0);
    }

    $(this).addClass('open');
    currentSubmenu.addClass('open');
    currentDropdown.addClass('exchanger-currency-dropdown--overlay');
  });

  function closeAllExchangerSubmenus(deferred) {
    function close() {
      $(exchangerDropdown).removeClass('exchanger-currency-dropdown--overlay');
      $(exchangerCurrSubmenu).removeClass('open');
      $(exchangerCurrSubmenuTrigger).removeClass('open');
    }

    if (deferred) {
      setTimeout(function() {
        close();
      }, 300);
      return;
    }
    close();
  }

  $(exchangerDropdown).click(function(e) {
    if ( $(e.target).hasClass('exchanger-currency-dropdown--overlay') ) {
      closeAllExchangerSubmenus();
    }
  });


  // Currency selection

  var exchangerCurrButton = '.exchanger-currency-dropdown__button';
  var exchangerCurrSubmenuButton = exchangerCurrSubmenu + ' button';

  //$(exchangerCurrButton).click(function(e) {
 $("body").on("click", exchangerCurrButton, function(e) {
    e.preventDefault();
    e.stopPropagation();

    var targetCurrency = $(this).closest(exchangerCurrency).find(exchangerDropdownTrigger);

    if ( !$(e.currentTarget).hasClass(exchangerCurrSubmenuTrigger.slice(1)) ) {
      selectCurrency(e.currentTarget, targetCurrency);
    }
    var toid = $(e.currentTarget).data('toid');
    var fromid = $(e.currentTarget).data('fromid');
    if (typeof toid != 'undefined') {
		$('#from_id').val(toid);
	}
    if (typeof toid != 'undefined') {
		$('#amount_to').val(fromid);
	}
  });

  $(exchangerCurrSubmenuButton).click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var targetCurrency = $(this).closest(exchangerCurrency).find(exchangerDropdownTrigger);

    selectCurrency(e.currentTarget, targetCurrency);
  });

  function selectCurrency(el, target) {
    var icon = $(el).data('icon');
    var text = $(el).data('text');
    var hiddenInput = target.siblings('input[type="hidden"]');

    target.find('span:last-child').text(text);
    target.find('.currencies-icon').attr('class', 'currencies-icon currencies-icon--' + icon);
    hiddenInput.val(text);

    closeExchangerDropdown();

    var modal = el.closest('.modal');
    if (modal) bodyLocked('#modal-exchanger');
  }


  // min max info
  var exchangerDropdownMinMaxInfo = '.exchanger-currency-dropdown__min-max';

  $(exchangerCurrButton).mouseenter(function(e) {
    if ( !$(e.currentTarget).hasClass(exchangerCurrSubmenuTrigger.slice(1)) ) {
    	console.log($(e.currentTarget));
      var min = $(e.currentTarget).data('min');
      var max = $(e.currentTarget).data('max');
      var position = $(e.currentTarget).data('position');
      showMinMaxInfo(min, max, position, e.currentTarget);
    }
  });

  $(exchangerCurrButton).mouseleave(function(e) {
    if ( !$(e.currentTarget).hasClass(exchangerCurrSubmenuTrigger.slice(1)) ) {
      hideMinMaxInfo();
    }
  })

  $(exchangerCurrSubmenuButton).mouseenter(function(e) {
    var min = $(e.currentTarget).data('min');
    var max = $(e.currentTarget).data('max');
    var position = $(e.currentTarget).data('position');
    showMinMaxInfo(min, max, position, e.currentTarget);
  });

  $(exchangerCurrSubmenuButton).mouseleave(function(e) {
    hideMinMaxInfo();
  });

  function showMinMaxInfo(min, max, position, target) {
    if (min && max) {
      var minTarget = $(target).closest(exchangerDropdown).find(exchangerDropdownMinMaxInfo).find('div').eq(0).find('span');
      var maxTarget = $(target).closest(exchangerDropdown).find(exchangerDropdownMinMaxInfo).find('div').eq(1).find('span');

      $(exchangerDropdownMinMaxInfo).removeClass('top bottom').addClass('open');
      if (position) $(exchangerDropdownMinMaxInfo).addClass(position);

      if (min) minTarget.text(min).parent().show();
      else minTarget.parent().hide();

      if (max) maxTarget.text(max).parent().show();
      else maxTarget.parent().hide();
    }
  }

  function hideMinMaxInfo() {
    $(exchangerDropdownMinMaxInfo).removeClass('open');
  }


  /**
   * IMask
   * https://imask.js.org/
   */

  $('.imask-number').each(function(i, el) {
    new IMask(this, {
      mask: Number,
      scale: 2, // digits after point, 0 for integers
      signed: false, // disallow negative
      radix: '.',  // fractional delimiter
      mapToRadix: [','],  // symbols to process as radix
      thousandsSeparator: ' ',  // any single char
      padFractionalZeros: true,
    });
  });
 
$(document).on('keyup', '#amount_from', function (e) {
	e.preventDefault();
    
    var ths = $(this);
    var course_to = $('#amount_to').data('amount');
    var value = $(this).val();
    
    if (value.split('.').length > 2) {
        var arr = $(this).val().split('.');
        value = arr[0] + '.' + arr[1];
        $(this).val(value);
    }

    fixedFiat($(this), value);

    if ($(this).data('amount') === 0) {
        $(this).val(0);
        $('#amount_to').val(0);
    } else {
        var result = value * course_to;
        fixedFiat($('#amount_to'), result.toString())
    }
});

function fixedFiat(e, value) {
    var makeFixed = value.toString().split('.');

    if (makeFixed.length === 2 && makeFixed[1] !== '' && typeof makeFixed[1] !== "undefined" &&  parseFloat(makeFixed[1]) > 0) {
        value = Number(value);
        makeFixed = makeFixed[1].length;
    } else {
        makeFixed = 0;
    }
makeFixed >= 2 ? e.val(value.toFixed(2)) : e.val(value);

}

$(document).ready(function() {
	
	$("body").on("click", "#button_swap", function(e) {
		
		var token = $("#token").val();
		var data = {
			reverce: true,
			from_id: $("#from_id").val(),
			to_id: $("#to_id").val(),
			formData: $('form').serializeArray()
		}
		
		$.ajax({
			url: '/index.php?option=com_exchanger&task=exchange.exchangeReverce&token='+token,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function(res) {
				$('#form_exchanger').html(res.html);
				
			}
		});
	});
	
});

  
  /**
   * Mobile menu interactions
   */

  var mobileMenu = '.mobile-menu',
      mobileMenuButton = '.header-bottom__mobile-menu-button',
      mobileMenuCloseButton = '.mobile-menu__close';

  $(mobileMenuButton).click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $(mobileMenu).addClass('open');
    bodyLocked(mobileMenu);
    openOverlay(function () {
      closeMobileMenu();
    }, 'overlay--mobile-menu');
  });

  $(mobileMenuCloseButton).click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    closeMobileMenu();
  });

  function closeMobileMenu() {
    $(mobileMenu).removeClass('open');
    bodyUnLocked();
    closeOverlay();
  }

  // Mobile menu submenu

  var mobileMenuSubMenuLink = '.mobile-menu__list-link--submenu',
      mobileMenuSubMenuLinkTarget = '.mobile-menu__submenu-list';

  $(mobileMenuSubMenuLink).click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $(this).toggleClass('open').next(mobileMenuSubMenuLinkTarget).toggleClass('open');
  });

  /**
   * Modals interactions
   */

  var modal = '.modal',
      modalContent = '.modal__content',
      modalNotClickableClass = 'modal--not-clickable',
      modalOpener = '[data-modal]',
      modalCloseButton = '.modal__close',
      // modalAniSpeed = 50,
      modalCloseSelector = '[data-modal-close], .currencies-list__currency',
      modalToTopMobileSelector = 'modal--mobile-to-top';

  $(modalOpener).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var targetModal = $(this).data('modal');

    openModal(targetModal);
  });

  $(modal).on('mousedown', function(e) {
  // $(modal).click(function(e) {
    var targetModal = '#' + $(this).attr('id');
    if (e.target === this && !$(e.target).hasClass(modalNotClickableClass) ) {
      closeModal(targetModal);
    }
  });

  $(modalCloseSelector).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var targetModal = '#' + $(this).parents(modal).attr('id');

    closeModal(targetModal);
  });

  $(modalCloseButton).click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var targetModal = '#' + $(this).parents(modal).attr('id');

    closeModal(targetModal);
  });

  // For iOS inputs
  $(modal).find('input').on('blur', function() {
    if ($(this).parents(modal).hasClass(modalToTopMobileSelector) && window.matchMedia("(max-width: 1199px)").matches) {
      $(window).scrollTop(0);
    }
  });

  function openModal(targetModal) {

    closeMobileMenu(); // close mobile menu
    closeExchangerDropdown();

    if ( $(modal).is(':visible') ) {

      hideAllModals();

      if ( $(targetModal).is(':hidden') ) {
        // open modal and set "open" class to target element
        $(targetModal).show();
        setOpenClass();
        setToTopMobile();
        bodyLocked(targetModal);
      } else {
        // hide this modal and remove this "open" class
        $(targetModal).hide();
        $(modalOpener).removeClass('open');
        bodyUnLocked();
      }

    } else {

      hideAllModals();

      // open modal and set "open" class to target element
      $(targetModal).delay(10).fadeIn(0);
      setOpenClass();
      setToTopMobile();
      bodyLocked(targetModal);

    }

    function setToTopMobile() {
      if ($(targetModal).hasClass(modalToTopMobileSelector)) {
        if ( window.matchMedia("(max-width: 1199px)").matches ) {
          $(window).scrollTop(0);
        }
      }
    }

    function hideAllModals() {
      // hide all modals and remove all "open" classes
      if ($(targetModal).data('modal-second') === undefined) {
        $(modal).not(targetModal).hide();
        $(modalOpener).removeClass('open');
      }
    }

    function setOpenClass () {
      $(modalOpener).each(function(index, el) {
        if ( $(this).data('modal') === targetModal ) {
          $(this).addClass('open');
        }
      });
    }

  }

  function closeModal(targetModal) {
    // если вдруг нужно будет удалить этот класс только у определенного элемента
    // пройдись each'ом по всем элементам и сравни с радительской модалкой e.target'а
    // console.log(targetModal);
    if (targetModal !== undefined) {
      if ($(targetModal).data('modal-second') !== undefined) {
        $(targetModal).hide();
      } else {
        closeAll();
      }
    } else {
      closeAll();
    }

    function closeAll() {
      $(modalOpener).removeClass('open');
      $(modal).hide();
      bodyUnLocked();
    }
  }

  /**
   * View pass button
   */

  $('.form-group__password-show').click(function() {
    if ($(this).siblings('input').attr('type') == 'password') {
      $(this).siblings('input').attr('type', 'text');
      // $(this).siblings('input').attr('placeholder', 'password');
    } else {
      $(this).siblings('input').attr('type', 'password');
      // $(this).siblings('input').attr('placeholder', '••••••••');
    }
  });

  /**
   * Password validation
   */

  var passwordGroup = $('.form-password-group'),
      passwordCustomValidationMessage = 'Password not matching';

  passwordGroup.each(function(index, el) {
    $( $(el).find('.form-password-group__input')[0] ).on('keyup', function() {
      var passwordInput1 = $(this);
      var passwordInput2 = $( $(this).parent().parent().find('.form-password-group__input')[1] );

      if ( passwordInput1.val() === passwordInput2.val() ) {
        passwordInput2[0].setCustomValidity('');
      }
    });
  });

  passwordGroup.each(function(index, el) {
    $( $(el).find('.form-password-group__input')[1] ).on('keyup', function() {
      var passwordInput1 = $( $(this).parent().parent().find('.form-password-group__input')[0] );
      var passwordInput2 = $(this);

      if ( passwordInput1.val() !== passwordInput2.val() ) {
        this.setCustomValidity(passwordCustomValidationMessage);
      } else {
        this.setCustomValidity('');
      }
    });
  });


  /**
   * Fixed menu
   */

  var menuToFix = $('.header-bottom'),
      menuToFixFixedClass = 'fixed',
      menuToFixFixedSmallClass = 'fixed--small',
      indentationTarget1 = $('.header-top'),
      indentationTarget1Height = indentationTarget1.outerHeight(),
      indentationTarget1CssProp = 'margin-bottom',
      indentationTarget2 = $('body'),
      indentationTarget2CssProp = 'padding-top';

  $(window).scroll(function() {
    fixMenu();
  });

  $(window).resize(function() {
    // Clear first after resize
    indentationTarget1.css(indentationTarget1CssProp, 0);
    indentationTarget2.css(indentationTarget2CssProp, 0);

    fixMenu();
  });

  function fixMenu() {
    var menuToFixHeight = menuToFix.outerHeight();

    if (window.matchMedia('(min-width: 1200px)').matches) {

      if ($(this).scrollTop() > 161) {
        fix();
        indentationTarget2.css(indentationTarget2CssProp, 161 /*menuToFixHeight*/);
      } else {
        unfix();
        indentationTarget2.css(indentationTarget2CssProp, 0);
      }

      if ($(this).scrollTop() > menuToFixHeight) {
        setSmaller();
      } else {
        setBigger();
      }

    } else {

      if ($(this).scrollTop() > indentationTarget1Height) {
        fix();
        indentationTarget1.css(indentationTarget1CssProp, menuToFixHeight);
      } else {
        unfix();
        indentationTarget1.css(indentationTarget1CssProp, 0);
      }

      if ($(this).scrollTop() > indentationTarget1Height + menuToFixHeight) {
        setSmaller();
      } else {
        setBigger();
      }

    }
  }

  function fix() {
    menuToFix.addClass(menuToFixFixedClass);
  }

  function unfix() {
    menuToFix.removeClass(menuToFixFixedClass);
  }

  function setSmaller() {
    menuToFix.addClass(menuToFixFixedSmallClass);
  }

  function setBigger() {
    menuToFix.removeClass(menuToFixFixedSmallClass);
  }


  /**
   * Info popup
   */

  $('.info-btn').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();

    popupOpen(e.target);
    closeExchangerDropdown();
  });

  function popupOpen(elem) {
    var target = $('#' + $(elem).data('popup-target'));

    if (target) {
      var pos = elem.getBoundingClientRect(),
          posTop = $(elem).data('popup-top') ? $(elem).data('popup-top') : 0,
          posLeft = $(elem).data('popup-left') ? $(elem).data('popup-left') : 0,
          scrollTop = window.pageYOffset || document.documentElement.scrollTop,
          scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
          newPosTop = (pos.top + scrollTop) + posTop,
          newPosLeft = (pos.left + scrollLeft) + posLeft,
          windowTop = $(window).scrollTop(),
          maxY = windowTop + window.innerHeight,
          maxX = window.innerWidth - 20, // minus scroll width (approximately)
          popupWidth = target.outerWidth(),
          popupHeight = target.outerHeight(),
          dataModal = $(elem).data('modal') ? $(elem).data('modal') : 0;

      closeAllPopups(); // Before close all popups
      if (typeof $(elem).data('modal') !== 'undefined') {
        target.appendTo($(elem).parents('.modal'));
      } else {
        target.appendTo('body'); // for ios fixed position z-index issue && absolute positioning from body
      }

      if ( (newPosTop + popupHeight) > maxY ) {newPosTop = maxY - popupHeight;}
      if ( newPosTop < windowTop ) {newPosTop = windowTop;}
      if ( (newPosLeft + popupWidth) > maxX ) {newPosLeft = maxX - popupWidth;}
      if ( newPosLeft < 0 ) {newPosLeft = 0;}

      target.css({
        top: newPosTop,
        left: newPosLeft
      });
      target.show();
    }
  }

  $(window).bind('click touchend', function(e){
    if (!$(e.target).closest('.info-popup').length && !$(e.target).closest('.info-btn').length) {
      closeAllPopups();
    }
  });

  function closeAllPopups() {
    $('.info-popup').hide();
    $('.info-popup').css({
      top: '',
      left: ''
    });
  }

  /**
   * Chat
   */

  var chat = '.chat',
      chatMobilePlacement = '#modal-chat .modal__content-inner',
      chatDesktopPlacement = '.just-exchanged .just-exchanged__col:eq(1)',
      chatSendBtn = '.chat__send-btn',
      chatContent = '.chat__content',
      chatMessage = '.chat__send-message';

  // function chatRelocation() {
  //   if (window.matchMedia('(min-width: 1200px)').matches) {
  //     $(chat).appendTo(chatDesktopPlacement);
  //   } else {
  //     // Check that chat is moved once, otherwise android chrome keyboard will not open
  //     if (!$(chat).parents('.modal').length) {
  //       $(chat).appendTo(chatMobilePlacement);
  //     }
  //   }
  // }

  // $(window).resize(function() {
  //   chatRelocation();
  // });

  // chatRelocation();

  // Send message

  $(chatSendBtn).click(function(e) {
    e.preventDefault();
    var message = $(chatMessage).val(),
        checkMessage = message.replace(/\s/g, ''); // remove spaces

    if (checkMessage.length != 0) {
      addNewChatMessage('n', '#393559', 'Neo', '11:01', $(chatMessage).val());
      $(chatContent).scrollTop($(chatContent).prop('scrollHeight'));
      $(chatMessage).val('');
    }
  });

  function addNewChatMessage(abbr, color, name, date, message) {
    $(chatContent).append(
      '<div class="chat__message">' +
        '<div class="chat__message-icon" data-chat-abbr="' + abbr + '" style="background-color: ' + color + ';"></div>' +
        '<div class="chat__message-inner">' +
          '<div class="chat__message-header">' +
            '<div class="chat__message-name">' + name + '</div>' +
            '<div class="chat__message-date">' + date + '</div>' +
          '</div>' +
          '<div class="chat__message-text">' + message + '</div>' +
        '</div>' +
      '</div>'
      );
  }

  /**
   * Just exchanged popup window
   */

  function addNewJustExchangedPopup(curr1, curr2) {
    if (window.matchMedia('(max-width: 1199px)').matches) {
      var html =
      '<div class="just-exchanged-popup">' +
        '<button class="close-button just-exchanged-popup__close"></button>' +
        '<div class="just-exchanged-popup__text">Just exchanged</div>' +
        '<div class="just-exchanged-popup__currencies">' +
          '<span class="just-exchanged-popup__currency-1">' + curr1 + '</span>' +
          '<span class="just-exchanged-popup__currency-2">' + curr2 + '</span>' +
        '</div>' +
      '</div>';

      $('body').append(html);

      $('.just-exchanged-popup__close').click(function(e) {
        e.preventDefault();

        $(this).parents('.just-exchanged-popup').remove();
      });
    }
  }

  addNewJustExchangedPopup('4 000 DASH', '40 000 RUR');

  /**
   * Step section interactions
   */

  var stepDetailsSelector = '.step__details',
      stepSeeDetailsLink = '.step__see-details',
      stepDetailsCloseBtn = $('.step__details-close');

  $(stepSeeDetailsLink).click(function(e) {
    e.preventDefault();

    $(stepDetailsSelector).show();
  });

  stepDetailsCloseBtn.click(function(e) {
    e.preventDefault();

    stepDetailsClose();
  });

  $(window).bind('click touchend', function(e){
    if (!$(e.target).closest(stepDetailsSelector).length && !$(e.target).closest(stepSeeDetailsLink).length) {
      stepDetailsClose();
    }
  });

  function stepDetailsClose() {
    $(stepDetailsSelector).hide();
  }

  /**
   * Tabs section interactions
   */

  var tabsSectionTabBtn = $('.tabs-section__tab-btn'),
      tabsSectionTabSelector = '.tabs-section__tab';

  tabsSectionTabBtn.click(function(e) {
    e.preventDefault();
    var index = $(this).index();

    tabsSectionTabBtn.removeClass('active');

    if (!$(tabsSectionTabSelector).eq(index).is(':visible')) {
      $(tabsSectionTabSelector).css('display', 'none');
      $(tabsSectionTabSelector).eq(index).css('display', 'block');
      $(this).addClass('active');
    }

  });

  /**
   * To top button
   */

  checkToTopButton();

  $(window).on('scroll', function() {
    checkToTopButton();
  });

  function checkToTopButton() {
    var wHeight = window.innerHeight;

    if ($(window).scrollTop() > wHeight * 2) {
      $('.to-top-button').addClass('fixed');
    } else {
      $('.to-top-button').removeClass('fixed');
    }
  }

  $('.to-top-button').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $('html, body').animate({
      scrollTop: 0
    }, 400);
  });


  /**
   * Anchor jump offset
   */

  $('.anchor-jump-offset').click(function(e) {
    setTimeout(function() {
      window.scrollTo(window.scrollX, window.scrollY - 300);
    }, 0);
  });


  /**
   * Account attachments
   */

  $('.account-attachment input').change(function(e) {
    if ($(this).val() != '') {
      $(this).parent().parent().siblings('.account-attachment-info').addClass('active').text('uploaded');
    } else {
      $(this).parent().parent().siblings('.account-attachment-info').removeClass('active').text('no upload');
    }
  });


  /**
   * Attachments link
   */

  $('.attachment-link input').change(function(e) {
    if ($(this).val() != '') {
      $(this).parent().find('label').text('Uploaded');
    } else {
      $(this).parent().find('label').text('Upload attachment');
    }
  });


  /**
   * Accont wallets add modal
   */

  $('.wallets-list').find('input').change(function(e) {
    var iconClassSuffix = $(this).next().data('icon');
    var text = $(this).next().data('text');
    var targetModal = $('#modal-account-wallets-create');
    var targetHTML = targetModal.find('.modal__currency');
    var hiddenInput = targetModal.find('input[type="hidden"]');

    targetHTML
      .empty()
      .text(text)
      .prepend('<div class="currencies-icon currencies-icon--' + iconClassSuffix + '"></div>');

    hiddenInput.val(iconClassSuffix);
  });


  /* Chat close */

  $('.chat-floating-btn button').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $(this).parent().hide();
  });


  /**
   * On resize
   */

  // $(window).resize(function() {
  //  if (window.matchMedia('(min-width: 1200px)').matches) {}
  // });

  /**
   * On start
   */

});