<?php
defined('_JEXEC') or die();
$this->_generator = '';
unset($this->_links[array_search(array('relation' => 'canonical', 'relType' => 'rel', 'attribs' => array()), $this->_links )]);
$this->setHtml5(true);
/* Favicons */
$this->addFavicon('/templates/' . $this->template . '/img/favicon/favicon.ico', 'image/x-icon', 'shortcut icon');
$this->addHeadLink('/templates/' . $this->template . '/img/favicon/apple-touch-icon.png', 'apple-touch-icon');
$this->addHeadLink('/templates/' . $this->template . '/img/favicon/apple-touch-icon-72x72.png', 'apple-touch-icon', 'rel', array('sizes' => '72x72'));
$this->addHeadLink('/templates/' . $this->template . '/img/favicon/apple-touch-icon-114x114.png', 'apple-touch-icon', 'rel', array('sizes' => '114x114'));
/* Stylesheets */
$this->addStyleSheet('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap&subset=cyrillic');
$this->addStyleSheet('/templates/' . $this->template . '/libs/bootstrap/css/bootstrap-reboot.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/bootstrap/css/bootstrap-grid.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/jquery-ui/jquery-ui.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/jquery-ui/jquery-ui.structure.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/jquery.selectBoxIt.js/jquery.selectBoxIt.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/fontawesome/css/fontawesome.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/libs/fontawesome/css/brands.min.css');
$this->addStyleSheet('/templates/' . $this->template . '/css/style.css');
/* Scripts */
$this->addScript('/templates/' . $this->template . '/libs/jquery/dist/jquery.min.js' );
$this->addScript('/templates/' . $this->template . '/libs/jquery-ui/jquery-ui.min.js' );
$this->addScript('/templates/' . $this->template . '/libs/jquery.selectBoxIt.js/jquery.selectBoxIt.min.js' );
$this->addScript('/templates/' . $this->template . '/libs/bodyScrollLock/bodyScrollLock.min.js' );
//$this->addScript('/templates/' . $this->template . '/libs/imask/imask.min.js' );
$this->addScript('/templates/' . $this->template . '/libs/DTM/dtm.min.js' );
$this->addScript('/templates/' . $this->template . '/js/common.js' );
$app		 = JFactory::getApplication();
$config		 = JFactory::getConfig();
$language	 = EXSystem::getDefaultLanguage();
$user		 = JFactory::getUser();

$chatParams = JComponentHelper::getParams('com_exchat');
if ($this->countModules('exchanged') || $this->countModules('chat')) {
	$this->addScript('/plugins/system/exsocket/assets/socket.js');
	
	$script = 'jQuery(document).ready(function(){cometApi.start({dev_id:"'.$chatParams->get('dev_id').'"';
	if ($user->guest == 0) {
		$userHash = md5($user->username.$user->id);
		$script .= ', user_id:'.$user->id.', user_key:"'.$userHash.'"';
	}
	$script .= '});});';
	$this->addScriptDeclaration($script);
}

$logoutMenu	 = $app->getMenu()->getItems('link', 'index.php?option=com_users&view=login', true);
$logoutMenuParams = $logoutMenu->params;
\JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
$loginModel = JModelLegacy::getInstance('Login', 'UsersModel');
$loginForm = $loginModel->getForm();

$layout		 = $app->input->get('layout', '');
$logoutClass = ($layout == '') ? ' main--exchange' : '';
$view		 = $app->input->get('view', '');
//EXSystem::isMob();
?>

<!DOCTYPE html>
<html lang="<?= $this->language; ?>" dir="<?= $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<script>var G_USER = <?= $user->id; ?>;</script>
	<jdoc:include type="head" />
</head>

<body>
    <div class="wrapper">
      <!--Header-->
      <header class="header">
        <div class="header-top">
          <form id="mobile_user_form" action="<?= JRoute::_('index.php', true); ?>" method="post">
          <ul class="header-top__list">
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--exchange" href="javascript:void(0);"></a></li>
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--lang header-top__lang--<?= $language->image; ?>" href="#" data-modal="#modal-language"></a></li>
            <?php if ($user->id) : ?>
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--account" href="#" data-modal="#modal-account"></a></li>
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--log-out" href="#" onclick="document.getElementById('mobile_user_form').submit();"></a></li>
            <?php else : ?>
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--sign-in" href="#" data-modal="#modal-login"></a></li>
            <li class="header-top__list-item"><a class="header-top__list-link header-top__list-link--registration" href="#" data-modal="#modal-register"></a></li>
            <?php endif; ?>
          </ul>
            <input type="hidden" name="option" value="com_users" />
            <input type="hidden" name="task" value="user.logout" />
            <?php if ($logoutMenuParams->get('logout_redirect_url')) : ?>
            <input type="hidden" name="return" value="<?php echo base64_encode($logoutMenuParams->get('logout_redirect_url', $loginForm->getValue('return'))); ?>" />
            <?php else : ?>
            <input type="hidden" name="return" value="<?php echo base64_encode($logoutMenuParams->get('logout_redirect_menuitem', $loginForm->getValue('return'))); ?>" />
            <?php endif; ?>
			<?= JHtml::_('form.token'); ?>
			</form>
        </div>
        <div class="header-bottom">
          <div class="header-bottom__top">
            <div class="header-menu">
            	<jdoc:include type="modules" name="mainmenu" />
            </div>
          </div>
          <div class="header-bottom__bottom"><a class="logo" href="/"><img src="images/logo.png" alt="<?= htmlspecialchars($config->get('sitename')); ?>"></a>
            <button class="header-bottom__mobile-menu-button"><span></span></button>
            <div class="header-user-panel">
            	<jdoc:include type="modules" name="user_panel" />
            	<?php if ($user->guest) : ?>
	            	<button class="btn header-user-panel__btn" data-modal="#modal-login"><?= JText::_('JLOGIN') ?></button>
	            	<button class="btn btn--accent header-user-panel__btn" data-modal="#modal-register"><?= JText::_('JREGISTER') ?></button>
            	<?php else : ?>
	            	<div class="header-user-panel__account-wrapper"><a class="header-user-panel__account dropdown-link" href="#"><?= JText::_('TPL_EXCHANGER_YOU_ACCOUNT') ?></a>
		                <div class="dropdown dropdown--tight">
            				<jdoc:include type="modules" name="user" />
		                </div>
	            	</div>
	            	<form action="<?= JRoute::_('index.php', true); ?>" method="post">
	            		<button class="btn header-user-panel__btn"><?= JText::_('JLOGOUT'); ?></button>
	            		<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="user.logout" />
						<?php if ($logoutMenuParams->get('logout_redirect_url')) : ?>
							<input type="hidden" name="return" value="<?php echo base64_encode($logoutMenuParams->get('logout_redirect_url', $loginForm->getValue('return'))); ?>" />
						<?php else : ?>
							<input type="hidden" name="return" value="<?php echo base64_encode($logoutMenuParams->get('logout_redirect_menuitem', $loginForm->getValue('return'))); ?>" />
						<?php endif; ?>
						<?= JHtml::_('form.token'); ?>
					</form>
            	<?php endif; ?>
            </div>
          </div>
        </div>
      </header>
      <!--Mobile menu-->
      <div class="mobile-menu">
        <button class="close-button mobile-menu__close"><span></span></button>
        <jdoc:include type="modules" name="mainmenu_mobile" />
      </div>
      
      <!--Main content-->
      <main class="main<?= $logoutClass; ?>">
        	<jdoc:include type="message" />
			<jdoc:include type="component" />
      </main>
      
      <!--Footer-->
      <footer class="footer">
        <div class="container footer__inner">
          <div class="footer__logo-wrapper"><a class="logo footer__logo" href="<?= JUri::base(); ?>"><img src="images/logo-white.png" alt="<?= htmlspecialchars($config->get('sitename')); ?>"></a>
            <div class="footer__email"><a class="footer__email-link" href="mailto:<?php echo htmlspecialchars($config->get('mailfrom')); ?>"><?php echo htmlspecialchars($config->get('mailfrom')); ?></a></div>
          </div>
          <div class="footer__content">
            <?php if ($this->countModules('footer_menu')) : ?>
	            <div class="footer__lists">
	              <jdoc:include type="modules" name="footer_menu" />
	            </div>
            <?php endif; ?>
        	
        	<div class="footer__social"><a class="social-link social-link--fb footer__social-link" href="#"><i class="fab fa-facebook-f"></i></a><a class="social-link social-link--vk footer__social-link" href="#"><i class="fab fa-vk"></i></a><a class="social-link social-link--tw footer__social-link" href="#"><i class="fab fa-twitter"></i></a><a class="social-link social-link--ins footer__social-link" href="#"><i class="fab fa-instagram"></i></a></div>
	        
	        <div class="footer__twitter"><a class="twitter-timeline" data-width="300" data-height="300" data-dnt="true" data-theme="light" href="https://twitter.com/TwitterMusic/timelines/393773266801659904?ref_src=twsrc%5Etfw">Music Superstars - Curated tweets by TwitterMusic</a>
	              <script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	          </div>
          </div>
            
          <jdoc:include type="modules" name="footer" />
          
          <div class="footer__copy">copyright &copy; 2014-<?php echo date('Y'); ?> <?php echo JText::_('TPL_EXCHANGER_COPYRIGHT'); ?></div>
        	
        </div>
      </footer>
      
      <button class="to-top-button"></button>
      
      <!-- Modals-->
      <div class="modal modal--mobile-header modal--tablet-50perc modal--desktop-hidden modal--mobile-to-top" id="modal-account">
        <div class="modal__content">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
            <jdoc:include type="modules" name="mobile_user_menu" />
          </div>
        </div>
      </div>
      <div class="modal modal--mobile-full-height modal--mobile-fixed-width" id="modal-need-more">
        <div class="modal__content">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
            <jdoc:include type="modules" name="modal_contact" />
          </div>
        </div>
      </div>
      <?php if ($this->countModules('mobile_language')) : ?>
      <div class="modal modal--mobile-header modal--desktop-hidden modal--mobile-to-top" id="modal-language">
        <div class="modal__content">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
          	<jdoc:include type="modules" name="mobile_language" />
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('login')) : ?>
      <div class="modal modal--mobile-header modal--mobile-to-top" id="modal-login">
        <div class="modal__content modal__content--form">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
          	<jdoc:include type="modules" name="login" />
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('register')) : ?>
      <div class="modal modal--mobile-header modal--mobile-to-top" id="modal-register">
        <div class="modal__content">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
          	<?php require_once dirname(__FILE__).'/register.php'; ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('reset_password')) : ?>
      <div class="modal modal--mobile-header modal--mobile-to-top" id="modal-reset-password">
        <div class="modal__content modal__content--form">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
          	<jdoc:include type="modules" name="reset_password" />
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('chat_mobile')) : ?>
      <?php //if ($user->id) : ?>
      
      <div class="modal modal--not-clickable modal--theme-chat modal--desktop-hidden" id="modal-chat">
        <div class="modal__content modal__content--form">
          <button class="close-button modal__close"></button>
          <div class="modal__content-inner">
          	<jdoc:include type="modules" name="chat_mobile" />
          </div>
        </div>
      </div>
      <?php //endif; ?>
      <?php endif; ?>
    </div>
</body>
</html>