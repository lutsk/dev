<?php
defined('_JEXEC') or die();

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;

$plugin = JPluginHelper::getPlugin('user', 'agreement');
$agreementParams = new JRegistry($plugin->params);
$agreementArticle = $agreementParams->get('agreement_article', 0);
if ($agreementArticle) {
	JLoader::register('ContentHelperRoute', JPATH_BASE . '/components/com_content/helpers/route.php');
	
	$attribs = array("target"=>"_blank");
	
	$db    = Factory::getDbo();
	$query = $db->getQuery(true)
		->select($db->quoteName(array('id', 'alias', 'catid', 'language')))
		->from($db->quoteName('#__content'))
		->where($db->quoteName('id') . ' = ' . (int) $agreementArticle);
	$db->setQuery($query);
	$article = $db->loadObject();
	
	if (JLanguageAssociations::isEnabled())
	{
		$agreementAssociated = JLanguageAssociations::getAssociations('com_content', '#__content', 'com_content.item', $agreementArticle);
	}
	
	$currentLang = JFactory::getLanguage()->getTag();
	
	if (isset($agreementAssociated) && $currentLang !== $article->language && array_key_exists($currentLang, $agreementAssociated))
	{
		$url = ContentHelperRoute::getArticleRoute(
			$agreementAssociated[$currentLang]->id,
			$agreementAssociated[$currentLang]->catid,
			$agreementAssociated[$currentLang]->language
		);
		
		$link = JHtml::_('link', JRoute::_($url), JText::_('PLG_AGREEMENT_FIELD_LABEL'), $attribs);
	}
	else
	{
		$slug = $article->alias ? ($article->id . ':' . $article->alias) : $article->id;
		$url  = ContentHelperRoute::getArticleRoute($slug, $article->catid, $article->language);
		$link = JHtml::_('link', JRoute::_($url), JText::_('PLG_AGREEMENT_FIELD_LABEL'), $attribs);
	}
}
?>
<form id="modal-registration" class="form" action="<?= JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" enctype="multipart/form-data">
	<div class="form__title"><?= JText::_('JREGISTER'); ?></div>
	<fieldset class="">
		<div class="form-group">
			<label id="jform_name-lbl-modal" for="jform_name" class="required"><?= JText::_('COM_USERS_REGISTER_NAME_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<input type="text" name="jform[name]" id="jform_name-modal" value="" class="form-group__input required" size="30" required="" aria-required="true" />
		</div>

		<div class="form-group">
			<label id="jform_username-lbl-modal" for="jform_username" class="required"><?= JText::_('COM_USERS_REGISTER_USERNAME_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<input type="text" name="jform[username]" id="jform_username-modal" value="" class="form-group__input validate-username required" size="30" required="" aria-required="true" />
		</div>

		<div class="form-group">
			<label id="jform_password1-lbl-modal" for="jform_password1" class="required"><?= JText::_('COM_USERS_PROFILE_PASSWORD1_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<button class="form-group__password-show" type="button"></button>
			<input class="form-group__input form-password-group__input" type="password" name="jform[password1]" id="jform_password1-modal" value="" autocomplete="off" size="30" maxlength="99" required="" aria-required="true" title="<?= JText::_('COM_USERS_DESIRED_PASSWORD'); ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" />
		</div>

		<div class="form-group">
			<label id="jform_password2-lbl-modal" for="jform_password2" class="required"><?= JText::_('COM_USERS_PROFILE_PASSWORD2_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<button class="form-group__password-show" type="button"></button>
			<input class="form-group__input form-password-group__input" type="password" name="jform[password2]" id="jform_password2-modal" value="" autocomplete="off" size="30" maxlength="99" required="" aria-required="true" title="<?= JText::_('COM_USERS_PROFILE_PASSWORD2_DESC'); ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" />
		</div>

		<div class="form-group">
			<label id="jform_email1-lbl-modal" for="jform_email1" class="required"><?= JText::_('COM_USERS_REGISTER_EMAIL1_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<input type="email" name="jform[email1]" class="form-group__input validate-email required" id="jform_email1-modal" value="" size="30" autocomplete="email" required="" aria-required="true" />
		</div>

		<div class="form-group">
			<label id="jform_email2-lbl-modal" for="jform_email2" class="required"><?= JText::_('COM_USERS_REGISTER_EMAIL2_LABEL'); ?><span class="star">&nbsp;*</span></label>
			<input type="email" name="jform[email2]" class="form-group__input validate-email required" id="jform_email2-modal" value="" size="30" required="" aria-required="true" />
		</div>

	</fieldset>
	<fieldset class="checkbox_fieldset">
		<div class="form-checkbox-group modal__form-checkbox-group">
			<input type="checkbox" name="jform[agreement][agree]" id="jform_agreement_agree-modal" value="1" class="form-checkbox-group__input_modal" />
			<label id="jform_agreement_agree-lbl-modal" for="jform_agreement_agree-modal" class="form-checkbox-group__label_modal modal__form-checkbox-group-label required" title="<?= JText::_('PLG_AGREEMENT'); ?>" data-content="<?= JText::_('PLG_AGREEMENT_FIELD_DESC'); ?>"><?= JText::_('PLG_AGREEMENT_OPTION_AGREE'); ?> <?= $link; ?><span class="star">&nbsp;*</span></label>
		</div>

		<div class="form-checkbox-group modal__form-checkbox-group">
			<input type="checkbox" name="jform[agreement][citizen]" id="jform_agreement_citizen-modal" value="1" class="form-checkbox-group__input_modal" />
			<label id="jform_agreement_citizen-lbl-modal" for="jform_agreement_citizen-modal" class="form-checkbox-group__label_modal modal__form-checkbox-group-label"><?= JText::_('PLG_AGREEMENT_CITIZEN'); ?></label>
		</div>

	</fieldset>
					
	<div class="form-button-group">
		<button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JREGISTER'); ?></button>
		<button class="btn form-button-group__btn" type="button" data-modal="#modal-login"><?= JText::_('JLOGIN') ?></button>
	</div>
	
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="registration.register" />
	<input type="hidden" name="93f7e1ecca1f159d267acf7c05f79968" value="1">
		
</form>