<?php
defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;

class plgSystemExhistory extends JPlugin
{
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function onAfterInitialise()
	{
		$date = new Date('now -1 month');
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		try
		{
			
			$query->select($db->qn('id'))->from($db->qn('#__abc_orders'))->where($db->qn('created_time') . ' < ' . $db->q($date->toSQL()));
			$db->setQuery($query);
			$column = $db->loadColumn();
			
			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_exchanger/tables');
			$table = JTable::getInstance('order', 'Table', array());
			
			foreach($column as $pk)
			{
				$table->reset();
				$table->delete($pk);
			}
		}
		catch (Exception $e)
		{
			
		}
		
		try
		{
			
			$query->clear()->select($db->qn('id'))->from($db->qn('#__js_ticket_tickets'))->where($db->qn('created') . ' < ' . $db->q($date->toSQL()));
			$db->setQuery($query);
			$column = $db->loadColumn();
			
			JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_jssupportticket/tables');
			$table = JTable::getInstance('tickets', 'Table', array());
			
			foreach($column as $pk)
			{
				$table->reset();
				$table->delete($pk);
			}
		}
		catch (Exception $e)
		{
			
		}
		
		return true;
		
	}
}
