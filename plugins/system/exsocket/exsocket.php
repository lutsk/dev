<?php
defined( '_JEXEC' ) or die;

class plgSystemExsocket extends JPlugin
{
	private $input;
	private $app;
	
	public function __construct( & $subject, $config )
	{
		parent::__construct( $subject, $config );
		$this->loadLanguage();
		
		$this->app = JFactory::getApplication();
		$this->input = $this->app->input;
	}
	
	public function onAfterInitialise(){
		
	}
	
	public function onAjaxExsocket()
    {
    	$user = JFactory::getUser();
    	$message = $this->app->input->getVar('message', '');
    	$params = JComponentHelper::getParams('com_exchat');
    	
    	$host = "app.comet-server.ru";
		$key = $params->get('dev_id');
		$password = $params->get('dev_key');
    	
        if(trim($message) != '') {
            $comet = mysqli_connect($host, $key, $password, "CometQL_v1");
			if(mysqli_errno($comet))
			{
			    echo "Error:".mysqli_error($link);
			}
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')
					->from($db->qn('#__abc_user'))
					->where($db->qn('user_id') . ' = ' . $user->id);
			$db->setQuery($query);
			$userData = $db->loadObject();
			
			$msg = array(
				"user_id" => $user->id,
				"name" => $user->name,
				"abbr" => mb_substr($user->name, 0, 2),
				"color" => $userData->color,
				"text" => $message
			);
			
			$msg = json_encode($msg);
			$msg = mysqli_real_escape_string($comet, $msg);
			
			$query = "INSERT INTO pipes_messages (name, event, message)" .
			  "VALUES('web_chat_pipe', 'exchat', '".$msg."')";
			
			mysqli_query($comet, $query);
			
			if(mysqli_errno($comet))
			{
			    echo "Error:".mysqli_error($comet);
			} 
			else
			{
			    echo "ok";
			}
        } else {
            echo "OOPS";
        }
        
        $this->app->close(); 
    }
}
