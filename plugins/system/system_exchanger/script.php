<?php
defined('_JEXEC') or die;

class plgSystemSystem_ExchangerInstallerScript
{

	function postflight($type, $parent)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->update('#__extensions')->set('enabled = 1')->where('type = ' . $db->q('plugin'))->where('element = ' . $db->q('system_exchanger'));
		$db->setQuery($query)->execute();
	}
}