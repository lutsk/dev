<?php
defined( '_JEXEC' ) or die;

class plgSystemSystem_Exchanger extends JPlugin
{
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	public function onAfterInitialise()
	{
		JLoader::register('EXSystem', JPATH_ROOT . '/plugins/system/system_exchanger/helper.php');
		
	}

	public function onAfterRoute()
	{
		
	}

	public function onAfterDispatch()
	{
		
	}

	public function onBeforeRender()
	{
	}

	function onAfterRender()
	{
		
	}

	public function onBeforeCompileHead()
	{
		
	}
}
