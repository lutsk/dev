<?php
defined('JPATH_PLATFORM') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;

FormHelper::loadFieldClass('checkbox');

class JFormFieldagreement extends JFormFieldCheckbox
{
	protected $type = 'agreement';
	
	protected function getLabel()
	{
		$label = '';
		
		if ($this->hidden)
		{
			return $label;
		}
		
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;
		
		$this->required = true;
		
		$class = 'form-checkbox-group__label modal__form-checkbox-group-label required';
		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;
		
		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
		
		if (!empty($this->description))
		{
			$label .= ' title="' . htmlspecialchars(trim($text, ':'), ENT_COMPAT, 'UTF-8') . '"';
			$label .= ' data-content="' . htmlspecialchars(
				$this->translateDescription ? JText::_($this->description) : $this->description,
				ENT_COMPAT,
				'UTF-8'
			) . '"';
			
			if (JFactory::getLanguage()->isRtl())
			{
				$label .= ' data-placement="left"';
			}
		}
		
		$agreementArticle = $this->element['article'] > 0 ? (int) $this->element['article'] : 0;
		
		if ($agreementArticle)
		{
			JLoader::register('ContentHelperRoute', JPATH_BASE . '/components/com_content/helpers/route.php');
			
			$attribs = array("target"=>"_blank");
			
			$db    = Factory::getDbo();
			$query = $db->getQuery(true)
				->select($db->quoteName(array('id', 'alias', 'catid', 'language')))
				->from($db->quoteName('#__content'))
				->where($db->quoteName('id') . ' = ' . (int) $agreementArticle);
			$db->setQuery($query);
			$article = $db->loadObject();
			
			if (JLanguageAssociations::isEnabled())
			{
				$agreementAssociated = JLanguageAssociations::getAssociations('com_content', '#__content', 'com_content.item', $agreementArticle);
			}
			
			$currentLang = JFactory::getLanguage()->getTag();
			
			if (isset($agreementAssociated) && $currentLang !== $article->language && array_key_exists($currentLang, $agreementAssociated))
			{
				$url = ContentHelperRoute::getArticleRoute(
					$agreementAssociated[$currentLang]->id,
					$agreementAssociated[$currentLang]->catid,
					$agreementAssociated[$currentLang]->language
				);
				
				$link = JHtml::_('link', JRoute::_($url), $text, $attribs);
			}
			else
			{
				$slug = $article->alias ? ($article->id . ':' . $article->alias) : $article->id;
				$url  = ContentHelperRoute::getArticleRoute($slug, $article->catid, $article->language);
				$link = JHtml::_('link', JRoute::_($url), $text, $attribs);
			}
		}
		else
		{
			$link = $text;
		}
		
		$label .= '>' . JText::_('PLG_AGREEMENT_OPTION_AGREE') . ' ' . $link . '<span class="star">&#160;*</span></label>';
		
		return $label;
	}
}
