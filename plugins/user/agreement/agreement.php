<?php
defined( '_JEXEC' ) or die;
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\Utilities\ArrayHelper;

class plgUserAgreement extends CMSPlugin
{
	protected $autoloadLanguage = true;
	protected $app;
	protected $db;
	
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		FormHelper::addFieldPath(__DIR__ . '/field');
	}
	
	public function onContentPrepareForm($form, $data)
	{
		if (!($form instanceof JForm))
		{
			$this->_subject->setError('JERROR_NOT_A_FORM');
			return false;
		}
		
		$name = $form->getName();
		
		if (!in_array($name, array('com_users.registration')))
		{
			return true;
		}
		
		Form::addFormPath(__DIR__ . '/agreement');
		$form->loadFile('agreement');
		
		$agreementArticle = $this->params->get('agreement_article');
		
		$form->setFieldAttribute('agree', 'article', $agreementArticle, 'agreement');
	}

	public function onUserBeforeSave($user, $isnew, $data) 
	{
		if ($this->app->isClient('administrator'))
		{
			return true;
		}
		
		$userId = ArrayHelper::getValue($user, 'id', 0, 'int');
		
		if ($userId > 0)
		{
			return true;
		}
		
		$option = $this->app->input->getCmd('option');
		$task   = $this->app->input->get->getCmd('task');
		$form   = $this->app->input->post->get('jform', array(), 'array');
		
		if ($option == 'com_users' && in_array($task, array('registration.register')) && empty($form['agreement']['agree']))
		{
			throw new InvalidArgumentException(Text::_('PLG_AGREEMENT_FIELD_ERROR'));
		}
		
		return true;
	}
	
	public function onUserAfterSave($data, $isNew, $result, $error)
	{
		/*if (!$isNew || !$result)
		{
			return true;
		}
echo '<pre>'; print_r($image); echo '</pre>'; die();
*/
		$app	 = \JFactory::getApplication();
		$userId	 = ArrayHelper::getValue($data, 'id', 0, 'int');
		
		$jinput	 = $app->input;
		$advdata = $jinput->post->get('advdata', array(), 'array');
		$files	 = $jinput->files->get('advdata');
		
		
		$advdata['user_id']		 = $userId;
		$advdata['us_citizen']	 = (isset($data['agreement']['citizen'])) ? 0 : 1;
		$advdata['color']		 = sprintf( '#%02X%02X%02X', rand(60, 255), rand(60, 255), rand(60, 255) );
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn(array('id')))->from($db->qn('#__abc_user'))->where($db->qn('user_id') . ' = ' . $data['id']);
		$db->setQuery($query);
		$id = $db->loadResult();
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_exchanger/tables');
		$table = JTable::getInstance('user', 'Table', array());
		$table->load($id);
		
		try
		{
			$table->bind($advdata);
			
			if (!$table->store())
			{
				$this->setError($table->getError());
				return false;
			}
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}
		
		if(count($files) > 0)
		{
			jimport( 'joomla.filesystem.folder' );
			jimport('joomla.filesystem.file');
			$rootPath = JPATH_SITE . DS . 'images' . DS . 'users' . DS;
			$path = $rootPath . $userId . DS;
			$extensions = array('jpg','jpeg','png','gif');
			
			foreach ($files as $key => $image)
			{
				if($image['name'] != '')
				{
					$extension = JFile::getExt($image['name']);
					$oldFile = EXSystem::getUserFile($userId, $key);
					
					try {
						if (!in_array($extension, $extensions)) {
							$app->enqueueMessage(JText::sprintf('COM_EXCHANGER_MIME_ALERT',$image['name']));
							return false;
						}
						
						if (JFolder::create($rootPath)) {
							$html = "<html>\n<body bgcolor=\"#FFFFFF\">\n</body>\n</html>";
							JFile::write($rootPath."index.html", $html);
						}
						if (JFolder::create($path)) {
							$html = "<html>\n<body bgcolor=\"#FFFFFF\">\n</body>\n</html>";
							JFile::write($path."index.html", $html);
						}
						
						$image['name'] = JFile::makeSafe($image['name']);
						$mime = end(explode('.', $image['name']));
						$name = md5($image['name'] . mt_rand( 0, 9999 ) . time() ) . '.' . $mime;
						if (JFile::exists($path.$oldFile)) {
							JFile::delete($path.$oldFile);
						}
						
						if (JFile::upload($image['tmp_name'], $path.$name, false, true)) {
							//Сохраняем запись в БД
							$table->set($key, $name);
							$table->set($key, $name);
							
							if (!$table->store())
							{
								$this->setError($table->getError());
								return false;
							}
						}
					} catch ( Exception $ex ) {
						//Если не удалось сохранить то выводим сообшени об ошибке загрузки
						$app->enqueueMessage(JText::sprintf('COM_EXCHANGER_FILE_UPLOADED_ALERT',$image['name']));
						return false;
					}
				}
			}
		}
		
		JLoader::register('ActionlogsModelActionlog', JPATH_ADMINISTRATOR . '/components/com_actionlogs/models/actionlog.php');
		
		$message = array(
			'action'      => 'consent',
			'id'          => $userId,
			'title'       => $data['name'],
			'itemlink'    => 'index.php?option=com_users&task=user.edit&id=' . $userId,
			'userid'      => $userId,
			'username'    => $data['username'],
			'accountlink' => 'index.php?option=com_users&task=user.edit&id=' . $userId,
			'citizen'      => $citizen
		);
		
		$model = BaseDatabaseModel::getInstance('Actionlog', 'ActionlogsModel');
		$model->addLog(array($message), 'PLG_AGREEMENT_LOGGING_CONSENT_TO_TERMS', 'plg_user_agreement', $userId);
	}

	public function onUserAfterLogin($options = array())
	{
		$user = $options['user'];
		$params = JComponentHelper::getParams('com_exchat');
		
		$conn = new mysqli("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		
		$userHash = md5($user->username.$user->id);
		$sql = "INSERT INTO users_auth (id, hash ) VALUES (".$user->id.", '".$userHash."');";
		
		if ($conn->query($sql) !== TRUE) {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		
		$users = $this->getUsersOnline();
		$msg = array(
				"users_online" => 'Users online',
				"amount" => $users
		);
		$msg = json_encode($msg);
		
		$query = "INSERT INTO pipes_messages (name, event, message)" .
			  "VALUES('web_online', 'online', '".$msg."')";
		
		if ($conn->query($query) !== TRUE) {
			echo "Error: " . $query . "<br>" . $conn->error;
		}
		
		$conn->close();
	}

	public function onUserLogout($user, $options = array()){
		$params = JComponentHelper::getParams('com_exchat');
		
		$conn = new mysqli("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		
		$sql = "DELETE FROM users_auth WHERE id = ".$user['id'].";";
		
		if ($conn->query($sql) !== TRUE) {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		
		$users = $this->getUsersOnline();
		$msg = array(
				"users_online" => 'Users online',
				"amount" => $users - 1
		);
		$msg = json_encode($msg);
		
		$query = "INSERT INTO pipes_messages (name, event, message)" .
			  "VALUES('web_online', 'online', '".$msg."')";
		
		if ($conn->query($query) !== TRUE) {
			echo "Error: " . $query . "<br>" . $conn->error;
		}
		
		$conn->close();
	}
	
	protected function getUsersOnline()
	{
		$config = JFactory::getConfig();
		$total_users = 0;
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		if ($config->get('shared_session', '0'))
		{
			$query->select('COUNT(session_id)')
				->from('#__session')
				->where('guest = 0');
		}
		else {
			$query->select('COUNT(session_id)')
				->from('#__session')
				->where('guest = 0 AND client_id = 0');
		}
		$db->setQuery($query);
		$total_users = (int) $db->loadResult();
		
		return $total_users;
	}
}
