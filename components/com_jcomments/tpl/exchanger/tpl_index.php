<?php
defined('_JEXEC') or die;

class jtt_tpl_index extends JoomlaTuneTemplate
{
	function render() 
	{
		$user = JFactory::getUser();
		$acl = JCommentsFactory::getACL();
		
		$lang = JFactory::getLanguage();
		$lang->load('com_exchanger', JPATH_SITE);
		
		$order = JFactory::getApplication()->getUserState('com_exchanger.order');
		
		$object_id = $this->getVar('comment-object_id');
		$object_group = $this->getVar('comment-object_group');

		// comments data is prepared in tpl_list and tpl_comments templates 
		$comments = $this->getVar('comments-list', '');

		// form data is prepared in tpl_form template.
		$form = $this->getVar('comments-form');

		if ($comments != '' || $form != '' || $this->getVar('comments-anticache')) {
			// include comments css (only if we are in administor's panel)
			if ($this->getVar('comments-css', 0) == 1) {
				include_once (JCOMMENTS_HELPERS.'/system.php');
			}

			// include JComments JavaScript initialization
?>
<script>

var jcomments = new JComments(<?php echo $object_id;?>, '<?php echo $object_group; ?>','<?php echo $this->getVar('ajaxurl'); ?>');
jcomments.setList('comments-list');

</script>
<?php
			// IMPORTANT: Do not rename this div's id! Some JavaScript functions references to it!
?>
<div id="jc" class="container">
	<div class="feedback">
		<h2 class="h2"><?= JText::_('COM_EXCHANGER_FEEDBACK'); ?></h2>
		
		<div class="feedback__top">
			<div class="feedback__write"><a class="btn btn--filled feedback__write-btn anchor-jump-offset" href="#write-feedback"><?= JText::_('COM_EXCHANGER_WRITE_FEEDBACK'); ?></a>
				<?php if (!$acl->canComment()) { ?>
				<div class="feedback-login-section">
					<?= JText::sprintf('COM_EXCHANGER_REGISTER_RITE_FEEDBACK', '<a href="#" data-modal="#modal-login">'.JText::_('COM_EXCHANGER_SIGN_IN').'</a>', '<a href="#" data-modal="#modal-register">'.JText::_('COM_EXCHANGER_REGISTER').'</a>'); ?>
				</div>
				<?php } ?>
			</div>
			<div class="sort feedback__sort">
				<label><?= JText::_('COM_EXCHANGER_SORT_BY'); ?></label>
				<select class="select-sort-by" name="order" onchange="jcomments.showPage(<?php echo $object_id ?>,'<?php echo $object_group ?>',this.value);return false;">
					<option value="best"<?php if ($order == 'best') echo ' selected' ?>><?= JText::_('COM_EXCHANGER_SORT_BY_BEST'); ?></option>
					<option value="newest"<?php if ($order == 'newest') echo ' selected' ?>><?= JText::_('COM_EXCHANGER_SORT_BY_NEWEST'); ?></option>
					<option value="oldest"<?php if ($order == 'oldest') echo ' selected' ?>><?= JText::_('COM_EXCHANGER_SORT_BY_OLDEST'); ?></option>
				</select>
			</div>
		</div>
		
		<?php
			if ($this->getVar('comments-form-position', 0) == 1) {
				// Display comments form (or link to show form)
				if (isset($form)) {
					echo $form;
				}
			}
		?>
		<div id="comments" class="comments"><?php echo $comments; ?></div>
		<?php
			if ($this->getVar('comments-form-position', 0) == 0) {
				// Display comments form (or link to show form)
				if (isset($form)) {
					echo $form;
				}
			}
		?>
		
		<?php
			// Some magic like dynamic comments list loader (anticache) and auto go to anchor script
			$aca = (int) ($this->getVar('comments-gotocomment') == 1);
			$acp = (int) ($this->getVar('comments-anticache') == 1);
			$acf = (int) (($this->getVar('comments-form-link') == 1) && ($this->getVar('comments-form-locked', 0) == 0));

			if ($aca || $acp || $acf) {
		?>
		<script type="text/javascript">
			jcomments.setAntiCache(<?php echo $aca;?>,<?php echo $acp;?>,<?php echo $acf;?>);
		</script> 
		<?php
			}
	?>
	</div>
</div>
<?php
		}
	?>

<?php if (!$user->id) { ?>
<div class="want-to-exchange">
	<div class="container">
		<h2 class="h2"><?php echo JText::_('COM_EXCHANGER_WANT_BONUSES'); ?></h2>
		<div class="want-to-exchange__row">
			<div class="want-to-exchange__col want-to-exchange__col--1">
				<?php echo EXSystem::loadModules('promo_text'); ?>
			</div>
			<div class="want-to-exchange__col want-to-exchange__col--2">
				<div class="want-to-exchange__form-wrapper">
					<?php echo EXSystem::loadModules('register'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?= EXSystem::loadModules('bottom'); ?>

<?php
	}
}
?>