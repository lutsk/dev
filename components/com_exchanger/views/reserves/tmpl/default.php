<?php
defined( '_JEXEC' ) or die;

$user = JFactory::getUser();
$i = 1;
$divid = intdiv(count($this->items), 2) + 1;
?>

<section class="reserves reserves--reserves-page">
	<div class="container">
		<div class="account-title"><?= JText::_('COM_EXCHANGER_RESERVES'); ?></div>
		<div class="reserves__inner">
			<ul class="currencies-list reserves__currencies-list">
				<?php foreach ($this->items as $item) : ?>
				<li class="currencies-list__item reserves__currencies-list-item">
					<div class="currencies-list__currency currencies-icon"><img src="<?= $item->image ;?>" alt="<?= $item->name ;?>" width="40" style="margin-right:20px" /><?= $item->name ;?></div>
					<div class="currencies-list__ammount"><?= $item->amount ;?> <?= $item->code ;?></div>
				</li>
				<?php
				$i++;
				if($divid == $i) :
				?>
				</ul><ul class="currencies-list reserves__currencies-list">
				<?php
				endif;
				endforeach;
				?>
			</ul>
		</div>
		<a class="btn btn--filled reserves__see-more" href="<?= JRoute::_('index.php?option=com_exchanger&view=exchange'); ?>"><?= JText::_('COM_EXCHANGER_EXCHANGE'); ?></a>
	</div>
</section>

<?= EXSystem::loadModules('ater_content'); ?>

<?php if (!$user->id) : ?>
<div class="want-to-exchange">
	<div class="container">
		<h2 class="h2"><?= JText::_('COM_EXCHANGER_WANT_BONUSES'); ?></h2>
		<div class="want-to-exchange__row">
			<div class="want-to-exchange__col want-to-exchange__col--1">
				<?= EXSystem::loadModules('promo_text'); ?>
			</div>
			<div class="want-to-exchange__col want-to-exchange__col--2">
				<div class="want-to-exchange__form-wrapper">
					<?= EXSystem::loadModules('register'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?= EXSystem::loadModules('bottom'); ?>