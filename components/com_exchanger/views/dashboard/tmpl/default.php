<?php
defined( '_JEXEC' ) or die;
$user		 = JFactory::getUser();
$app		 = JFactory::getApplication();
$document	 = $app->getDocument();
$lang = JFactory::getLanguage();
$lang->load('com_exchanger', JPATH_SITE);
?>

<?= EXSystem::loadModules('dashboard_before'); ?>

<div class="just-exchanged" id="chat">
	<div class="container just-exchanged__row">
		<?php if ($document->countModules('exchanged')) : ?>
		<div class="just-exchanged__col">
			<?= EXSystem::loadModules('exchanged'); ?>
		</div>
		<?php endif; ?>
		<?php if ($document->countModules('chat')) : ?>
		<div class="just-exchanged__col">
			<?= EXSystem::loadModules('chat'); ?>
		</div>
		<?php endif; ?>
	</div>
</div>

<?= EXSystem::loadModules('ater_content'); ?>

<?php if (!$user->id) : ?>
<div class="want-to-exchange">
	<div class="container">
		<h2 class="h2"><?= JText::_('COM_EXCHANGER_WANT_BONUSES'); ?></h2>
		<div class="want-to-exchange__row">
			<div class="want-to-exchange__col want-to-exchange__col--1">
				<?= EXSystem::loadModules('promo_text'); ?>
			</div>
			<div class="want-to-exchange__col want-to-exchange__col--2">
				<div class="want-to-exchange__form-wrapper">
					<?= EXSystem::loadModules('register'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?= EXSystem::loadModules('bottom'); ?>