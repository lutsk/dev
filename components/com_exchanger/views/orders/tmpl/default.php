<?php
defined( '_JEXEC' ) or die;

//echo '<pre>'; print_r($this->items); echo '</pre>'; die();
?>
<script type="text/javascript">
function GoOrder(url){
	window.open(url,'_blank');
}
</script>
<div class="account">
	<div class="container account__container">
		<div class="account-content">
			<div class="account-history">
				<div class="account-title-with-search">
					<div class="account-title"><?= JText::_('COM_EXCHANGER_HISTORY_TITLE'); ?></div>
					<!--<form class="account-search">
						<input type="text" name="account-history-search">
						<button></button>
					</form>-->
				</div>
				
				<div class="account-table">
					<form id="adminForm" method="post" name="adminForm">
						<table>
						<tr>
							<th>№</th>
							<th><?= JText::_('COM_EXCHANGER_N'); ?></th>
							<th><?= ExchangerSiteHelper::sort(JText::_('COM_EXCHANGER_CREATION_DATE'), 'created_time', $this->sortDirection, $this->sortColumn); ?></th>
							<!--<th><a class="account-table__sorting" href="#">Operation</a></th>-->
							<th><?= ExchangerSiteHelper::sort(JText::_('COM_EXCHANGER_EXCHANGE_AMOUNT'), 'amount_from', $this->sortDirection, $this->sortColumn); ?></th>
							<th><?= ExchangerSiteHelper::sort(JText::_('COM_EXCHANGER_AMOUNT_RECEIVED'), 'amount_to', $this->sortDirection, $this->sortColumn); ?></th>
							<th><?= ExchangerSiteHelper::sort(JText::_('COM_EXCHANGER_STATUS'), 'status', $this->sortDirection, $this->sortColumn); ?></th>
						</tr>
						<?php foreach ($this->items as $i => $item): ?>
						<tr onClick="GoOrder('/index.php?option=com_exchanger&view=order&id=<?= $item->id; ?>')" style="cursor:pointer;">
							<td><?= ++$i; ?></td>
							<td>ID <?= $item->id; ?></td>
							<td><?= JHtml::_('date', $item->created_time, JText::_('DATE_FORMAT_LC6')); ?></td>
							<!--<td>Sell</td>-->
							<td><?= $item->currency_from_name; ?><b><?= $item->amount_from; ?> <?= $item->currency_from_code; ?></b></td>
							<td><?= $item->currency_to_name; ?><b><?= $item->amount_to; ?> <?= $item->currency_to_code; ?></b></td>
							<td><?= $item->statusTitle; ?></td>
						</tr>
							<p>
								<a href="<?= JRoute::_('index.php?option=com_exchanger&view=order&id=' . $item->id, false); ?>" target="_blank"><?= $item->title; ?></a>
							</p>
						<?php endforeach; ?>
						</table>
						<input type="hidden" name="filter_order" value="<?php echo $this->sortColumn; ?>" />
						<input type="hidden" name="filter_order_Dir" value="<?php echo $this->sortDirection; ?>" />
					</form>
				</div>
				
				<div class="pagination">
					<?= $this->pagination->getPagesLinks(); ?>
                </div>
			</div>
		</div>
		
		<div class="account-sidebar">
	        <?= EXSystem::loadModules('sidebar'); ?>
	    </div>
	</div>
</div>

<?= EXSystem::loadModules('bottom'); ?>