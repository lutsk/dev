<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$registry = new JRegistry;
$registry->loadString($this->item->currency_from_params);
$this->item->currency_from_params = $registry->toArray();
$currency_from_simbols = $this->item->currency_from_params['simbols'];
$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
$registry->loadString($this->item->currency_to_params);
$this->item->currency_to_params = $registry->toArray();
$currency_to_simbols = $this->item->currency_to_params['simbols'];
$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';

$checkUser = false;
$user = JFactory::getUser();
if ($user->id && ($user->id == $this->item->user_id)) $checkUser = true;
//echo '<pre>'; print_r($this->item); echo '</pre>'; die();
$lang = JFactory::getLanguage()->getTag();
$params = JFactory::getApplication()->getParams('com_exchanger');
$description = $params->get('description_'.$lang, '');
?>
<div class="exchange-steps">
	<div class="container exchange-steps__container">
		<div class="exchange-steps__content">
			<div class="exchange-steps__content-inner exchange-step-2">
				<div class="exchange-steps__top"><span><?= JText::_('COM_EXCHANGER_RATE'); ?> <?= sprintf($currency_from_pattern, $this->item->currency_from_course) ;?> <?= $this->item->code ;?> = <?= sprintf($currency_to_pattern, $this->item->currency_to_course) ;?> <?= $this->item->to_currency_code ;?></span><span><?= JText::_('COM_EXCHANGER_WAIT_TIME'); ?></span></div>
				
				<div class="exchange-steps__currencies">
					<div><span><?= JText::_('COM_EXCHANGER_I_GIVE'); ?></span>
						<div class="exchange-steps__currency"><span><?= $this->item->exchange_from_amount_input ;?></span><span><img class="currencies-icon" src="<?= $this->item->image ;?>" alt="<?= $this->item->name;?>" width="40" /><span><?= $this->item->code ;?></span></span></div>
					</div>
					<div>
						<div class="exchange-steps__currencies-swap"></div>
					</div>
					<div><span><?= JText::_('COM_EXCHANGER_I_RECEIVE'); ?></span>
						<div class="exchange-steps__currency"><span><?= $this->item->exchange_from_to_input ;?></span><span><img class="currencies-icon" src="<?= $this->item->to_currency_image ;?>" alt="<?= $this->item->name;?>" width="40" /><span><?= $this->item->to_currency_code ;?></span></span></div>
					</div>
				</div>
				
				<form class="exchange-step-2__form" action="/index.php?option=com_exchanger&task=order.save" method="post">
					<div class="exchange-step-2__input">
						<label><?= JText::sprintf('COM_EXCHANGER_WALLET_ADDRESS', $this->item->name); ?>, <?= $this->item->code; ?></label>
						<?= $this->form->getInput('address'); ?>
					</div>
					<div class="exchange-step-2__input">
						<label><?= JText::sprintf('COM_EXCHANGER_FILE_YOUR_WALLET', $this->item->to_currency_code); ?><span class="star">&nbsp;*</span></label>
						<?= $this->form->getInput('wallet'); ?>
					</div>
					<div class="exchange-step-2__input">
						<?= $this->form->getLabel('surname'); ?>
						<?= $this->form->getInput('surname'); ?>
					</div>
					<div class="exchange-step-2__input">
						<?= $this->form->getLabel('name'); ?>
						<?= $this->form->getInput('name'); ?>
					</div>
					<div class="exchange-step-2__input">
						<?= $this->form->getLabel('email'); ?>
						<?= $this->form->getInput('email'); ?>
					</div>
					<div class="exchange-step-2__checkboxes">
						<div class="form-checkbox-group exchange-step-2__checkbox">
							<?= $this->form->getInput('agree'); ?>
							<?= $this->form->getLabel('agree'); ?>
						</div>
						<div class="form-checkbox-group exchange-step-2__checkbox">
							<?= $this->form->getInput('regulations'); ?>
							<?= $this->form->getLabel('regulations'); ?>
						</div>
					</div>
					<?php if (trim($description) != '') : ?>
					<div class="exchange-step-2__textarea"><?= $description; ?></div>
					<?php endif; ?>
					<div class="exchange-step-2__btn-group"><!--<a class="btn btn--filled btn--wide-md" href="exchange-3.html">Confirm Exchange</a>-->
						
						<button class="btn btn--filled btn--wide-md" type="submit"><?= JText::_('COM_EXCHANGER_RATE_CONFIRM'); ?></button>
					</div>
					<div class="exchange-step-2__wain-mobile">Wait time 1 min (up to 1 h)</div>
					
					<?= $this->form->getInput('currency_from_id'); ?>
					<?= $this->form->getInput('currency_to_id'); ?>
					<?= $this->form->getInput('amount_from'); ?>
					<?= $this->form->getInput('amount_to'); ?>
					<?= JHtml::_('form.token'); ?>
				</form>
			</div>
		</div>
		
		<div class="exchange-steps__sidebar">
			<ul>
				<li><span><?= JText::_('COM_EXCHANGER_CHOOSE_CURRENCY'); ?></span></li>
				<li class="active"><span><?= JText::_('COM_EXCHANGER_ADDITIONATAL_INFO'); ?></span></li>
				<li><span><?= JText::_('COM_EXCHANGER_PAYMENT_EXCHANGE'); ?></span></li>
			</ul>
	    </div>
	</div>
</div>

<?php //if ($checkUser && $this->item->status == 0) : ?>
<?php if (!$user->id) : ?>
<div class="want-to-exchange">
	<div class="container">
		<h2 class="h2"><?= JText::_('COM_EXCHANGER_WANT_BONUSES'); ?></h2>
		<div class="want-to-exchange__row">
			<div class="want-to-exchange__col want-to-exchange__col--1">
				<?= EXSystem::loadModules('promo_text'); ?>
			</div>
			<div class="want-to-exchange__col want-to-exchange__col--2">
				<div class="want-to-exchange__form-wrapper">
					<?= EXSystem::loadModules('register'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?= EXSystem::loadModules('bottom'); ?>