<?php
defined('_JEXEC') or die;

class ExchangerViewExchange extends JViewLegacy
{
	public $item;
	public $fromList;
	public $toList;
	public $state;
	public $from;
	public $params;
	
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$layout = $app->input->get('layout', '');
		
		$this->item		 = $this->get('Item');
		$this->fromList	 = $this->get('FromList');
		$this->toList	 = $this->get('ToList');
		$this->state	 = $this->get('State');
		$this->params	 = $app->getParams('com_exchanger');
		
		if ($layout == 'step2')
		{
			$this->from = $this->getForm();
			$this->item = $this->get('Step2Item');
			
			if (!$this->item)
			{
				JError::raiseError(404, JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'));
			}
		}
		
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		$this->prepareDocument();
		
		return parent::display($tpl);
	}
	
	protected function prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$menu = $menus->getActive();
		$title = null;
		
		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_EXCHANGER'));
		}
		
		$title = $this->params->get('page_title', '');
		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}
		
		$this->document->setTitle($title);
		
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}
		
		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
		
		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}