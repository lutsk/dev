<?php
defined('_JEXEC') or die;
//echo '<pre>'; print_r($this->list); echo '</pre>'; die();
?>
<?php foreach ($this->list as $list) : ?>
	<div class="rates">
		<div class="rates__top">
			<div class="rates__heading"><img src="/<?= $list['header']->image ;?>" alt="<?= $list['header']->name ;?>" width="40" style="margin-right:10px" /><?= $list['header']->name ;?></div>
		</div>
		<ul class="rates__list">
			<?php foreach ($list['rates'] as $item) : ?>
			<li class="rates__list-item">
				<div class="rates__list-currency-1"><img src="/<?= $item->from_image ;?>" alt="<?= $item->from_name ;?>" width="20" style="margin-right:8px" /> <?= $item->from_name ;?>, <?= $item->from_code ;?></div>
				<div class="rates__list-arrow">
					<button class="rates__list-arrow-btn"></button>
				</div>
				<div class="rates__list-currency-2"><img src="/<?= $item->to_image ;?>" alt="<?= $item->to_name ;?>" width="20" style="margin-right:8px" /> <?= $item->to_name ;?>, <?= $item->to_code ;?></div>
				<div class="rates__list-rate"><span><?= $item->currency_from_course ;?> <?= $item->from_code ;?></span><span><?= $item->currency_to_course ;?> <?= $item->to_code ;?></span></div>
			</li>
			<?php endforeach; ?>
		</ul>
</div>
<?php endforeach; ?>