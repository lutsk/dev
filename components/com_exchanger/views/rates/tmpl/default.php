<?php
defined('_JEXEC') or die;
$user = JFactory::getUser();
$token = \JFactory::getSession()->getFormToken();
?>

<div class="container">
	<div class="exchange-rate-hero">
		<h1 class="h2"><?= JText::_('COM_EXCHANGER_RATES'); ?></h1>
		<div class="exchange-rate-hero__subtitle"><?= JText::_('COM_EXCHANGER_СHOOSE_CURRENCY'); ?></div>
		<form id="exchange_form" class="exchanger exchanger--rates" action="" data-get-rate-action="/index.php?option=com_exchanger&task=rates.exchangeRates">
			<div class="exchanger__currency">
				<div class="exchanger__currency-inner">
					<div class="exchanger__currency-input-group">
						<input type="hidden" id="exchange-form-from-curr" name="currency_from_id" value="0">
						<button class="exchanger-currency-dropdown-trigger" type="button"><img src="/images/currency/empty.png" /><span><?= JText::_('COM_EXCHANGER_ALL_CURRENCY'); ?></span></button>
					</div>
					<div class="exchanger-currency-dropdown">
						<button class="close-button exchanger-currency-dropdown__close" type="button"></button>
						<ul>
						<?php foreach ($this->listCurrency as $i => $item) : ?>
							<li>
							<?php if (isset($item->parent)) : ?>
								<button class="exchanger-currency-dropdown__button exchanger-currency-dropdown__submenu-trigger<?php if ($i > 4) echo ' upward'; ?>" type="button" title="<?= $item->name ;?>"><img src="<?= $item->big_image ;?>" alt="<?= $item->name ;?>" /><span><?= $item->name ;?></span></button>
								<div class="exchanger-currency-dropdown__submenu<?php if ($i > 4) echo ' upward'; ?>">
									<?php foreach ($item->childList as $child) : ?>
									<button class="curr-choice" type="button" data-text="<?= $child->name ;?>" data-id="<?= $child->id ;?>" direction="from"><?= $child->code ;?></button>
									<?php endforeach; ?>
								</div>
							<?php else : ?>
								<button class="curr-choice exchanger-currency-dropdown__button" type="button" title="<?= $item->name ;?>" direction="from" data-text="<?= $item->name ;?>" data-id="<?= $item->id ;?>"><img src="<?= $item->image ;?>" alt="<?= $item->name ;?>" /><span><span><?= $item->name ;?></span></span></button>
							<?php endif; ?>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="exchanger__swap">
				<button type="button"></button>
			</div>
			<div class="exchanger__currency">
				<div class="exchanger__currency-inner">
					<div class="exchanger__currency-input-group">
						<input type="hidden" id="exchange-form-to-curr" name="currency_to_id" value="0">
						<button class="exchanger-currency-dropdown-trigger" type="button"><img src="/images/currency/empty.png" /><span><?= JText::_('COM_EXCHANGER_ALL_CURRENCY'); ?></span></button>
					</div>
					<div class="exchanger-currency-dropdown exchanger-currency-dropdown--big">
						
						<button class="close-button exchanger-currency-dropdown__close" type="button"></button>
						<ul>
						<?php foreach ($this->listCurrency as $k => $item) : ?>
							<li>
							<?php if (isset($item->parent)) : ?>
								<button class="exchanger-currency-dropdown__button exchanger-currency-dropdown__submenu-trigger<?php if ($k > 8) echo ' upward'; ?>" type="button" title="<?= $item->name ;?>"><img src="<?= $item->big_image ;?>" alt="<?= $item->name ;?>" /><span><?= $item->name ;?></span></button>
								<div class="exchanger-currency-dropdown__submenu<?php if ($k > 8) echo ' upward'; ?>">
									<?php foreach ($item->childList as $child) : ?>
									<button class="curr-choice" type="button" data-text="<?= $child->name ;?>" data-id="<?= $child->id ;?>" direction="to"><?= $child->code ;?></button>
									<?php endforeach; ?>
								</div>
							<?php else : ?>
								<button class="curr-choice exchanger-currency-dropdown__button" type="button" title="<?= $item->name ;?>" direction="to" data-text="<?= $item->name ;?>" data-id="<?= $item->id ;?>"><img src="<?= $item->image ;?>" alt="<?= $item->name ;?>" /><span><span><?= $item->name ;?></span></span></button>
							<?php endif; ?>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
			
			<input id="token" type="hidden" name="token" value="<?= $token;?>" />
		</form>
	</div>
</div>

<div id="rateList" class="container">
	<?= $this->loadTemplate('ratelist');?>
</div>

<?= EXSystem::loadModules('ater_content'); ?>

<?php if (!$user->id) : ?>
<div class="want-to-exchange">
	<div class="container">
		<h2 class="h2"><?= JText::_('COM_EXCHANGER_WANT_BONUSES'); ?></h2>
		<div class="want-to-exchange__row">
			<div class="want-to-exchange__col want-to-exchange__col--1">
				<?= EXSystem::loadModules('promo_text'); ?>
			</div>
			<div class="want-to-exchange__col want-to-exchange__col--2">
				<div class="want-to-exchange__form-wrapper">
					<?= EXSystem::loadModules('register'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<?= EXSystem::loadModules('bottom'); ?>