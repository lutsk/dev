<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$token = \JFactory::getSession()->getFormToken();
//echo '<pre>'; print_r($this->wallets); echo '</pre>'; die();

?>
<div class="account">
	<div class="container account__container">
		<div class="account-content">
			<div class="account-wallets">
				<div class="account-title account-wallets__title"><?= JText::_('COM_EXCHANGER_MY_WALLETS'); ?></div>
				<div class="account-wallets__items">
					<?php foreach ($this->wallets as $wallet) : ?>
					<div class="account-wallets__item">
						<div class="account-wallets__item-name">
							<img class="currencies-icon" src="<?= $wallet->image ;?>" alt="<?= $item->name;?>" width="40" /><?= $wallet->name;?>
						</div>
						<div class="account-wallets__item-number"><?= $wallet->wallet ;?></div>
						<div class="account-wallets__item-note" style="display: none;"><?= $wallet->note ;?></div>
						<div class="account-wallets__item-controls">
							<a class="edit_wallet account-wallets__button account-wallets__button--edit" href="#" title="<?= JText::_('JACTION_EDIT'); ?>" data-name="<?= $wallet->name;?>" data-wallet="<?= $wallet->id;?>" data-modal="#modal-account-wallets-edit"></a>
							<a class="account-wallets__button account-wallets__button--delete" href="<?php echo JRoute::_('index.php?option=com_exchanger&task=wallets.delete&id='.$wallet->id.'&'. $token .'=1'); ?>" title="<?= JText::_('JACTION_DELETE'); ?>"></a>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<div class="account-wallets__btn-container">
					<button class="btn btn--filled btn--iconed-wallet btn--wide-md" data-modal="#modal-account-wallets-add"><span><?= JText::_('COM_EXCHANGER_ADD_WALLET'); ?></span></button>
				</div>
			</div>
		</div>
		
		<div class="account-sidebar">
            <?= EXSystem::loadModules('sidebar'); ?>
		</div>
	</div>
</div>


<div class="modal modal--centered modal--mobile-fixed-width" id="modal-account-wallets-edit">
	<div class="modal__content">
		<button class="close-button modal__close"></button>
		<div class="modal__content-inner">
			<form action="/index.php?option=com_exchanger&task=wallets.save" method="post" name="adminForm" id="item-form" class="form form-validate" enctype="multipart/form-data">
				<div class="form__title"><?= JText::_('COM_EXCHANGER_EDIT_WALLET'); ?></div>
				<div class="modal__currency">
					<div class="currencies-icon currencies-icon--bitcoin"></div>Bitcoin
				</div>
				<div class="form-group">
					<?php echo $this->form->getLabel('wallet'); ?>
					<?php echo $this->form->getInput('wallet'); ?>
				</div>
				<div class="form-group">
					<?php echo $this->form->getLabel('note'); ?>
					<?php echo $this->form->getInput('note'); ?>
				</div>
				<div class="form-button-group">
					<button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JAPPLY'); ?></button>
					<button class="btn form-button-group__btn" type="button" data-modal-close><?= JText::_('JCANCEL'); ?></button>
				</div>
				
				<input id="wallet_id" name="id" type="hidden" value="">
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</div>
	</div>
</div>

<div class="modal modal--centered modal--big" id="modal-account-wallets-add">
	<div class="modal__content">
		<button class="close-button modal__close"></button>
			<div class="modal__content-inner">
				<div class="modal__title modal__title--offset-bottom-md"><?= JText::_('COM_EXCHANGER_CHOOSE_TYPE_WALLET'); ?></div>
				<ul class="wallets-list">
					<?php foreach ($this->list as $i => $item) : ?>
					<li>
						<input id="account-wallets-add-radio-<?= $i;?>" type="radio" name="account-wallets-add">
						<label for="account-wallets-add-radio-<?= $i;?>" data-id="<?= $item->id ;?>" data-text="<?= $item->name ;?>"><img class="currencies-icon" src="<?= $item->image ;?>" alt="<?= $item->name ;?>" width="42" /><?= $item->name ;?></label>
					</li>
					<?php endforeach; ?>
				</ul>
				
				<a class="next-btn" href="#" data-modal="#modal-account-wallets-create"><?= JText::_('COM_EXCHANGER_NEXT'); ?></a>
			</div>
	</div>
</div>
<div class="modal modal--centered modal--mobile-fixed-width" id="modal-account-wallets-create">
	<div class="modal__content">
		<button class="close-button modal__close"></button>
		<div class="modal__content-inner">
			<form action="/index.php?option=com_exchanger&task=wallets.save" method="post" name="adminForm" id="item-form" class="form form-validate" enctype="multipart/form-data">
				<div class="form__title"><?= JText::_('COM_EXCHANGER_NEW_WALLET'); ?></div>
				<div class="modal__currency"></div>
				<div class="form-group">
					<?php echo $this->form->getLabel('wallet'); ?>
					<?php echo $this->form->getInput('wallet'); ?>
				</div>
				<div class="form-group">
					<?php echo $this->form->getLabel('note'); ?>
					<?php echo $this->form->getInput('note'); ?>
				</div>
				<div class="form-button-group">
					<button class="btn btn--filled form-button-group__btn" type="submit"><?= JText::_('JAPPLY'); ?></button>
					<button class="btn form-button-group__btn" type="button" data-modal-close><?= JText::_('JCANCEL'); ?></button>
				</div>
				
				<div class="modal__back-btn-container"><a class="back-btn" href="#" data-modal="#modal-account-wallets-add"><?= JText::_('COM_EXCHANGER_BACK'); ?></a></div>
				
				<?php echo $this->form->getInput('currency_id'); ?>
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</div>
	</div>
</div>