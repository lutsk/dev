<?php
defined( '_JEXEC' ) or die;

$checkUser = false;
$user = JFactory::getUser();
if ($user->id && ($user->id == $this->item->user_id)) $checkUser = true;
?>
<div class="exchange-steps">
	<div class="container exchange-steps__container">
		<div class="exchange-steps__content">
			<div class="exchange-steps__content-inner exchange-step-3">
				<div class="exchange-steps__top"><span><?= JText::_('COM_EXCHANGER_BID_ID'); ?> <?= $this->item->id; ?></span><span class="<?= $this->item->statusClass; ?>"><?= $this->item->statusTitle; ?></span></div>
				<div class="exchange-step-3__rate"><?= JText::_('COM_EXCHANGER_RATE'); ?> <?= floatval($this->item->rate); ?></div>
				
				<div class="exchange-steps__currencies">
					<div><span><?= JText::_('COM_EXCHANGER_I_GIVE'); ?><!-- <span class="info-btn" data-popup-target="info-popup-exchange-1" data-popup-left="-75" data-popup-top="20"></span>--></span>
						<div class="exchange-steps__currency"><span><?= $this->item->amount_from; ?></span><span><img class="currencies-icon" src="<?= $this->item->currency_from_image ;?>" width="40" /><span><?= $this->item->currency_from_code ;?></span></span></div>
					</div>
					<div>
						<div class="exchange-steps__currencies-swap"></div>
					</div>
					<div><span><?= JText::_('COM_EXCHANGER_I_RECEIVE'); ?><!-- <span class="info-btn" data-popup-target="info-popup-exchange-2" data-popup-left="-100" data-popup-top="20"></span>--></span>
						<div class="exchange-steps__currency"><span><?= $this->item->amount_to; ?></span><span><img class="currencies-icon" src="<?= $this->item->currency_to_image ;?>" width="40" /><span><?= $this->item->currency_to_code ;?></span></span></div>
					</div>
				</div>
				<?php if ($checkUser && $this->item->status == 0) : ?>
				<div class="exchange-step-3__application"><?= JText::sprintf('COM_EXCHANGER_APPLICATION_YOUR_ACCOUNT', '<a href="'.JRoute::_('index.php?option=com_exchanger&view=orders').'">'.JText::_('COM_EXCHANGER_IN_ACCOUNT').'</a>'); ?></div>
				<div class="exchange-step-3__warning">
					<b><?= JText::_('COM_EXCHANGER_FOLLOWING_INSTRUCTIONS'); ?></b>
					<p><?= JText::sprintf('COM_EXCHANGER_PASS_ON', '<a href="'.JRoute::_('index.php?option=com_exchanger&view=orders').'">'.JText::_('COM_EXCHANGER_PASS_ON_LINK').'</a>', $this->item->currency_from_name.' '.$this->item->amount_from.' '.$this->item->currency_from_code); ?></p>
					<p><?= JText::sprintf('COM_EXCHANGER_RETURNED_MINUS', $this->item->currency_from_name, $this->item->currency_from_name); ?></p>
				</div>
				<?php endif; ?>
				<div class="exchange-step-3__info">
					<div><span><?= JText::_('COM_EXCHANGER_RECIPIENT_ADDRESS'); ?></span><span><?= $this->item->address; ?></span></div>
					<div><span><?= JText::_('COM_EXCHANGER_TRANSFER_AMOUNT'); ?></span><span><?= $this->item->amount_from; ?> <?= $this->item->currency_from_code ;?></span></div>
				</div>
				<!--
				<div class="info-popup" id="info-popup-exchange-1">
                  <p class="info-popup__text">It begins with you fixing a certain sales commission rate, which is a percentage of the sales value. Keep track of the amount of sales every single employee generates, then multiply the sales commission rate of each employee by the amount of sales he makes.</p>
                </div>
                <div class="info-popup" id="info-popup-exchange-2">
                  <p class="info-popup__text">It begins with you fixing a certain sales commission rate, which is a percentage of the sales value. Keep track of the amount of sales every single employee generates, then multiply the sales commission rate of each employee by the amount of sales he makes.</p>
                </div>
                -->
			</div>
		</div>
		
		<div class="exchange-steps__sidebar">
			<ul>
				<li><span><?= JText::_('COM_EXCHANGER_CHOOSE_CURRENCY'); ?></span></li>
				<li><span><?= JText::_('COM_EXCHANGER_ADDITIONATAL_INFO'); ?></span></li>
				<li class="active"><span><?= JText::_('COM_EXCHANGER_PAYMENT_EXCHANGE'); ?></span></li>
			</ul>
		</div>
	</div>
</div>

<?= EXSystem::loadModules('bottom'); ?>