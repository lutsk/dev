<?php
defined('_JEXEC') or die;

class ExchangerViewOrder extends JViewLegacy
{
	public $item;
	
	public function display($tpl = null)
	{
		$user = JFactory::getUser();
		
		$cookieLogin = $user->get('cookieLogin');
		
		if (!empty($cookieLogin))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
			$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
			
			return false;
		}
		
		$this->item = $this->get('Item');
		
		if (!$this->item->id)
		{
			JError::raiseError(404, JText::_('JERROR_USERS_PROFILE_NOT_FOUND'));
			return false;
		}
		parent::display($tpl);
	}

}