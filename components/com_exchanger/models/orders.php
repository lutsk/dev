<?php
defined('_JEXEC') or die;

class ExchangerModelOrders extends JModelList
{
	public function __construct($config = array())
	{   
		$config['filter_fields'] = array(
			'created_time',
			'amount_from',
			'amount_to',
			'status'
		);
		parent::__construct($config);
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		parent::populateState('created_time', 'DESC');
		$input = JFactory::getApplication()->input;
		$params = JComponentHelper::getParams('com_exchanger');
		$this->setState( 'list.start', $input->get('start'));
		$this->setState( 'list.limit', $params->get('limit', 10));
	}
	
	protected function getListQuery()
	{
		$user = JFactory::getUser();
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from($db->qn('#__abc_orders'))->where($db->qn('user_id') . ' = ' . (int) $user->id);
		$query->order($db->escape($this->getState('list.ordering', 'created_time')).' '.
		$db->escape($this->getState('list.direction', 'DESC')));
		return $query;
	}
	
	public function getItems()
	{
		$items		 = parent::getItems($tpl);
		$registry	 = new JRegistry;
		
		foreach ($items as $item)
		{
			$item->currency_from_name = ExchangerSiteHelper::getCurrencyName($item->currency_from_id);
			$item->currency_to_name	 = ExchangerSiteHelper::getCurrencyName($item->currency_to_id);
			$item->currency_from_code	 = ExchangerSiteHelper::getCurrencyCode($item->currency_from_id);
			$item->currency_to_code	 = ExchangerSiteHelper::getCurrencyCode($item->currency_to_id);
			
			$currency_from_params = ExchangerSiteHelper::getCurrencyParams($item->currency_from_id);
			$currency_to_params = ExchangerSiteHelper::getCurrencyParams($item->currency_to_id);
			
			$registry->loadString($currency_from_params);
			$currency_from_params = $registry->toArray();
			$currency_from_simbols = $currency_from_params['simbols'];
			$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
			
			$registry->loadString($currency_to_params);
			$currency_to_params = $registry->toArray();
			$currency_to_simbols = $currency_to_params['simbols'];
			$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
			
			$item->amount_from	 = sprintf($currency_from_pattern, $item->amount_from);
			$item->amount_to	 = sprintf($currency_to_pattern, $item->amount_to);
			
			switch ($item->status) {
				case 1:
					$item->statusClass = 'order_info';
				    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PERFORMED');
				    break;
				case 2:
				    $item->statusClass = 'order_primary';
				    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_CANCELED');
				    break;
				case 3:
				    $item->statusClass = 'order_danger';
				    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PAYMENT_ERROR');
				    break;
				case 4:
				    $item->statusClass = 'order_success';
				    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_COMPLETED');
				    break;
				default:
				    $item->statusClass = 'order_primary';
				    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PAYMENT');
			}
		}
		
		return $items;
	}
}