<?php
defined('_JEXEC') or die;

class ExchangerModelRates extends JModelLegacy
{
	public function getCurrency()
	{
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select('DISTINCT parent')
			->from($db->qn('#__abc_currency'));
			$db->setQuery($query);
			$parentMapIds = $db->loadColumn();
			
			foreach ($parentMapIds as $id)
			{
				if ($id > 0)
				{
					$query->clear()
						->select(array(
							$db->qn('m.id'), $db->qn('m.alias'), $db->qn('m.image', 'big_image'), $db->qn('m.'.$name, 'name')
						))
					->from($db->qn('#__abc_multicurrency', 'm'))
					->where($db->qn('m.id') . ' = ' . $id);
					$db->setQuery($query);
					
					$parent = $db->loadObject();
					$parent->parent = 1;
					
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.'.$name, 'name')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					$query->where($db->qn('c.parent') . ' = ' . $parent->id);
					$db->setQuery($query);
					
					$parentList = $db->loadObjectList();
					
					foreach ($parentList as $i => $child)
					{
						$child->alias = $parent->alias;
						$parent->childList[] = $child;
					}
					
					if (count($parent->childList) > 1)
					{
						$list[] = $parent;
					}
					else {
						$list[] = $parent->childList[0];
					}
					
				}
				else {
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.'.$name, 'name')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					$query->where($db->qn('c.parent') . ' = 0');
					$db->setQuery($query);
					
					$other = $db->loadObjectList();
					
					foreach ($other as $item)
					{
						$item->alias = JApplication::stringURLSafe($item->name);
						$list[] = $item;
					}
				}
			}
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
	}
		
	public function getList($from = 0, $to = 0, $direction = null)
	{
		if ($from == 0 && $to == 0)
		{
			return $this->getAllRates();
		}
		else {
			return $this->getRates($from, $to);
		}
		
	}
	
	protected function getAllRates()
	{
		$registry = new JRegistry;
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select('DISTINCT parent')
			->from($db->qn('#__abc_currency'));
			$db->setQuery($query);
			$parentMapIds = $db->loadColumn();
			$i = 0;
			
			foreach ($parentMapIds as $id)
			{
				if ($id > 0)
				{
					$query->clear()
						->select(array(
							$db->qn('id'), $db->qn($name, 'name'), $db->qn('image')
						))
					->from($db->qn('#__abc_multicurrency'))
					->where($db->qn('id') . ' = ' . $id);
					$db->setQuery($query);
					
					$header = $db->loadObject();
					
					$query->clear()
					->select(array(
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					))
					->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_from_id'));
					
					$query->from($db->qn('#__abc_rate', 'r'));
					$query->where($db->qn('c.parent') . ' = ' . $header->id);
					$query->where($db->qn('r.published') . ' = 1');
					
					$db->setQuery($query);
					
					$rateList = $db->loadObjectList();
					
					foreach ($rateList as $rate)
					{
						$rate->from_name = ExchangerSiteHelper::getCurrencyName($rate->currency_from_id);
						$rate->to_name = ExchangerSiteHelper::getCurrencyName($rate->currency_to_id);
						$rate->from_code = ExchangerSiteHelper::getCurrencyCode($rate->currency_from_id);
						$rate->to_code = ExchangerSiteHelper::getCurrencyCode($rate->currency_to_id);
						$rate->from_image = ExchangerSiteHelper::getCurrencyImage($rate->currency_from_id);
						$rate->to_image = ExchangerSiteHelper::getCurrencyImage($rate->currency_to_id);
						
						$from_params = ExchangerSiteHelper::getCurrencyParams($rate->currency_from_id);
						$registry->loadString($from_params);
						$from_params = $registry->toArray();
						$currency_from_simbols = $from_params['simbols'];
						$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
						
						$rate->currency_from_course = sprintf($currency_from_pattern, $rate->currency_from_course);
						
						$to_params = ExchangerSiteHelper::getCurrencyParams($rate->currency_to_id);
						$registry->loadString($to_params);
						$to_params = $registry->toArray();
						$currency_to_simbols = $to_params['simbols'];
						$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
						
						$rate->currency_to_course = sprintf($currency_to_pattern, $rate->currency_to_course);
					}
					
					$list[$i]['header'] = $header;
					$list[$i]['rates'] = $rateList;
					$i++;
				}
				else {
					$query->clear()
					->select(array(
						$db->qn('c.'.$name, 'name'), $db->qn('c.image'), 
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					))
					->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_from_id'));
					
					$query->from($db->qn('#__abc_rate', 'r'));
					
					$query->where($db->qn('c.parent') . ' = 0');
					$query->where($db->qn('r.published') . ' = 1');
					
					$db->setQuery($query);
					
					$otherList = $db->loadObjectList();
					
					foreach ($otherList as $k => $item)
					{
						$header = new stdClass();
						$header->name = $item->name;
						$header->image = $item->image;
						$list[$i]['header'] = $header;
						
						$item->from_name = $item->name;
						unset($item->name);
						$item->to_name = ExchangerSiteHelper::getCurrencyName($item->currency_to_id);
						$item->from_code = ExchangerSiteHelper::getCurrencyCode($item->currency_from_id);
						$item->to_code = ExchangerSiteHelper::getCurrencyCode($item->currency_to_id);
						$item->from_image = ExchangerSiteHelper::getCurrencyImage($item->currency_from_id);
						$item->to_image = ExchangerSiteHelper::getCurrencyImage($item->currency_to_id);
						unset($item->image);
						
						$from_params = ExchangerSiteHelper::getCurrencyParams($item->currency_from_id);
						$registry->loadString($from_params);
						$from_params = $registry->toArray();
						$currency_from_simbols = $from_params['simbols'];
						$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
						
						$item->currency_from_course = sprintf($currency_from_pattern, $item->currency_from_course);
						
						$to_params = ExchangerSiteHelper::getCurrencyParams($item->currency_to_id);
						$registry->loadString($to_params);
						$to_params = $registry->toArray();
						$currency_to_simbols = $to_params['simbols'];
						$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
						
						$item->currency_to_course = sprintf($currency_to_pattern, $item->currency_to_course);
						
						$list[$i]['rates'][$k] = $item;
						$i++;
					}
				}
			}
			
			return $list;
			
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
	}
	
	protected function getRates($from = 0, $to = 0)
	{
		$registry = new JRegistry;
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			if ($from > 0 && $to == 0)
			{
			$query->select(array(
						$db->qn('m.'.$name, 'parent_name'), $db->qn('m.image', 'parent_image'),
						$db->qn('c.id'), $db->qn('c.'.$name, 'name'), $db->qn('c.image'),
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					));
				$query->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_from_id'));
			$query->join('LEFT', $db->qn('#__abc_multicurrency', 'm') . ' ON ' . $db->qn('m.id') . ' = ' . $db->qn('c.parent'));
				$query->where($db->qn('r.currency_from_id') . ' = ' . $from);
			}
			if ($from == 0 && $to > 0)
			{
			$query->select(array(
						$db->qn('m.'.$name, 'parent_name'), $db->qn('m.image', 'parent_image'),
						$db->qn('c.id'), $db->qn('c.'.$name, 'name'), $db->qn('c.image'),
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					));
				$query->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_to_id'));
			$query->join('LEFT', $db->qn('#__abc_multicurrency', 'm') . ' ON ' . $db->qn('m.id') . ' = ' . $db->qn('c.parent'));
				$query->where($db->qn('r.currency_to_id') . ' = ' . $to);
			}
			if ($from > 0 && $to > 0)
			{
			$query->select(array(
						$db->qn('m.'.$name, 'parent_name'), $db->qn('m.image', 'parent_image'),
						$db->qn('c.id'), $db->qn('c.'.$name, 'name'), $db->qn('c.image'),
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					));
				$query->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_from_id'));
			$query->join('LEFT', $db->qn('#__abc_multicurrency', 'm') . ' ON ' . $db->qn('m.id') . ' = ' . $db->qn('c.parent'));
				$query->where($db->qn('r.currency_from_id') . ' = ' . $from);
				$query->where($db->qn('r.currency_to_id') . ' = ' . $to);
			}
			
					
			$query->from($db->qn('#__abc_rate', 'r'));
			
			if ($from > 0)
			{
			}
			if ($to > 0)
			{
			}
			
			$query->where($db->qn('r.published') . ' = 1');
			$db->setQuery($query);
			
			$rateList = $db->loadObjectList();
			
			foreach ($rateList as $i => $item)
			{
				$header = new stdClass();
				
				if ($from == 0 && $to > 0)
				{
					$item->from_name = ExchangerSiteHelper::getCurrencyName($item->currency_from_id);
					$item->to_name = $item->name;
				}
				else {
					$item->from_name = $item->name;
					$item->to_name = ExchangerSiteHelper::getCurrencyName($item->currency_to_id);
				}
					unset($item->name);
					$item->from_code = ExchangerSiteHelper::getCurrencyCode($item->currency_from_id);
					$item->to_code = ExchangerSiteHelper::getCurrencyCode($item->currency_to_id);
					$item->from_image = ExchangerSiteHelper::getCurrencyImage($item->currency_from_id);
					$item->to_image = ExchangerSiteHelper::getCurrencyImage($item->currency_to_id);
					unset($item->image);
					
					$from_params = ExchangerSiteHelper::getCurrencyParams($item->currency_from_id);
					$registry->loadString($from_params);
					$from_params = $registry->toArray();
					$currency_from_simbols = $from_params['simbols'];
					$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
					
					$item->currency_from_course = sprintf($currency_from_pattern, $item->currency_from_course);
					
					$to_params = ExchangerSiteHelper::getCurrencyParams($item->currency_to_id);
					$registry->loadString($to_params);
					$to_params = $registry->toArray();
					$currency_to_simbols = $to_params['simbols'];
					$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
					
					$item->currency_to_course = sprintf($currency_to_pattern, $item->currency_to_course);
					
					$header->name = $item->parent_name;
					$header->image = $item->parent_image;
					
					if ($item->parent_name == '') $header->name = $item->name;
					if ($item->parent_image == '') $header->image = $item->image;
				
				$list[0]['header'] = $header;
				$list[0]['rates'][$i] = $item;
			}
			
			return $list;
			
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
		
	}
}