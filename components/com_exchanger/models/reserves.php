<?php
defined('_JEXEC') or die;

class ExchangerModelReserves extends JModelLegacy
{
	public function getItems()
	{
		$registry = new JRegistry;
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select(array(
				$db->qn('id'), $db->qn('code'), $db->qn('image'), $db->qn($name, 'name'), $db->qn('amount')
			))->from($db->qn('#__abc_currency'));
		
		$db->setQuery($query);
		
		$items = $db->loadObjectList();
		
		foreach ($items as $item)
		{
			$params = ExchangerSiteHelper::getCurrencyParams($item->id);
			$registry->loadString($params);
			$params = $registry->toArray();
			$simbols = $params['simbols'];
			$pattern = (string) '%01.'.$simbols.'f';
			
			$item->amount = sprintf($pattern, $item->amount);
		}
		
		return $items;
	}
}