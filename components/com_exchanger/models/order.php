<?php
defined( '_JEXEC' ) or die;

class ExchangerModelOrder extends JModelForm
{
	public function getItem($id = 0)
	{
		$registry = new JRegistry;
		if ($id == 0)
		{
			$id = \JFactory::getApplication()->input->getInt('id', 0);
		}
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		/*
		$query->select(array('o.*', $db->qn('r.currency_to_course', 'rate')))
			->from($db->qn('#__abc_orders', 'o'))
			->join('LEFT', $db->qn('#__abc_rate', 'r') . ' ON ' . $db->qn('r.currency_from_id') . ' = ' . $db->qn('o.currency_from_id'))
			->where($db->qn('o.id') . ' = ' . $id);
		*/
		$query->select('*')
			->from($db->qn('#__abc_orders'))
			->where($db->qn('id') . ' = ' . $id);
		
		$item = $this->getDbo()->setQuery($query)->loadObject();
		
		if (!$item) return false;
		
		$currency_from_params = ExchangerSiteHelper::getCurrencyParams($item->currency_from_id);
		$currency_to_params = ExchangerSiteHelper::getCurrencyParams($item->currency_to_id);
		
		$registry->loadString($currency_from_params);
		$currency_from_params = $registry->toArray();
		$currency_from_simbols = $currency_from_params['simbols'];
		$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
		
		$registry->loadString($currency_to_params);
		$currency_to_params = $registry->toArray();
		$currency_to_simbols = $currency_to_params['simbols'];
		$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
		
		$item->amount_from	 = sprintf($currency_from_pattern, $item->amount_from);
		$item->amount_to	 = sprintf($currency_to_pattern, $item->amount_to);
		
		$item->currency_from_name = ExchangerSiteHelper::getCurrencyName($item->currency_from_id);
		$item->currency_to_name	 = ExchangerSiteHelper::getCurrencyName($item->currency_to_id);
		$item->currency_from_image = ExchangerSiteHelper::getCurrencyImage($item->currency_from_id);
		$item->currency_to_image	 = ExchangerSiteHelper::getCurrencyImage($item->currency_to_id);
		$item->currency_from_code	 = ExchangerSiteHelper::getCurrencyCode($item->currency_from_id);
		$item->currency_to_code	 = ExchangerSiteHelper::getCurrencyCode($item->currency_to_id);
		
		$rate = $item->amount_to / $item->amount_from;
		$item->rate = sprintf('%01.3f', $rate);
		
		switch ($item->status) {
			case 1:
				$item->statusClass = 'order_info';
			    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PERFORMED');
			    break;
			case 2:
			    $item->statusClass = 'order_primary';
			    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_CANCELED');
			    break;
			case 3:
			    $item->statusClass = 'order_danger';
			    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PAYMENT_ERROR');
			    break;
			case 4:
			    $item->statusClass = 'order_success';
			    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_COMPLETED');
			    break;
			default:
			    $item->statusClass = 'order_primary';
			    $item->statusTitle = JText::_('COM_EXCHANGER_WAITING_PAYMENT');
		}
		
		if ( empty( $item->id ) ) {
			throw new Exception( 'No entry found', 404 );
		}
		return $item;
	}
	
	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_exchanger.order', 'order', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	
	public function save($data)
	{
		$table = $this->getTable();
		$key = $table->getKeyName();
		$pk = (int) $this->getState($this->getName() . '.id');
		
		try
		{
			if (!$table->bind($data))
			{
				$this->setError($table->getError());
				return false;
			}
			
			if (!$table->check())
			{
				$this->setError($table->getError());
				return false;
			}
			
			if (!$table->store())
			{
				$this->setError($table->getError());
				return false;
			}
			
			$this->setState($this->getName() . '.id', $table->$key);
			
			$this->cleanCache();
		}
		catch (\Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}
		
		return true;
	}
	
	public function checkout($pk = null)
	{
		if ($pk)
		{
			$table = $this->getTable();
			
			if (!$table->load($pk))
			{
				$this->setError($table->getError());
				return false;
			}
			
			$checkedOutField = $table->getColumnAlias('checked_out');
			$checkedOutTimeField = $table->getColumnAlias('checked_out_time');
			
			if (!property_exists($table, $checkedOutField) || !property_exists($table, $checkedOutTimeField))
			{
				return true;
			}
			
			$user = \JFactory::getUser();
			
			if ($table->{$checkedOutField} > 0 && $table->{$checkedOutField} != $user->get('id'))
			{
				$this->setError(\JText::_('JLIB_APPLICATION_ERROR_CHECKOUT_USER_MISMATCH'));
				return false;
			}
			
			if (!$table->checkOut($user->get('id'), $pk))
			{
				$this->setError($table->getError());
				return false;
			}
		}
		
		return true;
	}
	
	public function sendToCometQL($recordId)
    {
    	$order = $this->getItem($recordId);
    	
    	$params = JComponentHelper::getParams('com_exchat');
    	
    	$host = "app.comet-server.ru";
		$key = $params->get('dev_id');
		$password = $params->get('dev_key');
    	
        if(trim($order->id) != '') {
            $comet = mysqli_connect($host, $key, $password, "CometQL_v1");
			if(mysqli_errno($comet))
			{
			    echo "Error:".mysqli_error($link);
			}
			
			$msg = array(
				"amount_from" => $order->amount_from,
				"amount_to" => $order->amount_to,
				"currency_from_code" => $order->currency_from_code,
				"currency_to_code" => $order->currency_to_code
			);
			
			$msg = json_encode($msg);
			$msg = mysqli_real_escape_string($comet, $msg);
			
			$query = "INSERT INTO pipes_messages (name, event, message)" .
			  " VALUES ('web_exchange_data', 'exchange', '".$msg."')";
			
			mysqli_query($comet, $query);
			
			if(mysqli_errno($comet))
			{
				return false;
			} 
			else
			{
			    return false;
			}
        } else {
            return false;
        }
        
		return true;
    }
}