<?php
defined('_JEXEC') or die;
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

jimport( 'joomla.filesystem.folder' );
jimport('joomla.filesystem.file');

class ExchangerModelExchange extends JModelForm
{
	//protected $_context = 'com_exchanger.exchange';
	
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		
		$id = $app->input->getInt('from_id');
		$to_id = $app->input->getInt('to_id', 0);
		$reverce = $app->input->get('reverce');
		
		//$pk = 9;
		$pk = ($reverce) ? $to_id : $id;
		$this->setState('exchange.id', $pk);
		
	}
	
	public function getItem($pk = null)
	{
		$pk = (!empty($pk)) ? $pk : (int) $this->getState('exchange.id');
		//$pk = (!empty($pk)) ? $pk : 9;
		
		if ($this->_item === null)
		{
			$this->_item = array();
		}
		
		if (!isset($this->_item[$pk]))
		{
			try
			{
				$lang = JFactory::getLanguage()->getTag();
				$name = 'name_' . $lang;
				$description = 'description_' . $lang;
				
				$db = $this->getDbo();
				$query = $db->getQuery(true)
					->select($this->getState('item.select',array(
					$db->qn('r.id'), $db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course'), $db->qn('r.ref_id'), $db->qn('r.amount_fee'), $db->qn('r.'.$description, 'description'), $db->qn('r.published')
					)))
					->from($db->qn('#__abc_rate', 'r'));
					
				$query->select(array(
					$db->qn('c.parent', 'currency_from_parent'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.min_buy'), $db->qn('c.'.$name, 'name'), $db->qn('c.params', 'currency_from_params')
				));
				$query->select(array(
					$db->qn('rev.currency_from_course', 'currency_from_reverse_course'), $db->qn('rev.currency_to_course', 'currency_to_reverse_course'), $db->qn('rev.amount_fee', 'to_amount_fee')
				));
				$query->select(array(
					$db->qn('t.parent', 'currency_to_parent'), $db->qn('t.'.$name, 'to_currency_name'), $db->qn('t.image', 'to_currency_image'), $db->qn('t.code', 'to_currency_code'),
					$db->qn('t.amount'), $db->qn('t.max_sell'), $db->qn('t.params', 'currency_to_params')
				));
				
				$query->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('r.currency_from_id') . ' = ' . $db->qn('c.id'));				
				$query->join('LEFT', $db->qn('#__abc_rate', 'rev') . ' ON ' . $db->qn('r.ref_id') . ' = ' . $db->qn('rev.id'));
				$query->join('LEFT', $db->qn('#__abc_currency', 't') . ' ON ' . $db->qn('t.id') . ' = ' . $db->qn('r.currency_to_id'));
				
				if ($pk > 0)
				{
					$query->where($db->qn('r.id') . ' = ' . (int) $pk);
				}
				else {
					//$query->where($db->qn('r.ordering') . ' = 1');
				}
				if ($pk == null)
				{
					$query->where($db->qn('r.main') . ' = 1');
				}
					//echo $query->dump(); die();
				$db->setQuery($query);
				
				$data = $db->loadObject();
				
				if ($data->amount_fee > 0)
				{
					$data->fee = ExchangerSiteHelper::feeCalculate($data->min_buy, $data->amount_fee);
				}
				
				/*if (!empty($data->image))
				{
					$data->image_class = $this->addImageClass($data->image);
				}
				if (!empty($data->to_currency_image))
				{
					$data->to_image_class = $this->addImageClass($data->to_currency_image);
				}*/
				
				if (empty($data))
				{
					JError::raiseError(404, JText::_('COM_NEWSFEEDS_ERROR_FEED_NOT_FOUND'));
				}
				$this->_item[$pk] = $data;
			}
			catch (Exception $e)
			{
				$this->setError($e);
				$this->_item[$pk] = false;
			}
		}
		
		return $this->_item[$pk];
	}
	
	/*
	* Current lists
	*/
	public function getFromList()
	{
		return $this->getList();
	}
	
	public function getToList()
	{
		return $this->getList(false);
	}
	
	protected function getList($to = true)
	{
		$lang = JFactory::getLanguage()->getTag();
		$name		 = 'name_' . $lang;
		$description = 'description_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select('DISTINCT parent')
			->from($db->qn('#__abc_currency'));
			$db->setQuery($query);
			$parentMapIds = $db->loadColumn();
			
			foreach ($parentMapIds as $id)
			{
				if ($id > 0)
				{
					$query->clear()
						->select(array(
							$db->qn('m.id'), $db->qn('m.alias'), $db->qn('m.image', 'big_image'), $db->qn('m.'.$name, 'name')
						))
					->from($db->qn('#__abc_multicurrency', 'm'))
					->where($db->qn('m.id') . ' = ' . $id);
					$db->setQuery($query);
					
					$parent = $db->loadObject();
					$parent->parent = 1;
					if (!empty($parent->big_image))
					{
						//$parent->big_icon_class = $this->addImageClass($parent->big_image);
					}
					
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.amount'), $db->qn('c.'.$name, 'name'),
							$db->qn('c.min_buy'), $db->qn('c.max_buy'), $db->qn('c.min_sell'), $db->qn('c.max_sell'), $db->qn('c.params')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					/*if ($to)
					{
						$query->where('(' . $db->qn('c.min_buy') . ' != 0 AND ' . $db->qn('c.max_buy') . ' != 0)');
					//$query->where($db->qn('c.min_buy') . ' > 0')->where($db->qn('c.max_buy') . ' > 0');
					}
					else {
						$query->where('(' . $db->qn('c.min_sell') . ' != 0 AND ' . $db->qn('c.max_sell') . ' != 0)');
					//$query->where($db->qn('c.min_sell') . ' > 0')->where($db->qn('c.max_sell') . ' > 0');
					}*/
					
					$query->where($db->qn('c.parent') . ' = ' . $parent->id);
					$db->setQuery($query);
					
					$parentList = $db->loadObjectList();
					
					foreach ($parentList as $i => $child)
					{
						if ($to)
						{
							if ((float)$child->min_buy == 0 && (float)$child->max_buy == 0) continue;
						}
						else {
							if ((float)$child->min_sell == 0 && (float)$child->max_sell == 0) continue;
						}
							
						//$child->alias = JApplication::stringURLSafe($child->name);
						$child->alias = $parent->alias;
						
						if (!empty($child->image))
						{
							//$child->icon_class = $this->addImageClass($child->image);
						}
						$parent->childList[] = $child;
					}
					
					if (count($parent->childList) > 1)
					{
						$list[] = $parent;
					}
					else {
						/*$parent->childList[0]->big_icon_class = $parent->big_icon_class;
						unset($parent->childList[0]->icon_class);*/
						$list[] = $parent->childList[0];
					}
					
				}
				else {
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.amount'), $db->qn('c.'.$name, 'name'),
							$db->qn('c.min_buy'), $db->qn('c.max_buy'), $db->qn('c.min_sell'), $db->qn('c.max_sell'), $db->qn('c.params')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					/*if ($to)
					{
						$query->where('(' . $db->qn('c.min_buy') . ' > 0 AND ' . $db->qn('c.max_buy') . '> 0)');
						//$query->where($db->qn('c.min_buy') . ' > 0')->where($db->qn('c.max_buy') . ' > 0');
					}
					else {
						$query->where('(' . $db->qn('c.min_sell') . ' > 0 AND ' . $db->qn('c.max_sell') . '> 0)');
						//$query->where($db->qn('c.min_sell') . ' > 0')->where($db->qn('c.max_sell') . ' > 0');
					}*/
					
					$query->where($db->qn('c.parent') . ' = 0');
					$db->setQuery($query);
					
					$other = $db->loadObjectList();
					
					foreach ($other as $item)
					{
						/*if ($to)
						{
							if ((float)$item->min_buy == 0 && (float)$item->max_buy == 0) continue;
						}
						else {
							if ((float)$item->min_sell == 0 && (float)$item->max_sell == 0) continue;
						}*/
						
						$item->alias = JApplication::stringURLSafe($item->name);
						
						/*if (!empty($item->image))
						{
							//$item->big_icon_class = $this->addImageClass($item->image);
						}*/
						
						$list[] = $item;
					}
				}
			}
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
	}
	
	public function getRateId($from, $to)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true)->select($db->qn('id'))->from($db->qn('#__abc_rate'))->where($db->qn('currency_from_id') . ' = ' . (int) $from)->where($db->qn('currency_to_id') . ' = ' . (int) $to);
		$db->setQuery($query);
		$pk = $db->loadResult();
		if ($pk !== null)
		{
			return $this->getItem($pk);
		}
		
		
		
		return false;
	}
	
	public function getTable($type = 'order', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_exchanger.order', 'order', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	protected function loadFormData()
	{
		$user = JFactory::getUser();
		$userData = ExchangerSiteHelper::getUserData($user->id);
		
		$data = (array) JFactory::getApplication()->getUserState('com_exchanger.exchange.step2.data', array());
		
		$formData['currency_from_id'] = $data['from'];
		$formData['currency_to_id'] = $data['to'];
		$formData['amount_from'] = $data['exchange_from_amount_input'];
		$formData['amount_to'] = $data['exchange_from_to_input'];
		$formData['address'] = ExchangerSiteHelper::getWallet($data['from'], null, true)->wallet;
		$formData['wallet'] = ExchangerSiteHelper::getWallet($data['to'], $user->id)->wallet;
		$formData['surname'] = $userData->surname;
		$formData['name'] = $user->name;
		$formData['email'] = $user->email;
		
		return $formData;
	}
	
	public function getStep2Item()
	{
		
		$data = JFactory::getApplication()->getUserState('com_exchanger.exchange.step2.data');
		$item = new stdClass();
		
		if ($data['from'] > 0 && $data['to'] > 0)
		{
			$item = $this->getRateId($data['from'], $data['to']);
			
			$item->exchange_from_amount_input = $data['exchange_from_amount_input'];
			$item->exchange_from_to_input = $data['exchange_from_to_input'];
			$item->from_title = $data['from_title'];
			$item->to_title = $data['to_title'];
		}
		
		return $item;
	}
	
	public function getAjax()
	{
		$app	 = JFactory::getApplication('site');
		$classFrom		 = $app->input->getInt('classFrom', 0);
		$classTo		 = $app->input->getInt('classTo', 0);
        $amountFrom		 = str_replace(' ', '', $app->input->get('amountFrom', 0));
        $amountTo		 = str_replace(' ', '', $app->input->get('amountTo', 0));
        $amountFromHint	 = str_replace(' ', '', $app->input->get('amountFromHint', 0));
        $activeDirection = $app->input->getWord('activeDirection', 'from');
        
		$data						 = array();
		$rates['direct']['rate']	 = '<span></span><span></span>';
		$rates['reverse']['rate']	 = '<span></span><span></span>';
		$error						 = false;
		$message					 = '';
        
		$item	 = $this->getRateId($classFrom, $classTo);
        
        if($item)
		{
			$registry = new JRegistry;
			$registry->loadString($item->currency_from_params);
			$item->currency_from_params = $registry->toArray();
			$currency_from_simbols = $item->currency_from_params['simbols'];
			$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
			$registry->loadString($item->currency_to_params);
			$item->currency_to_params = $registry->toArray();
			$currency_to_simbols = $item->currency_to_params['simbols'];
			$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
			
			$rates['direct']['rate'] = '<span>'.JText::_('COM_EXCHANGER_BUY_RATE').':</span><span>'
				.sprintf($currency_from_pattern, $item->currency_from_course).' '.$item->code
				.' = '
				.sprintf($currency_to_pattern, $item->currency_to_course).' '.$item->to_currency_code
				.'</span>';
			
			$rates['reverse']['rate'] = '<span>'.JText::_('COM_EXCHANGER_SELL_RATE').':</span><span>'
				.sprintf($currency_to_pattern, $item->currency_from_reverse_course).' '.$item->to_currency_code
				.' = '
				.sprintf($currency_from_pattern, $item->currency_to_reverse_course).' '.$item->code
				.'</span>';
			
	        $actualRate = $item->currency_to_course / $item->currency_from_course;
	        
	        if($activeDirection == 'from')
	        {
	        	$fee = ExchangerSiteHelper::feeCalculate($amountFrom, $item->amount_fee);
				$systemFromFee	 = $fee->comission;
				$amountFromHint	 = $amountFrom + $systemFromFee;
	        	$amountTo = $actualRate * $amountFrom;
			}
	        elseif($activeDirection == 'from_fee')
	        {
	        	$fee = ExchangerSiteHelper::feeCalculate($amountFromHint, $item->amount_fee);
				$systemFromFee	 = $fee->comission;
				$amountFrom		 = $amountFromHint - $systemFromFee;
	        	$amountTo = $actualRate * $amountFrom;
			}
			else {
				$amountFrom		 = $amountTo / $actualRate;
	        	$fee = ExchangerSiteHelper::feeCalculate($amountFrom, $item->amount_fee);
				$systemFromFee	 = $fee->comission;
	        	$amountFromHint = $fee->total;
			}
			
			
			
			$data['systemFromTitle'] = ExchangerSiteHelper::getCurrencyName($classFrom);
			$data['systemToTitle']	 = ExchangerSiteHelper::getCurrencyName($classTo);
			
			if($item->published)
			{			
				$data['amountFrom']		 = sprintf($currency_from_pattern, $amountFrom);
		        $data['amountFromHint']	 = sprintf($currency_from_pattern, $amountFromHint);
		        $data['amountTo']		 = sprintf($currency_to_pattern, $amountTo);
		        $data['currFrom']		 = $item->code;
		        $data['currTo']			 = $item->to_currency_code;
		        $data['actualRate']		 = $actualRate;
		        $data['rateFrom']		 = sprintf($currency_from_pattern, $item->currency_to_reverse_course);
		        $data['rateTo']			 = sprintf($currency_to_pattern, $item->currency_from_reverse_course);
		        $data['activeDirection'] = $activeDirection;
		        $data['currFee']		 = $item->amount_fee;
		        $data['fee_description'] = $item->description;
		        $data['systemFromFee']	 = sprintf($currency_from_pattern, $systemFromFee); // Сумма комиссии системы на снятие
		        $data['systemToFee']	 = 0; // Комиссия системы при зачислении
		        $data['id']				 = $item->id;
		        
		        if($item->min_buy > 0) {
					$data['minPayIn']	 = sprintf($currency_from_pattern, $item->min_buy) . ' ' . $item->to_currency_code;
					if($data['amountFrom'] < $item->min_buy) {
						$error = true;
						$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR_SMALL_AMOUNT');
					}
				}
		        if($item->max_buy > 0) {
					$data['maxPayIn']	 = sprintf($currency_from_pattern, $item->max_buy) . ' ' . $item->to_currency_code;
					if($data['amountFrom'] > $item->max_buy) {
						$error = true;
						$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR_BIG_AMOUNT');
					}
				}		        
		        if($item->min_sell > 0) {
					$data['minPayOut']	 = sprintf($currency_from_pattern, $item->min_sell) . ' ' . $item->to_currency_code;
					if($data['amountTo'] < $item->min_sell) {
						$error = true;
						$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR_SMALL_AMOUNT');
					}
				}		        
		        if($item->max_sell > 0) {
					$data['maxPayOut']	 = sprintf($currency_from_pattern, $item->max_sell) . ' ' . $item->to_currency_code;
					if($data['amountTo'] > $item->max_sell) {
						$error = true;
						$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR_BIG_AMOUNT');
					}
				}
			}
			else {
				$error = true;
				$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR');
			}
		}
		else {
			$error = true;
			$message = JText::_('COM_EXCHANGER_EXCHANGE_ERROR');
		} 
		
		$response['error']	 = $error;
		$response['rates']	 = $rates;
		$response['form']	 = $data;
		
		if($message != '')
		{
			$response['message'] = $message;
		}
		
        return $response;
	}
	
	/*protected function addImageClass($image)
	{
		$image_class = '';
		$img_path = JPATH_SITE . DS . $image;
		
		if(JFile::exists($img_path))
		{
			$img_name = JFile::getName($img_path);
			$img_ext = JFile::getExt($img_path);
			$image_class = str_replace('.'.$img_ext, '', $img_name);
		}
		
		return $image_class;
	}*/
}