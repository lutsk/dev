<?php
defined('_JEXEC') or die;

class ExchangerModelRates extends JModelLegacy
{
	public function getCurrency()
	{
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select('DISTINCT parent')
			->from($db->qn('#__abc_currency'));
			$db->setQuery($query);
			$parentMapIds = $db->loadColumn();
			
			foreach ($parentMapIds as $id)
			{
				if ($id > 0)
				{
					$query->clear()
						->select(array(
							$db->qn('m.id'), $db->qn('m.alias'), $db->qn('m.image', 'big_image'), $db->qn('m.'.$name, 'name')
						))
					->from($db->qn('#__abc_multicurrency', 'm'))
					->where($db->qn('m.id') . ' = ' . $id);
					$db->setQuery($query);
					
					$parent = $db->loadObject();
					$parent->parent = 1;
					
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.'.$name, 'name')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					$query->where($db->qn('c.parent') . ' = ' . $parent->id);
					$db->setQuery($query);
					
					$parentList = $db->loadObjectList();
					
					foreach ($parentList as $i => $child)
					{
						$child->alias = $parent->alias;
						$parent->childList[] = $child;
					}
					
					if (count($parent->childList) > 1)
					{
						$list[] = $parent;
					}
					else {
						$list[] = $parent->childList[0];
					}
					
				}
				else {
					$query->clear()
						->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.'.$name, 'name')
						))
					->from($db->qn('#__abc_currency', 'c'));
					
					$query->where($db->qn('c.parent') . ' = 0');
					$db->setQuery($query);
					
					$other = $db->loadObjectList();
					
					foreach ($other as $item)
					{
						$item->alias = JApplication::stringURLSafe($item->name);
						$list[] = $item;
					}
				}
			}
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
	}
		
	public function getList($from = 6, $to = 0, $direction = null)
	{
		$registry = new JRegistry;
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$list = array();
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select(array(
						$db->qn('m.'.$name, 'parent_name'), $db->qn('m.image', 'parent_image'),
						$db->qn('c.id'), $db->qn('c.'.$name, 'name'), $db->qn('c.image'),
						$db->qn('r.currency_from_id'), $db->qn('r.currency_to_id'), $db->qn('r.currency_from_course'), $db->qn('r.currency_to_course')
					))
					->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('c.id') . ' = ' . $db->qn('r.currency_from_id'))
					->join('LEFT', $db->qn('#__abc_multicurrency', 'm') . ' ON ' . $db->qn('m.id') . ' = ' . $db->qn('c.parent'));
					
			$query->from($db->qn('#__abc_rate', 'r'));
			$query->where($db->qn('r.currency_from_id') . ' = ' . $from);
			$query->where($db->qn('r.published') . ' = 1');
			$db->setQuery($query);
			
			$rateList = $db->loadObjectList();
			
			foreach ($rateList as $i => $item)
			{
				$header = new stdClass();
				$header->name = $item->parent_name;
				$header->image = $item->parent_image;
				
				if ($item->parent_name == '') $header->name = $item->name;
				if ($item->parent_image == '') $header->image = $item->image;
				
				$list['header'] = $header;
				$list['rates'][$i] = $item;
			}
			
			return $list;
			
		}
		catch (Exception $e)
		{
			$this->setError($e);
		}
		
		return $list;
		
	}
}