<?php
defined('_JEXEC') or die;

class ExchangerModelWallets extends JModelForm
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_exchanger.wallet', 'wallet', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	
	public function getTable($type = 'wallet', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getWallets()
	{		
		try
		{
			$user_id = JFactory::getUser()->get('id');
			$lang = JFactory::getLanguage()->getTag();
			$name = 'name_' . $lang;
			
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select(array('w.*', $db->qn('c.'.$name, 'name'), $db->qn('c.image')))
					->from($db->qn('#__abc_wallet', 'w'))
					->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('w.currency_id') . ' = ' . $db->qn('c.id'))
					->where($db->qn('w.user_id') . ' = ' . $user_id);
			$db->setQuery($query);
		}
		catch (Exception $e)
		{
			$this->setError($e);
			return array();
		}
		
		return $db->loadObjectList();
	}
	
	public function getList()
	{
		$lang = JFactory::getLanguage()->getTag();
		$name		 = 'name_' . $lang;
		$description = 'description_' . $lang;
		
		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select(array(
							$db->qn('c.id'), $db->qn('c.code'), $db->qn('c.image'), $db->qn('c.wallet'), $db->qn('c.'.$name, 'name')
						))
					->from($db->qn('#__abc_currency', 'c'));
			$db->setQuery($query);
		}
		catch (Exception $e)
		{
			$this->setError($e);
			return array();
		}
		
		return $db->loadObjectList();
	}
	
	public function save($data)
	{
		$table      = $this->getTable();
		
		$key = $table->getKeyName();
		$pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
		$isNew = true;
		
		try
		{
			if ($pk > 0)
			{
				$table->load($pk);
				$isNew = false;
			}
			
			if (!$table->bind($data))
			{
				$this->setError($table->getError());
				
				return false;
			}
			
			if (!$table->check())
			{
				$this->setError($table->getError());
				
				return false;
			}
			
			if (!$table->store())
			{
				$this->setError($table->getError());
				
				return false;
			}
			
			$this->cleanCache();
		}
		catch (\Exception $e)
		{
			$this->setError($e->getMessage());
			
			return false;
		}
		
		if (isset($table->$key))
		{
			$this->setState($this->getName() . '.id', $table->$key);
		}
		
		$this->setState($this->getName() . '.new', $isNew);
		
		return true;
	}
	
	public function delete(&$pk)
	{
		$table = $this->getTable();
		
		if ($table->load($pk))
		{
			/*if ($this->canDelete($table))
			{*/
				if (!$table->delete($pk))
				{
					$this->setError($table->getError());
					return false;
				}
			/*}
			else {
				$error = $this->getError();
				
				if ($error)
				{
					\JLog::add($error, \JLog::WARNING, 'jerror');
					return false;
				}
				else {
					\JLog::add(\JText::_('JLIB_APPLICATION_ERROR_DELETE_NOT_PERMITTED'), \JLog::WARNING, 'jerror');
					return false;
				}
			}*/
		}
		else {
			$this->setError($table->getError());
			return false;
		}
		$this->cleanCache();
		
		return true;
	}
	
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			return JFactory::getUser()->authorise('core.delete', '#__abc_wallet.' . (int)$record->id);
		}
	}
	
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', '#__abc_wallet.' . (int)$record->id);
		}
		elseif (!empty($record->catid)) {
			return $user->authorise('core.edit.state', '#__abc_wallet.' . (int)$record->catid);
		}
		else {
			return parent::canEditState('com_exchanger');
		}
	}
}