<?php
defined( '_JEXEC' ) or die;

class ExchangerSiteHelper
{
	static $menuIds = array();
	
	static function getRoute($option, $view, $query = '')
	{
		if (empty(self::$menuIds[$option . '.' . $view])) {
			$items = JMenuSite::getInstance('site')->getItems('component', $option);
			foreach ( $items as $item ) {
				if ( isset($item->query['view']) && $item->query['view'] === $view) {
					self::$menuIds[$option . '.' . $view] = $item->id;
				}
			}
		}
		return JRoute::_('index.php?view=' . $view . $query . '&Itemid=' . self::$menuIds[$option . '.' . $view]);
	}
	
	static function feeCalculate($amount, $fee)
	{
		$return = new stdClass();
		$return->comission = ($amount * $fee) / 100; // сумма комиссии
		$return->total = $return->comission + $amount; // итоговая общая сумма
		return $return;
	}
	
	static function getCurrencyName($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('name_' . $lang))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	static function getCurrencyTitle($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('name_' . $lang))->from($db->qn('#__abc_multicurrency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	public static function getCurrencyCode($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('code'))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	public static function getCurrencyImage($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('image'))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	public static function getCurrencyParams($currency)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('params'))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}

	/**
	 * Method to get user wallet.
	 * 
	 * @param   integer $currency  Id for the currency
	 * @param   integer $user_id  Id for the user
	 * @param   void $admin  Is admin wallet
	 * 
	 * @return  mixed Object or null
	 */
	
	static function getWallet($currency, $user_id = null, $admin = false)
	{
		try
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			
			if ($admin)
			{
				$query->select(array($db->qn('wallet')))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . $currency);
			}
			else {
				$query->select(array($db->qn('wallet'), $db->qn('note')))->from($db->qn('#__abc_wallet'))->where($db->qn('user_id') . ' = ' . $user_id)->where($db->qn('currency_id') . ' = ' . $currency);
			}
			
			$db->setQuery($query);
			return $db->loadObject();
		}
		catch (Exception $e)
		{
			return false;
		}
	}
	
	static function getUserData($user_id)
	{
		try
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->select('*')->from($db->qn('#__abc_user'))->where($db->qn('user_id') . ' = ' . $user_id);
			
			$db->setQuery($query);
			return $db->loadObject();
		}
		catch (Exception $e)
		{
			return false;
		}
	}
	
	static function getTicketReplies($ticketid)
	{
		try
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->select('COUNT(*)')->from($db->qn('#__js_ticket_replies'))->where($db->qn('ticketid') . ' = ' . $ticketid);
			
			$db->setQuery($query);
			return $db->loadResult();
		}
		catch (Exception $e)
		{
			return false;
		}
	}
	
	/**
	 * User Hash for Сhat.
	*/
	static function getJWT($data, $pass, $dev_id = 0)
	{
		// Create token header as a JSON string
		$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
		 
		if(isset($data['user_id']))
		{
		    $data['user_id'] = (int)$data['user_id'];
		}
		 
		// Create token payload as a JSON string
		$payload = json_encode($data);
		 
		// Encode Header to Base64Url String
		$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
		 
		// Encode Payload to Base64Url String
		$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
		 
		// Create Signature Hash
		$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $pass.$dev_id, true);
		 
		// Encode Signature to Base64Url String
		$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
		 
		// Create JWT
		return trim($base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature);
	}
	
	public static function sort($title, $order, $direction = 'asc', $selected = '', $task = null, $new_direction = 'asc', $tip = '', $form = null)
	{

		$direction = strtolower($direction);
		$index = (int) ($direction === 'desc');

		if ($order != $selected)
		{
			$direction = $new_direction;
		}
		else
		{
			$direction = $direction === 'desc' ? 'asc' : 'desc';
		}

		if ($form)
		{
			$form = ', document.getElementById(\'' . $form . '\')';
		}
		if ($order == $selected)
		{
			$class = ' active';
		}

		$html = '<a href="#" onclick="MainExt.tableOrdering(\'' . $order . '\',\'' . $direction . '\',\'' . $task . '\'' . $form . ');return false;"'
			. ' class="account-table__sorting' . $class . '" title="' . htmlspecialchars(JText::_($tip ?: $title)) . '"'
			. ' >';

		if (isset($title['0']) && $title['0'] === '<')
		{
			$html .= $title;
		}
		else
		{
			$html .= JText::_($title);
		}


		$html .= '</a>';

		return $html;
	}
	
}