<?php
defined('_JEXEC') or die;

class ExchangerControllerWallets extends JControllerForm
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function save($key = null, $urlVar = null)
	{
		$this->checkToken();
		
		$app   = \JFactory::getApplication();
		$model = $this->getModel('wallets', 'ExchangerModel');
		$table = $model->getTable();
		$user	 = JFactory::getUser();
		$data  = $this->input->post->get('jform', array(), 'array');
		$context = "$this->option.edit.$this->context";
		
		if (empty($key))
		{
			$key = $table->getKeyName();
		}
		
		if (empty($urlVar))
		{
			$urlVar = $key;
		}
		
		$recordId = $this->input->getInt($urlVar);
		
		$data[$key] = $recordId;
		$data['user_id'] = (int) $user->get('id');
		$form = $model->getForm($data, false);
		
		if (!$form)
		{
			$app->enqueueMessage($model->getError(), 'error');
			return false;
		}
		
		$validData = $model->validate($form, $data);
		
		$url = 'index.php?option=' . $this->option . '&view=' . $this->view_item;
		
		if ($validData === false)
		{
			$errors = $model->getErrors();
			
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof \Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}
			
			$app->setUserState($context . '.data', $data);
			$this->setRedirect(\JRoute::_($url, false));
			
			return false;
		}
		
		if (!$model->save($validData))
		{
			$app->setUserState($context . '.data', $validData);
			
			$this->setError(\JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(\JRoute::_($url, false));
			
			return false;
		}
		
		$this->setMessage(\JText::_('COM_EXCHANGER_WALLET_SAVE_SUCCESS'));
		$this->setRedirect($url, false);
		
		return true;
	}

	public function delete()
	{
		$this->checkToken('get');
		
		$id = $this->input->getInt('id', 0);
		
		if ($id == 0)
		{
			\JLog::add(\JText::_('COM_EXCHANGER_NO_WALLET_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{
			$model = $this->getModel();
			
			if ($model->delete($id))
			{
				$this->setMessage(\JText::_('COM_EXCHANGER_WALLET_DELETED'));
			}
			else
			{
				$this->setMessage($model->getError(), 'error');
			}
		}
		
		$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item, false));
	}
}