<?php
defined('_JEXEC') or die;

class ExchangerControllerRates extends JControllerLegacy
{
	
	public function exchangeRates()
	{
		$token = \JFactory::getSession()->getFormToken();
		if ($token !== $this->input->get('token', '')) jexit(\JText::_('JINVALID_TOKEN'));
		
		$app			 = JFactory::getApplication('site');
		$classFrom		 = $app->input->getInt('classFrom', 0);
		$classTo		 = $app->input->getInt('classTo', 0);
		$direction		 = $app->input->getWord('activeDirection', 'from');
        $status			 = '<div class="rates"><div class="form-error-block" style="display:block;max-width:100%;">'.JText::_('COM_EXCHANGER_EXCHANGE_EMPTY').'</div></div>';
		
        $model = $this->getModel('rates');
		$view = $this->getView('rates','html');
		
		$data = $model->getList($classFrom, $classTo, $direction);
		
        $view->assignRef('list', $data);
		$res = $view->loadTemplate('ratelist');
		
		if ($res) $status = 200;
		
		$response = array
			(
				/*'classFrom'  => $classFrom,
				'classTo'  => $classTo,*/
				'status'  => $status,
				'res'	  => $res,
				'data'	  => $data
			);
		$response = json_encode($response);
		echo $response;
		jexit();
	}
	
}