<?php
defined('_JEXEC') or die;

class ExchangerControllerExchange extends JControllerLegacy
{
	public function step2()
	{
		$token = \JFactory::getSession()->getFormToken();
		if ($token !== $this->input->get('token', '')) jexit(\JText::_('JINVALID_TOKEN'));
        
		if ($this->task == 'step2') {
			$app = \JFactory::getApplication();
			$data = array();
			
			$from = $this->input->get('currency_from_id', 0);
			$to = $this->input->get('currency_to_id', 0);
			if ($from > 0 && $to > 0)
			{
				$data['from'] = $from;
				$data['to'] = $to;
				$data['exchange_from_amount_input'] = $this->input->get('exchange_from_amount_input', 0);
				$data['exchange_from_to_input'] = $this->input->get('exchange_from_to_input', 0);
				$data['from_title'] = $app->input->get('from_title', '', 'string');
				$data['to_title'] = $app->input->get('to_title', '', 'string');
				
				$app->setUserState('com_exchanger.exchange.step2.data', $data);
				
				$app->redirect(\JRoute::_('index.php?option=com_exchanger&view=exchange&layout=step2'));
			}
		}
		
		$app->redirect(\JRoute::_('index.php?option=com_exchanger&view=exchange'));
	}
	
	public function exchangeReverce()
	{
		$token = \JFactory::getSession()->getFormToken();
		if ($token !== $this->input->get('token', '')) jexit(\JText::_('JINVALID_TOKEN'));
		
        $model = $this->getModel('exchange');
		
		$data = $model->getAjax();
		
		$response = json_encode($data);
		echo $response;
		jexit();
	}
	
}