<?php
defined('_JEXEC') or die;

class ExchangerControllerOrder extends JControllerForm
{
	
	public function save($key = null, $urlVar = null)
	{
		$this->checkToken();
		
		$app   = \JFactory::getApplication();
		$model = $this->getModel('Order', 'ExchangerModel');
		$table = $model->getTable();
		$data  = $this->input->post->get('jform', array(), 'array');
		$context = "$this->option.edit.$this->context";
		$key = $table->getKeyName();
		
		if (empty($urlVar))
		{
			$urlVar = $key;
		}
		
		$recordId = $this->input->getInt($urlVar);
		
		$data[$key] = $recordId; // Populate the row id from the session.
		$data['user'] = $data['surname'].' '.$data['name'];
		
		if (!$this->allowSave($data, $key))
		{
			$this->setError(\JText::_('JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend(), false
				)
			);
			return false;
		}
		
		if (!$model->save($data))
		{
			$app->setUserState($context . '.data', $data);
			$this->setError(\JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);
			return false;
		}
		
		
		//$this->setMessage(\JText::_('COM_EXCHANGER_ORDER_SAVE_SUCCESS'));
		
		$recordId = $model->getState($this->context . '.id');
		$this->holdEditId($context, $recordId);
		$app->setUserState($context . '.data', null);
		$model->checkout($recordId);
		
		$model->sendToCometQL($recordId);
		
		$mail = JFactory::getMailer();
		$config = JFactory::getConfig();
		
		$mailfrom = $app->get('mailfrom');
		$fromname = $app->get('fromname');
		$sitename = $app->get('sitename');
		
        $mail->addRecipient($mailfrom);
		$mail->addReplyTo($mailfrom, $fromname);
		$mail->setSender(array($mailfrom, $fromname));
		$mail->setSubject($sitename . ': Поступила заявка на обмен.');
		$message = '<p>На ресурсе новая заявка на обмен. ID заявки: <strong>'.$recordId.'</strong></p>';
		
        $mail->setBody($message);
        $mail->IsHTML(true);//
        $send = $mail->Send();
		
		$this->setRedirect(
					\JRoute::_(
						'index.php?option=' . $this->option . '&view=' . $this->view_item
						. $this->getRedirectToItemAppend($recordId, $urlVar), false
					)
		);
		
		return true;
	}
	
	protected function allowAdd($data = array())
	{
		$user = JFactory::getUser();
		//return $user->authorise('core.create', 'com_exchanger');
		return true;
	}
}