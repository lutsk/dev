<?php
defined('_JEXEC') or die;

class ExchangerController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = array())
	{
		$this->default_view = 'exchange';
		
		parent::display($cachable, $urlparams);
		return $this;
	}
}