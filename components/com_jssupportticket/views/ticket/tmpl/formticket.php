<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
 + Contact:     www.burujsolutions.com , info@burujsolutions.com
 * Created on:  May 22, 2015
  ^
  + Project:    JS Tickets
  ^
 */
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
JHTML::_('behavior.formvalidation');
$document = JFactory::getDocument();
$document->addScript('administrator/components/com_jssupportticket/include/js/file/file_validate.js');
JText::script('Error file size too large');
JText::script('Error file extension mismatch');
?>
<div class="js-row js-null-margin">
    <?php require_once JPATH_COMPONENT_SITE . '/views/header.php'; 
    $document = JFactory::getDocument();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/inc.css/ticket-formticket.css', 'text/css');
    $language = JFactory::getLanguage();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketresponsive.css');
    if($language->isRTL()){
        $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketdefaultrtl.css');
    }?>

    <script language="javascript">
        function myValidate(f){
            if (document.formvalidator.isValid(f)){
                f.check.value = '<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
            }else{
                alert("<?php echo JText::_('Some values are not acceptable please retry'); ?>");
                return false;
            }
            jQuery('#submit_app_button').attr('disabled',true);
    		f.submit();
            return true;
        }

        function saveticket(formobj){
            var formObjdata = {};
            var inputs = jQuery('#adminForm').serializeArray();
            jQuery.each(inputs, function (i, input) {
                formObjdata[input.name] = input.value;
            });
            var xhr;
            try {
                xhr = new ActiveXObject('Msxml2.XMLHTTP');
            }
            catch (e) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
                catch (e2) {
                    try {
                        xhr = new XMLHttpRequest();
                    }
                    catch (e3) {
                        xhr = false;
                    }
                }
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    jQuery('#message_text').html(xhr.responseText);
                }
            }
            alert(xhr.readyState + " " + xhr.status);
            xhr.open("GET", "index.php?option=com_jssupportticket&c=ticket&task=saveticket&data=" + formobj, true);
            xhr.send(null);
        }
        function getDataForDepandantField(parentf, childf, type) {
            if (type == 1) {
                var val = jQuery("select#" + parentf).val();
            } else if (type == 2) {
                var val = jQuery("input[name=" + parentf + "]:checked").val();
            }
            jQuery.post('index.php?option=com_jssupportticket&c=ticket&task=datafordepandantfield&<?php echo JSession::getFormToken(); ?>=1', {fvalue: val, child: childf}, function (data) {
                if (data) {
                    console.log(data);
                    var d = jQuery.parseJSON(data);
                    jQuery("select#" + childf).replaceWith(d);
                }
            });
        }

        function deleteCutomUploadedFile (field1) {
            jQuery("input#"+field1).val(1);
            jQuery("span."+field1).hide();
            
        }        
    </script>
    <?php 
    if($this->config['offline'] == '1'){
        messagesLayout::getSystemOffline($this->config['title'],$this->config['offline_text']);
    }else{  ?>
        <div id="js-tk-formwrapper">
            <form action="index.php" method="post" name="adminForm" id="adminForm" class="jsticket_form" enctype="multipart/form-data">
                <?php 
                $fieldcounter = 0;
                $i = 0;
                $j = 0;
                foreach($this->fieldsordering AS $field) {
                    switch($field->field){
                        case 'email':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                $readonly = '';
                                if(isset($field->readonly) && $field->readonly == 1){
                                    $readonly = 'readonly';
                                }
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="email"><?php echo JText::_($field->fieldtitle); ?>&nbsp;<font color="red">*</font></label></div>
                                    <div class="js-form-value"><input class="js-form-input-field required validate-email" type="text" name="email" id="email" size="40" maxlength="255" value="<?php if(isset($this->data['email'])) echo $this->data['email']; elseif (isset($this->email)) echo $this->email;  ?>" /></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'fullname':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="name"><?php echo JText::_($field->fieldtitle); ?>&nbsp;<font color="red">*</font></label></div>
                                    <div class="js-form-value"><input class="js-form-input-field required" type="text" name="name" id="name"size="40" maxlength="255" value="<?php if(isset($this->data['name'])) echo $this->data['name']; elseif (isset($this->name)) echo $this->name; ?>" /></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'phone':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="phone"><?php echo JText::_($field->fieldtitle); ?><?php if($field->required == 1) echo ' <span style="color:red;">*</span>'; ?></label></div>
                                    <div class="js-form-value"><input class="js-form-input-field" type="text" name="phone" id="phone" size="40" maxlength="255" value="<?php if(isset($this->data['phone'])) echo $this->data['phone']; ?>" /></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'phoneext':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="phoneext"><?php echo JText::_($field->fieldtitle); ?><?php if($field->required == 1) echo ' <span style="color:red;">*</span>'; ?></label></div>
                                    <div class="js-form-value"><input class="js-form-input-field" type="text" name="phoneext" id="phoneext" size="5" maxlength="255" value="<?php if(isset($this->data['phoneext'])) echo $this->data['phoneext']; ?>" /></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'department':
                            if ($field->published == 1){
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                            <div class="js-form-title"><label for="departmentid"><?php echo JText::_($field->fieldtitle); ?></label></div>
                                    <div class="js-form-value"><?php echo $this->lists['departments']; ?></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'priority':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-6 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="priorityid"><?php echo JText::_($field->fieldtitle); ?>&nbsp;<font color="red">*</font></label></div>
                                    <div class="js-form-value"><?php echo $this->lists['priorities']; ?></div>
                                </div>
                                <?php
                            }
                            break;
                        case 'subject':
                            if ($field->published == 1) {
                                if($fieldcounter % 2 == 0){
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-12 js-col-xs-12  js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="subject"><?php echo JText::_($field->fieldtitle); ?>&nbsp;<font color="red">*</font></label></div>
                                    <div class="js-form-value"><input class="js-form-input-field required" type="text" name="subject" id="subject" size="40" maxlength="255" value="<?php if(isset($this->data['subject'])) echo $this->data['subject']; ?>" /></div>
                                </div>
                                <?php
                            }
                            break;
                         case 'issuesummary':
                            if ($field->published == 1) {
                                if($fieldcounter != 0){
                                    echo '</div>';
                                }
                                 echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++;
                                ?>
                                <div class="js-col-md-12 js-col-xs-12 js-margin-bottom js-padding-null">
                                    <div class="js-form-title"><label for="issuesummary"><?php echo JText::_($field->fieldtitle); ?>&nbsp;<font color="red">*</font></label></div>
                                    <div class="js-form-value"><?php $editor = JFactory::getEditor(); echo $editor->display('message', '', '550', '300', '60', '20', array('class'=>'required')); ?></div>
                                </div>
                                <?php  
                            break;
                        case 'attachments':
                            $flag = true;
                            if($flag){
                                if ($field->published == 1) {
                                    if($fieldcounter != 0){
                                        echo '</div>';
                                    }
                                    echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                }
                                $fieldcounter++; ?>
                                <div class="js-col-md-12 js-col-xs-12 js-margin-bottom js-padding-null js-attachment-wrp">
                                    <div class="js-form-title"><?php echo JText::_($field->fieldtitle); ?></div>
                                    <div class="js-form-value js-attachment-files-wrp">
                                        <div id="js-attachment-files" class="js-attachment-files">
                                            <span class="js-attachment-file-box">
                                                <input type="file" class="js-form-input-field-attachment" name="filename[]" onchange="uploadfile(this, '<?php echo $this->config["filesize"]; ?>', '<?php echo $this->config["fileextension"]; ?>');" size="20" maxlenght='30'/>
                                                <span class='js-attachment-remove'></span>
                                            </span>
                                        </div>
                                        <div id="js-attachment-option">
                                            <?php echo JText::_('Maximum File Size') . ' (' . $this->config['filesize']; ?>KB)<br><?php echo JText::_('File Extension Type') . ' (' . $this->config['fileextension'] . ')'; ?></small>
                                        </div>
                                        <span id="js-attachment-add"><?php echo JText::_('Add Files'); ?></span>
                                    </div>
                                </div>
                            <?php }
                            break;
                            default:
                                $params = NULL;
                                $id = NULL;
                                $isadmin = false;
                                if(isset($this->editticket)){
                                    $id = $this->editticket->id; 
                                    $params = $this->editticket->params; 
                                }elseif(isset($this->plg_params)){
                                    $id = 999;
                                    $params = $this->plg_params;
                                }
                                switch ($field->size) {
                                    case '100':
                                        if($fieldcounter != 0){
                                            echo '</div>';
                                            $fieldcounter = 0;
                                        }
                                        echo getCustomFieldClass()->formCustomFields($field , $id , $params , $isadmin);
                                    break;
                                    case '50':
                                        if($fieldcounter % 2 == 0){
                                            if($fieldcounter != 0){
                                                echo '</div>';
                                            }
                                            echo '<div class="js-col-md-12 js-form-wrapper js-padding-null">';
                                        }
                                        $fieldcounter++;
                                        echo getCustomFieldClass()->formCustomFields($field , $id , $params , $isadmin );
                                    break;
                                }
                                break;
                            }
                        }
                        if($fieldcounter != 0){
                            echo '</div>'; // close extra div open in user field
                        }
                        if(isset($this->ticket)){
                            if (($this->ticket->created == '0000-00-00 00:00:00') || ($this->ticket->created == ''))
                                $curdate = date('Y-m-d H:i:s');
                            else
                                $update = date('Y-m-d H:i:s');
                        }else{
                            $curdate = date('Y-m-d H:i:s'); ?>
                            <input type="hidden" name="update" value="<?php if (isset($update)) echo $update; else echo ""; ?>" /> <?php
                        } ?>
                    
                        <input type="hidden" name="created" value="<?php echo $curdate; ?>" />
                        <input type="hidden" name="view" value="ticket" />
                        <input type="hidden" name="c" value="ticket" />
                        <input type="hidden" name="layout" value="formticket" />
                        <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                        <input type="hidden" name="task" value="saveticket" />
                        <input type="hidden" name="check" value="" />
                        <input type="hidden" name="status" value="0" />
                        <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
                        <input type="hidden" name="id" value="<?php if (isset($this->ticket)) echo $this->ticket->id; ?>" />
                        <?php echo JHtml::_('form.token'); ?>
                     
                        <div class="js-form-submit-btn-wrp">
                            <input class="js-save-button" type="submit" onclick="return myValidate(document.adminForm);"  name="submit_app" id="submit_app_button" value="<?php echo JText::_('Submit Ticket'); ?>" />
                        </div>

            </form>
        </div>
        <script language="Javascript" >
            jQuery("#js-attachment-add").click(function () {
            var obj = this;
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            var append_text = "<span class='js-attachment-file-box'><input name='filename[]' class='js-form-input-field-attachment' type='file' onchange=uploadfile(this,'<?php echo $this->config['filesize']; ?>','<?php echo $this->config['fileextension']; ?>'); size='20' maxlenght='30' /><span  class='js-attachment-remove'></span></span>";
            if (current_files < total_allow) { 
                jQuery(".js-attachment-files").append(append_text);
            } else if ((current_files === total_allow) || (current_files > total_allow)) {
                alert("<?php echo JText::_('File upload limit exceed'); ?>");
                jQuery(obj).hide();
            }
        });

        jQuery(document).delegate(".js-attachment-remove", "click", function (e) {
            var current_files = jQuery('input[name="filename[]"]').length;
            if(current_files!=1)
                jQuery(this).parent().remove();
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            if (current_files < total_allow) {
                jQuery("#js-attachment-add").show();
            }
        });

        </script>
            <?php 
        } ?>

    <div id="js-tk-copyright">
        <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
        &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
    </div>
</div>
