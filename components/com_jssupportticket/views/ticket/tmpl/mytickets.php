<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
 + Contact:     www.burujsolutions.com , info@burujsolutions.com
 * Created on:  May 22, 2015
  ^
  + Project:    JS Tickets
  ^
 */
defined('_JEXEC') or die('Restricted access');
$dash = '-';
$dateformat = $this->config['date_format'];
$firstdash = strpos($dateformat, $dash, 0);
$firstvalue = substr($dateformat, 0, $firstdash);
$firstdash = $firstdash + 1;
$seconddash = strpos($dateformat, $dash, $firstdash);
$secondvalue = substr($dateformat, $firstdash, $seconddash - $firstdash);
$seconddash = $seconddash + 1;
$thirdvalue = substr($dateformat, $seconddash, strlen($dateformat) - $seconddash);
$js_dateformat = '%' . $firstvalue . $dash . '%' . $secondvalue . $dash . '%' . $thirdvalue;

?>
<div class="js-row js-null-margin">
<?php

if ($this->config['offline'] == '1') {
    messagesLayout::getSystemOffline($this->config['title'],$this->config['offline_text']);
}else{ 
    require_once JPATH_COMPONENT_SITE . '/views/header.php';
    $document = JFactory::getDocument();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/status_graph.css', 'text/css');
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/inc.css/ticket-mytickets.css', 'text/css');
     $language = JFactory::getLanguage();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketresponsive.css');
    if($language->isRTL()){
        $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketdefaultrtl.css');
    }?>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //jQuery('.custom_date').datepicker({dateFormat: 'yy-mm-dd'});
            var combinesearch = "<?php echo isset($this->filter_data['iscombinesearch']) ? $this->filter_data['iscombinesearch'] : ''; ?>";
            if (combinesearch) {
                doVisible();
                jQuery("#js-filter-wrapper-toggle-area").show();
            }
            jQuery("#js-filter-wrapper-toggle-btn").click(function () {
                if (jQuery("#js-filter-wrapper-toggle-search").is(":visible")) {
                    doVisible();
                } else {
                    jQuery("#js-filter-wrapper-toggle-search").show();
                    jQuery("#js-filter-wrapper-toggle-ticketid").hide();
                    jQuery("#js-filter-wrapper-toggle-minus").hide();
                    jQuery("#js-filter-wrapper-toggle-plus").show();
                }
                jQuery("#js-filter-wrapper-toggle-area").toggle();
            });
            function doVisible() {
                jQuery("#js-filter-wrapper-toggle-search").hide();
                jQuery("#js-filter-wrapper-toggle-ticketid").show();
                jQuery("#js-filter-wrapper-toggle-minus").show();
                jQuery("#js-filter-wrapper-toggle-plus").hide();
            }
        });
        function getDataForDepandantField(parentf, childf, type) {
            if (type == 1) {
                var val = jQuery("select#" + parentf).val();
            } else if (type == 2) {
                var val = jQuery("input[name=" + parentf + "]:checked").val();
            }
            jQuery.post('index.php?option=com_jssupportticket&c=userfields&task=datafordepandantfield&<?php echo JSession::getFormToken(); ?>=1', {fvalue: val, child: childf}, function (data) {
                if (data) {
                    console.log(data);
                    var d = jQuery.parseJSON(data);
                    jQuery("select#" + childf).replaceWith(d);
                }
            });
        }
    </script>
    
    <!-- Top Circle Count Boxes -->
        <div class="js-row js-ticket-top-cirlce-count-wrp">
            <?php if($this->ticketinfo['mytickets'] != 0){
                $open_percentage        =  round(($this->ticketinfo['open'] / $this->ticketinfo['mytickets']) * 100);
                $close_percentage       =  round(($this->ticketinfo['close'] / $this->ticketinfo['mytickets']) * 100);
                $answered_percentage    =  round(($this->ticketinfo['isanswered'] / $this->ticketinfo['mytickets']) * 100);}
                
                if($this->ticketinfo['mytickets'] != 0){
                    $allticket_percentage = 100;
                }else{
                    $allticket_percentage = 0;
                } ?>
            <div class="js-col-xs-12 js-col-md-2 js-myticket-link js-ticket-myticket-link-myticket">
                <a class="js-ticket-green js-myticket-link <?php echo $open; ?>" <?php if ($this->lt == 1) echo 'class="selected"'; ?> href="index.php?option=com_jssupportticket&c=ticket&layout=mytickets<?php echo htmlspecialchars('&lt'); ?>=1<?php echo "&sortby=".$this->sortlinks['sorton']. strtolower($this->sortlinks['sortorder'])."&Itemid=".$this->Itemid; ?>">
                    <div class="js-ticket-cricle-wrp" data-per="<?php echo $open_percentage; ?>" >
                        <div class="js-mr-rp" data-progress="<?php echo $open_percentage; ?>">
                            <div class="circle">
                                <div class="mask full">
                                     <div class="fill js-ticket-open"></div> 
                                </div> 
                                <div class="mask half"> 
                                    <div class="fill js-ticket-open"></div> 
                                    <div class="fill fix"></div> 
                                </div> 
                                <div class="shadow"></div> 
                            </div> 
                            <div class="inset"> 
                            </div>
                        </div>
                    </div>
                    <span class="js-ticket-circle-count-text js-ticket-green">
                        <?php 
                            echo JText::_('Open');
                            if($this->config['show_count_tickets'] == 1)
                            echo " ( " . $this->ticketinfo['open'] . " ) "; 
                        ?>
                    </span>
                </a>
            </div>
            <div class="js-col-xs-12 js-col-md-2 js-myticket-link js-ticket-myticket-link-myticket">
                <a class="js-ticket-red js-myticket-link <?php echo $answered; ?>" <?php if ($this->lt == 4) echo 'class="selected"'; ?> href="index.php?option=com_jssupportticket&c=ticket&layout=mytickets<?php echo htmlspecialchars('&lt'); ?>=4<?php echo "&sortby=".$this->sortlinks['sorton']. strtolower($this->sortlinks['sortorder'])."&Itemid=".$this->Itemid; ?>">
                    <div class="js-ticket-cricle-wrp" data-per="<?php echo $close_percentage; ?>" >
                        <div class="js-mr-rp" data-progress="<?php echo $close_percentage; ?>">
                            <div class="circle">
                                <div class="mask full">
                                     <div class="fill js-ticket-close"></div> 
                                </div> 
                                <div class="mask half"> 
                                    <div class="fill js-ticket-close"></div> 
                                    <div class="fill fix"></div> 
                                </div> 
                                <div class="shadow"></div> 
                            </div> 
                            <div class="inset"> 
                            </div>
                        </div>
                    </div>
                    <span class="js-ticket-circle-count-text js-ticket-red">
                        <?php 
                            echo JText::_('Close');
                            if($this->config['show_count_tickets'] == 1)
                            echo " ( " . $this->ticketinfo['close'] . " ) "; 
                        ?>
                    </span>
                </a>
            </div>
            <div class="js-col-xs-12 js-col-md-2 js-myticket-link js-ticket-myticket-link-myticket">
                <a class="js-ticket-pink js-myticket-link <?php echo $overdue; ?>" <?php if ($this->lt == 3) echo 'class="selected"'; ?> href="index.php?option=com_jssupportticket&c=ticket&layout=mytickets<?php echo htmlspecialchars('&lt'); ?>=3<?php echo "&sortby=".$this->sortlinks['sorton']. strtolower($this->sortlinks['sortorder'])."&Itemid=".$this->Itemid; ?>">
                    <div class="js-ticket-cricle-wrp" data-per="<?php echo $answered_percentage; ?>" >
                        <div class="js-mr-rp" data-progress="<?php echo $answered_percentage; ?>">
                            <div class="circle">
                                <div class="mask full">
                                     <div class="fill js-ticket-answer"></div> 
                                </div> 
                                <div class="mask half"> 
                                    <div class="fill js-ticket-answer"></div> 
                                    <div class="fill fix"></div> 
                                </div> 
                                <div class="shadow"></div> 
                            </div> 
                            <div class="inset"> 
                            </div>
                        </div>
                    </div>
                    <span class="js-ticket-circle-count-text js-ticket-pink">
                        <?php 
                            echo JText::_('Answer');
                            if($this->config['show_count_tickets'] == 1)
                            echo " ( " . $this->ticketinfo['isanswered'] . " ) "; 
                        ?>
                    </span>
                </a>
            </div>
            <div class="js-col-xs-12 js-col-md-2 js-myticket-link js-ticket-myticket-link-myticket">
                <a class="js-ticket-blue js-myticket-link <?php echo $myticket; ?>" <?php if ($this->lt == 5) echo 'class="selected"'; ?> href="index.php?option=com_jssupportticket&c=ticket&layout=mytickets<?php echo htmlspecialchars('&lt'); ?>=5<?php echo "&sortby=".$this->sortlinks['sorton']. strtolower($this->sortlinks['sortorder'])."&Itemid=".$this->Itemid; ?>">
                    <div class="js-ticket-cricle-wrp" data-per="<?php echo $allticket_percentage; ?>">
                        <div class="js-mr-rp" data-progress="<?php echo $allticket_percentage; ?>">
                            <div class="circle">
                                <div class="mask full">
                                     <div class="fill js-ticket-allticket"></div> 
                                </div> 
                                <div class="mask half"> 
                                    <div class="fill js-ticket-allticket"></div> 
                                    <div class="fill fix"></div> 
                                </div> 
                                <div class="shadow"></div> 
                            </div> 
                            <div class="inset"> 
                            </div>
                        </div>
                    </div>
                    <span class="js-ticket-circle-count-text js-ticket-blue">
                        <?php 
                            echo JText::_('All Tickets');
                            if($this->config['show_count_tickets'] == 1)
                            echo " ( " . $this->ticketinfo['mytickets'] . " ) "; 
                        ?>
                    </span>
                </a>
            </div>
        </div>

    <!-- Search Portion -->
        <div class="js-combine-search-wrapper">
            <div class="js-heading-wrp"><?php echo JText::_('Search'); ?></div>
            <div class="js-combine-search-form-wrp">
                <form class="js-tk-combinesearch" action="index.php?option=com_jssupportticket&c=ticket&layout=mytickets&lt=<?php echo $this->lt; ?>&Itemid=<?php echo $this->Itemid; ?>" method="post" name="adminForm" id="adminForm">
                    <div class="js-filter-wrapper">    
                        <div class="js-filter-form-fields-wrp" id="js-filter-wrapper-toggle-search">
                            <input type="text" name="filter_ticketsearchkeys" id="filter_ticketsearchkeys" value="<?php echo isset($this->filter_data['searchkeys']) ? $this->filter_data['searchkeys'] : ''; ?>" class="js-ticket-input-field" placeholder="<?php echo JText::_('Ticket ID').' '.JText::_('Or').' '.JText::_('Email Address').' '.JText::_('Or').' '.JText::_('Subject'); ?>"/>
                        </div>
                        <div class="js-filter-form-fields-wrp" id="js-filter-wrapper-toggle-ticketid">
                            <input type="text" name="filter_ticketid" id="filter_ticketid" value="<?php if (isset($this->filter_data['ticketid'])) echo $this->filter_data['ticketid']; ?>" class="js-ticket-input-field" placeholder="<?php echo JText::_('Ticket ID'); ?>" />
                        </div>
                        <div id="js-filter-wrapper-toggle-btn">
                            <div id="js-filter-wrapper-toggle-plus">
                                <img class="js-toggle-img" src="components/com_jssupportticket/include/images/plus.png" />
                            </div> 
                            <div id="js-filter-wrapper-toggle-minus">
                                <img class="js-toggle-img" src="components/com_jssupportticket/include/images/minus.png" />
                            </div>
                        </div>
                        <div id="js-filter-wrapper-toggle-area">
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <input type="text" name="filter_from" id="filter_from" class="js-ticket-input-field" value="<?php if (isset($this->filter_data['from'])) echo $this->filter_data['from']; ?>" placeholder="<?php echo JText::_('From'); ?>" />
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <input type="text" name="filter_email" id="filter_email" class="js-ticket-input-field" value="<?php if (isset($this->filter_data['email'])) echo $this->filter_data['email']; ?>" placeholder="<?php echo JText::_('Email'); ?>" />
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <?php echo $this->lists['departments']; ?>
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <?php echo $this->lists['priorities']; ?>
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <input type="text" name="filter_subject" id="filter_subject" class="js-ticket-input-field" value="<?php if (isset($this->filter_data['subject'])) echo $this->filter_data['subject']; ?>" placeholder="<?php echo JText::_('Subject'); ?>" />
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <?php echo JHTML::_('calendar', isset($this->filter_data['datestart']) ? $this->filter_data['datestart'] : '', 'filter_datestart', 'filter_datestart', $js_dateformat, array('class' => 'js-ticket-input-field', 'size' => '10', 'maxlength' => '19' , 'placeholder' => JText::_('Start Date'))); ?>
                            </div>
                            <div class="js-col-md-12 js-filter-field-wrp">
                                <?php echo JHTML::_('calendar', isset($this->filter_data['dateend']) ? $this->filter_data['dateend'] : '', 'filter_dateend', 'filter_dateend', $js_dateformat, array('class' => 'js-ticket-input-field', 'size' => '10', 'maxlength' => '19' , 'placeholder' => JText::_('End Date'))); ?>
                            </div>
                            <?php $params = null;
                                if(isset($this->filter_data['params'])){
                                    $params = $this->filter_data['params'];
                                }
                                $k = 1;
                                $customfields = getCustomFieldClass()->userFieldsForSearch(1);
                                if(!empty($customfields)){
                                    foreach ($customfields as $field) {
                                        getCustomFieldClass()->formCustomFieldsForSearch($field, $k, $params);
                                    }
                                    echo '</div>'; // last div close on the user fields
                                }?>
                        </div>
                        <div class="js-filter-button-wrp">
                            <button class="js-ticket-filter-button js-ticket-search-btn" onclick="this.form.submit();"><?php echo JText::_('Search'); ?></button>
                            <button class="js-ticket-filter-button js-ticket-reset-btn" onclick="resetJsForm();this.form.submit();"><?php echo JText::_('Reset'); ?></button>
                        </div>
                    </div>
                </form> 
            </div>
        </div>
        <!-- Sorting Portion -->
        <?php
            $link = 'index.php?option=com_jssupportticket&c=ticket&layout=mytickets&email=' .htmlspecialchars("&lt").'=' . $this->lt . '&Itemid=' . $this->Itemid;
            if ($this->sortlinks['sortorder'] == 'ASC')
                $img = "components/com_jssupportticket/include/images/sort1.png";
            else
                $img = "components/com_jssupportticket/include/images/sort2.png";
        ?>
        <div id="js-tk-sort-wrapper">
            <ul id="js-tk-sort-manu">
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'subject') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['subject']; ?>"><?php echo JText::_('Subject'); ?><?php if ($this->sortlinks['sorton'] == 'subject') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'priority') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['priority']; ?>"><?php echo JText::_('Priority'); ?><?php if ($this->sortlinks['sorton'] == 'priority') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'ticketid') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['ticketid']; ?>"><?php echo JText::_('Ticket ID'); ?><?php if ($this->sortlinks['sorton'] == 'ticketid') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'answered') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['answered']; ?>"><?php echo JText::_('Answered'); ?><?php if ($this->sortlinks['sorton'] == 'answered') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'status') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['status']; ?>"><?php echo JText::_('Status'); ?><?php if ($this->sortlinks['sorton'] == 'status') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
                <li class="js-tk-sort-manulink">
                    <a class="<?php if ($this->sortlinks['sorton'] == 'created') echo 'selected'; ?>" href="<?php echo $link ?>&sortby=<?php echo $this->sortlinks['created']; ?>"><?php echo JText::_('Created'); ?><?php if ($this->sortlinks['sorton'] == 'created') { ?> <img src="<?php echo $img; ?>"> <?php } ?></a>
                </li>
            </ul>
        </div>
    <?php
    if (!(empty($this->result)) && is_array($this->result)) {
        $rows = $this->result;
        $no = 1;
        global $row;
        $trclass = array("odd", "even");
        $k = 0;
        foreach ($rows as $row) {
            $link = JFilterOutput::ampReplace('index.php?option=com_jssupportticket&c=ticket&layout=ticketdetail&id=' . $row->id. '&Itemid=' . $this->Itemid);
            ?>
            <div id="js-tk-wrapper">
                <div class="js-col-xs-2 js-col-md-2 js-icon">
                    <img class="js-ticket-icon-img" src="components/com_jssupportticket/include/images/ticketman.png">
                </div>
                <div class="js-col-xs-6 js-col-md-6 js-middle">
                    <div class="js-col-md-12 js-wrapper js-tk-textwrap"><span class="js-tk-title"><?php echo JText::_('Subject'); ?><font>:</font> </span><span class="js-tk-value"><a href="<?php echo $link; ?>"> <?php echo $row->subject; ?></a></span></div>
                    <div class="js-col-md-12 js-wrapper"><span class="js-tk-title"><?php echo JText::_('From'); ?><font>:</font></span><span class="js-tk-value"><?php echo $row->name; ?></span></div>
                    <div class="js-col-md-12 js-tk-preletive js-wrapper">
                        <div class="js-tk-pabsolute">
                            <?php if ($row->ticketviaemail == 1) { ?>
                                <span style="background-color: #0066CC;"><?php echo JText::_('Ticket via email'); ?></span>
                            <?php } ?>
                            <?php
                            $counter = 'one';
                            if ($row->lock == 1) { ?>
                                <img class="ticketstatusimage <?php echo $counter;$counter = 'two'; ?>" src="<?php echo JURI::root(); ?>components/com_jssupportticket/include/images/lockstatus.png" title="<?php echo JText::_('Ticket Is Locked'); ?>" />
                            <?php } ?>
                            <?php if ($row->isoverdue == 1) { ?>
                                <img class="ticketstatusimage <?php echo $counter; ?>" src="<?php echo JURI::root(); ?>components/com_jssupportticket/include/images/mark_over_due.png" title="<?php echo JText::_('Ticket mark overdue'); ?>" />
                            <?php } ?>
                            <?php if ($row->status == 0) { ?>
                                <span style="background-color: #9ACC00;"><?php echo JText::_('New'); ?></span>
                            <?php } elseif ($row->status == 1) { ?>
                                <span style="background-color: orange;"><?php echo JText::_('Waiting reply'); ?></span>
                            <?php } elseif ($row->status == 2) { ?>
                                <span style="background-color: #FF7F50;"><?php echo JText::_('In progress'); ?></span>
                            <?php } elseif ($row->status == 3) { ?>
                                <span style="background-color: #507DE4;"><?php echo JText::_('Replied'); ?></span>
                            <?php } elseif ($row->status == 4) { ?>
                                <span style="background-color: #CB5355;"><?php echo JText::_('Closed'); ?></span>
                            <?php } elseif ($row->status == 5){ ?>
                                <span style="background-color: #ee1e22;"><?php echo JText::_('Close due to Merge'); ?></span>
                            <?php } ?> 
                        </div>
                        <span class="js-tk-title"><?php echo JText::_('Department'); ?><font>:</font></span>
                        <span class="js-tk-value"><?php echo $row->departmentname; ?></span>
                    </div>
                    <?php
                        $customfields = getCustomFieldClass()->userFieldsData(1, 1);
                        if(!empty($customfields)){
                            foreach ($customfields as $field) {
                                echo getCustomFieldClass()->showCustomFields($field,1, $row->params , $row->id);
                            }
                        }
                    ?>
                </div>
                <div class="js-col-xs-4 js-col-md-4 js-right">
                    <div class="js-col-md-12 js-wrapper"><span class="js-col-xs-6 js-col-md-6 js-tk-title"><?php echo JText::_('Ticket ID'); ?></span><span class="js-col-xs-6 js-col-md-6 js-tk-value"> <?php echo $row->ticketid; ?></span></div>
                    <div class="js-col-md-12 js-wrapper"><span class="js-col-xs-6 js-col-md-6 js-tk-title"><?php echo JText::_('Last Reply'); ?></span><span class="js-col-xs-6 js-col-md-6 js-tk-value"><?php if ($row->lastreply == '' || $row->lastreply == '0000-00-00 00:00:00') echo JText::_('No reply'); else echo JHtml::_('date',$row->lastreply,$this->config['date_format']); ?></span></div>
                    <div class="js-col-md-12 js-wrapper"><span class="js-col-xs-6 js-col-md-6 js-tk-title"><?php echo JText::_('Priority'); ?></span><span class="js-col-xs-6 js-col-md-6 js-tk-value js-ticket-priorty-box" style="background:<?php echo $row->prioritycolour; ?>;color:#fff;"><?php echo JText::_($row->priority); ?></span></div>
                </div>
            </div> 
        <?php } ?>
        <form action="<?php echo JRoute::_('index.php?option=com_jssupportticket&c=ticket&layout=mytickets&lt='.$this->lt.'&Itemid=' . $this->Itemid); ?>" method="post">
            <div id="jl_pagination" class="pagination">
                <div id="jl_pagination_pageslink">
                    <?php echo $this->pagination->getPagesLinks(); ?>
                </div>
                <div id="jl_pagination_box">
                    <?php   
                        echo JText::_('Display #');
                        echo $this->pagination->getLimitBox();
                    ?>
                </div>
                <div id="jl_pagination_counter">
                    <?php echo $this->pagination->getResultsCounter(); ?>
                </div>
            </div>
        </form>

    <?php } else {
        messagesLayout::getRecordNotFound();
        } ?>

    <?php } ?>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            /*
            var classes = ".tk_mt_detail_status_reply_cus, .tk_mt_detail_status_reply_staff ";
            jQuery(classes).on("mouseover", function () {

                var wait_text = "";
                var current_class = jQuery(this).attr('class');
                if (current_class === "tk_mt_detail_status tk_mt_detail_status_reply_cus") {
                    wait_text = "<?php echo JText::_('Waiting your reply'); ?>";
                } else if (current_class === "tk_mt_detail_status tk_mt_detail_status_reply_staff") {
                    wait_text = "<?php echo JText::_('Waiting Staff Reply'); ?>";
                }
                jQuery(this).text(wait_text);
                jQuery(this).stop().animate({width: '150px'}, 500);
            });
            jQuery(classes).on("mouseout", function () {
                var wait_text = "<?php echo JText::_('Waiting'); ?>...";
                jQuery(this).text(wait_text);
                jQuery(this).stop().animate({width: '70px'}, 500);
            });


            jQuery('div.tk_mt_create_info').on("mouseover", function () {

                var obj = this;
                var scrollingWidth = jQuery(obj).find('span.tk_mt_create_info_text span.tk_mt_detail_text_sliding').width();
                scrollingWidth = scrollingWidth + 10;
                var initialOffset = jQuery(obj).find('span.tk_mt_create_info_text span.tk_mt_detail_text_sliding').offset().left;
                stopAnim = false;
                animateTitle(obj, scrollingWidth, initialOffset);
            });
            jQuery('div.tk_mt_create_info').on("mouseout", function () {
                obj = this;
                stopAnim = true;
                jQuery(obj).find('span.tk_mt_create_info_text span.tk_mt_detail_text_sliding').stop(true, true).css("left", "0");
            });

            var stopAnim = false;
            function animateTitle(obj, scrollingWidth, initialOffset) {
                if (!stopAnim) {
                    var $span = jQuery(obj).find('span.tk_mt_create_info_text span.tk_mt_detail_text_sliding');

                    var parent_div_width = jQuery(obj).width();

                    var child_div = jQuery(obj).find('span.tk_mt_create_info_text span.tk_mt_detail_text_clr').width();

                    var scroll_div_width = parent_div_width - child_div;

                    var animatewidth = (jQuery($span).width()) / 4;

                    if ((animatewidth < scroll_div_width)) {
                        $span.animate({left: (($span.offset().left === (scrollingWidth + initialOffset)) ? -initialOffset : ("-=" + animatewidth))},
                        {
                            duration: 6000,
                            easing: 'swing',
                            complete: function () {
                                if ($span.offset().left < scroll_div_width) {
                                    jQuery(this).css("left", scrollingWidth);
                                }
                            }
                        });
                    }
                }
            }
            */
        });


    </script>
<div id="js-tk-copyright">
    <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
    &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
</div>
</div>

<script type="text/javascript">
    function resetJsForm(){
        var form = jQuery('form#adminForm');
        form.find("input[type=text], input[type=email], input[type=password], textarea").val("");
        form.find('input:checkbox').removeAttr('checked');
        form.find('select').prop('selectedIndex', 0);
        form.find('input[type="radio"]').prop('checked', false);
        jQuery("<input type='hidden' value='1' />")
         .attr("id", "jsresetbutton")
         .attr("name", "jsresetbutton")
         .appendTo(form);
    }
</script>
