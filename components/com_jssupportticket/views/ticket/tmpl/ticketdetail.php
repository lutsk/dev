<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.formvalidation');
JHTML::_('behavior.modal');
$document = JFactory::getDocument();
$document->addScript('administrator/components/com_jssupportticket/include/js/file/file_validate.js');

JText::script('Error file size too large');
JText::script('Error file extension mismatch');
?>
<div class="js-row js-null-margin">
    <?php require_once JPATH_COMPONENT_SITE . '/views/header.php';
    $document = JFactory::getDocument();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/inc.css/ticket-ticketdetail.css', 'text/css');
    $language = JFactory::getLanguage();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketresponsive.css');
    if($language->isRTL()){
        $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketdefaultrtl.css');
    }?> 
    <script language="javascript">
        function isTinyMCE(){
            is_tinyMCE_active = false;
            if (typeof(tinyMCE) != "undefined") {
                if(tinyMCE.editors.length > 0){
                    is_tinyMCE_active = true;
                }
            }
            return is_tinyMCE_active;
        }
        function myValidate(f) {
            var chk_reopen = jQuery('input#isreopen').val();
            if (document.formvalidator.isValid(f)) {
                if (chk_reopen == "") {
                    var msg_obj = jQuery("#message-required");
                    if (typeof msg_obj !== 'undefined' && msg_obj !== null) {
                        var msg_required_val = jQuery("#message-required").val();
                        if (msg_required_val != '') {
                            if(isTinyMCE()){
                                var message = tinyMCE.get('message').getContent();
                            }else{
                                var message = jQuery('textarea#message').val();
                            }
                            if (message == '') {
                                alert("<?php echo JText::_('Some values are not acceptable please retry'); ?>");
                                if(isTinyMCE()){
                                    tinyMCE.get('message').focus();
                                }else{
                                    jQuery('textarea#message').focus();
                                }
                                return false;
                            }
                        }
                    }
                } else if (chk_reopen == 1) {
                    var msg_ro_obj = jQuery("#reopen-message-required");
                    if (typeof msg_ro_obj !== 'undefined' && msg_ro_obj !== null) {
                        var msg_ro_required_val = jQuery("#reopen-message-required").value;
                        if (msg_ro_required_val != '') {                        
                            if(isTinyMCE()){
                                var message_ro = tinyMCE.get('messages').getContent();
                            }else{
                                var message_ro = jQuery('textarea#messages').val();
                            }
                            if (message_ro == '') {
                                alert("<?php echo JText::_('Some values are not acceptable please retry'); ?>");
                                if(isTinyMCE()){
                                    tinyMCE.get('messages').focus();
                                }else{
                                    jQuery('textarea#messages').focus();
                                }
                                return false;
                            }
                        }
                    }
                }
                f.check.value = '<?php if (JVERSION < 3) echo JUtility::getToken(); else echo JSession::getFormToken(); ?>';//send token
            } else {
                alert("<?php echo JText::_('Some values are not accepatable please retry'); ?>");
                return false;
            }
            return true;
        }
    </script>
    <?php 
    if ($this->config['offline'] == '1') {
        messagesLayout::getSystemOffline($this->config['title'],$this->config['offline_text']);
    } else { ?>
        <?php if(!empty($this->ticket)){ ?>
            <div id="tk_detail_wraper">
                <form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data"  onSubmit="return myValidate(this);">
                    <div id="tk_detail_content_wraper">
                        <?php
                            $overdueindays = $this->config['ticket_overdue_indays'];
                            $autocloseindays = $this->config['ticket_auto_close_indays'];
                            $reopendays = $this->config['ticket_reopen_within_days'];
                            $isautoclose = 0;
                            $isoverdue = 0;
                            $reopenticket = 1;
                        ?>
                        <div class="js-col-md-12 js-ticket-detail-wrapper"> <!-- Ticket Detail Data Top -->
                            <div class="js-ticket-detail-box"><!-- Ticket Detail Box -->
                                <div class="js-ticket-detail-left"><!-- Left Side Image -->
                                    <div class="js-ticket-user-img-wrp">
                                        <img class="js-ticket-staff-img" src="components/com_jssupportticket/include/images/ticketman.png" alt="<?php echo JText::_('New Ticket'); ?>" />
                                    </div>
                                    <div class="js-ticket-user-name-wrp">
                                        <?php echo $this->ticket->name; ?>
                                    </div>
                                    <div class="js-ticket-user-email-wrp">
                                        <?php echo $this->ticket->email; ?>
                                    </div>
                                    <div class="js-ticket-user-email-wrp">
                                        <?php echo $this->ticket->phone; ?>
                                    </div>
                                </div>
                                <div class="js-ticket-detail-right"><!-- Right Side Ticket Data -->
                                    <div class="js-ticket-rows-wrp" >
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                                <?php echo JText::_('Subject'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                               <span class="js-ticket-subject-link"><?php echo $this->ticket->subject; ?></span>
                                            </div>
                                        </div>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                                <?php echo JText::_('Department'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                                <?php echo $this->ticket->departmentname; ?>
                                            </div>
                                        </div>
                                        <?php if($this->ticket->ticketviaemail == 1 && $isstaff){ ?>
                                            <div class="js-ticket-row">
                                                <div class="js-ticket-field-title">
                                                    <?php echo JText::_('Ticket Email'); ?>&nbsp;:
                                                </div>
                                                <div class="js-ticket-field-value">
                                                    <?php echo $this->ticket->ticketviaemail; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                               <?php echo JText::_('Created'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                                <?php
                                                $startTimeStamp = strtotime($this->ticket->created);
                                                $endTimeStamp = strtotime("now");
                                                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                                                $numberDays = $timeDiff / 86400;  // 86400 seconds in one day
                                                // and you might want to convert to integer
                                                $numberDays = intval($numberDays);
                                                if ($numberDays != 0 && $numberDays == 1) {
                                                    $day_text = JText::_('Day');
                                                } elseif ($numberDays > 1) {
                                                    $day_text = JText::_('Days');
                                                } elseif ($numberDays == 0) {
                                                    $day_text = JText::_('Today');
                                                }
                                                ?>
                                                <?php
                                                if ($numberDays == 0) {
                                                    echo $day_text;
                                                } else {
                                                    echo $numberDays . ' ' . $day_text . ' ';
                                                    echo JText::_('Ago');
                                                }
                                                ?>
                                                <?php echo JHtml::_('date',$this->ticket->created,"d F, Y");
                                                ?>
                                            </div>
                                        </div>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                               <?php echo JText::_('Ticket ID'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                               <?php echo $this->ticket->ticketid; ?>
                                            </div>
                                        </div>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                               <?php echo JText::_('Priority'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value js-ticket-priorty" style="background:<?php echo $this->ticket->prioritycolour; ?>; color:#ffffff;">
                                               <?php echo $this->ticket->priority; ?>
                                            </div>
                                        </div>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                               <?php echo JText::_('Last Reply'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                               <?php if ($this->ticket->lastreply == '' || $this->ticket->lastreply == '0000-00-00 00:00:00') {echo JText::_('No Last Reply'); } else {echo JHtml::_('date',$this->ticket->lastreply,"d F, Y"); } ?>
                                            </div>
                                        </div>
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                                <?php echo JText::_('Due Date'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                               <?php if ($this->ticket->duedate == '' || $this->ticket->duedate == '0000-00-00 00:00:00') echo JText::_('JNone'); else echo JHtml::_('date',$this->ticket->duedate,"d F, Y"); ?>
                                            </div>
                                        </div>
                                        <?php if(isset($this->time_taken)){ ?>
                                            <div class="js-ticket-row">
                                                <div class="js-ticket-field-title">
                                                     <?php echo JText::_('Total Time Taken'); ?>&nbsp;:
                                                </div>
                                                <div class="js-ticket-field-value">
                                                    <?php 
                                                    $time = $this->time_taken;
                                                        $hours = floor($time / 3600);
                                                        $mins = floor($time / 60 % 60);
                                                        $secs = floor($time % 60);
                                                        echo jText::_(''). sprintf('%02d:%02d:%02d', $hours, $mins, $secs); 
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php 
                                    $customfields = getCustomFieldClass()->userFieldsData(1);
                                    if(!empty($customfields)){ ?>
                                        <div class="js-ticket-row">
                                            <?php
                                                foreach ($customfields as $field) {
                                                    $array =  getCustomFieldClass()->showCustomFields($field, 5, $this->ticket->params , $this->ticket->id);
                                                    if(!empty($array)){ ?>
                                                        <div class="js-ticket-row">
                                                            <div class="js-ticket-field-title">
                                                                <?php echo JText::_($array['title']); ?>&nbsp;:
                                                            </div>
                                                            <div class="js-ticket-field-value">
                                                                <?php echo JText::_($array['value']); ?>
                                                            </div>
                                                        </div>

                                                <?php
                                                    }
                                                }
                                            ?>
                                        </div>
                                    <?php } ?>
                                        <!-- Status box -->
                                        <?php
                                            if ($this->ticket->lock == 1) {
                                                $color = "#5bb12f;";
                                                $ticketmessage = JText::_('Lock');
                                            } elseif ($this->ticket->status == 0) {
                                                $color = "#5bb12f;";
                                                $ticketmessage = JText::_('Open');
                                            } elseif ($this->ticket->status == 1) {
                                                $color = "#28abe3;";
                                                $ticketmessage = JText::_('On Waiting');
                                            } elseif ($this->ticket->status == 2) {
                                                $color = "#69d2e7;";
                                                $ticketmessage = JText::_('In Progress');
                                            } elseif ($this->ticket->status == 3) {
                                                $color = "#FFB613;";
                                                $ticketmessage = JText::_('Replied');
                                            } elseif ($this->ticket->status == 4) {
                                                $color = "#ed1c24;";
                                                $ticketmessage = JText::_('Closed');
                                            } elseif ($this->ticket->status == 5) {
                                                $color = "#dc2742;";
                                                $ticketmessage = JText::_('Closed and Merged');
                                            }
                                        ?>
                                        <div class="js-ticket-openclosed-box" style="background-color:<?php echo $color;?>; box-shadow:<?php echo $boxshadow;?>;">
                                            <?php echo $ticketmessage; ?>
                                        </div>
                                    </div>
                                    <div class="js-ticket-right-bottom"><!-- Right Side Bottom Data -->
                                        <div class="js-ticket-row">
                                            <div class="js-ticket-field-title">
                                               <?php echo JText::_('Created By'); ?>&nbsp;:
                                            </div>
                                            <div class="js-ticket-field-value">
                                               <span class="js-ticket-subject-link">
                                                    <?php
                                                        if ($this->ticket->name != "") {
                                                            echo $this->ticket->name;
                                                        } else {
                                                            echo JText::_('Not Created');
                                                        } ?>        
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="js-ticket-action-btn-wrp"> <!-- Ticket Action Button -->
                                <div class="js-ticket-btn-box">
                                    <a class="js-button" href="#" onclick="actioncall('<?php if ($this->ticket->status == 4) echo 8; else echo 3; ?>')">
                                        <?php if ($this->ticket->status == 4){  ?>
                                            <img title="<?php echo JText::_('Reopen Ticket'); ?>" src="components/com_jssupportticket/include/images/reopen.png">
                                            <?php echo JText::_('Reopen'); ?>
                                        <?php }else{ ?>
                                            <img title="<?php echo JText::_('Close Ticket'); ?>" src="components/com_jssupportticket/include/images/close.png">
                                            <?php echo JText::_('Close'); ?>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Ticket Post Replied -->
                        <div class="js-ticket-post-reply-wrapper">
                            <div class="js-ticket-thread-heading"><?php echo JText::_('Ticket Thread'); ?></div> <!-- Heading -->
                            <div class="js-ticket-detail-box js-ticket-post-reply-box"><!-- Ticket Detail Box -->
                                <div class="js-ticket-detail-left js-ticket-white-background"><!-- Left Side Image -->
                                    <div class="js-ticket-user-img-wrp">
                                         <img class="js-ticket-staff-img" src="<?php echo JURI::root(); ?>components/com_jssupportticket/include/images/ticketman.png" />
                                    </div>
                                    <div class="js-ticket-user-name-wrp">
                                        <?php echo $this->ticket->name; ?>
                                    </div>
                                    <div class="js-ticket-user-email-wrp">
                                        <?php echo $this->ticket->email; ?>
                                    </div>
                                </div>
                                <div class="js-ticket-detail-right js-ticket-background"><!-- Right Side Ticket Data -->
                                    <div class="js-ticket-rows-wrapper">
                                       <div class="js-ticket-rows-wrp" >
                                            <div class="js-ticket-row">
                                                <div class="js-ticket-field-value">
                                                   <?php echo $this->ticket->message; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if (isset($this->attachment[0]->filename)) { ?>
                                            <div class="js-ticket-attachments-wrp">
                                                <?php foreach ($this->attachment as $attachment) {
                                                    echo '
                                                        <div class="js_ticketattachment">
                                                            <span class="js-ticket-download-file-title">'
                                                                . $attachment->filename . "&nbsp(" . round($attachment->filesize, 2) . " KB)";
                                                        echo '</span> 
                                                            <a class="js-download-button" target="_blank" href="index.php?option=com_jssupportticket&c=ticket&task=getdownloadbyid&id='.$attachment->attachmentid.'&'. JSession::getFormToken() .'=1">
                                                                <img class="js-ticket-download-img" src="components/com_jssupportticket/include/images/download-all.png">
                                                            </a>  
                                                        </div>';
                                                }?>
                                           </div>
                                        <?php     
                                        } ?>
                                    </div>
                                    <div class="js-ticket-time-stamp-wrp">
                                        <span class="js-ticket-ticket-created-date">
                                            <?php echo JHtml::_('date',$this->ticket->created,"l F d, Y");?>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <?php for ($i = 0, $n = count($this->messages); $i < $n; $i++) {
                                $row = & $this->messages[$i]; ?>
                                <div class="js-ticket-detail-box js-ticket-post-reply-box"><!-- Ticket Detail Box -->
                                    <div class="js-ticket-detail-left js-ticket-white-background"><!-- Left Side Image -->
                                        <div class="js-ticket-user-img-wrp">
                                            <img class="js-ticket-staff-img" src="<?php echo JURI::root(); ?>components/com_jssupportticket/include/images/ticketman.png" />
                                        </div>
                                        <div class="js-ticket-user-name-wrp">
                                           <?php if($row->name) echo $row->name; else echo $this->ticket->name; ?>
                                        </div>
                                    </div>
                                    <div class="js-ticket-detail-right js-ticket-background"><!-- Right Side Ticket Data -->
                                        <div class="js-ticket-rows-wrapper">
                                            <div class="js-ticket-rows-wrp" >
                                                <div class="js-ticket-row">
                                                    <div class="js-ticket-field-value">
                                                        <?php echo $row->message; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $count = $row->count;
                                            if ($count >= 1) {
                                                $outdex = $i + $count;
                                                echo ' <div class="js-ticket-attachments-wrp">';
                                                            for ($j = $i; $j < $outdex; $j++) {
                                                                if ($row->filename && $row->filename <> '') {
                                                                    $datadirectory = $this->config['data_directory'];
                                                                    $path = 'index.php?option=com_jssupportticket&c=ticket&task=getdownloadbyid&id='.$row->attachmentid.'&'.JSession::getFormToken().'=1';
                                                                    echo '  <div class="js_ticketattachment">
                                                                                <span class="js-ticket-download-file-title">'
                                                                                    . $row->filename . "&nbsp(" . round($row->filesize, 2) . " KB)";
                                                                                echo '</span>
                                                                                <a class="js-download-button" target="_blank" href="' . $path . '">
                                                                                    <img class="js-ticket-download-img" src="components/com_jssupportticket/include/images/download-all.png">
                                                                                </a> 
                                                                            </div>';         
                                                                                       
                                                                }
                                                                
                                                            };
                                                            echo ' </div>';
                                                $i = $outdex - 1; 
                                            }?>
                                        </div>
                                        <div class="js-ticket-time-stamp-wrp">
                                            <span class="js-ticket-ticket-created-date">
                                                 <?php echo JHtml::_('date',$row->created,"l F d, Y");?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- Ticket Post Replay -->
                        <?php if ($this->ticket->lock == 0 && $this->ticket->status != 4 && $isautoclose != 1) { ?>
                            <div class="js-ticket-reply-forms-heading"><?php echo JText::_('Reply a Message'); ?></div>
                                <div class="js-ticket-reply-field-wrp">
                                    <div class="js-ticket-reply-field">
                                        <?php
                                            $editor = JFactory::getEditor();
                                            echo $editor->display('message', '', '550', '300', '60', '20', false);
                                        ?>
                                        <input type='hidden' id='message-required' name="message-required" value="<?php echo 'required'; ?>">
                                    </div>
                                </div>
                                <div class="js-attachment-wrp">
                                    <div class="js-form-title"><?php echo JText::_('Attachments'); ?></div>
                                    <div class="js-form-value js-attachment-files-wrp">
                                        <div id="js-attachment-files" class="js-attachment-files">
                                            <span class="js-attachment-file-box">
                                                <input type="file" class="inputbox js-attachment-inputbox js-form-input-field-attachment" name="filename[]" onchange="uploadfile(this, '<?php echo $this->config["filesize"]; ?>', '<?php echo $this->config["fileextension"]; ?>');" size="20" maxlenght='30'/>
                                                <span class='js-attachment-remove'></span>
                                            </span>
                                        </div>
                                        <div id="js-attachment-option">
                                            <?php echo JText::_('Maximum File Size') . ' (' . $this->config['filesize']; ?>KB)<br><?php echo JText::_('File Extension Type') . ' (' . $this->config['fileextension'] . ')'; ?>
                                        </div>
                                        <span id="js-attachment-add"><?php echo JText::_('Add Files'); ?></span>
                                    </div>
                                </div>
                                <div class="js-ticket-reply-form-button-wrp">
                                    <input  class="js-ticket-save-button" type="submit" name="submit_app" value="<?php echo JText::_('Post Reply'); ?>" />
                                </div> 
                        <?php } ?>
                        <input type="hidden" name="created" value="<?php echo $curdate = date('Y-m-d H:i:s'); ?>" />
                        <input type="hidden" name="view" value="ticket" />
                        <input type="hidden" name="c" value="ticket" />
                        <input type="hidden" name="layout" value="ticketdetail" />
                        <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                        <input type="hidden" name="task" value="actionticket" />
                        <input type="hidden" name="check" value="" />
                        <input type="hidden" name="email" value="<?php echo $this->email; ?>" />
                        <input type="hidden" name="callaction" id="callaction" value="" />
                        <input type="hidden" name="callfrom" id="callfrom" value="savemessage" />
                        <input type="hidden" name="isreopen" id="isreopen" value="" />
                        <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
                        <input type="hidden" name="lastreply" value="<?php echo $this->ticket->lastreply; ?>" />
                        <input type="hidden" name="ticketid" value="<?php if (isset($this->ticket)) echo $this->ticket->id; ?>" />
                        <input type="hidden" name="ticketrandomid" value="<?php if (isset($this->ticket)) echo $this->ticket->ticketid; ?>" />
                        <?php echo JHtml::_('form.token'); ?>
                    </div>
                </form>           
            </div>    
    <?php }else{
            messagesLayout::getRecordNotFound();
          }
        } ?>
    <div id="js-tk-copyright">
        <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
        &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
    </div>
</div>
<script language=Javascript>
    
        jQuery("#js-attachment-add").click(function () {
            var obj = this;
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            var append_text = "<span class='js-attachment-file-box'><input name='filename[]' class=' js-attachment-inputbox js-form-input-field-attachment' type='file' onchange=uploadfile(this,'<?php echo $this->config['filesize']; ?>','<?php echo $this->config['fileextension']; ?>'); size='20' maxlenght='30' /><span  class='js-attachment-remove'></span></span>";
            if (current_files < total_allow) { 
                jQuery("#js-attachment-files").append(append_text);
            } else if ((current_files === total_allow) || (current_files > total_allow)) {
                alert("<?php echo JText::_('File upload limit exceed'); ?>");
                jQuery(obj).hide();
            }
        });
        jQuery(document).delegate(".js-attachment-remove", "click", function (e) {
            var current_files = jQuery('input[name="filename[]"]').length;
            if(current_files!=1)
                jQuery(this).parent().remove();
            var current_files = jQuery('input[name="filename[]"]').length;
            var total_allow =<?php echo $this->config['noofattachment']; ?>;
            if (current_files < total_allow) {
                jQuery("#js-attachment-add").show();
            }
        });

    function actioncall(value) {
        if (value == 8) {
            /*
            jQuery('#isreopen').val(1);
            jQuery('div#tk_reopenticket').slideDown();
            tinyMCE.get('messages').focus();
            */
            jQuery('#callfrom').val('action');
            jQuery('#callaction').val(value);
            document.adminForm.submit();
        } else if (value == 3) {
            var yesclose = confirm('<?php echo JText::_('Are you sure to close ticket'); ?>');                
            if(yesclose == true){
                jQuery('#callfrom').val('action');
                jQuery('#callaction').val(value);
                document.adminForm.submit();
            }
        }
    }
    function closereopndiv() {
        jQuery('div#tk_reopenticket').slideUp();
    }
</script>
