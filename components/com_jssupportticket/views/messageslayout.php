<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		Buruj Solutions
 + Contact:		www.burujsolutions.com , info@burujsolutions.com
 * Created on:	May 22, 2015
  ^
  + Project: 	JS Tickets
  ^
 */

defined('_JEXEC') or die('Restricted access');
class messagesLayout{
	public static function getRecordNotFound(){
		$layout='
				<div class="js-ticket-error-message-wrapper">
				<div class="js-ticket-message-image-wrapper">
					<img class="js-ticket-message-image" src="'.JURI::root().'components/com_jssupportticket/include/images/error/no-record-icon.png"/>
				</div>
				<div class="js-ticket-messages-data-wrapper">
					<span class="js-ticket-messages-main-text">
				    	' . JText::_('Sorry') . '!
					</span>
					<span class="js-ticket-messages-block_text">
				    	' . JText::_('No record found') . '...!
					</span>
				</div>
			</div>';
		return $layout;
	}

	public static function getSystemOffline($title,$message){
		$layout='
			<div class="js-ticket-error-message-wrapper">
				<div class="js-ticket-message-image-wrapper">
					<img class="js-ticket-message-image" src="'.JURI::root().'components/com_jssupportticket/include/images/error/offline-icon.png"/>
				</div>
				<div class="js-ticket-messages-data-wrapper">
					<span class="js-ticket-messages-main-text">
				    		'.$title.'
					</span>
					<span class="js-ticket-messages-block_text">
				    		'.$message.'
					</span>
				</div>
			</div>
	';
		echo $layout;
	}
	public static function getUserNotLogin(){
		$layout='
			<div class="js-ticket-error-message-wrapper">
						<div class="js-ticket-message-image-wrapper">
							<img class="js-ticket-message-image" src="'.JURI::root().'components/com_jssupportticket/include/images/error/not-login-icon.png"/>
						</div>
						<div class="js-ticket-messages-data-wrapper">
							<span class="js-ticket-messages-main-text">
						    	' . JText::_('You are not logged In !') . '
							</span>
							<span class="js-ticket-messages-block_text">
						    	' . JText::_('To access this page, Please login') . '
							</span>
							<span class="js-ticket-user-login-btn-wrp">';
                    			$layout .= '<a class="js-ticket-login-btn" href="#" title="Login">' . JText::_('Login') . '</a>';
	                    	$layout .= '</span> 
	                    </div>
					</div>';
		echo $layout;
	}
}
?>