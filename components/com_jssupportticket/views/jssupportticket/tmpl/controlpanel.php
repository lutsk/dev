<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		Buruj Solutions
 + Contact:		www.burujsolutions.com , info@burujsolutions.com
 * Created on:	May 22, 2015
 ^
 + Project: 	JS Tickets
 ^
*/
defined('_JEXEC') or die('Restricted access');
?>
<div class="js-row js-null-margin">
<?php
if($this->config['offline'] == '1'){
    messagesLayout::getSystemOffline($this->config['title'],$this->config['offline_text']);
}else{
    require_once JPATH_COMPONENT_SITE . '/views/header.php';
    $document = JFactory::getDocument();
    $language = JFactory::getLanguage();
    $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketresponsive.css');
    if($language->isRTL()){
        $document->addStyleSheet(JURI::root().'components/com_jssupportticket/include/css/jssupportticketdefaultrtl.css');
    }
    ?>
    <div id="js-dash-menu-link-wrp"><!-- Dashboard Links -->
        <div class="js-section-heading"><?php echo JText::_('Dashboard Links'); ?></div>
        <div class="js-menu-links-wrp">
            <div class="js-ticket-menu-links-row">
                <?php if($this->config['cplink_openticket_user'] == 1){ ?>
                    <a class="js-col-xs-12 js-col-sm-6 js-col-md-4 js-ticket-dash-menu" href="index.php?option=com_jssupportticket&c=ticket&layout=formticket&Itemid=<?php echo $this->Itemid; ?>">
                        <div class="js-ticket-dash-menu-icon">
                            <img class="js-ticket-dash-menu-img" src="components/com_jssupportticket/include/images/add-ticket.png" />
                        </div>
                        <span class="js-ticket-dash-menu-text"><?php echo JText::_('Open Ticket'); ?></span>
                    </a>
                <?php } ?>
                <?php if($this->config['cplink_myticket_user'] == 1){ ?>
                    <a class="js-col-xs-12 js-col-sm-6 js-col-md-4 js-ticket-dash-menu" href="index.php?option=com_jssupportticket&c=ticket&layout=mytickets&Itemid=<?php echo $this->Itemid; ?>">
                        <div class="js-ticket-dash-menu-icon">
                            <img class="js-ticket-dash-menu-img" src="components/com_jssupportticket/include/images/my-tickets.png" />
                        </div>
                        <span class="js-ticket-dash-menu-text"><?php echo JText::_('My Tickets'); ?></span>
                    </a>
                <?php } ?>
                <?php if($this->config['cplink_userdata_user'] == 1){ ?>
                    <a class="js-col-xs-12 js-col-sm-6 js-col-md-4 js-ticket-dash-menu" href="index.php?option=com_jssupportticket&c=gdpr&layout=adderasedatarequest&Itemid=<?php echo $this->Itemid; ?>">
                        <div class="js-ticket-dash-menu-icon">
                            <img class="js-ticket-dash-menu-img" src="components/com_jssupportticket/include/images/gdpr.png" />
                        </div>
                        <span class="js-ticket-dash-menu-text"><?php echo JText::_('User Data'); ?></span>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<div id="js-tk-copyright">
    <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
    &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
</div>
</div>
