<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
  + Contact:    www.burujsolutions.com , info@burujsolutions.com
 * Created on:  May 22, 2015
  ^
   + Project:    JS Tickets
  ^
 */

defined('_JEXEC') or die('Restricted access');

    $color1 = "#8f5600"; /* header bk color */
    $color2 = "#018e97";/* header link hover , header bottom bk, content heading bottom line, content box hover bk+ border color , */
    $color3 = "#fafafa"; /* content box background color */
    $color4 = "#373435";/* every text color in content */
    $color5 = "#dedede";/* border color and div  border line  */
    $color6 = "#e7e7e7"; /* Every button and tab background color*/
    $color7 = "#ffffff"; /* header top and bottom text color,sort text color,headind seprater and text color */
    $color8 = "#2DA1CB";/* subject and name color for ticket listing */
    $color9 = "#000000";

    $cssString = "

    /*Top Header*/
            div#jsst-header{background-color:$color2;}
            a.js-ticket-header-links{color:$color7;}
            a.js-ticket-header-links:hover{color: $color7;}
            div#jsst-header div#jsst-header-heading{color:$color3;}
            div#jsst-header span.jsst-header-tab a.js-cp-menu-link{background:rgba(0,0,0,0.4);color:$color7;}
            div#jsst-header span.jsst-header-tab a.js-cp-menu-link:hover{background:rgba(0,0,0,0.6);color:$color7;}
            div#jsst-header span.jsst-header-tab.active a.js-cp-menu-link{background:$color1;color:$color7;}
            div#jsst_breadcrumbs_parent div.home a{background:$color2;}

    /* Dashboard Menu Links */
            div.js-section-heading{background-color: $color3;border:1px solid $color5;color $color4;}
            a.js-ticket-dash-menu{background-color: $color3;border:1px solid $color5;}
            a.js-ticket-dash-menu span.js-ticket-dash-menu-text{color:#838386;}
            .js-col-xs-12.js-col-sm-6.js-col-md-4.js-ticket-dash-menu:hover{box-shadow: 0 1px 3px 0 rgba(60,64,67,0.302),0 4px 8px 3px rgba(60,64,67,0.149);background-color: #fafafb;}
    

    /* AddTicketForm */

            div#js-tk-formwrapper div.js-form-value input.js-form-input-field,
            div#js-tk-formwrapper div.js-form-value select.js-form-select-field,
            div#js-tk-formwrapper div.js-form-value textarea{background-color:$color3 !important;border:1px solid $color5;}
            div#js-tk-formwrapper div.js-form-value input.js-form-input-field:focus,
            div#js-tk-formwrapper div.js-form-value select.js-form-select-field:focus,
            div#js-tk-formwrapper div.js-form-value textarea:focus{box-shadow: unset !important;border-color:$color5;}
            div.js-ticket-from-field div.js-ticket-select-user-field input.js-ticket-form-field-input{background-color:$color3;border:1px solid $color5;}
            div.js-ticket-select-user-btn a#userpopup{background-color:$color2;border:1px solid transparent;color:#fff;}
            span.js-ticket-error-premade{background-color:$color3;border:1px solid $color5;color:red;}
            div#js-tk-formwrapper div.js-append-premadecheck{background-color:$color3;border:1px solid $color5;}
            div.js-ticket-from-field input.js-ticket-form-field-input{background-color:$color3;border:1px solid $color5;}
            div.field-calendar div.input-append input#ticket_duedate{background-color:$color3;border:1px solid $color5;}


            div.jsst-popup-header{background: $color2;color:$color7;}
            div.jsst-popup-wrapper{background-color:$color7;}
            div.js-ticket-search-top div.js-ticket-search-left div.js-ticket-search-fields-wrp input.js-ticket-search-input-fields{border:1px solid $color5;background-color:$color3;}
            div.js-ticket-search-top div.js-ticket-search-right div.js-ticket-search-btn-wrp input.js-ticket-search-btn{background: $color2;color:$color7; border:1px solid transparent;}
            div.js-ticket-search-top div.js-ticket-search-right div.js-ticket-search-btn-wrp input.js-ticket-reset-btn{background: #606062;color:$color7; border:1px solid transparent;}

            div.js-ticket-table-header{background-color:#ecf0f5;border:1px solid $color5;}
            div.js-ticket-table-header div.js-ticket-table-header-col{border-right:1px solid $color5;}
            div.js-ticket-table-header div.js-ticket-table-header-col:last-child{border-right:none;}
            div.js-ticket-table-body div.js-ticket-data-row{border:1px solid $color5;border-top:none}
            div.js-ticket-table-body div.js-ticket-data-row div.js-ticket-table-body-col{border-right:1px solid $color5;}
            div.js-ticket-table-body div.js-ticket-data-row div.js-ticket-table-body-col:last-child{border-right:none;}
            th.js-ticket-table-th{border-right:1px solid $color5;}
            tbody.js-ticket-table-tbody{border:1px solid $color5;}
            td.js-ticket-table-td{border-right:1px solid $color5;}
            div.js-ticket-select-user-btn a#userpopup{background-color:$color2; color:$color7;}
            
            /*Submit Button*/
            div.js-form-submit-btn-wrp{border-top:1px solid $color2;}
            div.js-form-submit-btn-wrp input.js-save-button{background-color:$color2;color: #ffffff;}
            div.js-form-submit-btn-wrp a.js-cancel-button{background: #606062;color: #ffffff;}
            
            /*File Attachment*/
            div#js-attachment-files{background-color:$color3; border: 1px solid $color5;}
            div#js_attachment_files_internalnote{background-color:$color3; border: 1px solid $color5;}
            span.js-attachment-file-box{border: 1px solid $color5;background-color:#fff;}
            div#js-attachment-option span#js-attachment-add,
            div#js-tk-formwrapper div.js-form-submit input{color: $color4;background-color: $color6;}
            div#js-attachment-option span#js-attachment-add:hover,
            div#js-tk-formwrapper div.js-form-submit input:hover{color: $color7; background-color: $color2;}
            span#js-attachment-add{color: $color7; background-color: $color2;}
            span#js_attachment_add_internal{color: $color7; background-color: $color2;}

         /*My Tickets*/
          /* Top Circle Count Box*/
            div.js-myticket-link a.js-myticket-link{background-color:$color3;border:1px solid$color5;}
            div.js-myticket-link a.js-myticket-link:hover{background: rgba(227, 231, 234, 0.7);}
            .js-ticket-answer{background-color:#D79922;}
            .js-ticket-close{background-color:#e82d3e;}
            .js-ticket-allticket{background-color:#5AB9EA;}
            .js-ticket-open{background-color:#14A76C;}
            .js-ticket-overdue{background-color:#FF652F;}
            div.js-myticket-link a.js-myticket-link span.js-ticket-circle-count-text.js-ticket-blue{color:#5ab9ea;}
            div.js-myticket-link a.js-myticket-link span.js-ticket-circle-count-text.js-ticket-red{color:#e82d3e;}
            div.js-myticket-link a.js-myticket-link span.js-ticket-circle-count-text.js-ticket-orange{color:#5AB9EA;}
            div.js-myticket-link a.js-myticket-link span.js-ticket-circle-count-text.js-ticket-green{color:#14A76C;}
            div.js-myticket-link a.js-myticket-link span.js-ticket-circle-count-text.js-ticket-pink{color:#D79922;}
            div.js-myticket-link a.js-myticket-link div.progress::after {border: 25px solid #bfbfbf;}
            div.js-myticket-link a.js-myticket-link:hover{box-shadow: 0 1px 3px 0 rgba(60,64,67,0.302),0 4px 8px 3px rgba(60,64,67,0.149);background-color: #fafafb;}
            div.js-myticket-link a.js-myticket-link.js-ticket-green:hover{border-color:#14A76C;}
            div.js-myticket-link a.js-myticket-link.js-ticket-blue:hover{border-color:#5ab9ea;}
            div.js-myticket-link a.js-myticket-link.js-ticket-red:hover{border-color:#e82d3e;}
            div.js-myticket-link a.js-myticket-link.js-ticket-orange:hover{border-color:#5AB9EA;}
            div.js-myticket-link a.js-myticket-link.js-ticket-pink:hover{border-color:#D79922;}


          /*Search Portion*/
          div.js-combine-search-wrapper{border:1px solid $color5 ;}
          div.js-heading-wrp{background-color:#e7ecf2;border-bottom:1px solid $color5; color: $color4}
          div.js-filter-wrapper div.js-filter-form-fields-wrp input.js-ticket-input-field{background-color: $color3;border:1px solid $color5;}
          div#js-filter-wrapper-toggle-ticketid input.js-ticket-input-field{background-color: $color3; border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-field-wrp input.js-ticket-input-field{background-color: $color3; border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-field-wrp select#filter_department{background-color: $color3 !important;border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-field-wrp select#filter_priority{background-color: $color3 !important;border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-field-wrp select#staffid{background-color: $color3;border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-wrapper div.js-filter-value input.js-ticket-input-field{background-color: $color3;border:1px solid $color5;}
          div#js-filter-wrapper-toggle-area div.js-filter-field-wrp div.field-calendar div.input-append button#filter_datestart_btn{background-color: $color3;border:1px solid $color5;border-left:0px;}
          div#js-filter-wrapper-toggle-plus{background-color:#474749;}
          div#js-filter-wrapper-toggle-minus{background-color:#474749;}
          div.js-filter-button-wrp .js-ticket-search-btn{background-color: $color2;color: $color7; border:1px solid transparent;}
          div.js-filter-button-wrp .js-ticket-reset-btn{background-color:#474749;color: $color7; border:1px solid transparent;}
          span.js-ticket-wrapper-textcolor{color:$color7;}

          /*Sorting Portion*/
          div#js-tk-sort-wrapper ul#js-tk-sort-manu li.js-tk-sort-manulink a:hover{background: $color2; color: #fff; }
          div#js-tk-sort-wrapper ul#js-tk-sort-manu li.js-tk-sort-manulink a.selected{background: $color2; color: #fff; text-decoration:none !important; outline:0px !important; }
          div#js-tk-sort-wrapper ul#js-tk-sort-manu li.js-tk-sort-manulink a{background-color:#373435;color:#fff;}

          /*My tickets lists*/
          div#js-tk-wrapper{border: 1px solid $color5;box-shadow: 0 8px 6px -6px #dedddd;}
          div#js-tk-wrapper div.js-icon {border-right: 1px solid $color5;}
          div#js-tk-wrapper div.js-middle {border-right: 1px solid $color5;}
          div#js-tk-wrapper div.js-middle div.js-tk-pabsolute span{color: #FFFFFF;}
          div#js-tk-wrapper div.js-bottom{border-top: 1px solid $color5;}
          div#js-tk-wrapper div.js-bottom span.js-tk-actions a{color: $color7; border:1px solid $color2; background-color: $color2;}
          div#js-tk-wrapper div.js-bottom span.js-tk-actions :hover{color: $color2; border:1px solid $color2; background-color: $color7;}
    
    /*Ticket Details*/
          div.js-ticket-detail-wrapper{border:1px solid   $color5;}
          div.js-ticket-detail-box{border-bottom:1px solid   $color5;}
          div.js-ticket-detail-box div.js-ticket-detail-right div.js-ticket-rows-wrp{background-color:   $color3;}
          div.js-ticket-detail-box div.js-ticket-detail-right{border-left:1px solid   $color5;}
          div.js-ticket-detail-right div.js-ticket-row div.js-ticket-field-title{color:  $color1;}
          div.js-ticket-detail-right div.js-ticket-row div.js-ticket-field-value span.js-ticket-subject-link{color:  $color2;}
          div.js-ticket-detail-right div.js-ticket-openclosed-box{color:  $color7;}
          div.js-ticket-detail-right div.js-ticket-right-bottom{background-color:#fef1e6;color:  $color4;border-top:1px solid   $color5;}
          div.js-ticket-detail-wrapper div.js-ticket-action-btn-wrp div.js-ticket-btn-box{background-color:#e7ecf2;border:1px solid   $color5;}
          div#userpopupforchangepriority{background:   $color3;border: 1px solid   $color5;}
          div#userpopupforchangepriority div.js-ticket-priorty-header{background:   $color2;color:  $color7;}
          div#userpopupforchangepriority div.js-ticket-priorty-fields-wrp div.js-ticket-select-priorty select#priorityid{background-color:  $color3;border:1px solid   $color5;}
          div#userpopupforchangepriority div.js-ticket-priorty-btn-wrp{border-top:2px solid   $color2;}
          div.js-ticket-priorty-btn-wrp button.js-ticket-priorty-save{background-color:  $color2;color:  $color7; border:1px solid transparent;}
          div.js-ticket-priorty-btn-wrp button.js-ticket-priorty-cancel{background-color:#48484a;color:  $color7; border:1px solid transparent;}
          div.js-ticket-post-reply-wrapper div.js-ticket-thread-heading{background-color:#e7ecf2;border:1px solid   $color5;color:  $color4;}
          div.js-ticket-post-reply-box{border:1px solid   $color5;}
          div.js-ticket-white-background{background-color:  $color7;}
          div.js-ticket-background{background-color:  $color3;border-left:1px solid   $color5;}
          div.js-ticket-attachments-wrp{border-top:1px solid   $color5;}
          div.js-ticket-attachments-wrp div.js_ticketattachment{border:1px solid   $color5;background-color:  $color7;}
          div.js-ticket-attachments-wrp div.js_ticketattachment a.js-download-button{background-color:  $color2;color:  $color7;border:1px solid   $color5;}
          div.js-ticket-attachments-wrp a.js-all-download-button{background-color:  $color2;color:  $color7;border:1px solid   $color5;}
          div.js-ticket-edit-options-wrp{border-top:1px solid   $color5;}
          div.js-ticket-edit-options-wrp a.js-button{background-color:#e7ecf2;border:1px solid   $color5;}
          
          div.jsst-ticket-detail-timer-wrapper div.timer-right div.timer-buttons span.timer-button{background-color:  $color2;color:  $color7;}
          div.jsst-ticket-detail-timer-wrapper{border:1px solid   $color5;background:  $color3;}
          div.jsst-ticket-detail-timer-wrapper div.timer-right div.timer{}
          div.jsst-ticket-detail-timer-wrapper div.timer-right div.timer-buttons span.timer-button.selected{background:#373435;}
          
          select#premadeid{background-color:   $color3 !important;border:1px solid   $color5;}
          div.js-ticket-time-stamp-wrp{border-top:1px solid   $color5;}
          
          /*Post Reply Section*/
                div.js-ticket-reply-forms-heading{background-color: #e7ecf2;border: 1px solid #DEDFE0;color: #373435;}
                div.tk_attachment_value_wrapperform{border: 1px solid   $color5;background:   $color3;}
                span.tk_attachment_value_text{border: 1px solid   $color5;background-color:  $color7;}
                div.js-ticket-reply-form-button-wrp{border-top: 2px solid   $color2;}
                div.js-ticket-reply-form-button-wrp input.js-ticket-save-button{background-color:  $color2;color:  $color7;border:1px solid transparent;}
                div.js-ticket-reply-form-button-wrp a.js-ticket-cancel-button{background-color:#48484a;color:  $color7}
                
                div.js-ticket-premade-msg-wrp div.js-ticket-premade-field-wrp select.js-ticket-premade-select{background-color:  $color3 !important;border:1px solid   $color5;} 
                span.js-ticket-apend-radio-btn{border:1px solid   $color5;background-color:  $color3;}
                div.js-ticket-premade-msg-wrp div.js-ticket-premade-field-title{color:  $color4;}
                div.js-ticket-append-signature-wrp div.js-ticket-signature-radio-box{border:1px solid   $color5;background-color:  $color3;}
                div.js-ticket-assigntome-wrp div.js-ticket-assigntome-field-wrp{border:1px solid   $color5;background-color:  $color3;}
                div.js-ticket-closeonreply-wrp div.js-form-title-position-reletive-left{border:1px solid   $color5;background-color:  $color3;}
                div.js-ticket-internalnote-wrp div.js-ticket-internalnote-field-wrp input.js-ticket-internalnote-input{border:1px solid   $color5;background-color:  $color3;}
                span.tk_attachments_addform{background-color: $color2;color: $color7;}
                div#userpopupforchangepriority div.js-ticket-priorty-btn-wrp{border-top:2px solid  $color2;}
                div.js-ticket-priorty-btn-wrp input.js-ticket-priorty-save{background-color: $color2;color: $color7; border:1px solid transparent;}
                div.js-ticket-priorty-btn-wrp input.js-ticket-priorty-cancel{background-color:#48484a;color: $color7; border:1px solid transparent;}
                div.js-ticket-priorty-btn-wrp button.js-ticket-priorty-cancel{background-color:#48484a;color: $color7; border:1px solid transparent;}
          /*Post Reply Section*/

                 
                  
                  


    
    @media (max-width:480px){
        div#tk_detail_content_wraper div#tk_detail_post{border-left:0px;}
        div#tk_detail_content_wraper div#tk_detail_id{border-left:0px;}
        div#tk_detail_content_wraper div#tk_detail_reply{border-left:0px;}
    }
    ";
    $language = JFactory::getLanguage();
    if($language->isRTL()){
        $cssString .= "
            div.js-ticket-detail-box div.js-ticket-detail-right{border-right: 1px solid $color5;border-left:unset;}
            div#js-filter-wrapper-toggle-area div.js-filter-field-wrp div.field-calendar div.input-append button#filter_datestart_btn{border-right:unset;border-left:1px solid $color5;}
            div#js-filter-wrapper-toggle-area div.js-filter-field-wrp div.field-calendar div.input-append button#filter_dateend_btn{border-right:unset;border-left:1px solid $color5;}
        ";
    }

    $document = JFactory::getDocument();
    $document->addStyleDeclaration($cssString);


?>
