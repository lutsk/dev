<?php
defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldParentcurrency extends JFormFieldList
{
	protected $type = 'Parentcurrency';

	public function getOptions()
	{
		$id = JFactory::getApplication()->input->get('id', 0);
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		$options[] = JHtml::_('select.option', '', 'Выбрать');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select(array($db->qn('id', 'value'), $db->qn($name, 'text')))
			->from('#__abc_multicurrency');
		$db->setQuery($query);
		
		try
		{
			$list = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $db->getMessage());
		}
		foreach ($list as $item)
		{
			$options[] = JHtml::_('select.option', $item->value, $item->text);
		}
        
		return array_merge(parent::getOptions(), $options);
	}
}
