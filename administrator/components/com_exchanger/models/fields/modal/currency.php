<?php
defined('JPATH_BASE') or die;

class JFormFieldModal_Currency extends JFormField
{
	protected $type = 'Modal_Currency';
	
	protected function getInput()
	{
		JHtml::_( 'behavior.modal', 'a.modal' );
		
		$script = array();
		$script[] = '	function jSelectArticle_' . $this->id . '(id, title, catid, object) {';
		$script[] = '		document.id("' . $this->id . '_id").value = id;';
		$script[] = '		document.id("' . $this->id . '_name").value = title;';
		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';
		
		JFactory::getDocument()->addScriptDeclaration(implode( "\n", $script));
		
		$html = array();
		$link = 'index.php?option=com_exchanger&amp;view=currencys&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_' . $this->id;
		
		$db = JFactory::getDBO();
		$db->setQuery('SELECT title FROM #__currency  WHERE id=' . (int)$this->value);
		
		try {
			$title = $db->loadResult();
		} catch (RuntimeException $e) {
			JError::raiseWarning(500, $e->getMessage());
		}
		
		if (empty($title)) {
			$title = JText::_('JSELECT');
		}
		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
		
		$html[] = '<span class="input-append">';
		$html[] = '<input type="text" class="input-medium" id="' . $this->id . '_name" value="' . $title . '" disabled="disabled" size="35" /><a class="modal btn" title="' . JText::_('COM_EXCHANGER_CHANGE_CURRENCY') . '"  href="' . $link . '&amp;' . JSession::getFormToken() . '=1" rel="{handler: \'iframe\', size: {x: 800, y: 450}}"><i class="icon-file"></i> ' . JText::_('JSELECT') . '</a>';
		$html[] = '</span>';
		
		$value = (int)$this->value ? $value = (int)$this->value : '';
		
		$class = $this->required ? ' class="required modal-value"' : '';
		
		$html[] = '<input type="hidden" id="' . $this->id . '_id"' . $class . ' name="' . $this->name . '" value="' . $value . '" />';
		
		return implode("\n", $html);
	}
}
