<?php
defined( '_JEXEC' ) or die;

class ExchangerModelOrders extends JModelList
{
	public function __construct($config = array())
	{
		if ( empty( $config['filter_fields'] ) ) {
			$config['filter_fields'] = array('id', 'created_time');
		}
		parent::__construct($config);
	}
	
	public function getModel($name = 'Order', $prefix = 'ExchangerModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		if ($layout = JFactory::getApplication()->input->get('layout')) {
			$this->context .= '.' . $layout;
		}
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		$status = $this->getUserStateFromRequest($this->context . '.filter.status', 'status', '');
		$this->setState('filter.status', $status);
		$userId = $this->getUserStateFromRequest($this->context . '.filter.user_id', 'filter_user_id');
		$this->setState('filter.user_id', $userId );
		
		parent::populateState('id');
	}
	
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.status');
		$id .= ':' . $this->getState('filter.user_id');
		return parent::getStoreId($id);
	}
	
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery( true );
		$query->select('o.*');
		$query->select('u.username, eu.middle_name');
		$query->join('LEFT', '#__users AS u ON u.id = o.user_id');
		$query->join('LEFT', '#__abc_user AS eu ON eu.user_id = o.user_id');
		$query->from('#__abc_orders as o');
		$status = $this->getState('filter.status');
		if (is_numeric($status)) {
			$query->where('o.status = ' . (int)$status);
		}
		$userId = $this->getState('filter.user_id');
		if (is_numeric($userId)) {
			$query->where('o.user_id = ' . $userId);
		}
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $this->getDbo()->Quote('%' . $this->getDbo()->escape($search, true) . '%');
			$query->where(
				'(o.user LIKE ' . $search . ' OR eu.middle_name LIKE ' . $search . ')'
			);
		}
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($this->getDbo()->escape($orderCol . ' ' . $orderDirn));
		
		return $query;
	}
	
	public function getAuthors()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__abc_orders AS c ON c.user_id = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');
		return $this->getDbo()->setQuery($query)->loadObjectList();
	}
}