<?php
defined('_JEXEC') or die;

class ExchangerModelMulticurrencys extends JModelList
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('*')->from('#__abc_multicurrency');
		
		return $query;
	}
}