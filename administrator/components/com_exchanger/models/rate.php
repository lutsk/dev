<?php
defined('_JEXEC') or die;

class ExchangerModelRate extends JModelAdmin
{
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_exchanger.rate', 'rate', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		
		ExchangerHelper::setLangFormField($form, 'description', 'editor', false, '', 'descriptions');
		
		$user = JFactory::getUser();
		if (!$user->authorise('core.edit.state', '#__abc_rate.' . $this->getState('extdataedit.id'))) {
			$form->setFieldAttribute('published', 'disabled', 'true');
			$form->setFieldAttribute('published', 'filter', 'unset');
		}
		return $form;
	}
	
	public function getItem($id = null)
	{
		$item = parent::getItem($id);
		
		return $item;
	}
	
	public function getSelectOptions($id)
	{
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select(array(
				$db->qn('id', 'value'),
				$db->qn($name, 'text')
			))
			->from($db->qn('#__abc_currency'))
			->where($db->qn('id') . ' != ' . $id);
		$db->setQuery($query);
		
		return $db->loadObjectList();
	}
	
	public function save($data)
	{
		ExchangerHelper::setLangTableColumn('description', '#__abc_rate'); // Если добавился язык у сайта - генерируем для него поле в таблице
		
		$table      = $this->getTable();
		$input      = JFactory::getApplication()->input;
		$pk         = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
		$isNew      = true;
		$context    = $this->option . '.' . $this->name;
		
		if ($pk > 0)
		{
			$table->load($pk);
			$isNew = false;
		}
		
		if (!$table->bind($data))
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->check())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->store())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		$this->setState($this->getName() . '.id', $table->id);
		
		$this->cleanCache();
		
		return true;
	}
	
	public function getTable($type = 'rate', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_exchanger.edit.rate.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		
		if (!empty($data->id)) {
			$reverseData = $this->getReverseData($data);
			$data->amount_from_reverse = $reverseData->amount_from_reverse;
			$data->amount_to_reverse = $reverseData->amount_to_reverse;
		}
		
		return $data;
	}
	
	protected function getReverseData($data)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select(array($db->qn('currency_from_course', 'amount_to_reverse'), $db->qn('currency_to_course', 'amount_from_reverse')))
			->from($db->qn('#__abc_rate'))
			->where($db->qn('id') . ' = ' . $data->ref_id);
		$db->setQuery($query);
		
		return $db->loadObject();
	}
	
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			return JFactory::getUser()->authorise('core.delete', '#__abc_rate.' . (int)$record->id);
		}
	}
	
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', '#__abc_rate.' . (int)$record->id);
		}
		else {
			return parent::canEditState('com_exchanger');
		}
	}

}