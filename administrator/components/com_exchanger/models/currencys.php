<?php
defined('_JEXEC') or die;

class ExchangerModelCurrencys extends JModelList
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$lang = JFactory::getLanguage()->getTag();
		$name = 'name_' . $lang;
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select('c.*');
		$query->select($db->qn('p.alias', 'parent_alias'));
		$query->select($db->qn('p.' . $name, 'parent_name'));
		$query->join('LEFT', $db->qn('#__abc_multicurrency', 'p') . ' ON ' . $db->qn('p.id') . ' = ' . $db->qn('c.parent'));
		$query->from('#__abc_currency as c');
		
		return $query;
	}
}