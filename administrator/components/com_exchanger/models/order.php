<?php
defined( '_JEXEC' ) or die;

class ExchangerModelOrder extends JModelAdmin {

	public function getForm( $data = array( ), $loadData = true ) {
		$form = $this->loadForm( 'com_exchanger.order', 'order', array( 'control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	
	public function getItem($id = null) {
		return $item = parent::getItem($id);
	}
	
	public function getTable($type = 'order', $prefix = 'Table', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_exchanger.edit.order.data', array());
		if ( empty( $data ) ) {
			$data = $this->getItem();
		}
		return $data;
	}
	
	protected function canDelete($record)
	{
		if (!empty( $record->id ) ) {
			return JFactory::getUser()->authorise('core.delete', '#__abc_orders.' . (int)$record->id);
		}
	}
	
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		
		if ( !empty( $record->id ) ) {
			return $user->authorise( 'core.edit.state', '#__abc_orders.' . (int)$record->id);
		}
		else {
			return parent::canEditState('com_exchanger');
		}
	}

}