<?php
defined('_JEXEC') or die;

class ExchangerModelCurrency extends JModelAdmin
{
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_exchanger.currency', 'currency', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		
		ExchangerHelper::setLangFormField($form, 'name', 'text', true, 'input-xlarge input-large-text', 'names');		
		
		return $form;
	}
	
	public function getItem($id = null)
	{
		$item = parent::getItem($id);
		return $item;
	}
	
	public function getTable($type = 'currency', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_exchanger.edit.currency.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
	
	public function save($data)
	{		
		ExchangerHelper::setLangTableColumn('name', '#__abc_currency');
		
		$table      = $this->getTable();
		$input      = JFactory::getApplication()->input;
		$pk         = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
		$isNew      = true;
		$context    = $this->option . '.' . $this->name;
		
		if ($pk > 0)
		{
			$table->load($pk);
			$isNew = false;
		}
		
		if (!$table->bind($data))
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->check())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->store())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		$this->setState($this->getName() . '.id', $table->id);
		
		$this->cleanCache();
		
		return true;
	}
	
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			return JFactory::getUser()->authorise('core.delete', '#__abc_currency.' . (int)$record->id);
		}
	}
	
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', '#__abc_currency.' . (int)$record->id);
		}
		elseif (!empty($record->catid)) {
			return $user->authorise('core.edit.state', '#__abc_currency.' . (int)$record->catid);
		}
		else {
			return parent::canEditState('com_exchanger');
		}
	}

}