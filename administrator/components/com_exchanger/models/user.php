<?php
defined('_JEXEC') or die;

class ExchangerModelUser extends JModelAdmin
{
	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_exchanger.user', 'user', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		$user = JFactory::getUser();
		if (!$user->authorise( 'core.edit.state', '#__abc_user.' . $this->getState('extdataedit.id'))) {
			$form->setFieldAttribute('published', 'disabled', 'true');
			$form->setFieldAttribute('published', 'filter', 'unset');
		}
		return $form;
	}
	
	public function getItem($id = null)
	{
		$item = parent::getItem($id);
		
		$item->wallets = $this->getWallets($item->user_id);
		foreach ($item->wallets as $wallet)
		{
			$wallet->code = ExchangerHelper::getCurrencyCode($wallet->currency_id);
		}
		
		return $item;
	}
	
	public function getTable($type = 'user', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_exchanger.edit.user.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
	
	public function save($data)
	{	
		$table      = $this->getTable();
		$input      = JFactory::getApplication()->input;
		$pk         = (!empty($data['id'])) ? $data['id'] : (int) $this->getState($this->getName() . '.id');
		$isNew      = true;
		$context    = $this->option . '.' . $this->name;
		
		if ($pk > 0)
		{
			$table->load($pk);
			$isNew = false;
		}
		
		if (!$table->bind($data))
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->check())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		if (!$table->store())
		{
			$this->setError($table->getError());
			
			return false;
		}
		
		$this->setState($this->getName() . '.id', $table->id);
		
		$this->cleanCache();
		
		return true;
	}
	
	protected function getWallets($user_id)
	{		
		try
		{
			$lang = JFactory::getLanguage()->getTag();
			$name = 'name_' . $lang;
			
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			
			$query->select(array('w.*', $db->qn('c.'.$name, 'name'), $db->qn('c.image')))
					->from($db->qn('#__abc_wallet', 'w'))
					->join('LEFT', $db->qn('#__abc_currency', 'c') . ' ON ' . $db->qn('w.currency_id') . ' = ' . $db->qn('c.id'))
					->where($db->qn('w.user_id') . ' = ' . $user_id);
			$db->setQuery($query);
		}
		catch (Exception $e)
		{
			$this->setError($e);
			return array();
		}
		
		return $db->loadObjectList();
	}
	
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			return JFactory::getUser()->authorise('core.delete', '#__abc_user.' . (int)$record->id);
		}
	}
	
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', '#__abc_user.' . (int)$record->id);
		}
		elseif (!empty($record->catid)) {
			return $user->authorise('core.edit.state', '#__abc_user.' . (int)$record->catid);
		}
		else {
			return parent::canEditState('com_exchanger');
		}
	}

}