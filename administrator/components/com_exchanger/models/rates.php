<?php
defined('_JEXEC') or die;

class ExchangerModelRates extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array('title', 'state', 'ordering', 'id');
		}
		parent::__construct($config);
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		if ($layout = JFactory::getApplication()->input->get('layout')) {
			$this->context .= '.' . $layout;
		}
		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		parent::populateState('id');
	}
	
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.published');
		
		return parent::getStoreId($id);
	}
	
	public function getItems()
	{
		$items = parent::getItems();
		
		foreach ($items as $item)
		{
			$item->currency_from_name = $this->getCurrencyName($item->currency_from_id);
			$item->currency_to_name = $this->getCurrencyName($item->currency_to_id);
		}
		
		return $items;
	}
	
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('r.*');
		/*$query->select('u.username as created_by, u.id as author_id');
		$query->join('LEFT', '#__abc_currency AS c ON c.id = r.created_by');*/
		$query->from('#__abc_rate as r');
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			$query->where('r.published=' . (int)$published);
		}
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order( $this->getDbo()->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}
	
	protected function getCurrencyName($id)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->qn('name_ru-RU'))
			->from('#__abc_currency')
			->where($db->qn('id') . ' = ' . $id);
		$db->setQuery($query);
		return $db->loadResult();
	}
}