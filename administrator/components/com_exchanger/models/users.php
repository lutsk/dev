<?php
defined('_JEXEC') or die;

class ExchangerModelUsers extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array('username', 'state', 'ordering', 'id', 'user_id');
		}
		parent::__construct( $config );
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		if ( $layout = JFactory::getApplication()->input->get('layout')) {
			$this->context .= '.' . $layout;
		}
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState( 'filter.search', $search);
		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState( 'filter.published', $published );
		$userId = $this->getUserStateFromRequest($this->context . '.filter.user_id', 'filter_user_id');
		$this->setState( 'filter.user_id', $userId);
		parent::populateState('id');
	}
	
	protected function getStoreId( $id = '' )
	{
		$id .= ':' . $this->getState( 'filter.search' );
		$id .= ':' . $this->getState( 'filter.published' );
		return parent::getStoreId( $id );
	}
	
	protected function getListQuery()
	{		
		$query = $this->getDbo()->getQuery(true);
		
		$query->select('u.*');
		$query->select('pu.name, pu.username');
		$query->from('#__abc_user as u');
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			$query->where('u.published = ' . (int)$published);
		}
		$userId = $this->getState('filter.user_id');
		if ( is_numeric($userId)) {
			$query->where( 'u.user_id = ' . $user_id );
		}
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $this->getDbo()->Quote('%' . $this->getDbo()->escape($search, true) . '%');
			$query->where('(u.surname LIKE ' . $search . ')');
		}
		$query->join('LEFT', '#__users AS pu ON pu.id = u.user_id');
		
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order( $this->getDbo()->escape($orderCol . ' ' . $orderDirn));
		
		return $query;
	}
	
	public function getAuthors()
	{
		$query = $this->getDbo()->getQuery( true );
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__abc_user AS c ON c.user_id = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');
		return $this->getDbo()->setQuery($query)->loadObjectList();
	}
}