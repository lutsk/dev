<?php
defined('_JEXEC') or die;

class TableCurrency extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_currency', 'id', $db);
	}
	
	public function bind($array, $ignore = '')
	{
		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}
		if (isset($array['wallet']))
		{
			$array['wallet'] = str_replace(' ', '', $array['wallet']);
		}
		
		return parent::bind($array, $ignore);
	}
}