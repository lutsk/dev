<?php
defined('_JEXEC') or die;

class TableUser extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_user', 'id', $db);
	}
	
	public function bind($array, $ignore = '')
	{
		if (isset($array['rules']) && is_array($array['rules'])) {
			$rules = new JAccessRules($array['rules']);
			$this->setRules($rules);
		}
		
		return parent::bind($array, $ignore);
	}
}