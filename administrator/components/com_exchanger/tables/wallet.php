<?php
defined('_JEXEC') or die;

class TableWallet extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_wallet', 'id', $db);
	}
	
	public function bind($array, $ignore = '')
	{
		if (isset($array['wallet']))
		{
			$array['wallet'] = str_replace(' ', '', $array['wallet']);
		}
		return parent::bind($array, $ignore);
	}
}