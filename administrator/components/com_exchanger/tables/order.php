<?php
defined('_JEXEC') or die;

class TableOrder extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_orders', 'id', $db);
		JTableObserverContenthistory::createObserver($this, array('typeAlias' => 'com_exchanger.order'));
	}
	
	public function bind($array, $ignore = '')
	{
		if (isset($array['rules']) && is_array($array['rules'])) {
			$rules = new JAccessRules($array['rules']);
			$this->setRules($rules);
		}
		
		return parent::bind($array, $ignore);
	}
	
	public function check()
	{
		$date = JFactory::getDate();
		if (!(int) $this->checked_out_time)
		{
			$this->checked_out_time = $date->toSql();
		}
		return true;
	}
	
	public function store($updateNulls = false)
	{
		$user = JFactory::getUser();
		$date = JFactory::getDate();
		
		if (!JFactory::getApplication()->isAdmin())
		{
			$this->user_id = $user->id;
		} 
		
		$this->created_time = $date->toSql();
		
		return parent::store($updateNulls);
	}
}