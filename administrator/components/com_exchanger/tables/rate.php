<?php
defined('_JEXEC') or die;

class TableRate extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_rate', 'id', $db);
	}
	
	public function bind($array, $ignore = '')
	{
		if (isset($array['rules']) && is_array($array['rules'])) {
			$rules = new JAccessRules($array['rules']);
			$this->setRules($rules);
		}
		
		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}
		
		return parent::bind($array, $ignore);
	}
	
}