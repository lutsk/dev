<?php
defined('_JEXEC') or die;

class TableMulticurrency extends JTable
{
	function __construct(&$db)
	{
		parent::__construct('#__abc_multicurrency', 'id', $db);
	}
	
	public function bind($array, $ignore = '')
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$array['alias'] = JApplication::stringURLSafe($array['alias']);
		if (trim(str_replace('-', '', $array['alias'])) == '') {
			if (empty($array['name_en-GB'])) {
				$name = str_replace('_'.$lang, '', $array['name_' . $lang]);
			} else {
				$name = str_replace('_'.$lang, '', $array['name_en-GB']);
			}			
			$array['alias'] = JApplication::stringURLSafe($name);
		}
		
		if (isset($array['rules']) && is_array($array['rules'])) {
			$rules = new JAccessRules($array['rules']);
			$this->setRules($rules);
		}
		
		return parent::bind($array, $ignore);
	}
}