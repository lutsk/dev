<?php
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/helpers/exchanger.php';

$controller = JControllerLegacy::getInstance('exchanger');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();