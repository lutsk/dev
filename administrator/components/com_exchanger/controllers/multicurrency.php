<?php
defined('_JEXEC') or die;

class ExchangerControllerMulticurrency extends JControllerForm
{

	function __construct($config = array())
	{
		$this->view_list = 'multicurrencys';
		parent::__construct($config);
	}
	
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int)isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		
		if ($user->authorise('core.edit', 'com_exchanger.multicurrency.' . $recordId)) {
			return true;
		}
		
		return false;
	}
}