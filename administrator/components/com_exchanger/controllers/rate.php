<?php
defined('_JEXEC') or die;

class ExchangerControllerRate extends JControllerForm
{

	function __construct($config = array())
	{
		$this->view_list = 'rates';
		parent::__construct($config);
	}
	
	public function makeSelect() {
		$id = $this->input->getInt('id');
		$model = $this->getModel();
		$data = $model->getSelectOptions($id);
		$response = array
			(
				'data' => $data,
				'status'  => 200
			);
		$response = json_encode($response);
		echo $response;
		exit;
	}
	
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int)isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		
		if ($user->authorise('core.edit', 'com_exchanger.rate.' . $recordId)) {
			return true;
		}
		
		return false;
	}
	
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
		$item = $model->getItem();
		$itemid = $item->get('id');
		$data = JFactory::getApplication()->input->post->get('jform', null, 'array');
		
		if ($data['id'] == 0) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->update($db->qn('#__abc_rate'))->set(array($db->qn('ref_id') . ' = ' . ((int)$itemid + 1)))->where($db->qn('id') . ' = ' . $itemid);
			$db->setQuery($query)->execute();
			
            $dataItem = (array) $item;
            
            $dataItem['id'] = null;
            $dataItem['currency_from_id'] = $data['currency_to_id'];
            $dataItem['currency_to_id'] = $data['currency_from_id'];
            $dataItem['currency_from_course'] = $data['amount_from_reverse'];
            $dataItem['currency_to_course'] = $data['amount_to_reverse'];
            $dataItem['ref_id'] = $itemid;
            
            $table = $model->getTable();
            $table->bind($dataItem);
			if (!$table->store())
			{
				return false;
			}
        } else {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->update($db->qn('#__abc_rate'))
				->set(array(
					$db->qn('currency_from_course') . ' = ' . $data['amount_to_reverse'],
					$db->qn('currency_to_course') . ' = ' . $data['amount_from_reverse']
				))
				->where($db->qn('id') . ' = ' . $data['ref_id']);
			$db->setQuery($query)->execute();
		}
        
		return true;
	}
}