<?php
defined('_JEXEC') or die;

class ExchangerControllerCurrencys extends JControllerAdmin
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function getModel($name = 'Currency', $prefix = 'ExchangerModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}
}