<?php
defined('_JEXEC') or die;

class ExchangerControllerRates extends JControllerAdmin
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function getModel($name = 'Rate', $prefix = 'ExchangerModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}
	
	public function saveOrderAjax()
	{
		$pks = $this->input->post->get('cid', array(), 'array');
		$order = $this->input->post->get('order', array(), 'array');
		
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);
		
		$model = $this->getModel();
		
		$return = $model->saveorder($pks, $order);
		
		if ( $return ) {
			echo '1';
		}
		
		JFactory::getApplication()->close();
	}
}