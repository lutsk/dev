<?php
defined('_JEXEC') or die;

class ExchangerControllerOrder extends JControllerForm
{
	function __construct($config = array())
	{
		$this->view_list = 'orders';
		parent::__construct($config);
	}
	
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int)isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		$userId = $user->get('id');
		
		if ($user->authorise('core.edit', 'com_exchanger.order.' . $recordId)) {
			return true;
		}
		
		return false;
	}
}