<?php
defined('_JEXEC') or die;

class ExchangerHelper
{
	static function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
			JText::_('Мультивалюты'),
			'index.php?option=com_exchanger&view=multicurrencys',
			$vName == 'multicurrencys');
		JHtmlSidebar::addEntry(
			JText::_('Валюты'),
			'index.php?option=com_exchanger&view=currencys',
			$vName == 'currencys');
		JHtmlSidebar::addEntry(
			JText::_('CURRENCY_RATES'),
			'index.php?option=com_exchanger&view=rates',
			$vName == 'rates');
		JHtmlSidebar::addEntry(
			JText::_('Пользователи'),
			'index.php?option=com_exchanger&view=users',
			$vName == 'users');
		JHtmlSidebar::addEntry(
			JText::_('Заказы'),
			'index.php?option=com_exchanger&view=orders',
			$vName == 'orders');
	}
	
	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;
		$assetName = 'com_exchanger';
		$actions = JAccess::getActions($assetName);
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}
		return $result;
	}
	
	public static function setLangFormField($form, $name, $type, $required = false, $class = '', $fieldset = '')
	{
		$languages = JLanguageHelper::getLanguages();
		
		foreach ($languages as $language)
		{
			$field = '
				<field name="'.$name.'_'.$language->lang_code.'"
            		type="'.$type.'"
            		label="'.$language->title_native.'"
            		description="Название на этом языке"
            		required="'.$required.'"
            		class="inputbox '.$class.'"
			';
			switch ($type) {
				case 'editor':
				    $field .= '
				    	buttons="true"
                		filter="RAW"';
				    break;
				default:
					$field .= '';
			}
			
			$field .= '/>';
			
			$xml = new SimpleXMLElement($field);
			
			$form->setField($xml, null, true, $fieldset);
		}
		return true;
	}
	
	public static function setLangFormValue($form, $name, $data)
	{
		$languages = JLanguageHelper::getLanguages();
		
		foreach ($languages as $language)
		{
			$str = $name.'_'.$language->lang_code;
				
			$form->setValue($name.'_'.$language->lang_code, null, $data->$str);
		}
		return $form;
	}
	
	public static function setLangTableColumn($name, $table)
	{
		$languages = JLanguageHelper::getLanguages();
		
		foreach ($languages as $language)
		{
			$db = JFactory::getDBO();
			$query = "SHOW COLUMNS FROM `".$table."` LIKE '".$name."_".$language->lang_code."'";
			$db->setQuery($query);
			$res = $db->loadResult();
			
			if (!$res) {
				$query = "ALTER TABLE `".$table."` ADD `".$name."_".$language->lang_code."` VARCHAR(100) NOT NULL;";
				$db->setQuery($query);
    			$result = $db->query();
				if (!$result) return false;
			}
		}
		return true;
	}
	
	public static function getCurrencyName($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('name_' . $lang))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	public static function getCurrencyCode($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('code'))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	public static function getCurrencyParams($currency)
	{
		$lang = JFactory::getLanguage()->getTag();
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)->select($db->qn('params'))->from($db->qn('#__abc_currency'))->where($db->qn('id') . ' = ' . (int) $currency);
		$db->setQuery($query);
		return $db->loadResult();
	}
}