<?php
defined('_JEXEC') or die;

class ExchangerViewOrder extends JViewLegacy
{
	public $form;
	public $item;
	public $user;
	public $state;
	
	public function display($tpl = null)
	{
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->user = JFactory::getUser();
		$this->state = $this->get('State');
		
		if (count( $errors = $this->get('Errors'))) {
			JError::raiseError( 500, implode('\n', $errors));
			return false;
		}
		
		$this->loadHelper('exchanger');
		$this->canDo = exchangerHelper::getActions('order');
		$this->_setToolBar();
		
		parent::display($tpl);
	}
	
	protected function _setToolBar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$isNew = ($this->item->id == 0);
		$canDo = exchangerHelper::getActions('order', $this->item->id);
		JToolBarHelper::title('Заказ: <small>[' . ($isNew ? 'Новый' : 'Изменить') . ']</small>');
		
		if ($isNew) {
			JToolBarHelper::apply('order.apply');
			JToolBarHelper::save('order.save');
			JToolBarHelper::cancel('order.cancel');
		} else {
			if ($canDo->get('core.edit')) {
				JToolBarHelper::apply('order.apply');
				JToolBarHelper::save('order.save');
			}
			JToolBarHelper::cancel('order.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}