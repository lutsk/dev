<?php
defined('_JEXEC') or die;

class ExchangerViewUsers extends JViewLegacy
{
	public $items;
	public $pagination;
	public $state;
	public $user;
	public $userId;
	
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->userId = $this->get('Authors');
		$this->user = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			exchangerHelper::addSubmenu('users');
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}
	
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('Пользователи'));
		$canDo = exchangerHelper::getActions('user');
		
		/*if ($canDo->get('core.create') > 0) {
			JToolBarHelper::addNew('user.add');
		}*/		
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('user.edit');
		}		
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();			
			if ($canDo->get('core.delete')) {
				JToolBarHelper::deleteList('DELETE_QUERY_STRING', 'users.delete', 'JTOOLBAR_DELETE');
				JToolBarHelper::divider();
			}			
			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_exchanger');
				JToolBarHelper::divider();
			}
		}		
	}

	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'published' => JText::_('JSTATUS'),
			'username' => JText::_('Пользователь'),
			'user_id' => JText::_('ID пользователя'),
			'id' => JText::_('JGRID_HEADING_ID')
		);
	}
}