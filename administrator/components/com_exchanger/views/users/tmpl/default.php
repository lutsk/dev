<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		var table = document.getElementById("sortTable");
		var direction = document.getElementById("directionTable");
		var order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="<?= JRoute::_('index.php?option=com_exchanger&view=users'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?= $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>
			
			<?php
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>
			
			<table class="table table-striped" id="userList">
				<thead>
				<tr>
					<th width="1%" class="center hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?= JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="5%" style="min-width:55px" class="center">
						<?php echo JHtml::_( 'searchtools.sort', 'JSTATUS', 'u.published', $listDirn, $listOrder ); ?>
					</th>
					<th>
						Пользователь
					</th>
					<th width="1%" class="center nowrap hidden-phone">
						<?= JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'u.id', $listDirn, $listOrder); ?>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering = ( $listOrder == 'u.id' );
					$canEdit = $user->authorise('core.edit', '#__abc_user.' . $item->id);
					$canCheckin = $user->authorise('core.manage', 'com_exchanger');
					$canChange = $user->authorise( 'core.edit.state', '#__abc_user.' . $item->id ) && $canCheckin;
					?>
					<tr class="row<?= $i % 2; ?>">
						<td class="center hidden-phone">
							<?= JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td class="center">
							<div class="btn-group">
								<?php echo JHtml::_('jgrid.published', $item->published, $i, 'users.', $canChange, 'cb'); ?>
							</div>
						</td>
						<td class="nowrap has-context">
							<div class="pull-left">
								<?php if ($canEdit) : ?>
									<a href="<?php echo JRoute::_('index.php?option=com_exchanger&task=user.edit&id=' . $item->id);?>" title="<?php echo JText::_('JACTION_EDIT'); ?>">
										<?php echo $this->escape($item->name); ?> <?php echo $this->escape($item->surname ); ?>
									</a> (<?php echo $this->escape($item->username); ?>, ID: <?php echo $item->user_id; ?>)
								<?php else : ?>
									<span title="<?php echo JText::sprintf( 'JFIELD_ALIAS_LABEL', $this->escape( $item->alias ) ); ?>"><?php echo $this->escape( $item->title ); ?></span>
								<?php endif; ?>
							</div>
						</td>
						<td class="center hidden-phone">
							<?php echo (int)$item->id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
			
		</div>
</form>