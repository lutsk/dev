<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$language = JFactory::getLanguage()->getTag();
$user = JFactory::getUser();
$userId = $user->get('id');
//echo '<pre>'; print_r($this->items); echo '</pre>'; die();
?>

<form action="<?= JRoute::_('index.php?option=com_exchanger&view=currencys'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?= $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>
			
			<table class="table table-striped" id="articleList">
				<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?= JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th>
						<?= JText::_('CURRENCY_CURRENCY'); ?>
					</th>
					<th>
						Оператор обмена
					</th>
					<th width="1%" class="nowrap hidden-phone">
						<?= JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$title = 'name_' . $language;
					$canEdit = $user->authorise('core.edit', '#__abc_currency.' . $item->id);
					$canCheckin = $user->authorise('core.manage', 'com_exchanger') || $item->checked_out == $userId || $item->checked_out == 0;
					?>
					<tr class="row<?= $i % 2; ?>">
						<td class="center hidden-phone">
							<?= JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td class="nowrap has-context">
							<div class="pull-left">
								<?php if ($canEdit) : ?>
									<a href="<?= JRoute::_('index.php?option=com_exchanger&task=currency.edit&id=' . $item->id); ?>" title="<?= JText::_('JACTION_EDIT'); ?>">
										<?= $this->escape($item->$title); ?></a>
								<?php else : ?>
									<span title="<?= $this->escape($item->$title); ?></span>
								<?php endif; ?>
							</div>
						</td>
						<td class="hidden-phone">
							<?= $item->parent_name; ?>
						</td>
						<td class="center hidden-phone">
							<?= (int)$item->id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
			
		</div>
</form>