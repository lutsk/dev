<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
//echo '<pre>'; print_r($this->items); echo '</pre>'; die();
$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get( 'list.direction'));
$saveOrder = $listOrder == 'ordering';

if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_exchanger&task=rates.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		var table = document.getElementById("sortTable");
		var direction = document.getElementById("directionTable");
		var order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="<?= JRoute::_('index.php?option=com_exchanger&view=rates'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?= $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>
			
			<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>
			
			<table class="table table-striped" id="articleList">
				<thead>
				<tr>
					<th width="1%" class="center hidden-phone" nowrap="nowrap">
						<?= JHtml::_('searchtools.sort', '', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
					</th>
					<th width="1%" class="center hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?= JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="5%" style="min-width:55px" class="center">
						<?= JHtml::_('searchtools.sort', 'JSTATUS', 'state', $listDirn, $listOrder); ?>
					</th>
					<th>
						Направление
					</th>
					<th>
						Курс обмена
					</th>
					<th>
						
					</th>
					<th width="1%" class="nowrap hidden-phone">
						<?= JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering = ($listOrder == 'a.ordering');
					$canEdit = $user->authorise('core.edit', '#__abc_rate.' . $item->id);
					$canCheckin = $user->authorise('core.manage', 'com_exchanger') || $item->checked_out == $userId || $item->checked_out == 0;
					$canChange = $user->authorise('core.edit.state', '#__abc_rate.' . $item->id) && $canCheckin;
					?>
					<tr class="row<?= $i % 2; ?>">
						<td class="order nowrap center hidden-phone">
							<?php if ( $canChange ) :
								$disableClassName = '';
								$disabledLabel = '';
								if (!$saveOrder) :
									$disabledLabel = JText::_('JORDERINGDISABLED');
									$disableClassName = 'inactive tip-top';
								endif; ?>
								<span class="sortable-handler <?= $disableClassName ?>" title="<?= $disabledLabel ?>" rel="tooltip"><i class="icon-menu"></i></span>
								<input type="text" style="display:none" name="order[]" size="5"
									value="<?= $item->ordering; ?>" class="width-20 text-area-order " />
							<?php else : ?>
								<span class="sortable-handler inactive"><i class="icon-menu"></i></span>
							<?php endif; ?>
						</td>
						<td class="center hidden-phone">
							<?= JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td class="center">
							<div class="btn-group">
								<?= JHtml::_('jgrid.published', $item->published, $i, 'rates.', $canChange, 'cb'); ?>
							</div>
						</td>
						<td class="nowrap has-context">
							<div class="pull-left">
								<strong><?= $item->currency_from_name; ?></strong> &#8594; <strong><?= $item->currency_to_name; ?></strong>								
							</div>
						</td>
						<td class="hidden-phone">
							<?= $item->currency_from_course; ?> &#8594; <?= $item->currency_to_course; ?>
						</td>
						<td class="hidden-phone">
							<?php if ($canEdit) : ?>
							<a href="<?= JRoute::_('index.php?option=com_exchanger&task=rate.edit&id=' . $item->id); ?>" title="<?= JText::_('JACTION_EDIT'); ?>"><?= JText::_('JACTION_EDIT'); ?></a>
							<?php endif; ?>
						</td>
						<td class="center hidden-phone">
							<?= (int)$item->id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			
			<?= $this->pagination->getListFooter(); ?>
			
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?= JHtml::_('form.token'); ?>
			
		</div>
</form>