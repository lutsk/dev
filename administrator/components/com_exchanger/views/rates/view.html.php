<?php
defined('_JEXEC') or die;

class ExchangerViewRates extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $user;
	
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->user = JFactory::getUser();
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		$this->loadHelper('exchanger');
		
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			exchangerHelper::addSubmenu('rates');
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}
	
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('Направления обмена'));
		$canDo = exchangerHelper::getActions('rate');
		
		if ($canDo->get('core.create') > 0) {
			JToolBarHelper::addNew('rate.add');
		}		
		if (($canDo->get('core.edit'))) {
			JToolBarHelper::editList('rate.edit');
		}		
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::publish('rates.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('rates.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();			
			if ($canDo->get('core.delete')) {
				JToolBarHelper::deleteList('DELETE_QUERY_STRING', 'rates.delete', 'JTOOLBAR_DELETE');
				JToolBarHelper::divider();
			}			
			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_exchanger');
				JToolBarHelper::divider();
			}
		}		
	}

	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'published' => JText::_('JSTATUS'),
			'id' => JText::_('JGRID_HEADING_ID')
		);
	}
}