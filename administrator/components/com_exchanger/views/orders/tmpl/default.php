<?php
defined( '_JEXEC' ) or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'id';

$sortFields = $this->getSortFields();

$registry = new JRegistry;
?>
<style>
.order_status .btn-micro {
	font-size: 14px;
}
.order_status .icon-arrow-right-2::before {
	color: #378137;
}
</style>

<form action="<?php echo JRoute::_('index.php?option=com_exchanger&view=orders'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar )): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<?php
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>

			<table class="table table-striped" id="articleList">
				<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="1%" class="nowrap hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
					</th>
					<th width="10%">
						Дает
					</th>
					<th>
						Сумма
					</th>
					<th width="10%">
						Получает
					</th>
					<th>
						Сумма
					</th>
					<th>
						Кошелек
					</th>
					<th>
						Юзер
					</th>
					<th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'JDATE', 'created_time', $listDirn, $listOrder); ?>
					</th>
					<th width="10%" class="center">
						Статус
					</th>
					<th width="8%" class="center">
						Действия
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($this->items as $i => $item) :
					$canEdit = $user->authorise('core.edit', '#__abc_orders.' . $item->id);
					
					$currency_from_params = exchangerHelper::getCurrencyParams($item->currency_from_id);
					$currency_to_params = exchangerHelper::getCurrencyParams($item->currency_to_id);
					
					$registry->loadString($currency_from_params);
					$currency_from_params = $registry->toArray();
					$currency_from_simbols = $currency_from_params['simbols'];
					$currency_from_pattern = (string) '%01.'.$currency_from_simbols.'f';
					
					$registry->loadString($currency_to_params);
					$currency_to_params = $registry->toArray();
					$currency_to_simbols = $currency_to_params['simbols'];
					$currency_to_pattern = (string) '%01.'.$currency_to_simbols.'f';
					
					switch ($item->status) {
						case 1:
						    $btnClass = 'btn-info';
						    $btnTitle = 'Исполняется';
						    break;
						case 2:
						    $btnClass = 'btn-warning';
						    $btnTitle = 'Отменена';
						    break;
						case 3:
						    $btnClass = 'btn-danger';
						    $btnTitle = 'Ошибка оплаты';
						    break;
						case 4:
						    $btnClass = 'btn-success';
						    $btnTitle = 'Завершена';
						    break;
						default:
						    $btnClass = 'btn-primary';
						    $btnTitle = 'Обрабатывается';
					}
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td class="hidden-phone">
							<?php echo $item->id; ?>
						</td>
						<td class="nowrap">
							<?php echo exchangerHelper::getCurrencyName($item->currency_from_id); ?>, <?php echo exchangerHelper::getCurrencyCode($item->currency_from_id); ?>
						</td>
						<td class="">
							<?= sprintf($currency_from_pattern, $item->amount_from) ;?>
						</td>
						<td class="nowrap">
							<?php echo exchangerHelper::getCurrencyName($item->currency_to_id); ?>, <?php echo exchangerHelper::getCurrencyCode($item->currency_to_id); ?>
						</td>
						<td class="">
							<?= sprintf($currency_to_pattern, $item->amount_to) ;?>
						</td>
						<td class="has-context">
							<?php echo $item->wallet; ?>
						</td>
						<td class="nowrap">
							<?php echo $item->user; ?> <?php echo $item->middle_name; ?>
						</td>
						<td class="nowrap hidden-phone">
							<?php echo JHtml::_('date', $item->created_time, JText::_('DATE_FORMAT_LC2')); ?>
						</td>
						<td>
							<span class="btn  btn-small <?php echo $btnClass; ?>" style="display:block;"><?php echo $btnTitle; ?></span>
						</td>
						<td class="center order_status">
							<?php if ($canEdit) : ?>
								<a href="<?php echo JRoute::_('index.php?option=com_exchanger&task=order.edit&id=' . $item->id); ?>" title="<?php echo JText::_( 'JACTION_EDIT' ); ?>"><span class="btn icon-edit"></span></a>
							<?php endif; ?>
							<a href="<?php echo JRoute::_(JUri::root().'index.php?option=com_exchanger&view=order&id=' . $item->id, false); ?>" title="Посмотреть" target="_blank"><span class="btn icon-arrow-right-2"></span></a>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php echo $this->pagination->getListFooter(); ?>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>

		</div>
</form>