<?php
defined( '_JEXEC' ) or die;

class ExchangerViewOrders extends JViewLegacy
{
	public $items;
	public $pagination;
	public $state;
	public $user;
	public $authors;
	
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->authors = $this->get('Authors');
		$this->user = JFactory::getUser();
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		$this->loadHelper('exchanger');
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			exchangerHelper::addSubmenu('orders');
			$this->sidebar = JHtmlSidebar::render();
		}
		parent::display( $tpl );
	}
	
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('Заказы'));
		$canDo = exchangerHelper::getActions('order');
		
		if (($canDo->get('core.edit'))) {
			JToolBarHelper::editList('order.edit');
		}
		if ($canDo->get('core.delete')) {
			JToolBarHelper::deleteList('DELETE_QUERY_STRING', 'orders.delete', 'JTOOLBAR_DELETE');
			JToolBarHelper::divider();
		}
		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_exchanger');
			JToolBarHelper::divider();
		}		
	}
	
	protected function getSortFields()
	{
		return array(
			'status' => JText::_( 'JSTATUS' ),
			'user_id' => JText::_( 'JAUTHOR' ),
			'created_time' => JText::_( 'JDATE' ),
			'id' => JText::_( 'JGRID_HEADING_ID' )
		);
	}
}