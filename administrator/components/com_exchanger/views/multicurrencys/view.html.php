<?php
defined('_JEXEC') or die;

class ExchangerViewMulticurrencys extends JViewLegacy
{
	public $items;
	public $user;
	
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->user = JFactory::getUser();
		
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			exchangerHelper::addSubmenu('multicurrencys');
			$this->sidebar = JHtmlSidebar::render();
		}
		
		parent::display($tpl);
	}
	
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('Мультивалюты'));
		$canDo = exchangerHelper::getActions('multicurrency');
		
		if ($canDo->get('core.create') > 0) {
			JToolBarHelper::addNew('multicurrency.add');
		}		
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('multicurrency.edit');
		}		
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();			
			if ($canDo->get('core.delete')) {
				JToolBarHelper::deleteList('DELETE_QUERY_STRING', 'multicurrencys.delete', 'JTOOLBAR_DELETE');
				JToolBarHelper::divider();
			}			
			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_exchanger');
				JToolBarHelper::divider();
			}
		}		
	}
}