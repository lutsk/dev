<?php
defined('_JEXEC') or die;

class ExchangerViewRate extends JViewLegacy
{
	public $form;
	public $item;
	public $user;
	public $state;
	
	public function display($tpl = null)
	{
		$this->item = $this->get('Item');
		$form = $this->get('Form');
		$this->form = exchangerHelper::setLangFormValue($form, 'description', $this->item);
		$this->user = JFactory::getUser();
		$this->state = $this->get('State');
		
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('\n', $errors));
			return false;
		}
		
		$this->loadHelper('exchanger');
		$this->canDo = exchangerHelper::getActions('rate');
		$this->_setToolBar();
		parent::display($tpl);
	}
	
	protected function _setToolBar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$isNew = ($this->item->id == 0);
		$canDo = exchangerHelper::getActions('rate', $this->item->id);
		JToolBarHelper::title(JText::_('COM_EXCHANGER') . ': <small>[ ' . ($isNew ? JText::_('JTOOLBAR_NEW') : JText::_('JTOOLBAR_EDIT')) . ']</small>');
		
		if ($isNew) {
			JToolBarHelper::apply('rate.apply');
			JToolBarHelper::save('rate.save');
			JToolBarHelper::save2new('rate.save2new');
			JToolBarHelper::cancel('rate.cancel');
		} else {
			if ($canDo->get('core.edit')) {
				JToolBarHelper::apply('rate.apply');
				JToolBarHelper::save('rate.save');
				
				if ($canDo->get('core.create')) {
					JToolBarHelper::save2new('rate.save2new');
				}
			}			
			JToolBarHelper::cancel('rate.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}