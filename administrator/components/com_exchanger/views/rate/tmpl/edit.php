<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$id = $input->getInt('id', 0);
?>
<style>iframe{max-height: 300px;}</style>
<script type="text/javascript">
	Joomla.submitbutton = function (task) {
		if (task == 'rate.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
			Joomla.submitform(task, document.getElementById('item-form'));
		} else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
		}
	}
</script>
<form action="<?= JRoute::_('index.php?option=com_exchanger&layout=edit&id=' . $this->form->getValue('id')); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">	
	<div class="form-horizontal">
		<?= JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
		
		<?= JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('Направление обмена', true)); ?>
		<div class="row-fluid">
			<div class="span4">
				<div class="form-vertical">
					<?= $this->form->renderFieldset('default_from'); ?>
				</div>
			</div>
			<div class="span4">
				<div class="form-vertical">
					<?= $this->form->renderFieldset('default_to'); ?>
				</div>
			</div>
			<div class="span4">
				<div class="form-vertical">
					<?= $this->form->renderFieldset('default'); ?>
				</div>
			</div>
		</div>
		
		<?php if ($id == 0) : ?>
		<div class="row-fluid">
			<div class="span4 text-center" id="btn-reverse">
                <button class="btn btn-primary" type="button" id="add_reverse_course">Обратный курс</button>
            </div>
		</div>
		<?php endif ?>
		
		<div id="reverse_course" class="row-fluid">
			<?php if ($id > 0) : ?>
			<div class="span4">
				<div class="form-vertical">
				    <?= $this->form->renderField('amount_to_reverse'); ?>
				</div>
			</div>
			<div class="span4">
				<div class="form-vertical">
					<?= $this->form->renderField('amount_from_reverse'); ?>
				</div>
			</div>
			<?php endif ?>
		</div>
		<?= JHtml::_('bootstrap.endTab'); ?>
		
		<?= JHtml::_('bootstrap.addTab', 'myTab', 'descriptions', JText::_('Описание', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<div class="form-vertical">
				<?= $this->form->renderFieldset('descriptions'); ?>
				</div>
			</div>
		</div>
		<?= JHtml::_('bootstrap.endTab'); ?>
		
		<?php if ($this->canDo->get('core.admin')) : ?>
			<?= JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JCONFIG_PERMISSIONS_LABEL', true)); ?>
			<?= $this->form->getInput('rules'); ?>
			<?= JHtml::_('bootstrap.endTab'); ?>
		<?php endif ?>
		
		<?= JHtml::_('bootstrap.endTabSet'); ?>
	</div>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="return" value="<?= $input->getCmd('return'); ?>" />
	<?= JHtml::_('form.token'); ?>
	
</form>

<div class="form-group hide" id="rev">
	<div class="span4">
		<div class="form-vertical">
			<?= $this->form->renderField('amount_from_reverse'); ?>
		</div>
	</div>
	<div class="span4">
		<div class="form-vertical">
		    <?= $this->form->renderField('amount_to_reverse'); ?>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).on('click', '#add_reverse_course', function (e) {
            const rev =jQuery('#rev').html();
            jQuery('#reverse_course').html(rev);
            jQuery('#btn-reverse').html('<button class="btn btn-warning" type="button" id="remove_reverse_course"><b>-</b></button>');
})
jQuery(document).on('click', '#remove_reverse_course', function (e) {
            jQuery('#reverse_course').html('');
            jQuery('#btn-reverse').html('<button class="btn btn-primary" type="button" id="add_reverse_course"><b>Обратный курс</b></button>');
});

jQuery(document).ready(function (e) {
    const id = jQuery('#jform_currency_from_id').val();
    const to = jQuery('#jform_currency_to_id').val();

    if (id !== "" && to === null) {
        //console.log("Make AJAX - " + id);
        //makeAjax(id);
    }
});
jQuery('#jform_currency_from_id').change(function (e) {
    var id = jQuery(this).children("option:selected").val();
    //console.log("You have selected the currency- " + id);
    makeAjax(id);

});

function makeAjax(id) {
    jQuery.ajax({
        url: '/administrator/index.php?option=com_exchanger&task=rate.makeSelect&id=' + id,
        type: 'POST',
        dataType: 'json',
        success: function (res) {
            if (res.status === 200) {
                //console.log(res.data);
                jQuery('#jform_currency_to_id')
                    .find('option')
                    .remove()
                    .end()
                ;
                res.data.map(function (e, key) {
                	console.log(e);
                    jQuery("#jform_currency_to_id").append(new Option(e.text, e.value));
                    if(jQuery('#jform_currency_to_id_chzn').length){
						jQuery('#jform_currency_to_id').trigger("liszt:updated");
					}
                });
            }

        }
    });
}
</script>