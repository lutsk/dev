<?php
defined('_JEXEC') or die;

class ExchangerViewUser extends JViewLegacy
{
	public $form;
	public $item;
	public $user;
	public $state;
	
	public function display($tpl = null)
	{
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->user = JFactory::getUser();
		$this->state = $this->get('State');
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('\n', $errors));
			return false;
		}
		$this->loadHelper('exchanger');
		$this->canDo = exchangerHelper::getActions('user');
		$this->_setToolBar();
		parent::display($tpl);
	}
	
	protected function _setToolBar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$isNew = ($this->item->id == 0);
		$canDo = exchangerHelper::getActions('user', $this->item->id);
		JToolBarHelper::title(JText::_('Пользователь') . ': <small>[ ' . ($isNew ? JText::_('JTOOLBAR_NEW') : JText::_('JTOOLBAR_EDIT')) . ']</small>');
		
		if ($isNew) {
			JToolBarHelper::apply('user.apply');
			JToolBarHelper::save('user.save');
			JToolBarHelper::save2new('user.save2new');
			JToolBarHelper::cancel('user.cancel');
		} else {
			if ($canDo->get('core.edit')) {
				JToolBarHelper::apply('user.apply');
				JToolBarHelper::save('user.save');
				
				if ($canDo->get('core.create')) {
					JToolBarHelper::save2new('user.save2new');
				}
			}
			
			JToolBarHelper::cancel('user.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}