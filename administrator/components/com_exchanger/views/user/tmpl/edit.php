<?php
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.modal');

$input = JFactory::getApplication()->input;

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'currency.cancel' || document.formvalidator.isValid(document.getElementById('item-form'))) {
			Joomla.submitform(task, document.getElementById('item-form'));
		}
	};
");

$input = JFactory::getApplication()->input;
?>
<form action="<?php echo JRoute::_('index.php?option=com_exchanger&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">
	
	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
		
		<?php echo JHtml::_( 'bootstrap.addTab', 'myTab', 'general', JText::_('Основные данные', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('user_id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('user_id'); ?> (ID пользователя: <?php echo $this->item->user_id; ?>)</div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('surname'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('surname'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('middle_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('middle_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('passport_series'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('passport_series'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('passport_id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('passport_id'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('phone'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('phone'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('mobile_phone'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('mobile_phone'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('us_citizen'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('us_citizen'); ?></div>
				</div>
				<?php if ($this->item->photo_passport != ''): ?>
				<div class="control-group">
					<div class="control-label">Фото паспорта</div>
					<div class="controls">
						<a class="modal" href="../images/users/<?= $this->item->user_id; ?>/<?= $this->item->photo_passport; ?>"><img src="../images/users/<?= $this->item->user_id; ?>/<?= $this->item->photo_passport; ?>" alt="" width="200" /></a>
					</div>
				</div>
				<?php endif ?>
				<?php if ($this->item->photo_card != ''): ?>
				<div class="control-group">
					<div class="control-label">Фото карточк</div>
					<div class="controls">
						<a class="modal" href="../images/users/<?= $this->item->user_id; ?>/<?= $this->item->photo_card; ?>"><img src="../images/users/<?= $this->item->user_id; ?>/<?= $this->item->photo_card; ?>" alt="" width="200" /></a>
					</div>
				</div>
				<?php endif ?>
			</div>
		</div>
		<?php echo JHtml::_( 'bootstrap.endTab' ); ?>
		
		<?php echo JHtml::_( 'bootstrap.addTab', 'myTab', 'publishing', JText::_('Кошельки', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<table class="table">
					<?php foreach ($this->item->wallets as $wallet) : ?>
					<tr>
						<td><img src="../<?= $wallet->image ;?>" alt="<?= $item->name;?>" width="40" style="margin-right: 20px;" /><?= $wallet->name;?>, <?= $wallet->code;?></td>
						<td style="vertical-align: middle;font-size: 16px;"><?= $wallet->wallet ;?></td>
					</tr>
				<?php endforeach; ?>
				</table>
			</div>
		</div>
		<?php echo JHtml::_( 'bootstrap.endTab' ); ?>
		
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</div>
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="return" value="<?php echo $input->getCmd('return'); ?>" />
	<?php echo JHtml::_('form.token'); ?>
	
</form>