<?php
defined( '_JEXEC' ) or die;

class ExchatViewExchat extends JViewLegacy
{
	protected $params;
	
	public function display($tpl = null)
	{
		$this->status = $this->get('Status');
		$this->params = JComponentHelper::getParams('com_exchat');
		
		if ( count( $errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('\n', $errors));
			return false;
		}
		$this->_setToolBar();
		ChatHelper::addSubmenu('chat');
		$this->sidebar = JHtmlSidebar::render();
		
		ChatHelper::setDocument();
		
		parent::display($tpl);
	}
	
	protected function _setToolBar()
	{
		JToolBarHelper::title('Чат');
		$canDo = ChatHelper::getActions('chat');
		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_exchat');
		}
	}
}