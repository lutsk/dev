<?php
defined('_JEXEC') or die;

ini_set('display_errors','on');
error_reporting(E_ALL);  
 
    $resTest = Array();
    $color = 'green';
    $f = fsockopen("app.comet-server.ru", 80,$e1,$e2);
    if(!$f)
    {
        $resTest[] = Array("info" => "Не удалось создать соединение к comet-server.ru: error code:".$e1."(".$e2.")", "error" => true);
    	$color = 'red';
    }
    else
    {
        $str1 = "GET / HTTP/1.1\r\nHost: app.comet-server.ru\r\n\r\n";  
        if( fputs($f, $str1, strlen($str1) ) === false)
        { 
            $resTest[] = Array("info" => "fputs error on app.comet-server.ru", "error" => true);
    		$color = 'red';
        }
        else
        {
            $tmp = fgets($f); 
            $resTest[] = Array("info" => "", "error" => false);
        }
    }
 
    $f = fsockopen("googl.com", 80,$e1,$e2);
    if(!$f)
    {
        $resTest[] = Array("info" => "Не удалось создать соединение к googl.com: error code:".$e1."(".$e2.")", "error" => true);
    	$color = 'red';
    }
    else
    {
        $str1 = "GET / HTTP/1.1\r\nHost: googl.com\r\n\r\n";  
        if( fputs($f, $str1, strlen($str1) ) === false)
        { 
            $resTest[] = Array("info" => "fputs error on googl.com", "error" => true);
    		$color = 'red';
        }
        else
        {
            $tmp = fgets($f); 
            $resTest[] = Array("info" => "", "error" => false);
        }
    }
 
    $link = mysqli_connect("app.comet-server.ru", $this->params->get('dev_id'), $this->params->get('dev_key'), "CometQL_v1");
    if(!$link)
    {
        $resTest[] = Array("info" => "Не удалось создать соединение c CometQL (Использование CometQL не возможно)", "error" => true);
    	$color = 'red';
    }
    else
    {
 
        $result = mysqli_query (  $link, "show status" ); 
        if(mysqli_errno($link) != 0 && @mysqli_num_rows($result))
        {
            $resTest[] = Array("info" => "<b>Error code:<a href='https://comet-server.ru/wiki/doku.php/comet:cometql:error'  target='_blank' >".mysqli_errno($link)."</a>&nbsp;&nbsp;Error text:<a href='https://comet-server.ru/wiki/doku.php/comet:cometql:error' target='_blank' >".mysqli_error($link)."</a></b>", "error" => true);
    		$color = 'red';
        }
        else
        { 
            $resTest[] = Array("info" => "", "error" => false);
        }
    }
    
?>
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?= $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>
			<script>
			/*jQuery(document).ready(function(){
				cometApi.start({dev_id:<?= $this->params->get('dev_id'); ?>, user_id:'', user_key:'', node:"app.comet-server.ru"});
				cometApi.count_users_in_pipe("web_chat_pipe", function(res)
				{
				    //console.log("count_users_in_pipe", res, res.data.user_in_pipe);
				    jQuery('#users_in_pipe').html(res.data[1]);
				})
			});*/
			</script>
			<?php 
			    if($resTest[0]["error"]){ echo "<b style='color:#F77;' >";}else{echo "<b style='color:#494;' >";}
			        echo $resTest[0]["info"]."</b><br>\n";
			 
			    if($resTest[1]["error"]){ echo "<b style='color:#F77;' >";}else{echo "<b style='color:#494;' >";}
			        echo $resTest[1]["info"]."</b><br>\n";
			 
			    if($resTest[2]["error"]){ echo "<b style='color:#F77;' >";}else{echo "<b style='color:#494;' >";}
			        echo $resTest[2]["info"]."</b><br>\n";
			 ?>
			<div class="connection-container center">
				<div id="websocket" class="connection_stat <?= $color; ?> darken"></div><p class="statistic_text_text">Статус соединения с сервером</p>
			</div>
			<!--<div class="connection-container center">
			<p>Пользователей: <span id="users_in_pipe"></span></p>
			</div>-->
	
	<div style="margin-top: 50px;">
		<div style="margin-bottom: 20px;">
			<p>Выборка истории сообщений, если функция сохранения истории включена для этого канала:</p>
			<code>SELECT * FROM pipes_messages WHERE name = 'web_chat_pipe';</code>
		</div>
		<div style="margin-bottom: 20px;">
			<p>Очищает историю сообщений:</p>
			<code>DELETE FROM pipes_messages WHERE name = 'web_chat_pipe';</code>
		</div>
		<div style="margin-bottom: 20px;">
			<p>Установка сохранения количества сообщений (20 - количество):</p>
			<code>INSERT INTO pipes_settings ("name", "length") VALUES ('web_chat_pipe', 20);</code>
		</div>
		<div style="margin-bottom: 20px;">
			<p>Получить значения настроек канала:</p>
			<code>SELECT * FROM pipes_settings WHERE name = 'web_chat_pipe';</code>
		</div>
		<div style="margin-bottom: 20px;">
			<p>Очистка настроек канала:</p>
			<code>DELETE FROM  pipes_settings WHERE name = 'web_chat_pipe';</code>
		</div>
	</div>
		</div>

<style>
#CometQL-cli{display:none;position:fixed;bottom:40px;right:20px;background-color:#fff;padding:10px;border:1px solid;z-index:30;box-shadow:0 0 10px #222}
#CometQL-cli-text{margin-bottom:5px;margin-top:5px}
#CometQL-cli-text textarea{padding:3px;font-family:Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;max-height:40px;height:40px;min-width:460px;max-width:1600px;margin-right:5px}
.CometQL-button{padding:3px;font-family:Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;float:right;height:40px;width:40px}
#CometQL-cli-reslog{overflow:auto;height:300px;border:1px solid #ccc;background-color:#fafafa;font-family:Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;padding:10px;max-width:1600px}
#CometQL-cli-Header a{float:right}
#CometQL-cli-open{font-size:16px;position:fixed;bottom:40px;right:20px;background-color:#fff;padding:10px;border:1px solid;z-index:30;box-shadow:0 0 10px #222;cursor:pointer}
</style>
<script>
    function send_cometQL_query(text)
    {
        jQuery.ajax({
            url: "/administrator/index.php?option=com_exchat&task=CometQL&query="+encodeURIComponent(text),
            type: "GET",
            success: function(data) 
            {
                jQuery("#CometQL-cli-reslog").append(data).scrollTop(999999); 
                
            }
        });
    } 
</script>
<div id="CometQL-cli">

    <div id='CometQL-cli-Header'>Консоль для работы с CometQL <a href="" onclick="jQuery('#CometQL-cli').hide();  jQuery('#CometQL-cli-open').show(); return false;" >[X]</a></div>
    <div id='CometQL-cli-text'>
        <textarea id='cometQL-query-text' >SELECT * FROM pipes_messages WHERE name = "web_chat_pipe";</textarea>
        <button class="CometQL-button" onclick='send_cometQL_query(jQuery("#cometQL-query-text").val())'>Ok</button>
    </div>
    <div id='CometQL-cli-reslog'></div>
</div>
<div id="CometQL-cli-open" onclick="jQuery('#CometQL-cli').show(); jQuery('#CometQL-cli-open').hide();" >
    Консоль для работы с CometQL.
</div>