<?php
defined( '_JEXEC' ) or die;

require_once JPATH_COMPONENT.'/helpers/chat.php';
$controller = JControllerLegacy::getInstance('exchat');
$controller->execute( JFactory::getApplication()->input->get( 'task' ) );
$controller->redirect();