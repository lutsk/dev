<?php
defined('_JEXEC') or die;

class ChatHelper
{
	static function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
			JText::_('Чат'),
			'index.php?option=com_exchat&view=chat',
			$vName == 'chat');
	}
	
	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;
		$assetName = 'com_exchat';
		$actions = JAccess::getActions($assetName);
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}
		return $result;
	}
	
	static function setDocument()
	{
		$doc = JFactory::getDocument();
		/*$doc->addScript($baseUrl . '/plugins/system/exsocket/assets/reconnecting-websocket.js');*/
		$doc->addScript(JUri::root() . 'plugins/system/exsocket/assets/chat.js');
		$doc->addStyleSheet(JUri::base() . 'components/com_exchat/assets/styles/chat.css');
	}
}