<?php
defined('_JEXEC') or die;

class ExchangerControllerCurrency extends JControllerForm
{

	function __construct($config = array())
	{
		$this->view_list = 'currencys';
		parent::__construct($config);
	}
	
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int)isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		
		if ($user->authorise('core.edit', 'com_exchanger.currency.' . $recordId)) {
			return true;
		}
		
		return false;
	}
}