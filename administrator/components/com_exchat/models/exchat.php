<?php
defined('_JEXEC') or die;

class ExchatModelExchat extends JModelLegacy
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	public function getStatus()
	{		
		return true;
	}
	
	public function CometQL($query)
	{		
		$params = JComponentHelper::getParams('com_exchat');

		$link = mysqli_connect("app.comet-server.ru", $params->get('dev_id'), $params->get('dev_key'), "CometQL_v1");
		if ( !$link ) die ("Unable to connect to CometQL");
		  
		if( !isset($query) || empty($query))
		{
		    echo "The query parameter is not set.";
		    exit();
		}

		$result = mysqli_query (  $link, $query ); 

		echo "CometQL>".htmlspecialchars($query)."<br>\n";
		    if(mysqli_errno($link) != 0)
		    {
		        echo "<b>Error code:<a href='https://comet-server.ru/wiki/doku.php/en:comet:cometql:error'  target='_blank' >".mysqli_errno($link)."</a>&nbsp;&nbsp;Error text:<a href='https://comet-server.com/wiki/doku.php/en:comet:cometql:error' target='_blank' >".mysqli_error($link)."</a></b>";
		    }
		    else if(@mysqli_num_rows($result))
		    {
		        $row = mysqli_fetch_assoc($result);
		        echo "<table>\n";
		                echo "\t<tr>\n";
		            foreach ($row as $key => $value) 
		            { 
		                echo "\t\t<td>".$key."</td>\n"; 
		            }
		                echo "\t</tr>\n";
		            do
		            {
		                echo "\t<tr>\n";
		                foreach ($row as $key => $value) 
		                { 
		                    echo "\t\t<td>".htmlspecialchars($value)."</td>\n"; 
		                } 
		                echo "\t</tr>\n";
		            }while($row = mysqli_fetch_assoc($result));
		        echo "</table>\n";
		        echo "<b>".mysqli_num_rows($result)." rows in set</b>\n";
		    }
		    else
		    {
		        echo "<b>Empty set</b>\n";
		    }
		    
		    echo "<hr>";

		mysqli_close ( $link );
	}
}