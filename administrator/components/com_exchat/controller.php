<?php
defined('_JEXEC') or die;

class ExchatController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = array())
	{
		$this->default_view = 'exchat';
		parent::display( $cachable, $urlparams );
		return $this;
	}

	function CometQL()
	{
		$query = JFactory::getApplication()->input->get('query', '', 'RAW');
		$model = $this->getModel('exchat');
		$model->CometQL($query);
		die();
	}
}