<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
 + Contact:     www.burujsolutions.com , info@burujsolutions.com
 * Created on:  May 22, 2015
  ^
  + Project:    JS Tickets
  ^
 */
defined('_JEXEC') or die('Restricted access');

global $mainframe;

$document = JFactory::getDocument();

if (JVERSION < 3) {
    JHtml::_('behavior.mootools');
    $document->addScript('components/com_jssupportticket/include/js/jquery.js');
} else {
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');
}
$document->addScript('components/com_jssupportticket/include/js/jquery_idTabs.js');

$captchaselection = array(
    array('value' => '1', 'text' => JText::_('Google Recaptcha')),
    array('value' => '2', 'text' => JText::_('Own Captcha'))
);
$owncaptchaoparend = array(
    array('value' => '2', 'text' => JText::_('2')),
    array('value' => '3', 'text' => JText::_('3'))
);
$owncaptchatype = array(
    array('value' => '0', 'text' => JText::_('Any')),
    array('value' => '1', 'text' => JText::_('Addition')),
    array('value' => '2', 'text' => JText::_('Subtraction'))
);


$date_format = array(
    '0' => array('value' => 'd-m-Y', 'text' => JText::_('DD-MM-YYYY')),
    '1' => array('value' => 'm-d-Y', 'text' => JText::_('MM-DD-YYYY')),
    '2' => array('value' => 'Y-m-d', 'text' => JText::_('YYYY-MM-DD')),);

$yesno = array(
    '0' => array('value' => '1',
        'text' => JText::_('JYes')),
    '1' => array('value' => '0',
        'text' => JText::_('JNo')),);


$overduetype_array = array(
    '0' => array('value' => '1',
        'text' => JText::_('Days')),
    '1' => array('value' => '2',
        'text' => JText::_('Hours')),);


$enableddisabled = array(
    '0' => array('value' => '1',
        'text' => JText::_('Enabled')),
    '1' => array('value' => '0',
        'text' => JText::_('Disabled')),);

$showhide = array(
    '0' => array('value' => '1',
        'text' => JText::_('Show')),
    '1' => array('value' => '0',
        'text' => JText::_('Hide')),);

$ticketidsequence = array(
    '0' => array('value' => '1',
        'text' => JText::_('Random')),
    '1' => array('value' => '2',
        'text' => JText::_('Sequential')),);

$offline = JHTML::_('select.genericList', $yesno, 'offline', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['offline']);

$curlocation = JHTML::_('select.genericList', $yesno, 'cur_location', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['cur_location']);

$date_format = JHTML::_('select.genericList', $date_format, 'date_format', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['date_format']);


$overduetype = JHTML::_('select.genericList', $overduetype_array, 'ticket_overdue_type', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['ticket_overdue_type']);


$big_field_width = 40;
$med_field_width = 25;
$sml_field_width = 15;
?>
<div id="js-tk-admin-wrapper">
    <div id="js-tk-leftmenu">
        <?php include_once('components/com_jssupportticket/views/menu.php'); ?>
    </div>
    <div id="js-tk-cparea">
        <div id="js-tk-heading"><h4><img id="js-admin-responsive-menu-link" src="components/com_jssupportticket/include/images/c_p/left-icons/menu.png" /><?php echo JText::_('Configurations'); ?></h4></div>
        <form action="index.php" method="POST" name="adminForm" id="adminForm">
            <div id="tabs_wrapper" class="tabs_wrapper js-col-lg-12 js-col-md-12">
                <div class="idTabs">
                    <span><a class="selected" href="#generalsetting"><?php echo JText::_('General Setting'); ?></a></span>
                    <span><a  href="#ticketsetting"><?php echo JText::_('Ticket Setting'); ?></a></span>
                    <span><a  href="#emialsetting"><?php echo JText::_('Default System Email'); ?></a></span>
                    <span><a  href="#auotrespondersetting"><?php echo JText::_('Mail Setting'); ?></a></span>
                    <span><a  href="#menusetting"><?php echo JText::_('Menu Setting'); ?></a></span>
                </div>
                <div id="generalsetting">
                        <legend><?php echo JText::_('General Setting'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Title'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><input type="text" name="title" value="<?php echo $this->configuration['title']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" maxlength="255" /></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Offline'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><?php echo $offline; ?></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Offline Message'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><textarea name="offline_text" cols="25" rows="3" class="inputbox"><?php echo $this->configuration['offline_text']; ?></textarea> </div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Data Directory'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><input type="text" name="data_directory" value="<?php echo $this->configuration['data_directory']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>"/> </div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('You need to rename the existing data directory in file system before changing the data directory name'); ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Date Format'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><?php echo $date_format; ?></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('No. of attachment'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><input type="text" name="noofattachment" value="<?php echo $this->configuration['noofattachment']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" /></div>
                                <div class="js-col-lg-4 js-col-md-4"><br clear="all"/><small><?php echo JText::_('No. of attachment allowed at a time'); ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('File maximum size'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><input type="text" name="filesize" value="<?php echo $this->configuration['filesize']; ?>" class="inputbox" size="<?php echo $med_field_width; ?>" /> &nbsp;KB</div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('File extension'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><textarea name="fileextension" cols="25" rows="3" class="inputbox"><?php echo $this->configuration['fileextension']; ?></textarea></div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('File extension allowed to attach') ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Breadcrumbs'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><?php echo $curlocation; ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><small><?php echo JText::_('Show hide breadcrumbs'); ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Show count on my tickets'); ?></div>
                                <div class="js-col-lg-8 js-col-md-8 js-config-value"><?php echo JHTML::_('select.genericList', $yesno, 'show_count_tickets', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['show_count_tickets']); ?></div>
                            </div>
                        </div>
                </div>
                <div id="ticketsetting">
                        <legend><?php echo JText::_('Ticket Setting'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Ticketid sequence'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><?php echo JHTML::_('select.genericList', $ticketidsequence, 'ticketid_sequence', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['ticketid_sequence']); ?></div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('Set ticketid sequential or random'); ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Reopen ticket within days'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><input type="text" name="ticket_reopen_within_days" value="<?php echo $this->configuration['ticket_reopen_within_days']; ?>" class="inputbox" size="<?php echo $sml_field_width; ?>" /></div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('Ticket can be reopen within given number of days'); ?></small></div>
                            </div>
                        </div>
                </div>
                <div id="emialsetting">
                        <legend><?php echo JText::_('Default System Emails'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Default Alert Email'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><?php echo JHTML::_('select.genericList', $this->lists['emails'], 'alert_email', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['alert_email']); ?>&nbsp;<a href="index.php?option=com_jssupportticket&c=email&layout=formemail"><?php echo JText::_('Add Email'); ?></a></div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('If Ticket Department Email Is Not Selected Then This Email Is Used To Send Emails'); ?></small></div>
                            </div>
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-4 js-col-md-4 js-config-title"><?php echo JText::_('Default admin email'); ?></div>
                                <div class="js-col-lg-4 js-col-md-4 js-config-value"><?php echo JHTML::_('select.genericList', $this->lists['emails'], 'admin_email', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['admin_email']); ?>&nbsp;<a href="index.php?option=com_jssupportticket&c=email&layout=formemail"><?php echo JText::_('Add Email'); ?></a></div>
                                <div class="js-col-lg-4 js-col-md-4"><small><?php echo JText::_('Admin Email Address To Receive Emails'); ?></small></div>
                            </div>
                        </div>
                </div>
                <div id="menusetting">
                        <legend><?php echo JText::_('User Control Panel Links'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin">
                                <div class="js-col-lg-3 js-col-md-3 js-config-title"><?php echo JText::_('Open Ticket'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3 js-config-value"><?php echo JHTML::_('select.genericList', $yesno, 'cplink_openticket_user', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['cplink_openticket_user']); ?></div>
                                <div class="js-col-lg-3 js-col-md-3 js-config-title"><?php echo JText::_('My Tickets'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3 js-config-value"><?php echo JHTML::_('select.genericList', $yesno, 'cplink_myticket_user', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['cplink_myticket_user']); ?></div>
                            </div>
                        </div>
                </div>
                <div id="auotrespondersetting">
                        <legend><?php echo JText::_('Ticket Operations Mail Setting'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin bgandfontcolor">
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Ticket Operations Mail Setting'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Admin'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Staff'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('User'); ?></div>
                            </div>
                            <div class="js-row">
                                <div class="js-col-lg-3 js-col-md-3 js-config-title"><?php echo JText::_('Ticket Change Priority'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'ticket_priority_admin', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['ticket_priority_admin']); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'ticket_priority_staff', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['ticket_priority_staff']); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'ticket_priority_user', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['ticket_priority_user']); ?></div>
                            </div>
                        </div>
                        <legend><?php echo JText::_('Erase Data'); ?></legend>
                        <div class="js-row js-null-margin">
                            <div class="js-row js-null-margin bgandfontcolor">
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Erase User data request'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Admin'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('Staff'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JText::_('User'); ?></div>
                            </div>
                            <div class="js-col-md-12">
                                <div class="js-col-lg-3 js-col-md-3 js-config-title"><?php echo JText::_('Erase request'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'erase_data_request_admin', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['erase_data_request_admin']); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'erase_data_request_user', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['erase_data_request_user']); ?></div>
                            </div>
                            <div class="js-col-md-12">
                                <div class="js-col-lg-3 js-col-md-3 js-config-title"><?php echo JText::_('Delete user data'); ?></div>
                                <div class="js-col-lg-3 js-col-md-3"></div>
                                <div class="js-col-lg-3 js-col-md-3"></div>
                                <div class="js-col-lg-3 js-col-md-3"><?php echo JHTML::_('select.genericList', $enableddisabled, 'delete_user_data', 'class="inputbox" ' . '', 'value', 'text', $this->configuration['delete_user_data']); ?></div>
                            </div>
                        </div>
                </div>
            </div>
            <input type="hidden" name="task" value="saveconf" />
            <input type="hidden" name="c" value="config" />
            <input type="hidden" name="layout" value="config" />
            <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
            <?php echo JHtml::_( 'form.token' ); ?>
        </form>
    </div>
    <div class="js-config-pro-version-text"><?php echo JText::_('* Pro version only'); ?></div>
</div>
<div id="js-tk-copyright">
    <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
    &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
</div>
