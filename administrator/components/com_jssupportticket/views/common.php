<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		Buruj Solutions
 + Contact:		www.burujsolutions.com , info@burujsolutions.com
 * Created on:	May 22, 2015
 ^
 + Project: 	JS Tickets
 ^ 
*/

defined('_JEXEC') or die('Restricted access');

$option = 'com_jssupportticket';
$mainframe = JFactory::getApplication();
$layoutName = $this->getLayout();

$config =  $this->getJSModel('config')->getConfigByFor('default');

$limit = $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
//$limitstart	= $mainframe->getUserStateFromRequest( $option.'.limitstart', 'limitstart', 0, 'int' );
$limitstart = JRequest::getVar('limitstart',0);

$this->assignRef('option', $option);
$this->assignRef('config', $config);

?>
