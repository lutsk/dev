<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
 + Contact:     www.burujsolutions.com , info@burujsolutions.com
 * Created on:  May 22, 2015
  ^
  + Project:    JS Tickets
  ^
 */
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
?>
<script language=Javascript>
    function confirmdelete() {
        if (confirm("<?php echo JText::_('Are you sure to delete'); ?>") == true) {
            return true;
        } else
            return false;
    }
</script>

<div id="js-tk-admin-wrapper">
    <div id="js-tk-leftmenu">
        <?php include_once('components/com_jssupportticket/views/menu.php'); ?>
    </div>
    <div id="js-tk-cparea">
        <div id="js-tk-heading">
            <h4><img id="js-admin-responsive-menu-link" src="components/com_jssupportticket/include/images/c_p/left-icons/menu.png" /><?php echo JText::_('User Data Erase Request'); ?></h4>
        </div>
        <div class="js-col-md-12">
            <form action="index.php" method="post" name="adminForm" id="adminForm">
                <div id="js-tk-filter">
                    <div class="tk-search-value"><input type="text" name="filter_email" id="filter_email" placeholder="<?php echo JText::_('Useremail') ?>" value="<?php if (isset($this->searchemail)) echo $this->searchemail; ?>" class="text_area"/></div>
                    <div class="tk-search-button">
                        <button class="jsst-search" onclick="this.form.submit();"><?php echo JText::_('Search'); ?></button>
                        <button class="jsst-reset" onclick="resetJsForm();this.form.submit();"><?php echo JText::_('Reset'); ?></button>
                    </div>
                </div>
                <input type="hidden" name="option" value="<?php echo $this->option; ?>" />
                <input type="hidden" name="c" value="gdpr" />
                <input type="hidden" name="layout" value="erasedatarequests" />
            </form>
        </div>
        <?php
            if (!(empty($this->result)) && is_array($this->result)) {  ?>
                <div class="js-col-md-12">
                    <table id="js-table">
                        <thead>
                        <tr>
                            <th><?php echo JText::_("Subject"); ?></th>
                            <th><?php echo JText::_("Message"); ?></th>
                            <th class="center"><?php echo JText::_("Email"); ?></th>
                            <th class="center"><?php echo JText::_("Request Status"); ?></th>
                            <th class="center"><?php echo JText::_("Created"); ?></th>
                            <th class="center"><?php echo JText::_("Action"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($this->result AS $request) { ?>
                                <tr>
                                    <td><?php echo JText::_($request->subject); ?></td>
                                    <td><?php echo JText::_($request->message); ?></td>
                                    <td class="center"><?php echo $request->email; ?></td>
                                    <td class="center">
                                      <?php if($request->status == 1){
                                          echo JText::_('Awaiting response');
                                      }elseif($request->status == 2){
                                        echo JText::_('Erased identifying data');
                                      }else{
                                        echo  JText::_('Deleted');
                                      }?>
                                    </td>
                                    <td class="center"><?php echo date($this->config['date_format'], strtotime($request->created)); ?></td>
                                    <td class="center">
                                        <a class="js-tk-button" onclick="return confirmdelete()" href="index.php?option=com_jssupportticket&c=gdpr&task=eraseidentifyinguserdata&id=<?php echo $request->uid; ?>&<?php echo JSession::getFormToken(); ?>=1">
                                          <?php echo JText::_('Erase identifying data');?>
                                        </a>&nbsp;
                                        <a class="js-tk-button" onclick="return confirmdelete()" href="index.php?option=com_jssupportticket&c=gdpr&task=deleteuserdata&id=<?php echo $request->uid; ?>&<?php echo JSession::getFormToken(); ?>=1">
                                          <?php echo JText::_('Delete data');?>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>
                <div class="js-row js-tk-pagination">
                    <?php echo $this->pagination->getListFooter(); ?>
                </div>
            <?php
            }else{
                messagesLayout::getRecordNotFound();
            } ?>
    </div>
</div>
<div id="js-tk-copyright">
    <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
    &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
</div>
<script type="text/javascript">
    function resetJsForm(){
        var form = jQuery('form#adminForm');
        form.find("input[type=text], input[type=email], input[type=password], textarea").val("");
    }
    var headertext = [],
    headers = document.querySelectorAll("#js-table th"),
    tablerows = document.querySelectorAll("#js-table th"),
    tablebody = document.querySelector("#js-table tbody");

    for(var i = 0; i < headers.length; i++) {
      var current = headers[i];
      headertext.push(current.textContent.replace(/\r?\n|\r/,""));
    }
    for (var i = 0, row; row = tablebody.rows[i]; i++) {
      for (var j = 0, col; col = row.cells[j]; j++) {
        col.setAttribute("data-th", headertext[j]);
      }
    }
</script>
