<?php
/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:		Buruj Solutions
 + Contact:		www.burujsolutions.com , info@burujsolutions.com
 * Created on:	May 22, 2015
  ^
  + Project: 	JS Tickets
  ^
 */
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_jssupportticket/include/css/custom.boots.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_jssupportticket/include/css/jssupportticketadmin.css');
if (JVERSION < 3) {
    JHtml::_('behavior.mootools');
    $document->addScript('components/com_jssupportticket/include/js/jquery.js');
} else {
    JHtml::_('behavior.framework');
    JHtml::_('jquery.framework');
}
JHTML::_('behavior.calendar');
JHTML::_('behavior.formvalidation');

$yesno = array(
    '0' => array('value' => 1, 'text' => JText::_('JYes')),
    '1' => array('value' => 0, 'text' => JText::_('JNo')),);
$fieldsize = array(
    '0' => array('value' => 50, 'text' => JText::_('50%')),
    '1' => array('value' => 100, 'text' => JText::_('100%')));

$fieldtype = array(
    '0' => array('value' => 'text', 'text' => JText::_('Text Field')),
    '1' => array('value' => 'checkbox', 'text' => JText::_('Check Box')),
    '2' => array('value' => 'date', 'text' => JText::_('Date')),
    '3' => array('value' => 'combo', 'text' => JText::_('Drop Down')),
    '4' => array('value' => 'email', 'text' => JText::_('Email Address')),
    '6' => array('value' => 'textarea', 'text' => JText::_('Text Area')),
    '7' => array('value' => 'radio', 'text' => JText::_('Radio Button')),
    '8' => array('value' => 'depandant_field', 'text' => JText::_('Depandent Field')),
    '9' => array('value' => 'file', 'text' => JText::_('Upload File')),
    '10' => array('value' => 'multiple', 'text' => JText::_('Multi Select')));
if (isset($this->userfield)) {
    $lstype = JHTML::_('select.genericList', $fieldtype, 'userfieldtype', 'class="inputbox" ' . 'onchange="toggleType(this.options[this.selectedIndex].value);"', 'value', 'text', $this->userfield->userfieldtype);
    $lsrequired = JHTML::_('select.genericList', $yesno, 'required', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->required);
    $lspublished = JHTML::_('select.genericList', $yesno, 'published', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->published);
    $isvisitorpublished = JHTML::_('select.genericList', $yesno, 'isvisitorpublished', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->isvisitorpublished);
    $search_user = JHTML::_('select.genericList', $yesno, 'search_user', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->search_user);
    $search_visitor = JHTML::_('select.genericList', $yesno, 'search_visitor', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->search_visitor);
    $showonlisting = JHTML::_('select.genericList', $yesno, 'showonlisting', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->showonlisting);
    $fieldsize = JHTML::_('select.genericList', $fieldsize, 'size', 'class="inputbox" ' . '', 'value', 'text', $this->userfield->size);
} else {
    $lstype = JHTML::_('select.genericList', $fieldtype, 'userfieldtype', 'class="inputbox" ' . 'onchange="toggleType(this.options[this.selectedIndex].value);"', 'value', 'text', 0);
    $lsrequired = JHTML::_('select.genericList', $yesno, 'required', 'class="inputbox" ' . '', 'value', 'text', 0);
    $lspublished = JHTML::_('select.genericList', $yesno, 'published', 'class="inputbox" ' . '', 'value', 'text', 1);
    $isvisitorpublished = JHTML::_('select.genericList', $yesno, 'isvisitorpublished', 'class="inputbox" ' . '', 'value', 'text', 1);
    $search_user = JHTML::_('select.genericList', $yesno, 'search_user', 'class="inputbox" ' . '', 'value', 'text', 1);
    $search_visitor = JHTML::_('select.genericList', $yesno, 'search_visitor', 'class="inputbox" ' . '', 'value', 'text', 1);
    $showonlisting = JHTML::_('select.genericList', $yesno, 'showonlisting', 'class="inputbox" ' . '', 'value', 'text', 0);
    $fieldsize = JHTML::_('select.genericList', $fieldsize, 'size', 'class="inputbox" ' . '', 'value', 'text', 0);
}
?>

<div id="js-tk-admin-wrapper">
    <div id="js-tk-leftmenu">
        <?php include_once('components/com_jssupportticket/views/menu.php'); ?>
    </div>
    <div id="js-tk-cparea">
        <div id="js-tk-heading"><h4><img id="js-admin-responsive-menu-link" src="components/com_jssupportticket/include/images/c_p/left-icons/menu.png" /><?php echo JText::_('Add User Field'); ?></h4></div>
        <form action="index.php" method="POST" name="adminForm" id="adminForm" >
        <!-- old -->
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Field Type'); ?><font class="required-notifier">*</font></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $lstype; ?></div>
        </div>
        <?php /*
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Field Name'); ?><font class="required-notifier">*</font></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><input onchange="prep4SQL(this);" type="text" id="field" name="field" mosReq=1 mosLabel="Name" class="inputbox" value="<?php if (isset($this->userfield)) echo $this->userfield->field; ?>"/></div>
        </div>
        */ ?>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Field Title'); ?><font class="required-notifier">*</font></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value">
            <input type="text" id="fieldtitle" name="fieldtitle" class="inputbox" value="<?php if (isset($this->userfield)) echo $this->userfield->fieldtitle; ?>"/></div>
        </div>
        <div class="js-form-wrapper" id="for-combo-wrapper" style="display:none;">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Select','js-jobs') .'&nbsp;'. JText::_('Parent Field'); ?><font class="required-notifier">*</font></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value" id="for-combo"></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Show on listing'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $showonlisting; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('User Published'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $lspublished; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Visitor Published'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $isvisitorpublished; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('User Search'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $search_user; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Visitor Search'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $search_visitor; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Required'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $lsrequired; ?></div>
        </div>
        <div class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Field Size'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><?php echo $fieldsize; ?></div>
        </div>

        <div id="for-combo-options" >
            <?php
            $arraynames = '';
            $comma = '';
            if (isset($this->userfieldparams) && $this->userfield->userfieldtype == 'depandant_field') {
                foreach ($this->userfieldparams as $key => $val) {
                    $textvar = $key;
                    $textvar .='[]';
                    $arraynames .= $comma . "$key";
                    $comma = ',';
                    ?>
                    <div class="js-form-wrapper">
                        <div class="js-form-title js-col-xs-12 js-col-md-2 js-title">
                            <?php echo $key; ?>
                        </div>
                        <div class="js-col-lg-9 js-col-md-9 no-padding combo-options-fields" id="<?php echo $key; ?>">
                            <?php
                            if (!empty($val)) {
                                foreach ($val as $each) {
                                    ?>
                                    <span class="input-field-wrapper">
                                        <input name="<?php echo $textvar; ?>" id="<?php echo $textvar; ?>" value="<?php echo $each; ?>" class="inputbox one user-field" type="text">
                                        <img class="input-field-remove-img" src="components/com_jssupportticket/include/images/deletes.png">
                                    </span><?php
                                }
                            }
                            ?>
                            <input id="depandant-field-button" onclick="getNextField( '<?php echo $key; ?>',this );" value="Add More" type="button">
                        </div>
                    </div><?php
                }
            }
            ?>
        </div>

        <div id="divText" class="js-form-wrapper">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Max Length'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value">
                <input type="text" id="maxlength" name="maxlength" class="inputbox" value="<?php if (isset($this->userfield)) echo $this->userfield->maxlength; ?>" />
            </div>
        </div>
        <div class="js-form-wrapper divColsRows">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Columns'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><input type="text" id="cols" name="cols" class="inputbox" value="<?php if (isset($this->userfield)) echo $this->userfield->cols; ?>" /></div>
        </div>
        <div class="js-form-wrapper divColsRows">
            <div class="js-form-title js-col-xs-12 js-col-md-2 js-title"><?php echo JText::_('Rows'); ?></div>
            <div class="js-form-field js-col-xs-12 js-col-md-10 js-value"><input type="text" id="rows" name="rows" class="inputbox" value="<?php if (isset($this->userfield)) echo $this->userfield->rows; ?>" /></div>
        </div>
        <div id="divValues" class="js-form-wrapper divColsRowsno-margin">
            <span class="js-admin-title"><?php echo JText::_('Use The Table Below To Add New Values'); ?></span>
            <div class="page-actions no-margin">
                <div id="user-field-values" class="no-padding">
                    <?php
                    if (isset($this->userfield) && $this->userfield->userfieldtype != 'depandant_field') {
                        if (isset($this->userfieldparams) && !empty($this->userfieldparams)) {
                            foreach ($this->userfieldparams as $key => $val) {
                                ?>
                                <span class="input-field-wrapper">
                                <input type="text" class="inputbox one user-field" id="values" name="values[]" class="inputbox" value="<?php echo isset($val) ? $val : ''; ?>" />
                                    <img class="input-field-remove-img" src="components/com_jssupportticket/include/images/deletes.png" />
                                </span>
                            <?php
                            }
                        } else {
                            ?>
                            <span class="input-field-wrapper">
                            <input type="text" class="inputbox one user-field" id="values" name="values[]" class="inputbox" value="<?php echo isset($val) ? $val : ''; ?>" />
                                <img class="input-field-remove-img" src="components/com_jssupportticket/include/images/deletes.png" />
                            </span>
                        <?php
                        }
                    }
                    ?>
                    <a class="js-button-link button user-field-val-button" id="user-field-val-button" onclick="insertNewRow();"><?php echo JText::_('Add Value') ?></a>
                </div>
            </div>
        </div>

        <input type="hidden" id="id" name="id" value="<?php if (isset($this->userfield->id)) echo $this->userfield->id; ?>" />
        <input type="hidden" id="fieldfor" name="fieldfor" value="1" />
        <input type="hidden" id="field" name="field" value="<?php if (isset($this->userfield->field)) echo $this->userfield->field; ?>" />
        <input type="hidden" id="ordering" name="ordering" value="<?php echo isset($this->userfield->ordering) ? $this->userfield->ordering : ''; ?>" />
        <input type="hidden" id="c" name="c" value="userfields" />
        <input type="hidden" id="layout" name="layout" value="formuserfield" />
        <input type="hidden" id="task" name="task" value="saveuserfield" />
        <input type="hidden" id="isuserfield" name="isuserfield" value="1" />
        <input type="hidden" id="option" name="option" value="<?php echo $this->option; ?>" />
        <input type="hidden" id="arraynames2" name="arraynames2" value="<?php echo $arraynames; ?>" />
        <input type="hidden" id="fieldname" name="fieldname" value="<?php echo isset($this->userfield->field) ? $this->userfield->field : ''; ?>" />
        <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
<script type="text/javascript">
        jQuery(document).ready(function () {
            toggleType(jQuery('#userfieldtype').val());
        });
        function disableAll() {
            jQuery("#divValues").slideUp();
            jQuery(".divColsRows").slideUp();
            jQuery("#divText").slideUp();
        }
        function toggleType(type) {
            disableAll();
            //prep4SQL(document.forms['adminForm'].elements['field']);
            selType(type);
        }
        function prep4SQL(field) {
            if (field.value != '') {
                field.value = field.value.replace('js_', '');
                field.value = 'js_' + field.value.replace(/[^a-zA-Z]+/g, '');
            }
        }
        function selType(sType) {
            var elem;
            /*
             text
             checkbox
             date
             combo
             email
             textarea
             radio
             editor
             depandant_field
             multiple*/

            switch (sType) {
                case 'editor':
                    jQuery("#divText").slideUp();
                    jQuery("#divValues").slideUp();
                    jQuery(".divColsRows").slideUp();
                    jQuery("div#for-combo-wrapper").hide();
                    jQuery("div#for-combo-options").hide();
                    jQuery("div#for-combo-options-head").hide();
                    break;
                case 'textarea':
                    jQuery("#divText").slideUp();
                    jQuery(".divColsRows").slideDown();
                    jQuery("#divValues").slideUp();
                    jQuery("div#for-combo-wrapper").hide();
                    jQuery("div#for-combo-options").hide();
                    jQuery("div#for-combo-options-head").hide();
                    break;
                case 'email':
                case 'password':
                case 'text':
                    jQuery("#divText").slideDown();
                    jQuery("div#for-combo-wrapper").hide();
                    jQuery("div#for-combo-options").hide();
                    jQuery("div#for-combo-options-head").hide();
                    break;
                case 'combo':
                case 'multiple':
                    jQuery("#divValues").slideDown();
                    jQuery("div#for-combo-wrapper").hide();
                    jQuery("div#for-combo-options").hide();
                    jQuery("div#for-combo-options-head").hide();
                    break;
                case 'depandant_field':
                    comboOfFields();
                    break;
                case 'radio':
                case 'checkbox':
                    //jQuery(".divColsRows").slideDown();
                    jQuery("#divValues").slideDown();
                    jQuery("div#for-combo-wrapper").hide();
                    jQuery("div#for-combo-options").hide();
                    jQuery("div#for-combo-options-head").hide();
                    /*
                     if (elem=getObject('jsNames[0]')) {
                     elem.setAttribute('mosReq',1);
                     }
                     */
                    break;
                case 'delimiter':
                default:
            }
        }

        function comboOfFields() {
            ajaxurl = 'index.php?option=com_jssupportticket&c=userfields&task=getfieldsforcombobyfieldfor&<?php echo JSession::getFormToken(); ?>=1';
            var ff = jQuery("input#fieldfor").val();
            var pf = jQuery("input#field").val();
            jQuery.post(ajaxurl, { fieldfor: ff, parentfield:pf}, function (data) {
                if (data) {
                    var d = jQuery.parseJSON(data);
                    jQuery("div#for-combo").html(d);
                    jQuery("div#for-combo-wrapper").show();
                }
            });
        }

        function getDataOfSelectedField() {
            ajaxurl = 'index.php?option=com_jssupportticket&c=userfields&task=getsectiontofillvalues&<?php echo JSession::getFormToken(); ?>=1';
            var field = jQuery("select#parentfield").val();
            jQuery.post(ajaxurl, { pfield: field}, function (data) {
                if (data) {
                    var d = jQuery.parseJSON(data);
                    jQuery("div#for-combo-options-head").show();
                    jQuery("div#for-combo-options").html(d);
                    jQuery("div#for-combo-options").show();
                }
            });
        }

        function getNextField(divid,object) {
            var textvar = divid + '[]';
            var fieldhtml = "<span class='input-field-wrapper' ><input type='text' name='" + textvar + "' class='inputbox one user-field'  /><img class='input-field-remove-img' src='components/com_jssupportticket/include/images/deletes.png' /></span>";
            jQuery(object).before(fieldhtml);
        }

        function getObject(obj) {
            var strObj;
            if (document.all) {
                strObj = document.all.item(obj);
            } else if (document.getElementById) {
                strObj = document.getElementById(obj);
            }
            return strObj;
        }

        function insertNewRow() {
            var fieldhtml = '<span class="input-field-wrapper" ><input name="values[]" id="values" value="" class="inputbox one user-field" type="text" /><img class="input-field-remove-img" src="components/com_jssupportticket/include/images/deletes.png" /></span>';
            jQuery("#user-field-val-button").before(fieldhtml);
        }
        jQuery(document).ready(function () {
            jQuery("body").delegate("img.input-field-remove-img", "click", function () {
                jQuery(this).parent().remove();
            });
        });
    </script>

    </div>
</div>
<div id="js-tk-copyright">
    <img width="85" src="https://www.joomsky.com/logo/jssupportticket_logo_small.png">&nbsp;Powered by <a target="_blank" href="https://www.joomsky.com">Joom Sky</a><br/>
    &copy;Copyright 2008 - <?php echo date('Y'); ?>, <a target="_blank" href="http://www.burujsolutions.com">Buruj Solutions</a>
</div>
