<?php

/**
 * @Copyright Copyright (C) 2015 ... Ahmad Bilal
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * Company:     Buruj Solutions
  + Contact:    www.burujsolutions.com , info@burujsolutions.com
 * Created on:	May 22, 2015
  ^
  + Project:    JS Tickets
  ^
 */
defined('_JEXEC') or die('Not Allowed');

jimport('joomla.application.component.model');
jimport('joomla.html.html');

class JSSupportticketModelTicket extends JSSupportTicketModel {

    var $activity_log;

    function __construct() {
        parent::__construct();
    }

    function getRandomFolderName() {
        $foldername = "";
        $length = 7;
        $possible = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        // we refer to the length of $possible a few times, so let's grab it now
        $maxlength = strlen($possible);
        if ($length > $maxlength) { // check for length overflow and truncate if necessary
            $length = $maxlength;
        }
        // set up a counter for how many characters are in the ticketid so far
        $i = 0;
        // add random characters to $password until $length is reached
        while ($i < $length) {
            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if ($i == 0) {
                if (ctype_alpha($char)) {
                    $foldername .= $char;
                    $i++;
                }
            } else {
                $foldername .= $char;
                $i++;
            }
        }
        return $foldername;
    }

    function storeTicket($data){
        $user = JSSupportTicketCurrentUser::getInstance();
        $eventtype = JText::_('New Ticket');
        //for new ticket case
        if (($data['id']) == ''){
            $data['ticketid'] = $this->getTicketId();
            $data['attachmentdir'] = $this->getRandomFolderName();
            $data['created'] = date('Y-m-d H:i:s');
        }
        $row = $this->getTable('tickets');
        $data['message'] = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWHTML);
        if(!$user->getIsAdmin())
            $data['uid'] = $user->getId();

        //custom field code start
        $customflagforadd = false;
        $customflagfordelete = false;
        $custom_field_namesforadd = array();
        $custom_field_namesfordelete = array();
        $userfield = $this->getJSModel('userfields')->getUserfieldsfor(1);
        $params = array();
        foreach ($userfield AS $ufobj) {
			$vardata = '';
            if($ufobj->userfieldtype == 'file'){
                if(isset($data[$ufobj->field.'_1']) && $data[$ufobj->field.'_1']== 0){
                    $vardata = $data[$ufobj->field.'_2'];
                }else{
                    $vardata = $_FILES[$ufobj->field]['name'];
                }
				$config = $this->getJSModel('config')->getConfigByFor('default');
				$model_attachment = $this->getJSModel('attachments');
				$file_size = $config['filesize'];
				if($_FILES[$ufobj->field]['size'] > ($file_size * 1024)){
					$vardata = '';
				}else{
					if ($_FILES[$ufobj->field]['name'] != "") {
						$is_allow = $model_attachment->checkExtension($_FILES[$ufobj->field]['name']);
						if($is_allow == 'N'){
							$vardata = '';
						}else{
							$vardata = $_FILES[$ufobj->field]['name'];
							$customflagforadd=true;
							$custom_field_namesforadd[]=$ufobj->field;
						}
					} 
				}
            }elseif($ufobj->userfieldtype == 'date'){
                if(isset($data[$ufobj->field]) && !empty($data[$ufobj->field])){
                    $tempdate = $data[$ufobj->field];
                    $dateformat = JSSupportTicketModel::getJSModel('config')->getConfigurationByName("date_format");
                    if ($dateformat == 'm-d-Y') {
                      $arr = explode('-', $tempdate);
                      $tempdate = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
                    } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
                      $arr = explode('-', $tempdate);
                      $tempdate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
                    }
                    $vardata = JHTML::_('date',strtotime($tempdate),"Y-m-d" );
                }else{
                    $vardata = '';
                }
            }
            else{
                $vardata = isset($data[$ufobj->field]) ? $data[$ufobj->field] : '';
            }
            if(isset($data[$ufobj->field.'_1']) && $data[$ufobj->field.'_1'] == 1){
                $customflagfordelete = true;
                $custom_field_namesfordelete[]= $data[$ufobj->field.'_2'];
            }
            if($vardata != ''){
                //had to comment this so that multpli field should work properly
                // if($ufobj->userfieldtype == 'multiple'){
                //     $vardata = explode(',', $vardata[0]); // fixed index
                // }
                if(is_array($vardata)){
                    $vardata = implode(', ', $vardata);
                }
                $params[$ufobj->field] = htmlspecialchars($vardata);
            }
        }
        if($data['id'] != ''){
            if(is_numeric($data['id'])){
                $db = $this->getDbo();
                $query = "SELECT params FROM `#__js_ticket_tickets` WHERE id = " . $data['id'];
                $db->setQuery($query);
                $oParams = $db->loadResult();

                if(!empty($oParams)){
                    $oParams = json_decode($oParams,true);
                    $unpublihsedFields = $this->getJSModel('userfields')->getUserUnpublishFieldsfor(1);
                    foreach($unpublihsedFields AS $field){
                        if(isset($oParams[$field->field])){
                            $params[$field->field] = $oParams[$field->field];
                        }
                    }
                }
            }
        }

        if (!empty($params)) {
            $params = json_encode($params);
        }
        $data['params'] = $params;


        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $return_value = false;
        }
        if(!$data['id'])
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return MESSAGE_EMPTY;
        }
        if (!$row->store()) {
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->setError($this->_db->getErrorMsg());
            $return_value = false;
        }

        if($data['id'] == ''){
            $db = JFactory::getDbo();
            $query = "UPDATE `#__js_ticket_tickets` SET attachmentdir = CONCAT(attachmentdir,id) WHERE id = ".$row->id;
            $db->setQuery($query);
            $db->query($query);
            
            if (!empty($data['transaction_number'])) {
	            $query = "UPDATE `#__js_ticket_tickets` SET transaction_number = ".$data['transaction_number']." WHERE id = ".$row->id;
	            $db->setQuery($query);
	            $db->query($query);
			}
        }

        if (isset($return_value) && $return_value == false) {
            return SAVE_ERROR;
        }

        $ticketid = $row->id;
        $ATTACHMENTRESULT = $this->getJSModel('attachments')->storeTicketAttachment($ticketid);
        if($ATTACHMENTRESULT !== true){
            return $ATTACHMENTRESULT;
        }

        // new
        //removing custom field attachments
        
        if($customflagfordelete == true){
            foreach ($custom_field_namesfordelete as $key) {
                $res = $this->removeFileCustom($ticketid,$key);
            }
        }
        //storing custom field attachments
        if($customflagforadd == true){
            foreach ($custom_field_namesforadd as $key) {
                if ($_FILES[$key]['size'] > 0) { // logo
                    $res = $this->uploadFileCustom($ticketid,$key);
                }
            }
        }

        if ($data['id'] == '')  // only for new ticket
            $this->getJSModel('email')->sendMail(1,1,$row->id); // Mailfor,Create Ticket,Ticketid

        JSSupportTicketMessage::$recordid = $ticketid;
        return SAVED;
    }

    function getAdminMyTickets($searchdepartment, $searchpriority, $searchsubject, $searchfrom, $searchfromemail, $searchticketid, $listtype,$sortby, $datestart, $dateend, $limitstart, $limit) {
        $db = $this->getDBO();
        $user = JSSupportTicketCurrentUser::getInstance();
        // $listtype == 1  - open
        // $listtype == 2  - answerd
        // $listtype == 4  - close
        // $listtype == 5  - all tickets

        $query = "SELECT ticket.*, dep.departmentname AS departmentname, dep.id AS departmentid , priority.priority AS priority, priority.prioritycolour AS prioritycolour
                  FROM `#__js_ticket_tickets` AS ticket
                  JOIN `#__js_ticket_priorities` AS priority ON ticket.priorityid = priority.id
                  LEFT JOIN `#__js_ticket_departments` AS dep ON ticket.departmentid = dep.id
                  WHERE 1=1";
        
        $wherequery = '';
        
        $data = getCustomFieldClass()->userFieldsForSearch(1);
        $valarray = array();
        if (!empty($data)) {
            foreach ($data as $uf) {
                $valarray[$uf->field] = JRequest::getVar($uf->field,NULL, 'post');
                if (isset($valarray[$uf->field]) && $valarray[$uf->field] != null) {
                    switch ($uf->userfieldtype) {
                        case 'text':
                            
                            $wherequery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($valarray[$uf->field]) . '.*"\' ';
                            break;
                        case 'combo':
                            
                            $wherequery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'depandant_field':
                            
                            $wherequery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'radio':
                            
                            $wherequery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'checkbox':
                            $finalvalue = '';
                            foreach($valarray[$uf->field] AS $value){
                                $finalvalue .= $value.'.*';
                            }
                            
                            $wherequery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($finalvalue) . '.*"\' ';
                            break;
                        case 'date':
                            $tempdate = htmlspecialchars($valarray[$uf->field]);
                            $dateformat = JSSupportTicketModel::getJSModel('config')->getConfigurationByName("date_format");
                            if ($dateformat == 'm-d-Y') {
                              $arr = explode('-', $tempdate);
                              $tempdate = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
                            } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
                              $arr = explode('-', $tempdate);
                              $tempdate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
                            }
                            $tempdate = JHTML::_('date',strtotime($tempdate),"Y-m-d" );
                            $valarray[$uf->field] = $tempdate;
                            $wherequery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'textarea':
                            
                            $wherequery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($valarray[$uf->field]) . '.*"\' ';
                            break;
                        case 'multiple':
                            $finalvalue = '';
                            foreach($valarray[$uf->field] AS $value){
                                if($value != null){
                                    $finalvalue .= $value.'.*';
                                }    
                            }
                            if($finalvalue !=''){
                                
                                $wherequery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*'.htmlspecialchars($finalvalue).'"\'';
                            }
                            break;
                    }
                    $params_filter = $valarray;
                }
            }
        }




		$return_datestart = '';
		$return_dateend = '';
        if ($searchsubject <> '')
            $wherequery .= " AND ticket.subject LIKE " . $db->quote('%' . $searchsubject . '%');
        if ($searchfrom <> '')
            $wherequery .= " AND ticket.name LIKE " . $db->quote('%' . $searchfrom . '%');
        if ($searchfromemail <> '')
            $wherequery .= " AND ticket.email LIKE " . $db->quote('%' . $searchfromemail . '%');
        if ($searchticketid <> '')
            $wherequery .= " AND ticket.ticketid LIKE " . $db->quote('%' . $searchticketid . '%');
        if ($searchdepartment <> '')
            $wherequery .= " AND ticket.departmentid = " . $searchdepartment;
        if ($searchpriority <> '')
            $wherequery .= " AND ticket.priorityid = " . $searchpriority;
        if(isset($datestart) && !empty($datestart)){
            $return_datestart = $datestart;
            $dateformat = JSSupportTicketModel::getJSModel('config')->getConfigurationByName("date_format");
            if ($dateformat == 'm-d-Y') {
              $arr = explode('-', $datestart);
              $datestart = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
            } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
              $arr = explode('-', $datestart);
              $datestart = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
            }
            $datestart = JHTML::_('date',strtotime($datestart),"Y-m-d" );
            $wherequery .=" AND DATE(ticket.created) >= ".$db->quote($datestart);
        }
        if(isset($dateend) && !empty($dateend)){
            $return_dateend = $dateend;
            $dateformat = JSSupportTicketModel::getJSModel('config')->getConfigurationByName("date_format");
            if ($dateformat == 'm-d-Y') {
              $arr = explode('-', $dateend);
              $dateend = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
            } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
              $arr = explode('-', $dateend);
              $dateend = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
            }
            $dateend = JHTML::_('date',strtotime($dateend),"Y-m-d" );
            $wherequery .=" AND DATE(ticket.created) <= ".$db->quote($dateend);
        }
        $query .= $wherequery;

        switch ($listtype) {
            case 1:
                $query .= " AND ticket.status != 4 ";
                break;
            case 2:
                $query .= " AND ticket.status != 4 AND ticket.isanswered = 1 ";
                break;
            case 4:
                $query .= " AND ticket.status = 4  ";
                break;
            case 5: // Admin all tickets
                $query .= " ";
                break;
        }
        $query .= ' ORDER BY '.$sortby;

        $db->setQuery($query, $limitstart, $limit);
        $tickets = $db->loadObjectList(); // Tickets
        $ticketinfo = array();
        $config = $this->getJSModel('config')->getConfigs();
        $total = 0;
        if($config['show_count_tickets'] == 1){
            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.status != 4";
            $query .= $wherequery;
            $db->setQuery($query);
            $ticketinfo['open'] = $db->loadResult(); // Open Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.status = 4";
            $query .= $wherequery;
            $db->setQuery($query);
            $ticketinfo['close'] = $db->loadResult(); // Closed Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE status != 4 AND isanswered = 1";
            $query .= $wherequery;
            $db->setQuery($query);
            $ticketinfo['isanswered'] = $db->loadResult(); // IsAnswered Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE 1 = 1";
            $query .= $wherequery;
            $db->setQuery($query);
            $ticketinfo['mytickets'] = $db->loadResult(); // My Tickets

            switch ($listtype) {
                case 1:
                    $total = $ticketinfo['open'];    
                    break;
                case 2:
                    $total = $ticketinfo['isanswered'];
                    break;
                case 4:
                    $total = $ticketinfo['close'];
                    break;
                case 5:
                    $total = $ticketinfo['mytickets'];  
                    break;
            }
        }else{
            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE 1=1 ";
            $query .= $wherequery;
            $db->setQuery($query);
            $total = $db->loadResult(); 
        }
        $departments = $this->getJSModel('department')->getDepartmentsForCombobox();
        $priorities = $this->getJSModel('priority')->getPrioritiesForCombobx(JText::_('Select priority'));
        $lists['params'] =  isset($params_filter) ? $params_filter : '';
        $lists['datestart'] = $datestart;
        $lists['dateend'] = $dateend;        
        $lists['searchsubject'] = $searchsubject;
        $lists['searchfrom'] = $searchfrom;
        $lists['searchfromemail'] = $searchfromemail;
        $lists['searchticket'] = $searchticketid;
        $lists['departments'] = JHTML::_('select.genericList', $departments, 'filter_department', '', 'value', 'text',$searchdepartment);
        $lists['priorities'] = JHTML::_('select.genericList', $priorities, 'filter_priority', '', 'value', 'text',$searchpriority);
        $lists['return_datestart'] = $return_datestart;
        $lists['return_dateend'] = $return_dateend;
        $result[0] = $tickets;
        $result[1] = $total;
        $result[2] = $lists;
        $result[3] = $ticketinfo;
        return $result;
    }

    function getusersearchajax() {
      $userlimit = JRequest::getVar('userlimit',0);
      $maxrecorded = 4;
      $db = JFactory::getDbo();
      $name = JRequest::getVar('name','');
      $username = JRequest::getVar('username','');
      $emailaddress = JRequest::getVar('emailaddress','');
      $wherequery = '';
      if($name!=''){
        $wherequery = " AND user.name LIKE ".$db->quote('%'.$name.'%');
      }
      if($username!=''){
        $wherequery = " AND user.username LIKE ".$db->quote('%'.$username.'%');
      }
      if($emailaddress!=''){
        $wherequery = " AND user.email LIKE ".$db->quote('%'.$emailaddress.'%');
      }
      $query = "SELECT DISTINCT COUNT(user.id)
              FROM `#__users` AS user
              WHERE 1 = 1 ";
      $query .= $wherequery;
      $db->setQuery($query);
      $total = $db->loadResult();
      $limit = $userlimit * $maxrecorded;
      if($limit >= $total){
          $limit = 0;
      }
      $query = "SELECT DISTINCT user.id AS userid, user.name AS displayname, user.email AS useremail, user.username AS username
              FROM `#__users` AS user
              WHERE 1 = 1 ";
      $query .= $wherequery;
      $query .= " LIMIT $limit, $maxrecorded ";
      $db->setQuery($query);
      $users = $db->loadObjectList();
      $html = $this->makeUserList($users,$total,$maxrecorded,$userlimit);
      return $html;
    }

    function makeUserList($users,$total,$maxrecorded,$userlimit){
        $html = '';
        if(!empty($users)){
            if(is_array($users)){
                $html ='
                    <div class="js-col-md-2 js-title">'.JText::_('User ID').'</div>
                    <div class="js-col-md-3 js-title">'.JText::_('Username').'</div>
                    <div class="js-col-md-4 js-title">'.JText::_('Email Address').'</div>
                    <div class="js-col-md-3 js-title">'.JText::_('Name').'</div>';
                foreach($users AS $user){
                    $html .='
                        <div class="user-records-wrapper js-value" style="display:inline-block;width:100%;">
                            <div class="js-col-xs-12 js-col-md-2">
                                <span class="js-user-title-xs">'.JText::_('User ID').' : </span>'.$user->userid.'
                            </div>
                            <div class="js-col-xs-12 js-col-md-3">
                                <span class="js-user-title-xs">'.JText::_('Username').' : </span>';
                                    $html .='<a href="#" class="js-userpopup-link" data-id="'.$user->userid.'" data-email="'.$user->useremail.'" data-name="'.$user->username.'">'.$user->username.'</a> </div>';
                            $html .=
                            '<div class="js-col-xs-12 js-col-md-4">
                                <span class="js-user-title-xs">'.JText::_('Email Address').' : </span>'.$user->useremail.'
                            </div>
                            <div class="js-col-xs-12 js-col-md-3">
                                <span class="js-user-title-xs">'.JText::_('Display name').' : </span>'.$user->displayname.'
                            </div>
                        </div>';
                }
                $num_of_pages = ceil($total / $maxrecorded);
                $num_of_pages = ($num_of_pages > 0) ? ceil($num_of_pages) : floor($num_of_pages);
                if($num_of_pages > 0){
                    $page_html = '';
                    $prev = $userlimit;
                    if($prev > 0){
                        $page_html .= '<a class="jsst_userlink" href="#" onclick="updateuserlist('.($prev - 1).');">'.JText::_('Previous').'</a>';
                    }
                    for($i = 0; $i < $num_of_pages; $i++){
                        if($i == $userlimit)
                            $page_html .= '<span class="jsst_userlink selected" >'.($i + 1).'</span>';
                        else
                            $page_html .= '<a class="jsst_userlink" href="#" onclick="updateuserlist('.$i.');">'.($i + 1).'</a>';

                    }
                    $next = $userlimit + 1;
                    if($next < $num_of_pages){
                        $page_html .= '<a class="jsst_userlink" href="#" onclick="updateuserlist('.$next.');">'.JText::_('Next').'</a>';
                    }
                    if($page_html != ''){
                        $html .= '<div class="jsst_userpages">'.$page_html.'</div>';
                    }
                }
            }
        }else{
            $html = messagesLayout::getRecordNotFound();
        }
        return $html;
    }
    private function ticketMultiSearch($searchkeys){
        $db = JFactory::getDbo();
        $inquery="";
        $flag = true;
        if(!empty($searchkeys))
            if(isset($searchkeys['filter_ticketsearchkeys']) && !empty($searchkeys['filter_ticketsearchkeys'])){
                $keys = $searchkeys['filter_ticketsearchkeys'];
                if (strlen($keys) == 11 || is_numeric($keys))
                    $inquery = " AND ticket.ticketid = ".$db->quote($keys);
                else if (strpos($keys, '@') && strpos($keys, '.'))
                    $inquery = " AND ticket.email LIKE ".$db->quote('%'.$keys.'%');
                else
                    $inquery = " AND ticket.subject LIKE ".$db->quote('%'.$keys.'%');
                $result['searchkeys'] = $keys;
                $flag = false;
            }else{
                if(isset($searchkeys['filter_ticketid']) && !empty($searchkeys['filter_ticketid'])){
                    $inquery =" AND ticket.ticketid = ".$db->quote($searchkeys['filter_ticketid']);
                    $result['ticketid'] = $searchkeys['filter_ticketid'];
                }
                if(isset($searchkeys['filter_from']) && !empty($searchkeys['filter_from'])){
                    $inquery .=" AND ticket.name LIKE ".$db->quote('%'.$searchkeys['filter_from'].'%');
                    $result['from'] = $searchkeys['filter_from'];
                }
                if(isset($searchkeys['filter_email']) && !empty($searchkeys['filter_email'])){
                    $inquery .=" AND ticket.email LIKE ".$db->quote('%'.$searchkeys['filter_email'].'%');
                    $result['email'] = $searchkeys['filter_email'];
                }
                if(isset($searchkeys['filter_department']) && !empty($searchkeys['filter_department'])){
                    $inquery .=" AND ticket.departmentid =".$searchkeys['filter_department'];
                    $result['department'] = $searchkeys['filter_department'];
                }
                if(isset($searchkeys['filter_priority']) && !empty($searchkeys['filter_priority'])){
                    $inquery .=" AND ticket.priorityid = ".$searchkeys['filter_priority'];
                    $result['priority'] = $searchkeys['filter_priority'];
                }
                if(isset($searchkeys['filter_subject']) && !empty($searchkeys['filter_subject'])){
                    $inquery .=" AND ticket.subject LIKE ".$db->quote('%'.$searchkeys['filter_subject'].'%');
                    $result['subject'] = $searchkeys['filter_subject'];
                }
                $config = $this->getJSModel('config')->getConfigs();
                if(isset($searchkeys['filter_datestart']) && !empty($searchkeys['filter_datestart'])){
                    $dateformat = $config['date_format'];
                    if ($dateformat == 'm-d-Y') {
                      $arr = explode('-', $searchkeys['filter_datestart']);
                      $searchkeys['filter_datestart'] = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
                    } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
                      $arr = explode('-', $searchkeys['filter_datestart']);
                      $searchkeys['filter_datestart'] = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
                    }
                    $searchkeys['filter_datestart'] = JHTML::_('date',strtotime($searchkeys['filter_datestart']),"Y-m-d H:i:s" );
                    $inquery .=" AND DATE(ticket.created) >= ".$db->quote($searchkeys['filter_datestart']);
                    $result['datestart'] = $searchkeys['filter_datestart'];
                }
                if(isset($searchkeys['filter_dateend']) && !empty($searchkeys['filter_dateend'])){
                    $dateformat = $config['date_format'];
                    if ($dateformat == 'm-d-Y') {
                      $arr = explode('-', $searchkeys['filter_dateend']);
                      $searchkeys['filter_dateend'] = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
                    } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
                      $arr = explode('-', $searchkeys['filter_dateend']);
                      $searchkeys['filter_dateend'] = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
                    }
                    $searchkeys['filter_dateend'] = JHTML::_('date',strtotime($searchkeys['filter_dateend']),"Y-m-d H:i:s" );
                    $inquery .=" AND DATE(ticket.created) <= ".$db->quote($searchkeys['filter_dateend']);
                    $result['dateend'] = $searchkeys['filter_dateend'];
                }
                if($inquery=="")
                    $result['iscombinesearch'] = false;
                else
                    $result['iscombinesearch'] = true;

            }

        //Custom field search
        //start

        $mainframe = JFactory::getApplication();
        $option = 'com_jssupportticket';
        $data = getCustomFieldClass()->userFieldsForSearch(1);
        $valarray = array();
        $jsresetbutton = JRequest::getVar('jsresetbutton', NULL , 'post');
        if (!empty($data)) {
            foreach ($data as $uf) {
                $valarray[$uf->field] = $mainframe->getUserStateFromRequest($option . $uf->field , $uf->field , '','string');
                if($jsresetbutton == 1){//to reset date fields
                    $mainframe->setUserState($option.$uf->field,null);
                    $valarray[$uf->field] = null;
                }
                if (isset($valarray[$uf->field]) && $valarray[$uf->field] != null) {
                    switch ($uf->userfieldtype) {
                        case 'text':
                        case 'file':
                        case 'email':
                            $inquery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($valarray[$uf->field]) . '.*"\' ';
                            break;
                        case 'combo':
                            $inquery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'depandant_field':
                            $inquery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'radio':
                            if(isset($jsresetbutton)){
                                $mainframe->setUserState($option.$uf->field,'');
                                $valarray[$uf->field] = '';
                            }else{
                                $inquery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            }
                            break;
                        case 'checkbox':
                            if(isset($jsresetbutton)){
                                $mainframe->setUserState($option.$uf->field,array());
                                $valarray[$uf->field] = array();
                            }else{
                                $finalvalue = '';
                                foreach($valarray[$uf->field] AS $value){
                                    $finalvalue .= $value.'.*';
                                }
                                $inquery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($finalvalue) . '.*"\' ';
                            }
                            break;
                        case 'date':
                            $tempdate = htmlspecialchars($valarray[$uf->field]);
                            $dateformat = JSSupportTicketModel::getJSModel('config')->getConfigurationByName("date_format");
                            if ($dateformat == 'm-d-Y') {
                              $arr = explode('-', $tempdate);
                              $tempdate = $arr[2] . '-' . $arr[0] . '-' . $arr[1];
                            } elseif ($dateformat == 'd-m-Y' OR $dateformat == 'Y-m-d') {
                              $arr = explode('-', $tempdate);
                              $tempdate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
                            }
                            $tempdate = JHTML::_('date',strtotime($tempdate),"Y-m-d" );
                            $valarray[$uf->field] = $tempdate;

                            $inquery .= ' AND ticket.params LIKE \'%"' . $uf->field . '":"' . htmlspecialchars($valarray[$uf->field]) . '"%\' ';
                            break;
                        case 'textarea':
                            $inquery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*' . htmlspecialchars($valarray[$uf->field]) . '.*"\' ';
                            break;
                        case 'multiple':
                            $finalvalue = '';
                            foreach($valarray[$uf->field] AS $value){
                                if($value != null){
                                    $finalvalue .= $value.'.*';
                                }    
                            }
                            if($finalvalue !=''){
                                $inquery .= ' AND ticket.params REGEXP \'"' . $uf->field . '":"[^"]*'.htmlspecialchars($finalvalue).'"\'';
                            }
                            break;
                    }
                    $result['params'] = $valarray;
                }
            }
        }
        if($flag){
            if($inquery=="")
                $result['iscombinesearch'] = false;
            else
                $result['iscombinesearch'] = true;
        }

        //end

        $result['inquery'] = $inquery;
        return $result;
    }

    function getUserMyTickets($uid,$listtype,$searchkeys,$sortby,$limitstart,$limit) {
        if(!is_numeric($uid)) return false;
        $db = $this->getDBO();
        $multisearchquery = $this->ticketMultiSearch($searchkeys);
        $query = "SELECT COUNT(id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.uid = ".$db->quote($uid);
        $listquery = '';
        $query .= $multisearchquery['inquery'];
        switch ($listtype) {
            case 1:
                $listquery .= " AND ticket.status != 4 ";
            break;
            case 3:
                $listquery .= " AND ticket.status != 4 AND ticket.isanswered=1 ";//AND ticket.status = 3
            break;
            case 4:
                $listquery .= " AND ticket.status = 4";
            break;
            case 5:
                $listquery .= " ";
            break;
        }
        $query .= $listquery;
        $db->setQuery($query);
        $total = $db->loadResult(); //Total Tickets
        $query = "SELECT ticket.*,dep.departmentname AS departmentname, priority.priority AS priority, priority.prioritycolour AS prioritycolour, 
                    (SELECT COUNT(attach.id) From `#__js_ticket_attachments` AS attach WHERE attach.ticketid = ticket.id) AS attachments
                        FROM `#__js_ticket_tickets` AS ticket
                        JOIN `#__js_ticket_priorities` AS priority ON ticket.priorityid = priority.id
                        LEFT JOIN `#__js_ticket_departments` AS dep ON ticket.departmentid = dep.id
                        WHERE ticket.uid =".$uid;
        $departments = $this->getJSModel('department')->getDepartmentsForCombobox();
        $priorities = $this->getJSModel('priority')->getPrioritiesForCombobx(JText::_('Select Priority'));
        $departmentid = isset($multisearchquery['department']) ? $multisearchquery['department'] : '';
        $priorityid = isset($multisearchquery['priority']) ? $multisearchquery['priority'] : '';

        $lists['departments'] = JHTML::_('select.genericList', $departments, 'filter_department', '', 'value', 'text',$departmentid);
        $lists['priorities'] = JHTML::_('select.genericList', $priorities, 'filter_priority', '', 'value', 'text',$priorityid);

        $query .= $multisearchquery['inquery'];

        $query .= $listquery;
        $query .= " ORDER BY ".$sortby;

        $db->setQuery($query,$limitstart,$limit);
        $result = $db->loadObjectList(); //Tickets
        $ticketinfo = array();
        $config = $this->getJSModel('config')->getConfigs();
        if($config['show_count_tickets'] == 1){
            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.status != 4 AND ticket.uid = $uid ";
			$query .= $multisearchquery['inquery'];
            $db->setQuery($query);
            $ticketinfo['open'] = $db->loadResult(); // Open Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.status = 4 AND ticket.uid = $uid ";
			$query .= $multisearchquery['inquery'];
            $db->setQuery($query);
            $ticketinfo['close'] = $db->loadResult(); // Closed Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE status != 4 AND isanswered = 1 AND ticket.uid = $uid ";
			$query .= $multisearchquery['inquery'];
            $db->setQuery($query);
            $ticketinfo['isanswered'] = $db->loadResult(); // IsAnswered Tickets

            $query = "SELECT COUNT(ticket.id) FROM `#__js_ticket_tickets` AS ticket WHERE ticket.uid = $uid ";
			$query .= $multisearchquery['inquery'];
            $db->setQuery($query);
            $ticketinfo['mytickets'] = $db->loadResult(); // My Tickets
        }
        if($total == '') $total = 0;
        //$lists['searchticket'] = $searchticketid;
        $return[0] = $result;
        $return[1] = $total;
        $return[2] = $ticketinfo;
        $return[3] = $lists;        
        $return[4] = $multisearchquery;
        return $return;
    }

    function getFormData($id,$data) {
        if($id) if (!is_numeric($id)) return false;
        $db = $this->getDBO();
        $user = JSSupportTicketCurrentUser::getInstance();

        $departments = $this->getJSModel('department')->getDepartmentsForCombobox();
        $priorities = $this->getJSModel('priority')->getPrioritiesForCombobx();
        if (isset($id) && $id <> '') {
            $query = "SELECT ticket.*,user.username AS uname 
                        FROM `#__js_ticket_tickets` AS ticket 
                        LEFT JOIN `#__users` AS user ON user.id = ticket.uid
                        WHERE ticket.id = " . $db->quote($id);
            $db->setQuery($query);
            $editticket = $db->loadObject();
        }
        if (isset($id) && $id <> '') {
            $lists['departments'] = JHTML::_('select.genericList', $departments, 'departmentid', 'class="inputbox form-select-group__select required" ' . 'required="required"', 'value', 'text', $editticket->departmentid);
            $lists['priorities'] = JHTML::_('select.genericList', $priorities, 'priorityid', 'class="inputbox form-select-group__select required" ' . 'required="required"', 'value', 'text', $editticket->priorityid);
        } else {
            $query = "SELECT id FROM `#__js_ticket_priorities` WHERE isdefault = 1";
            $db->setQuery($query);
            $priority = $db->loadObject();
            $departmentid = isset($data['departmentid']) ? $data['departmentid'] : '';
            $priorityid = isset($data['priorityid']) ? $data['priorityid'] : $priority->id;
            $lists['departments'] = JHTML::_('select.genericList', $departments, 'departmentid', 'class="inputbox form-select-group__select required" ' . 'required="required"', 'value', 'text', $departmentid);
            $lists['priorities'] = JHTML::_('select.genericList', $priorities, 'priorityid', 'class="inputbox form-select-group__select required" ' . 'required="required"', 'value', 'text', $priorityid);
        }

        $model_userfields = $this->getJSModel('userfields');
        if (isset($editticket))
            $result[0] = $editticket;
        $result[1] = '';
        $result[2] = $lists;
        
        $result[4] = $model_userfields->getFieldsOrderingforForm(1);
        if (isset($id) && $id <> '') {
            $result[5] = $this->getJSModel('attachments')->getAttachmentForForm($id);
        }
        return $result;
    }
    function getTicketDetailById($id, $uid = '') {
        if (!is_numeric($id)) return false;
        if($uid) if(!is_numeric($uid)) return false;
        if(!$this->validateTicketDetailForUser($id)) return false;
        $db = $this->getDbo();
        $result = array();

        $query = "SELECT ticket.*,department.departmentname AS departmentname ,priority.priority AS priority,priority.prioritycolour AS prioritycolour,
            attach.filename,attach.filesize,attach.id AS attachmentid,
            (SELECT COUNT(id) FROM `#__js_ticket_attachments` WHERE ticketid = ticket.id AND replyattachmentid = 0) AS count
            FROM `#__js_ticket_tickets` AS ticket
            JOIN `#__js_ticket_priorities` AS priority ON ticket.priorityid = priority.id
            LEFT JOIN `#__js_ticket_departments` AS department ON ticket.departmentid = department.id
            LEFT JOIN `#__js_ticket_attachments` AS attach ON ticket.id = attach.ticketid AND attach.replyattachmentid = 0
            WHERE ticket.id=" . $id;
        if($uid) $query .= " AND ticket.uid = $uid";            
        $db->setQuery($query);
        $ticketdetails = $db->loadObjectList();
        // in replies staffid used as joomla userid
        $query = "SELECT replies.*, attachment.filename AS filename, attachment.filesize AS filesize, user.name AS name,attachment.id AS attachmentid, 
                 (SELECT count(id) FROM `#__js_ticket_attachments` WHERE ticketid = replies.ticketid AND replyattachmentid = replies.id) AS count
                 FROM`#__js_ticket_replies` AS replies
                 LEFT JOIN`#__users` AS user ON user.id = replies.staffid
                 LEFT JOIN `#__js_ticket_attachments` AS attachment ON replies.ticketid = attachment.ticketid AND replies.id = attachment.replyattachmentid
                 WHERE replies.ticketid = " . $id . " ORDER BY replies.created ASC";
        $db->setQuery($query);
        $replies = $db->loadObjectList();

        $app = JFactory::getApplication();

        if ($app->isAdmin()){
            $departments = $this->getJSModel('department')->getDepartmentsForCombobox();
            $priorities = $this->getJSModel('priority')->getPrioritiesForCombobx(JText::_("Select Priority"));
            $lists['departments'] = JHTML::_('select.genericList', $departments, 'departmentid', 'class="inputbox" ' . '', 'value', 'text', '');
            $lists['priorities'] = JHTML::_('select.genericList', $priorities, 'priorityid', 'class="inputbox"', 'value', 'text', '');
        }

        $model_userfields = $this->getJSModel('userfields');
        if(isset($ticketdetails[0])){
            $result[0] = $ticketdetails[0];
        }
        
        $result[2] = $replies;
        if(isset($lists)) $result[3] = $lists;
        $result[6] = $ticketdetails;
        //$result[7] = $model_userfields->getUserFieldsForView(1, $id);
        return $result;
    }

    function ticketClose($ticketid ,$created) {
        if (!is_numeric($ticketid))
            return false;
        $user = JSSupportTicketCurrentUser::getInstance();
        if(!$user->getIsAdmin()){
            if($user->getIsGuest()){
                $email = JRequest::getVar('email');
                $userTicket = $this->checkEmailAndTicketID($email,$ticketid);
            }else{
                $uid = $user->getId();
                $userTicket = $this->checkTicketIdAndUid($uid,$ticketid);
            }
            if (!$userTicket) {
                return OTHER_USER_TASK;
            }
        }

        $row = $this->getTable('tickets');
        $data['id'] = $ticketid;
        $data['reopened'] = '';
        $data['status'] = 4;
        $data['closed'] = $created;
        $data['update'] = $created;

        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            $return_value = false;
        }
        if (!$row->store()) {
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->setError($this->_db->getErrorMsg());
            $return_value = false;
        }
        if (isset($return_value) && $return_value == false) {
            return TICKET_ACTION_ERROR;
        }
        $this->getJSModel('email')->sendMail(1,2,$ticketid); // Mailfor,Close Ticket,Ticketid
        
        return TICKET_ACTION_OK;
    }

    function reopenTicket($ticketid, $lastreply) {
        if (!is_numeric($ticketid))
            return false;
        $eventtype = JText::_('Reopen Ticket');
        $user = JSSupportTicketCurrentUser::getInstance();
        if(!$user->getIsAdmin()){
            $canreopen = $this->checkCanReopenTicket($ticketid, $lastreply);
            if ($canreopen == false) {
                return TIME_LIMIT_END;
            }
            if($user->getIsGuest()){
                $email = JRequest::getVar('email');
                $userTicket = $this->checkEmailAndTicketID($email,$ticketid);
            }else{
                $uid = $user->getId();
                $userTicket = $this->checkTicketIdAndUid($uid,$ticketid);
            }
            if (!$userTicket) {
                return OTHER_USER_TASK;
            }
        }

        $row = $this->getTable('tickets');
        $data['id'] = $ticketid;
        $data['status'] = 0;
        $data['isanswered'] = 0;
        $data['reopened'] = date('Y-m-d H:i:s');
        $data['update'] = date('Y-m-d H:i:s');

        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());            
            $return_value = false;
        }
        if (!$row->store()) {
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->setError($this->_db->getErrorMsg());
            $return_value = false;
        }
        if (isset($return_value) && $return_value == false) {
            return TICKET_ACTION_ERROR;
        }
        return TICKET_ACTION_OK;
    }

    function changeTicketPriority($ticketid, $priorityid, $created) {
        if (!is_numeric($ticketid))
            return false;
        if (!is_numeric($priorityid))
            return false;
        $row = $this->getTable('tickets');
        $data['id'] = $ticketid;
        $data['priorityid'] = $priorityid;
        $data['update'] = $created;
        if (!$row->bind($data)) {
            $this->setError($row->_db->getErrorMsg());
            $return_value = false;
        }
        if (!$row->store()) {
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->setError($row->_db->getErrorMsg());
            $return_value = false;
        }
        if (isset($return_value) && $return_value == false) {
            return PRIORITY_CHANGE_ERROR;
        }
        $this->getJSModel('email')->sendMail(1,11,$ticketid); // Mailfor,priority change,Ticketid
        return PRIORITY_CHANGED;
    }

    function checkCanReopenTicket($ticketid, $lastreply) {
        if (!is_numeric($ticketid))
            return false;
        $config_ticket = $this->getJSModel('config')->getConfigByFor('ticket');
        $days = $config_ticket['ticket_reopen_within_days'];
        if (!$lastreply)
            $lastreply = date('Y-m-d H:i:s');
        $date = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($lastreply)) . " +" . $days . " day"));
        if ($date < date('Y-m-d H:i:s'))
            return false;
        else
            return true;
    }

    function getTicketUserNameById($ticketid) {
        if (!is_numeric($ticketid))
            return false;
        $db = $this->getDbo();
        $query = "SELECT ticket.name From `#__js_ticket_tickets` AS ticket WHERE ticket.id = " . $ticketid;
        $db->setQuery($query);
        $name = $db->loadResult();
        return $name;
    }

    function updateStatus($id, $status, $created) {
        if (!is_numeric($id))
            return false;
        $row = $this->getTable('tickets');
        $data['id'] = $id;
        $data['status'] = $status; // Ticket Closed
        $data['closed'] = $created;
        $data['update'] = $created;

        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        if (!$row->store()) {
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        return true;
    }

    function getTicketIdForEmail($id) {
        if (!is_numeric($id))
            return false;
        $db = $this->getDbo();
        $query = "Select ticketid,email from `#__js_ticket_tickets` where id = " . $id;
        $db->setQuery($query);
        $ticket = $db->loadObject();
        return $ticket;
    }

    function getTicketAttachmentDir($id){
        if(!is_numeric($id))
            return false;
        $db = $this->getDBO();
        $query = "SELECT attachmentdir FROM `#__js_ticket_tickets` WHERE id = $id";
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }

    function enforcedeleteTicket() {
        $id = JRequest::getVar('cid');
        if (!is_numeric($id))
            return false;
        $session = JFactory::getSession();
        $session->set('ticketid',$this->getTrackingIdById($id));
        $session->set('ticketemail',$this->getTicketEmailById($id));
        $session->set('ticketsubject',$this->getTicketSubjectById($id));
		$dir = $this->getTicketAttachmentDir($id);

        $db = $this->getDBO();
        $query = "DELETE ticket,reply,attach
                        FROM `#__js_ticket_tickets` AS ticket
                        LEFT JOIN `#__js_ticket_replies` AS reply ON reply.ticketid = ticket.id
                        LEFT JOIN `#__js_ticket_attachments` AS attach ON attach.ticketid = ticket.id
                        WHERE ticket.id = " . $id;
        $db->setQuery($query);
        if (!$db->query()) {
            return DELETE_ERROR;
        } else {
            //for email to sure ticket is deleted
			$this->getJSModel('attachments')->removeTicketAttachments( $dir );            
			$this->getJSModel('email')->sendMail(1,3,$id); // Mailfor,Delete Ticket,Ticketid
            return DELETED;
        }
    }

    function deleteTicket() {
        $id = JRequest::getVar('cid');
        if (!is_numeric($id))
            return false;
        $dir = $this->getTicketAttachmentDir($id);
        if($this->canDeleteTicket($id)){
            $db = $this->getDBO();
            $query = "DELETE ticket,attach
                            FROM `#__js_ticket_tickets` AS ticket
                            LEFT JOIN `#__js_ticket_attachments` AS attach ON attach.ticketid = ticket.id
                            WHERE ticket.id = " . $id;
            $db->setQuery($query);
            if (!$db->query()) {
                return DELETE_ERROR;
            } else {
	        //for email to sure ticket is deleted
			$this->getJSModel('attachments')->removeTicketAttachments( $dir );
	        $this->getJSModel('email')->sendMail(1,3,$id); // Mailfor,Delete Ticket,Ticketid
                return DELETED;
            }
        }else{
            return IN_USE;
        }
    }

    function canDeleteTicket($id){
        if(!is_numeric($id)) return false;
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(reply.id) FROM `#__js_ticket_replies` AS reply WHERE reply.ticketid = $id";
        $db->setQuery($query);
        $result = $db->loadResult();
        if($result == 0)
            return true;
        else
            return false;
    }

    function checkEmailAndTicketID($email, $ticketid) {
        $db = $this->getDBO();
        $query = "SELECT COUNT(id) FROM `#__js_ticket_tickets` WHERE email =" . $db->quote($email) . " AND ticketid =" . $db->quote($ticketid);
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }

    function checkTicketIdAndUid($uid,$ticketid) {
        if(!is_numeric($uid)) return false;
        if(!is_numeric($ticketid)) return false;
        $db = $this->getDBO();
        $query = "SELECT COUNT(id) FROM `#__js_ticket_tickets` WHERE uid =" . $uid . " AND id =" . $ticketid;
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }

    function getIdFromTrackingId($ticketid) {
        $db = $this->getDBO();
        $query = "SELECT id FROM `#__js_ticket_tickets` WHERE ticketid =" . $db->quote($ticketid);
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }

    private function sendEmail($id, $uid, $for, $message, $appendsignature, $to) {
        if ($to != '')
            $model_email = $this->getJSModel('email');
        switch ($to) {
            case 'user':
                $model_email->sendMail($id, $uid, 1, $message, '');
                break;
                break;
            case 'admin':
                $model_email->sendMailtoAdmin($id, $uid, 1, $message, '');
                break;
            case 'all':
                $model_email->sendMail($id, $uid, 1, $message, '');
                $model_email->sendMailtoAdmin($id, $uid, 1, $message, '');
                break;
        }
    }

    function getTicketId() {
        $db = $this->getDBO();
        $query = "SELECT ticketid FROM `#__js_ticket_tickets`";
        $ticketid_sequence = $this->getJSModel('config')->getConfigurationByName('ticketid_sequence');
        $match = '';
        $ticketid = "";
        do {
            if($ticketid_sequence == 1){ // Random ticketid
                $ticketid = "";
                $length = 13;
                $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
                $maxlength = strlen($possible);
                if ($length > $maxlength) {
                    $length = $maxlength;
                }
                $i = 0;
                while ($i < $length) {
                    $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
                    if (!strstr($ticketid, $char)) {
                        if ($i == 0) {
                            if (ctype_alpha($char)) {
                                $ticketid .= $char;
                                $i++;
                            }
                        } else {
                            $ticketid .= $char;
                            $i++;
                        }
                    }
                }
            }else{ // Sequential ticketid
                if($ticketid == ""){
                    $ticketid = 0; // by default its set to zero                    
                }
                $maxquery = "SELECT max(convert(ticketid, SIGNED INTEGER)) FROM `#__js_ticket_tickets`";
                $db->setQuery($maxquery);
                $maxticketid = $db->loadResult();
                if(is_numeric($maxticketid)){
                    $ticketid = $maxticketid + 1;
                }else{
                    $ticketid = $ticketid + 1;
                }
            }
            $db->setQuery($query);
            $rows = $db->loadObjectList();
            foreach ($rows as $row) {
                if ($ticketid == $row->ticketid){
                    $match = 'Y';
                    break;
                }else{
                    $match = 'N';
                }
            }
        }while ($match == 'Y');

        return $ticketid;
    }

    function updateIsAnswered($ticketid,$isanswered) {
        if (!is_numeric($ticketid))
            return false;
        if(!is_numeric($isanswered)) return false;
        $db = $this->getDbo();
        $query = "UPDATE `#__js_ticket_tickets` set isanswered = $isanswered WHERE id = " . $ticketid;
        $db->setQuery($query);
        if (!$db->query())
            return false;
        else
            return true;
    }

    function updateTicketLastReply($ticketid, $created) {
        if (!is_numeric($ticketid))
            return false;
        $db = $this->getDbo();
        $query = "UPDATE `#__js_ticket_tickets` set lastreply = " . $db->quote($created) . " WHERE id = " . $ticketid;
        $db->setQuery($query);
        if (!$db->query()) {
            return false;
        } else {
            return true;
        }
    }

    function getLatestReplyByTicketId($id) {
        if (!is_numeric($id))
            return false;
        $db = JFactory::getDBO();
        $query = "SELECT reply.message FROM `#__js_ticket_replies` AS reply WHERE reply.ticketid = " . $id . " ORDER BY reply.created DESC LIMIT 1";
        $db->setQuery($query);
        $message = $db->loadResult();
        
        return $message;
    }

    function saveResponceAJAX($id,$responce){
        if($id) if(!is_numeric($id)) return false;

        $user = JSSupportTicketCurrentUser::getInstance();
        $per = $user->checkUserPermission('Edit Ticket');
        if ($per == false) return PERMISSION_ERROR;
        $row = $this->getTable('replies');
        $data['id'] = $id;
        //$data['message'] = JRequest::getVar('message', '', 'post', 'string', JREQUEST_ALLOWHTML);
        $data['message'] = $responce;
        
        if (!$row->bind($data)){
            $this->setError($this->_db->getErrorMsg());
            return SAVE_ERROR;
        }
        if (!$row->check()){
            $this->setError($this->_db->getErrorMsg());
            return SAVE_ERROR;
        }
        if (!$row->store()){
            $this->getJSModel('systemerrors')->updateSystemErrors($row->getErrorMsg());
            $this->updateSystemErrors($this->_db->getErrorMsg());
            return SAVE_ERROR;
        }
        return SAVED;
    }
    
    function editResponceAJAX($id){
        $db = $this->getDBO();
        if($id) if(!is_numeric($id)) return false;

        $query = "SELECT message FROM `#__js_ticket_replies` WHERE id = ".$id;
        $db->setQuery( $query );
        $row = $db->loadObject();
        $editor = JFactory::getEditor();
        if(isset($row)){
            $return_value =  $editor->display("editor_responce_$id", $row->message, "600", "400", "80", "15", 1, null, null, null, array('mode' => 'advanced'));
        }else{
            $return_value = $editor->display('editor_responce_'.$id, '', '550', '300', '60', '20', false);    
        }

        $return_value .= '<br /> 
        <input type="button" class="tk_dft_btn" value="'.JText::_('Save').'" onclick="saveResponce('.$id.')">
        <input type="button" class="tk_dft_btn" value="'.JText::_('Close').'" onclick="closeResponce('.$id.')">';      
        return $return_value;
    }
    
    function deleteResponceAJAX($id){
        if($id) if(!is_numeric($id)) return false;
        $user = JSSupportticketCurrentUser::getInstance();
        $per = $user->checkUserPermission('Delete Ticket');
        if ($per == false) return PERMISSION_ERROR;
        $row = $this->getTable('replies');
        if (!$row->delete($id)){
            $this->setError($row->getErrorMsg());
            return DELETE_ERROR;
        }
        return DELETED;
    }
    function getUserListForRegistration() {
        $db = JFactory::getDbo();
        $query = "SELECT DISTINCT user.ID AS userid, user.username AS username, user.email AS useremail, user.name AS userdisplayname
                    FROM `#__users` AS user ORDER BY userdisplayname";
        $db->setQuery($query);
        $users = $db->loadObjectList();
        return $users;
    }
    function getTicketSubjectById($id) {
        if (!is_numeric($id))
            return false;
            $db = JFactory::getDbo();
        $query = "SELECT subject FROM `#__js_ticket_tickets` WHERE id = " . $id;
        $db->setQuery($query);
        $subject = $db->loadResult();
        return $subject;
    }

    function getTrackingIdById($id) {
        if (!is_numeric($id))
            return false;
            $db = JFactory::getDbo();
        $query = "SELECT ticketid FROM `#__js_ticket_tickets` WHERE id = " . $id;
        $db->setQuery($query);
        $ticketid = $db->loadResult();
        return $ticketid;
    }

    function getTicketEmailById($id) {
        if (!is_numeric($id))
            return false;
            $db = JFactory::getDbo();
        $query = "SELECT email FROM `#__js_ticket_tickets` WHERE id = " . $id;
        $db->setQuery($query);
        $ticketemail = $db->LoadResult();
        return $ticketemail;
    }
    
    function getDownloadAttachmentById($id){
        if(!is_numeric($id)) return false;
        $db = JFactory::getDbo();
        $query = "SELECT ticket.id AS ticketid,attach.filename, ticket.attachmentdir "
                . " FROM `#__js_ticket_attachments` AS attach "
                . " JOIN `#__js_ticket_tickets` AS ticket ON ticket.id = attach.ticketid "
                . " WHERE attach.id = $id";
        $db->setQuery($query);
        $object = $db->loadObject();
        $ticketid = $object->ticketid;
        $filename = $object->filename;
        $attachmentdir = $object->attachmentdir;
        $download = false;
        $user = JFactory::getUser();
        if(!$user->guest){
            if(JFactory::getApplication()->isAdmin()){
                $download = true;
            }else{
                if($this->getJSModel('ticket')->validateTicketDetailForUser($ticketid)){
                    $download = true;
                }
            }            
        }

        if($download == true){            
            $datadirectory = $this->getJSModel('config')->getConfigurationByName('data_directory');
            $base = JPATH_BASE;
            if(JFactory::getApplication()->isAdmin()){
                $base = substr($base, 0, strlen($base) - 14); //remove administrator    
            }  
            $path = $base.'/'.$datadirectory;
            $path = $path . '/attachmentdata';
            $path = $path . '/ticket/' . $attachmentdir;
            $file = $path . '/' . $filename;
            header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // ob_clean();
            flush();
            readfile($file);
            exit();
        }else{
            JError::raiseError(404, JText::_('Page not found'));
            exit;
        }
    }

    function getDownloadAttachmentByName($file_name,$id){
        if(empty($file_name)) return false;
        if(!is_numeric($id)) return false;
        $db = JFactory::getDbo();
		$file_name = basename($file_name);
        $filename = str_replace(' ', '_',$file_name);
        $query = "SELECT attachmentdir FROM `#__js_ticket_tickets` WHERE id = ".$id;
        $db->setQuery($query);
        $foldername = $db->loadResult();

        $datadirectory = $this->getJSModel('config')->getConfigurationByName('data_directory');
        $base = JPATH_BASE;
        if(JFactory::getApplication()->isAdmin()){
            $base = substr($base, 0, strlen($base) - 14); //remove administrator    
        }  
        $path = $base.'/'.$datadirectory;
        $path = $path . '/attachmentdata';
        $path = $path . '/ticket/' . $foldername;
        $file = $path . '/' . $filename;

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        //ob_clean();
        flush();
        readfile($file);
        exit();
        exit;
    }

    function validateTicketDetailForUser($id) {
        if (!is_numeric($id))
            return false;
        if(JFactory::getApplication()->isAdmin()) return true;
        $db = JFactory::getDbo();
        $query = "SELECT uid FROM `#__js_ticket_tickets` WHERE id = " . $id;
        $db->setQuery($query);
        $uid = $db->loadResult();
        $user = JFactory::getUser();
        if ($uid == $user->id) {
            return true;
        } else {
            return false;
        }
    }

    // new
    function uploadFileCustom($id,$field){
        if(! is_numeric($id)) return;

        $db = JFactory::getDbo();

        $config = $this->getJSModel('config')->getConfigByFor('default');
        $model_attachment = $this->getJSModel('attachments');

        if ($_FILES[$field]['size'] > 0) {
            $file_name = str_replace(' ', '_', $_FILES[$field]['name']);
            $file_tmp = $_FILES[$field]['tmp_name']; // actual location
        }else{
            return;
        }
        $file_size = $config['filesize'];
        if($_FILES[$field]['size'] > ($file_size * 1024)){
            return;
        }
        if ($file_name != "" AND $file_tmp != "") {
            $is_allow = $model_attachment->checkExtension($file_name);
            if($is_allow == 'N'){
                return;
            }
        } 
        $datadirectory = $config['data_directory'];
        $base = JPATH_BASE;
        if(JFactory::getApplication()->isAdmin()){
            $base = substr($base, 0, strlen($base) - 14); //remove administrator    
        }  
        $path = $base.'/'.$datadirectory;
        if (!file_exists($path)){ // create user directory
            $model_attachment->makeDir($path);
        }
        $path = $path . '/attachmentdata';
        if (!file_exists($path)){ // create user directory
            $model_attachment->makeDir($path);
        }
        $path = $path . '/ticket';
        if (!file_exists($path)){ // create user directory
            $model_attachment->makeDir($path);
        }

        $query = "SELECT attachmentdir FROM `#__js_ticket_tickets` WHERE id = ".$id;
        $db->setQuery($query);
        $foldername = $db->loadResult();
        $userpath = $path . '/' . $foldername;
        if (!file_exists($userpath)) { // create user directory
            $model_attachment->makeDir($userpath);
        }
        move_uploaded_file($file_tmp, $userpath . '/' . $file_name);
        /*
        //Override the record and delete the old file if exists
        $query = "SELECT params FROM `#__js_ticket_tickets` WHERE id = ".$id;
        $db->setQuery($query);
        $params = $db->loadResult();
        $p_array = json_decode($params,true);
        //Remove old file if exists
        $old_file = $p_array[$field];
        if(file_exists($userpath . '/' . $old_file)){
            unlink($userpath . '/' . $old_file);
        }
        //--------------------------
        $p_array[$field] = $file_name;
        $params = json_encode($p_array);
        $query = "UPDATE `#__js_ticket_tickets` SET params = '".$params."' WHERE id = ".$id;
        $db->setQuery($query);
        $db->query();
        */
        return;
    }

    function removeFileCustom($id, $key){
        $filename = str_replace(' ', '_', $key);

        if(! is_numeric($id))
            return;

        $db = JFactory::getDbo();
        $config = $this->getJSModel('config')->getConfigByFor('default');
        $datadirectory = $config['data_directory'];

        $base = JPATH_BASE;
        if(JFactory::getApplication()->isAdmin()){
            $base = substr($base, 0, strlen($base) - 14); //remove administrator    
        }

        $path = $base . '/' . $datadirectory. '/attachmentdata/ticket';

        $query = "SELECT attachmentdir FROM `#__js_ticket_tickets` WHERE id = ".$id;
        $db->setQuery($query);
        $foldername = $db->loadResult();
        $userpath = $path . '/' . $foldername.'/'.$filename;
        unlink($userpath);
        return;
    }
}
?>
